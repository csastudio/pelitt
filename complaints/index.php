<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Complaints");
?><h1>COMPLAINTS</h1>
 <br>
 <br>
<h4>1. GENERAL PROVISIONS AND CONCEPT DEFINITIONS</h4>
<ol type="a">
	<li>This Claims Code has been compiled pursuant to the Civil Code and the Consumer Protection Act in relation to European regulations as stipulated in&nbsp;<a href="/terms-conditions">Terms and Conditions</a>, and covers consumer goods (hereinafter Goods) where the Purchaser's rights for product liability are asserted (hereinafter Claims). These Complaints are effective for purchases made in the electronic shop&nbsp;<a href="http://www.pelitt.com/">www.pelitt.com</a>. Conditions are further defined and specified by the rights and duties of the contractual parties which are:</li>
	<ol type="i">
		<li>Merchant which is the company&nbsp;<strong>Pelitt Group SE</strong>, IČO 03815854, seated in Na Strži 1702/65, 140 00 Praha 4, Czech Republic (details in section&nbsp;<a href="/contact/">Contact Us</a>), a company incorporated in the Register of Companies at the Municipal Court in Prague under entry H 1549,</li>
		<li>The Purchaser is an entity which concluded a Purchase Contract with the Merchant.</li>
		<li>The Merchant can adequately apply this Claims Code to the Purchaser who is not a Consumer. Otherwise, the rights resulting from product liability abide by the concluded Contract and by the Civil Code.</li>
	</ol>
</ol>
 <br>
 <br>
<h4>2. INCONSISTENCY WITH PURCHASE CONTRACT</h4>
<ol type="a">
	<li>In case the state of the product on reception does not comply with the Purchase Contract (hereinafter Inconsistency with Purchase Contract), the Purchaser has a right to demand that the Merchant rectifies the situation free of charge and without delay either by means of exchange or repair of the product as the Purchaser requires. If such a procedure is not possible, the Purchaser can claim adequate discount or withdraws from the Contract.</li>
	<li>The above stated does not apply if prior to reception of the goods the Purchaser knew about the inconsistency with the Purchase Contract or if the inconsistency was caused by the Purchaser himself. Inconsistency with the Purchase Contract detected six months after reception of goods is deemed to be present at reception unless it contradicts the nature of the goods or unless proved otherwise.</li>
</ol>
 <br>
 <br>
<h4>3. TERMS OF WARRANTY</h4>
<ol type="a">
	<li>If after reception the Purchaser detects defects on the products within the warranty period, he can assert his warrantable claim.</li>
	<li>The warranty period complies with the valid legal regulations. Unless the Producer states otherwise, or in case of Consumer, unless the warranty period is longer, the warranty period is 24 months and begins on reception of goods.</li>
	<li>The Merchant is liable for any defects present on reception, any defects on material and production faults that emerged after reception in warranty period.</li>
	<li>Warranty cannot be enforced in the following cases:
	<ol type="i">
		<li>If the warranty period ran out before the warranty claim has been made, the warranty ceased to exist.</li>
		<li>The defect has resulted from misuse of the product.</li>
		<li>The defect has resulted from a failure to follow the Producer's instructions.</li>
		<li>The defect has resulted from unprofessional installation, handling, operation, manipulation or negligence of product maintenance.</li>
		<li>The defect has resulted from unauthorized interference in the goods or any other alterations not permitted by the Producer.</li>
		<li>Goods were damaged in natural disaster.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
<h4>4. WARRANTY RIGHTS</h4>
<ol type="a">
	<li>The warranty period is extended by the time the goods remain in repair under warranty. In case of exchange of goods, the Purchaser obtains a new warranty period of 24 months.</li>
	<li>When asserting his right of warranty the Consumer has:
	<ol type="i">
		<li>if there is a removable fault, if you claim proper and timely removal of the fault free of charge, exchange of faulty goods or a faulty part, unless it is inadequate considering the nature of the fault and unless such procedure is not possible, if you claim reasonable discount from the purchase price or if you wish to withdraw from the Purchase Contract,</li>
		<li>if there is an irremovable fault which prevents proper use of goods, if you claim exchange of faulty goods or if you wish to withdraw from the Purchase Contract,</li>
		<li>if there are removable faults with multiple and repeat occurrence preventing proper use of the goods, if you claim exchange of faulty goods or wish to withdraw from the Purchase Contract,</li>
		<li>if there are other irremovable faults and if you do not require exchange of goods, if you claim reasonable discount from the purchase price or wish to withdraw from the Purchase Contract.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
<h4>5. CLAIM HANDLING</h4>
<ol type="a">
	<li>The place where you should make your claim is the operation of the Technical Partners (Producer) or the Merchant's operation.</li>
	<li>Make your claims at:</li>
 <address style="background: #e8e8e8;"><strong>Pelitt Group SE</strong><br>
	 U Vodárny 3032/2a<br>
	 616 00 Brno<br>
	 Czech Republic<br>
 </address><br>
	<p>
 <a href="http://pelitt.com/complaints/Formular_Reklamace%20Pelitt%20(ENG).pdf" target="_blank">Download claim form</a>&nbsp;(english)<br>
	</p>
	<p>
 <strong><a href="http://pelitt.com/complaints/Reklamac%CC%8Cni%CC%81%20formula%CC%81r%CC%8C_Pelitt-1.pdf" target="_blank">Download claim form</a>&nbsp;(czech)</strong>
	</p>
	<li>If the Purchaser makes his claim personally at the operation of the Technical Partner or the Merchant we recommend that he makes an appointment in advance by e-mail or by phone.</li>
	<li>For returns and other related issues please <a href="http://pelitt.com/contact/">contact us</a>&nbsp;or send an e-mail to&nbsp;<a href="mailto:support@pelitt.com">support@pelitt.com</a>.</li>
	<li>In situation when the Merchant has decided that the goods will be returned to him, it is in the Purchaser's interest to make sure that the goods are packaged appropriately in suitable and protective packaging, meeting the needs for transport of fragile goods, and with proper symbols attached.</li>
	<li>As soon as the claim is properly handled, the staff of Technical Partner or at the branch office invites the Purchaser to collect the repaired goods</li>
	<li>It is always necessary to make a written record of defects detected and of the method of their removal. We strongly recommend that the Purchaser keeps this record while the warranty period is effective.</li>
	<li>In case of acknowledged claim the Customer is entitled to adequate compensation of postage.</li>
	<li>In case of rejected claim the Consumer cannot claim compensation of the expenses incurred in the claim process and, similarly, the Merchant cannot claim compensation of the expenses incurred, unless it has been a repeat unreasonable claim made by the Purchaser where abuse of law can be assumed.</li>
	<li>The Merchant or the Technical Partner decide about the claim immediately, in complicated cases within three working days. This period does not include the time necessary for technical review of the defect adequate according to the type of product or service. The claim and the defect will be handled without delay, no later than 30 calendar days from receiving the claim, unless agreed otherwise.</li>
	<li>The Merchant issues a written certificate to the Consumer stating the date of claim, its content and the requested method of handling. Further on, the Merchant issues a certificate stating the date and method of handling completion, incl. information about the repair done and its duration. In case of rejected claim the Merchant issues a written statement justifying his rejection.</li>
	<li>Storage fees and expenses for unwarrantable claims: if claimed goods have not been collected within 30 days of completed repair or if a product has not been collected in case of unwarrantable claim, the Purchaser can be charged a storage fee of CZK 30 (excl. VAT) for each calendar day. The Merchant is authorized not to hand over the product until the total amount for repair as well as the storage fee have been paid. As soon as this amount exceeds the product's current sale price, the Merchant is authorized to sell the product to cover the expenses incurred.</li>
</ol>
 <br>
 <br>
<h4>6. FINAL PROVISIONS</h4>
<ol type="a">
	<li>This Claims Code takes effect as of 1 May 2014.</li>
	<li>Changes of Claims Code reserved; Claim Code is part of the Merchant's Trade Terms and Conditions.</li>
</ol>
 <br>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>