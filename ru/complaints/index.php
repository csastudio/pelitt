<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Complaints");
?><h1>Жалобы</h1>
 <br>
 <br>
<h4><br>
 Согласно правилам интернет-магазина pelitt.com, в случае обнаружения дефекта доставки или заводского брака в течение 14 дней с момента получения посылки вы можете вернуть товар ненадлежащего качества. В течение 7 дней вы можете вернуть товар надлежащего качества, если он не подошёл вам.<br>
 &nbsp;<br>
 Обращаем ваше внимание, что если вы отказываетесь от заказа, доставленного курьером (при отсутствии брака), то полная стоимость заказа, за исключением стоимости доставки будет возвращена вам.<br>
 &nbsp;<br>
 В случае, если вы обнаружили брак и хотите сразу сдать телефон обратно, то мы вернём вам полную стоимость заказа и расходы на доставку.<br>
 &nbsp;<br>
 По любым вопросам, связанным с качеством доставленного товара, просим вас незамедлительно обращаться в чат.<br>
 </h4>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>