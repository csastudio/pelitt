<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$s = new \TransactPro\Client('TGDC-5120-V3XB-6909','t.LuSda@eI4z');
$res = $s->Success();
if($res['success']){
    CModule::IncludeModule("sale");
    CSaleOrder::PayOrder($res['order_id'], "Y");
}
localRedirect('https://pelitt.com/ru/checkout/?ORDER_ID=' . $res['order_id'] . '&res=' . $res['success']);