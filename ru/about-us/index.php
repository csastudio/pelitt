<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О нас");
?><h1>О компании Pelitt</h1>
 <br>
<p>
	 При выходе Pelitt на российский рынок было принято решение — работать напрямую с потребителем, без посредников, без локальных компаний-дистрибуторов и даже без розничных сетей. С этой целью и создана официальная штаб-квартира в Европе (юридическое лицо&nbsp;SIA PELITT LV)&nbsp;и сформирована сильная команда профессионалов с обширным опытом работы на европейских рынках и постсоветском пространстве.
</p>
<p>
	<br>
</p>
<p>
	 Потребителю отгружается товар, корректно локализованный, тщательно протестированный в сотовых сетях местных мобильных операторов и обеспеченный 100% фирменной поддержкой и сопровождением. Клиент защищён всеми законами, регулирующими права потребителя&nbsp;в своей стране.
</p>
<p>
	<br>
</p>
<p>
	 Но самое главное в подходе — товар отгружается напрямую потребителю, что обеспечивает самую низкую из всех возможных цен — посредников между производителем и потребителем нет вообще. Только фабрика и потребитель. Потребитель и фабрика.&nbsp;
</p>
<p>
	<br>
</p>
<p>
	 Такой подход позволяет, не неся убыток, значительно понизить цену, либо представлять более качественные, более совершенные в техническом плане устройства за те же деньги, что просят за менее «интересные» аналоги конкуренты.&nbsp;
</p>
<p>
	<br>
</p>
<p>
	 При покупке из Риги клиент получает цену, по которой телефон бы приехал к нему - прямо с завода компании Pelitt.&nbsp;
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>