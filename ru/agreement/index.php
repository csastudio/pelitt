<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пользовательское соглашение");
?><h1>ПОЛЬЗОВАТЕЛЬСКОЕ СОГЛАШЕНИЕ</h1>
<p>
 <br>
</p>
<p>
	 Уважаемый пользователь, Вы находитесь на официальном сайте торговой марки pelitt&nbsp; &nbsp; &nbsp; &nbsp; (русская версия сайта принадлежит компании&nbsp;PELITT LV SIA)&nbsp;– <a href="http://www.pelitt.com/ru">www.pelitt.com/ru</a> (в дальнейшем именуемый «сайт pelitt.com»). Торговая марка pelitt занимается разработкой, производством и продажей смартфонов и аксессуаров к ним. В соответствии с принципами электронной коммерции PELITT LV SIA обеспечивает корректную работу сайта pelitt.com/ru и доступ к функциональным возможностям сервиса – знакомство с ассортиментом интернет-магазина, заказ и оплата товаров, поддержка клиентов (в дальнейшем – услуги сайта pelitt.com/ru). PELITT LV SIA предоставляет услуги сайта pelitt.com/ru&nbsp;в соответствии с данным пользовательским соглашением. Перед тем, как совершать какие-либо действия на сайте pelitt.com/ru, просим Вас внимательно ознакомиться с данным документом. Принимая условия настоящего пользовательского соглашения, Вы гарантируете, что Вам уже есть 18 лет либо Вы совершаете знакомство с сайтом pelitt.com под присмотром родителей или опекунов.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ОБ ЭТОМ СОГЛАШЕНИИ</h2>
<p>
	 Настоящее пользовательское соглашение юридически регламентирует отношения между пользователем (Вами) и сайтом pelitt.com/ru. Перед тем, как зарегистрироваться на сайте pelitt&nbsp;и воспользоваться услугами сайта pelitt.com/ru, пожалуйста, внимательно прочитайте правила и условия данного пользовательского соглашения. Соглашаясь с условиями, Вы принимаете юридические обязательства пользовательского соглашения, которые вступают в силу во время вашего пребывания на сайте pelitt, а также при использовании услуг сайта pelitt.com/ru – вне зависимости от того,&nbsp;создали&nbsp;аккаунт&nbsp;на&nbsp;сайте pelitt.com/ru&nbsp;или&nbsp;являетесь&nbsp;незарегистрированным пользователем, оформили заказ или нет. В случае нарушения любого из правил данного пользовательского соглашения, администрация сайта pelitt.com оставляет за собой право запретить нарушителю пользоваться сайтом pelitt.com и оформлять заказ на нем.
</p>
<p>
	 PELITT LV SIA сохраняет за собой право периодически изменять данное пользовательское соглашение.&nbsp;<br>
	 Настоящее пользовательское соглашение регулируется законодательством Российской Федерации и будет толковаться в соответствии с ним.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ПРАВИЛА</h2>
<p>
	 1. Пользователь несёт ответственность в следующих случаях:
</p>
<p>
	 - Предоставление недостоверных данных, необходимых в процессе пользования услугами сайта pelitt.com/ru
</p>
<p>
	 - Размещение на сайте pelitt материалов, нарушающих конфиденциальность, приватность, авторское или патентное право, закон о торговых марках или другие права собственности физических лиц, а также материалов, способных вызвать сбои в работе сайта pelitt.com/ru&nbsp;
</p>
<p>
	 2. &nbsp;Администрация сайта pelitt.com имеет право удалять любую информацию, которая может быть расценена как неуместная, незаконная или нарушающая положения данного соглашения.
</p>
<p>
	 3. Пользователь регистрируется на сайте pelitt.com/ru при помощи электронной почты. Для авторизации аккаунта каждому пользователю присваивается уникальный номер учётной записи и пароль. Авторизированный пользователь имеет доступ ко всем услугам сайта pelitt.com/ru. Пользователь обязан сохранять конфиденциальность пароля для доступа на сайт pelitt.com/ru, а также других идентификационных данных. Рекомендуем пользователю не сообщать свой пароль третьим лицам, поскольку пользователь сам несёт ответственность за все действия (в том числе покупки), совершённые с применением имени пользователя и его пароля. &nbsp;В случае несанкционированного или некорректного использования аккаунта пользователю следует немедленно сообщить об этом в службу поддержки сайта pelitt.com/ru&nbsp;
</p>
<p>
	 4. Пользователь даёт согласие на использование его контактов (электронной почты и номера телефона) для получения информации от администрации сайта pelitt.com/ru.&nbsp;Все соглашения, уведомления, соглашения о неразглашении информации и другие документы, предоставленные в электронном виде от сайта pelitt.com/ru, соответствуют нормам действующего законодательства в отношении таких видов обмена информацией.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ОГРАНИЧЕНИЯ</h2>
<p>
	 В случае если пользователь нарушил одно из положений настоящего пользовательского соглашения, PELITT LV SIA имеет право отменить любое действие авторизированного пользователя или отказать ему в доступе к услугам сайта pelitt. Пользователь является ответственным за любой заказ, сделанный им на сайте pelitt. PELITT LV SIA оставляет за собой право периодически вносить изменения в работу сайта pelitt&nbsp;без предварительного уведомления пользователя.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ОШИБКА В ОПИСАНИИ ТОВАРА ИЛИ РАСЧЕТЕ ЦЕНЫ</h2>
<p>
	 На сайте pelitt публикуется описание товаров и услуг с указанием цен, которые могут быть скорректированы без предварительного уведомления пользователей. PELITT LV SIA &nbsp;оставляет за собой право менять цены в любое время без предупреждения и исправлять ошибки (например, неправильное отображение цен на сайте из-за технических сбоев), которые могут периодически возникать на сайте pelitt. Мы постараемся минимизировать риск подобных неполадок и в случае их возникновения устранять в кратчайшие сроки. PELITT LV SIA обещает следить за тем, чтобы информация на сайте pelitt&nbsp;была точной и актуальной. Предложение на сайте - не является публичной офертой.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ОБРАБОТКА ЗАКАЗА</h2>
<p>
	 После того, как пользователь оформит заказ на сайте pelitt, PELITT LV SIA проведёт проверку достоверности заказа. PELITT LV SIA оставляет за собой право отказать или отменить любой заказ без объяснения причин. Подобные ограничительные меры могут распространяться на заказы, сделанные с одного аккаунта на сайте pelitt, оплаченные с одной банковской карты; на заказы, где указывается одинаковый платежный адрес или адрес доставки. Если заказ пользователя отклонен, администрация сайта pelitt уведомит пользователя об отмене заказа по электронной почте, указанной при регистрации аккаунта на сайте pelitt.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ПОДТВЕРЖДЕНИЕ ЗАКАЗА</h2>
<p>
	 Уведомление, отправленное на электронную почту и в виде SMS на мобильный телефон пользователя, с деталями заказа (номером и составом) не является подтверждением принятия заказа и продажи товара или услуги.&nbsp;<br>
	 Сайт pelitt&nbsp;имеет право в любое время после оформления заказа принять или отменить его по какой-либо причине. Заказ будет считаться принятым после того, как пользователь получит по электронной почте соответствующее уведомление.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ОПЛАТА</h2>
<p>
	 Продавцом товара пользователю сайта pelitt.com/ru является компания PELITT LV SIA.
</p>
<p>
	 Оплата заказа производится на сайте pelitt.com/ru с помощью карт VISA/MasterCard&nbsp;в рублях.
</p>
<p>
	 Сайт pelitt.com/ru подтвердит заказ пользователя и начнет процесс отправки товара в течение 3 рабочих дней после осуществления оплаты.
</p>
<p>
	 Счет компании PELITT LV SIA открыт в <a href="https://www.rietumu.ru/bank">АО "Rietumu Banka"</a>: Латвия, Рига, LV-1013, ул. Весетас, 7&nbsp;&nbsp;
</p>
<p>
 <br>
</p>
<h2>ОГРАНИЧЕНИЕ ОТВЕТСТВЕННОСТИ</h2>
<p>
	 Пользуясь сайтом pelitt.com/ru, пользователь полностью соглашается с возможными рисками. Администрация сайта pelitt.com/ru не может гарантировать полного отсутствия ошибок и технических неполадок на сайте pelitt.com/ru. Также администрация сайта pelitt.com/ru не гарантирует, что информация, которую пользователь видит на сайте pelitt, является совершенно точной и верной.
</p>
<p>
	 То есть, пользователь признаёт, что данный пункт соглашения касается всех товаров и услуг, доступных на сайте pelitt.com/ru. Ни в каких случаях PELITT LV SIA не несёт ответственности за какие-либо убытки (прямые или косвенные), возникшие вследствие использования или невозможности использования сайта pelitt.com, в том числе в результате использования информации или услуг, полученных с сайта pelitt.com, которые могли бы вызвать потерю данных, задержки в сервисе и другие ошибки в работе сайта pelitt.com/ru.
</p>
<p>
	 Администрация сайта pelitt.com делает всё&nbsp;необходимое, чтобы информация на сайте pelitt.com/ru была верной и актуальной, но не гарантирует точности данных и обеспечивает информацию «без гарантии качества».&nbsp;
</p>
<p>
	 PELITT LV SIA, а также партнёры, филиалы и третьи стороны, с которыми сотрудничает компания, сотрудники, подрядчики и пользователи сайта pelitt&nbsp;в степени, установленной законом, отказываются от ответственности за какие-либо негативные последствия, которые могут возникнуть у пользователей или любой третьей стороны в результате использования сайта pelitt.com/ru, а также товаров и услуг, размещенных на нём. Данное условие распространяется на любые убытки и ущерб, возникшие в результате неправильного использования сайта pelitt.com.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ИНТЕЛЛЕКТУАЛЬНАЯ СОБСТВЕННОСТЬ</h2>
<p>
	 1. Собственностью компании PELITT LV SIA или его поставщиков контента является весь контент, представленный на сайте pelitt.com. В том числе изображения, фотографии, аудио- и видеозаписи, логотипы, иконки кнопок, программное обеспечение, сводки данных и текст.
</p>
<p>
	 2. Все графические изображения, логотипы, иконки кнопок, название услуг и товаров, скрипты, указанные на сайте pelitt.com, являются предметами фирменного стиля и торговой марки (бренда) PELITT LV SIA в России и других странах.
</p>
<p>
	 3. Бренд и фирменный стиль PELITT LV SIA&nbsp;нельзя использовать для других продуктов или услуг, а также ни в какой форме, которая может навредить PELITT LV SIA или клиентам компании.
</p>
<p>
	 4. Любые другие бренды, не принадлежащие PELITT LV SIA, которые могут появиться на сайте pelitt или в других услугах PELITT LV SIA, являются собственностью владельцев данных брендов, которые могут не иметь никакого отношения к PELITT LV SIA.
</p>
<p>
	 5. Использование любых элементов фирменного стиля и торговых марок без письменного разрешения PELITT LV SIA запрещено.
</p>
<p>
	 6. Кроме вышеописанной интеллектуальной собственности, весь контент, который пользователь видит на сайте pelitt.com, защищён международными законами об авторском праве и является собственностью PELITT LV SIA.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ДОСТУП ДЛЯ ПРИЛОЖЕНИЙ</h2>
<p>
	 Пользователь должен дать разрешение на определённый доступ к своему устройству, если работает в приложении, созданном для PELITT LV SIA.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ФОРС-МАЖОР</h2>
<p>
	 В случае непредвиденных обстоятельств непреодолимой силы PELITT LV SIA не несёт ответственности за задержку в выполнении любого из своих обязательств, указанных в пользовательском соглашении, и получает дополнительное время для выполнения своих обязательств.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p>
<h2>ПРЕТЕНЗИИ</h2>
<p>
	 Спорные ситуации и претензии должны быть направлены на рассмотрение в органы с соответствующими компетенциями – в суд, обладающий исключительной юрисдикцией, расположенный в Латвии.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>