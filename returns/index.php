<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Returns");
?><h1>RETURNS</h1>
 <br>
 <br>
<p>
	 In compliance with the European Directive 97/7/ES and the Civil Code of the Czech Republic, the Purchaser has a right to withdraw from the Contract&nbsp;<strong>within 14 days from reception of goods</strong>, if the Contract has been concluded through means of distant communication, especially through electronic shop. However, this right is not intended primarily as a way to handle complaints. If the received product is defective, it is in the Purchaser's interest to follow the Claims Code.
</p>
<p>
	 The 14-days period is fixed as a more favourable period as against the consumer protection in the country of the Merchant and the Purchaser for customers from EU countries.
</p>
<p>
	 If the Purchaser decides to enforce the above-mentioned right, he must deliver his withdrawal from the Contract no later than the 14th day from collection of goods. We recommend to do so in a written form in order to ensure verifiability of the legal act which terminates the Contract.
</p>
<p>
	 We also ask you to state the tax document number, date of purchase and your bank account number.
</p>
<p>
	 Withdrawal from the Contract takes effect on its delivery in written form within the statutory period. We recommend that you insure the returned goods. Provided the legal terms for withdrawal from the Contract have been complied with, the Purchaser will be sent a credit note with the amount corresponding to the purchase price of the goods reduced only by the real expenses the Merchant incurred as a result of return of the goods. The Purchaser signs the credit note in compliance with the Act on VAT in the Czech Republic and the European VAT Directive, and sends it back to the Merchant.&nbsp;<strong>Based on the signed credit note the Consumer will receive a refund for the goods to his bank account</strong>&nbsp;no later than thirty days from withdrawal from the Contract.
</p>
<p>
 <strong>Terms for returing goods:</strong>
</p>
<ul>
	<li>goods must be returned in a complete and original state, and in original packaging if possible,</li>
	<li>neither goods nor packaging must be damaged in any way, except for the usual damage of the packaging caused when goods are taken out,</li>
	<li>goods must not show signs of wear and tear.</li>
</ul>
<p>
	 Failure to comply with the given terms can result in the Merchant enforcing rights against the Consumer ensuing from his unjust enrichment in the form of reducing the price of the returned goods.
</p>
<p>
	 Do not send the goods back cash on delivery. The parcel will be rejected.
</p>
<p>
 <strong>Send the returned goods by mail to:</strong>
</p>
 <address style="background: #e8e8e8;"><strong>Pelitt Group SE</strong><br>
 U Vodárny 3032/2a<br>
 616 00 Brno<br>
 Czech Republic<br>
 </address><br>
<p>
 <strong><a href="/Reklamační formulář_Pelitt.pdf" target="_blank">Download return form</a>&nbsp;(czech)</strong>
</p>
<p>
	 As soon as the goods are returned and checked, the Merchant issues a credit note. The Purchaser is refunded the purchase price, the transport expenses are not refunded. The Merchant is entitled to a refund of the expenses incurred as a result of return of the goods. The refund will be reduced by wear and tear of the goods.
</p>
<p>
 <strong>Please note that this time period is intended for the Consumer to make himself adequately familiar with the features of the purchased goods and cannot be deemed as a right to loan the goods.</strong>
</p>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>