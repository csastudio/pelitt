<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$ORDER_ID = $_REQUEST["order"];
CModule::IncludeModule("sale");
if (!($arOrder = CSaleOrder::GetByID($ORDER_ID)))
{
    echo "Заказ с кодом ".$ORDER_ID." не найден";
}
else
{
    $fields = [
        'rs' => 'PT02',
        'merchant_transaction_id' => '1234567767432',
        'user_ip' => $_SERVER["REMOTE_ADDR"],
        'description' => 'pelitt.com/ru order ' . $_REQUEST["order"],
        'amount' => $arOrder["PRICE"],
        'currency' => 'RUB',
        'name_on_card' => $arOrder["USER_NAME"],
        'street' => 'no street',
        'zip' => 'no zip',
        'city' => 'no city',
        'country' => 'RU',
        'state' => 'NA',
        'email' => $arOrder["USER_EMAIL"],
        'phone' => '89000000000',
        'merchant_site_url' => 'https://pelitt.com',
        'custom_return_url' => 'https://pelitt.com/ru/my/?merchant_transaction_id=ZZZZZZZ'
    ];
    $db_props = CSaleOrderPropsValue::GetOrderProps($ORDER_ID);
    while ($arProps = $db_props->Fetch()){
    if ($arProps["CODE"] == 'USER_PHONE' && $arProps["VALUE"]){
        $fields['phone'] = $arProps["VALUE"];
    }
    if ($arProps["CODE"] == 'ADDRESS_1' && $arProps["VALUE"]){
        $fields['city'] = $arProps["VALUE"];
    }}
    $s = new TransactPro\Client('TGDC-5120-V3XB-6909','t.LuSda@eI4z');
    $s->Execute($ORDER_ID, $fields);
}