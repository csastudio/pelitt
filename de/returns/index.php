<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Returns");
?><h2>
<p>
	 Rückgabe von Waren<br></h2>
 <br>
 <br>
 <br>
</p>
<p>
	 Der Käufer hat in Übereinstimmung mit der Europäischen Richtlinie 97/7 / ES und dem Bürgerlichen Gesetzbuch der Tschechischen Republik das Recht, innerhalb von 14 Tagen ab Empfang der Ware vom Vertrag zurückzutreten, wenn der Vertrag durch Fernkommunikation, besonders durch elektronischen Shop geschlossen wurde. Dieses Recht ist jedoch nicht primär als Umgang mit Beschwerden gedacht. Wenn das empfangene Produkt defekt ist, liegt es im Interesse des Käufers, die Beschwerderegeln zu folgen.
</p>
<p>
 <br>
 <br>
 <br>
	 Die 14-Tage-Frist wird als günstigere Frist gegenüber dem Verbraucherschutz im Land des Kaufmanns und des Käufers für Kunden aus EU-Ländern festgelegt.
</p>
<p>
 <br>
 <br>
 <br>
	 Entscheidet sich der Besteller, das vorgenannte Recht geltend zu machen, so hat er seinen Rücktritt vom Vertrag spätestens am 14. Tag ab Abholung der Ware abzugeben. Wir empfehlen, dies in schriftlicher Form zu tun, um die Verifizierbarkeit des Rechtsakts, der den Vertrag kündigt, zu gewährleisten.
</p>
<p>
 <br>
 <br>
 <br>
	 Wir bitten Sie auch, die Steuerbelegnummer, das Kaufdatum und Ihre Kontonummer anzugeben.
</p>
<p>
 <br>
 <br>
 <br>
	 Der Rücktritt vom Vertrag bedarf der Schriftform innerhalb der gesetzlichen Frist. Wir empfehlen Ihnen, die zurückgesandte Ware zu versichern. Unter der Voraussetzung, dass die gesetzlichen Bestimmungen für den Rücktritt vom Vertrag erfüllt sind, erhält der Käufer eine Gutschrift mit dem Betrag, der dem Kaufpreis der Ware entspricht, und zwar nur durch die tatsächlichen Kosten des Kaufmanns, die durch die Rücksendung der Ware entstanden sind . Der Käufer unterzeichnet die Gutschrift in Übereinstimmung mit dem Gesetz über die Mehrwertsteuer in der Tschechischen Republik und der europäischen MwSt-Richtlinie und sendet es an den Händler zurück. Auf der Grundlage der unterzeichneten Gutschrift erhält der Verbraucher eine Rückerstattung der Ware auf sein Bankkonto spätestens 30 Tage nach Rücktritt vom Vertrag.
</p>
<p>
 <br>
 <br>
 <br>
	 Bedingungen für die Rücknahme von Waren:
</p>
<p>
 <br>
 <br>
 <br>
	 • Die Ware muss vollständig und original und in Originalverpackung sein, wenn es möglich ist,
</p>
<p>
 <br>
 <br>
 <br>
	 • weder Waren noch Verpackungen müssen nicht in irgendeiner Weise beschädigt werden, mit der Ausnahme der üblichen Beschädigung der Verpackung, die beim Herausnehmen enstanden ist.
</p>
<p>
 <br>
 <br>
 <br>
	 • Waren dürfen keine Verschleißerscheinungen aufweisen.
</p>
<p>
 <br>
 <br>
 <br>
	 Die Nichterfüllung der gegebenen Bedingungen kann dazu führen, dass der Händler die Rechte gegen den Verbraucher aus seiner ungerechtfertigten Bereicherung in Form einer Verringerung des Preises der zurückgesendeten Ware erzwingt.
</p>
<p>
 <br>
 <br>
 <br>
	 Senden Sie die Ware nicht per Nachnahme zurück. Das Paket wird abgelehnt.
</p>
<p>
 <br>
 <br>
 <br>
	 Senden Sie die zurückgesandte Ware per Post an:
</p>
<p>
 <br>
 <br>
 <br>
 <span>&nbsp;</span>
</p>
<p>
	 Pelitt Group SE<br>
	 U Vodárny 3032/2a<br>
	 616 00 Brno<br>
	 Czech Republic
</p>
<p>
 <br>
	 Rücksendung herunterladen&nbsp;(czech)
</p>
<p>
 <br>
 <br>
 <br>
	 Sobald die Ware zurückgegeben und geprüft wird, stellt der Händler eine Gutschrift aus. Der Käufer wird den Kaufpreis zurückerstattet, die Transportkosten werden nicht zurückerstattet. Der Kaufmann hat Anspruch auf Rückerstattung der durch die Rücksendung entstehenden Kosten. Die Rückerstattung wird durch Abnutzung der Waren verringert.
</p>
<p>
 <br>
 <br>
 <br>
	 Bitte beachten Sie, dass dieser Zeitraum für den Verbraucher bestimmt ist, sich mit den Merkmalen der gekauften Ware vertraut zu machen, und kann nicht als ein Recht auf Ausleihe angesehen werden.
</p>
<p>
 <br>
 <br>
 <br>
 <span>&nbsp;</span>
</p>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>