<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Privacy-policy");
?><h2>
<p>
	DATENSCHUTZ-BESTIMMUNGEN
</h2>
</p>
<p>
 <span>&nbsp;</span>
</p>
<p>
 <span>&nbsp;</span>
</p>
<p>
 <span>&nbsp;</span>
</p>
<p>
<br>
<br>
<br>
	 Die persönlichen Daten der Kunden werden nach den geltenden Gesetzen der Tschechischen Republik, insbesondere dem Gesetz Nr. 101/2000 Slg. Über den Schutz personenbezogener Daten, in der geänderten Fassung gespeichert. Mit dem Abschluss eines Vertrages verpflichtet sich der Käufer mit der Verarbeitung und Erhebung seiner personenbezogenen Daten in der Datenbank des Verkäufers nach erfolgreicher Vertragsdurchführung, bis er vom Besteller ausdrücklich widerrufen wird.
</p>
<p>
<br>
<br>
<br>
	 Der Besteller ist berechtigt, auf seine personenbezogenen Daten zuzugreifen und seine Änderung, einschließlich anderer gesetzlicher Rechte an diesen Daten, zu verlangen. Auf der Grundlage der schriftlichen Anfrage des Kunden können ihre persönlichen Daten aus der Datenbank entfernt werden. Die persönlichen Daten der Kunden sind vollständig gegen Kompromisse geschützt. Der Verkäufer übergibt die persönlichen Daten der Kunden nicht an Dritte. Die einzigen Ausnahmen sind die externen Netzbetreiber und Zahlungsanbieter, denen die personenbezogenen Daten der Kunden im erforderlichen Mindestmaß übergeben werden, um die Waren zu liefern oder das entsprechende Zahlungsmittel zu genehmigen.
</p>
<p>
<br>
<br>
<br>
	 Bei der Unterzeichnung werden die einzelnen Verträge vom Anbieter in elektronischer Form archiviert und sind dem E-Shop-Anbieter zugänglich.
</p>
<p>
<br>
<br>
<br>
	 Der E-Shop-Anbieter ist Pelitt Group SE., Na Strži 1702/65, 140 00 Prag, Tschechische Republik.
</p>
<p>
<br>
<br>
<br>
	 Schriftlicher Widerruf von Verarbeitung und Speicherung Persönliche Daten des Kunden können an die Adresse des Anbieters, Pelitt Group SE, wie oben, geschickt werden.
</p>
<br>
<br>
<br>
 

 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>