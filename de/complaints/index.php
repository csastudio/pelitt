<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Complaints");
?><h2>BESCHWERDE</h2>
<h1><br>
 <br>
<br>

<h2>ALLGEMEINE BESTIMMUNGEN UND BEGRIFFSBESTIMMUNGEN</h2>
<h1><br>
<ol type="a">
	<li>Dieses Schadenregulierungsgesetz wurde nach den Bestimmungen des Bürgerlichen Gesetzbuches und des Verbraucherschutzgesetzes in Bezug auf die in den Allgemeinen Geschäftsbedingungen festgelegten europäischen Bestimmungen erstellt und bezieht sich auf Verbrauchsgüter (im Folgenden: Waren), in denen die Ansprüche des Käufers auf Produkthaftung geltend gemacht werden (nachstehend Ansprüche). Diese Reklamationen gelten für Einkäufe im elektronischen Shop www.pelitt.com. Die Bedingungen sind ferner definiert und spezifiziert durch die Rechte und Pflichten der Vertragsparteien, die:
</li>
	<ol type="i">
<br>
		<li>Kaufmann, die Gesellschaft Pelitt Group SE, IČO 03815854, mit Sitz in Na Strži 1702/65, 140 00 Praha 4, Tschechische Republik (Einzelheiten in der Rubrik Kontakt), eine Gesellschaft, eingetragen im Handelsregister des Amtsgerichts Prag unter Eintrag H 1549 ist.</li>
	<br>	<li>Der Käufer ist ein Unternehmen, das mit dem Kaufmann einen Kaufvertrag geschlossen hat.</li>
	<br>	<li>Der Kaufmann kann den Käufer, der nicht Verbraucher ist, diesen Kodex angemessen anwenden. Andernfalls verbleiben die aus der <br>Produkthaftung resultierenden Rechte nach dem Vertrag und nach dem Bürgerlichen Gesetzbuch.</li>
	</ol>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h2>INKONSISTENZ MIT KAUFVERTRAG</h2>
<h1><br>
<ol type="a">
	<li>Falls der Zustand des Produktes bei Empfang nicht mit dem Kaufvertrag (im Folgenden: Inkonsistenz mit dem Kaufvertrag) übereinstimmt, hat der Käufer das Recht, vom Verkäufer die Situation kostenlos und unverzüglich durch Austausch oder Reparatur des Produkts, wie es der Besteller verlangt,zu beheben. Ist ein solches Verfahren nicht möglich, kann der Besteller einen angemessenen Nachlass geltend machen oder vom Vertrag zurücktreten.</li>
	<li>Das Vorstehende gilt nicht, wenn vor dem Erhalt der Ware der Besteller über den Widerspruch mit dem Kaufvertrag Bescheid wusste oder die Inkonsistenz durch den Besteller selbst verursacht wurde. Inkonsistenz mit dem Kaufvertrag erkannt sechs Monate nach Empfang der Ware gilt als anwesend an der Rezeption, es sei denn, es widerspricht der Art der Ware oder sofern nicht anders angegeben.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h2>GARANTIEBEDINGUNGEN</h2>
<h1>
<ol type="a"><br>
	<li>Wenn  der Besteller nach dem Empfang der Produkte Mängel innerhalb der Gewährleistungsfrist entdeckt, kann er seinen  Anspruch geltend machen.</li>
	<br><li>Die Gewährleistungsfrist entspricht den geltenden gesetzlichen Bestimmungen. Sofern der Produzent nichts anderes bestimmt oder im Falle des Verbrauchers, ist die Gewährleistungsfrist 24 Monate und beginnt mit dem Eingang der Ware, es sei denn, die Gewährleistungsfrist ist länger.</li>
	<br><li>Der Kaufmann haftet für etwaige Mängel an der Rezeption, Mängel bei Material- und Herstellungsfehlern, die nach Erhalt der Garantiezeit entstanden sind.</li>
<br>	<li>Die Gewährleistung kann nicht in folgenden Fällen erzwungen werden:
	<ol type="i">
	<br>	<li>Wenn  der Gewährleistungsanspruch nach dem Ablauf der Gewährleistungsfrist gemacht wird</li>
	<br>	<li>Der Mangel entstand durch Missbrauch des Produktes.</li>
	<br>	<li>Der Fehler ist durch eine Nichtbeachtung der Anweisungen des Herstellers enstanden.</li>
	<br>	<li>Der Fehler ist durch eine unprofessionelle Installation, Handhabung, Bedienung, Manipulation oder Fahrlässigkeit der Produktwartung enstanden.</li>
	<br>	<li>Der Mangel entstand durch unbefugte Eingriffe in die Ware oder sonstige vom Produzenten nicht zugelassene Änderungen.</li>
	<br>	<li>Waren wurden bei Naturkatastrophen beschädigt.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h2>GARANTIERECHTE</h2>
<h1>
<ol type="a"><br>
	<li>Die Gewährleistungsfrist verlängert sich um die Zeit, in der die Ware unter Gewährleistung in Reparatur bleibt. Im Falle eines Warenaustausches erhält der Besteller eine neue Gewährleistungsfrist von 24 Monaten.</li>
<br>	<li>Bei der Geltendmachung seines Gewährleistungsrechts hat der Verbraucher:
	<ol type="i">
	<br>	<li>Wenn ein ordnungsgemäßer und rechtzeitiger Ersatz des fehlerhaften Produkts oder eines fehlerhaften Teils die ordnungsgemäße und rechtzeitige Beseitigung des Fehlers verlangt ist, es sei denn, es ist aufgrund der Art des Fehlers nicht ausreichend, es sei denn, ein solches Verfahren ist nicht möglich Einen angemessenen Rabatt vom Kaufpreis zu verlangen oder vom Vertrag zurückzutreten,</li>
	<br>	<li>Wenn es einen nicht behebbaren Fehler gibt, der die ordnungsgemäße Verwendung von Waren verhindert, wenn Sie den Austausch fehlerhafter Ware verlangen oder vom Kaufvertrag zurücktreten möchten,</li>
	<br>	<li>Wenn es behebbare Fehler mit mehrfachem und wiederholtem Auftreten gibt, die eine ordnungsgemäße Verwendung der Ware verhindern, wenn Sie den Austausch fehlerhafter Waren verlangen oder vom Kaufvertrag zurücktreten möchten,</li>
	<br>	<li>Wenn es andere unwiderlegbare Mängel gibt und wenn Sie keinen Austausch von Waren benötigen, wenn Sie einen angemessenen Rabatt vom Kaufpreis verlangen oder vom Kaufvertrag zurücktreten möchten.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h2>ANSPRUCHSTELLUNG</h2>
<h1>
<ol type="a">
	<br><li>Der Ort, an dem Sie Ihren Anspruch geltend machen sollten, ist der Betrieb des Technisches Partner (Produzent) oder der Betrieb des Kaufmanns.</li>
	<br><li>Machen Sie Ihre Ansprüche an:</li>
 <address>Pelitt Group SE<br>
	 U Vodárny 3032/2a<br>
	 616 00 Brno<br>
	 Česká republika<br>
 </address><br>
<br>	<p>
 <a href="http://static-flamefox-catalog.bizboxlive.com/__files/FlameFox-Catalog/Reklama%C4%8Dn%C3%AD%20formul%C3%A1%C5%99_Pelitt.pdf?497e9" target="_blank">Anspruchformulart hetunterladen</a>&nbsp;
	</p>
<br>	<li>Kommt der Käufer persönlich zum Betrieb des Technischen Partners oder des Merchants, so empfiehlt es sich, einen Termin im Voraus per E-Mail oder telefonisch zu vereinbaren.<a href="http://dev2_pelitt.csabx.tmweb.ru/cz/contact/">Benutzen Sie bitte Kontaktformular</a>&nbsp;oder senden Sie bitte  E-mail an&nbsp;<a href="mailto:support@pelitt.com">support@</a><a href="mailto:support@pelitt.com">pelitt.com</a>.</li>
<br>	<li>In dem Fall, in dem der Kaufmann beschlossen hat, die Ware an ihn zurückzusenden, ist es im Interesse des Käufers, sicherzustellen, dass die Ware in geeigneter Verpackung verpackt wird.</li>
<br>	<li>Sobald die Forderung ordnungsgemäß behandelt wird, fordert das Personal des Technischen Partners oder der Zweigniederlassung den Besteller auf, die reparierten Waren einzusammeln</li>
<br>	<li>Es ist immer notwendig, eine schriftliche Aufzeichnung der festgestellten Mängel und der Methode ihrer Beseitigung zu machen. Wir empfehlen dringend, dass der Käufer diese Aufzeichnung behält, solange die Garantiezeit gültig ist.</li>
<br>	<li>Im Falle einer anerkannten Reklamation hat der Kunde Anspruch auf eine angemessene Vergütung</li>
<br>	<li>Im Falle einer abgelehnten Forderung kann der Verbraucher keine Entschädigung für die im Antragsverfahren entstandenen Kosten geltend machen, und in ähnlicher Weise kann der Kaufmann keine Entschädigung für die entstandenen Aufwendungen verlangen, es sei denn, es handelt sich um eine unzumutbare Forderung des Käufers, wenn ein Rechtsmissbrauch vorliegt angenommen wird.</li>
<br>	<li>Der Kaufmann oder der Technische Partner entscheidet über die Forderung sofort, in komplizierten Fällen innerhalb von drei Werktagen. Diese Frist enthält nicht die erforderliche Zeit für die technische Überprüfung des Mangels, die entsprechend der Art des Produkts oder der Dienstleistung angemessen ist. Der Anspruch und der Mangel werden unverzüglich, spätestens 30 Kalendertage nach Empfang der Forderung, behandelt, sofern nichts anderes vereinbart ist.</li>
	<br><li>Der Kaufmann stellt dem Verbraucher eine schriftliche Bescheinigung aus, in der das Datum des Anspruchs, sein Inhalt und die angeforderte Handhabung angegeben sind. Weiterhin erteilt der Kaufmann eine Bescheinigung über das Datum und die Art der Abwicklung, inkl. Informationen über die durchgeführte Reparatur und ihre Dauer. Im Falle einer abgelehnten Forderung stellt der Merchant eine schriftliche Erklärung vor, die seine Ablehnung rechtfertigt.</li>
<br>	<li>Aufbewahrungsgebühren und -aufwendungen für unberechtigte Forderungen: Ist die gestellte Forderung nicht innerhalb von 30 Tagen nach abgeschlossener Reparatur erhoben worden oder ist ein Produkt nicht im Schadensfall erhoben worden, so kann dem Käufer eine Lagergebühr in Höhe von CZK 30 (exkl ) für jeden Kalendertag bekommen . Der Händler ist berechtigt, das Produkt nicht abzugeben, bis der Gesamtbetrag für die Reparatur sowie die Lagergebühr bezahlt sind. Sobald dieser Betrag den aktuellen Verkaufspreis des Produktes übersteigt, ist der Händler berechtigt, das Produkt zur Deckung der anfallenden Kosten zu verkaufen.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h2>SCHLUSSBESTIMMUNGEN</h2>
<h1>
<ol type="a">
	<br><li>Dieser Kodex tritt mit Wirkung vom 1. Mai 2014 in Kraft.</li>
	<br><li>Änderungen der Ansprüche vorbehalten; Der Kodex ist Teil der Allgemeinen Geschäftsbedingungen des Kaufmanns.</li>
</ol>
 <br>
 <br>
 <br>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>