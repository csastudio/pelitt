<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Delivery-information");
?><h2>
<p>
	 LIEFERINFORMATIONEN </h2>
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 Der Preis der Lieferung ist für alle Bestellungen frei. Die Art der Auslieferung aller im E-Shop verkauften Waren der Pelitt Group SE wird durch das in der Bestellung angegebene Lieferland geteilt:
</p>
<p>
<br>
 <br>
 <br>
	 TSCHECHISCHE&nbsp; REPUBLIK
</p>
<p><br>
 <br>
 <br>
	 Die Lieferung erfolgt innerhalb von 2 Werktagen. In der Tschechischen Republik erfolgt die Zustellung durch die Kurierfirma Geis. Wir können auch in Ausgabestellen von Geis anbieten&nbsp;<a href="http://geispoint.cz/?#ss_vydejnimista">Geis Point</a>.
</p>
<p><br>
 <br>
 <br>
	 LIEFERUNG DURCH KURIER
</p>
<p><br>
 <br>
 <br>
	 • Der Kurier sendet Ihnen im Voraus eine Warnmeldung über die in Ihrer Bestellung angegebene Telefonnummer. Ohne Telefonnummer, wo Sie tagsüber erreichbar sind, wird Ihre Bestellung nicht bearbeitet.
</p>
<p><br>
 <br>
 <br>
	 • Die Ware wird von einem Spediteur oder Kurierdienst an Ihre Adresse geliefert. So werden versandte Waren vor der ersten Haustür ausgeliefert. Der Fahrer hat in der Regel keine Co-Fahrer, daher ist er nicht in der Lage, Waren innerhalb des Hauses zu bewegen.
</p>
<p><br>
 <br>
 <br>
	 • Sie erhalten eine SMS / E-Mail-Benachrichtigung, die Sie über die Lieferzeit im Voraus informiert.
</p>
<p><br>
 <br>
 <br>
	 • Wenn Sie per Nachnahme bestellen, müssen Sie den ganzen Betrag beim Kurier beim Kurier bezahlen.
</p>
<p><br>
 <br>
 <br>
	 • Der Besteller ist verpflichtet, die Ware ordnungsgemäß zu erhalten, die Unversehrtheit der Verpackung und die Anzahl der Pakete zu prüfen und im Falle eines Defektes dies der Filiale des Verkäufers mitzuteilen und den Mangel auf der Liefe- rungsleistung zu vermerken. Der Käufer unterschreibt das Zertifikat und bestätigt, dass das Paket alle oben genannten Bedingungen erfüllt hat und dass spätere Beschwerden über beschädigte Verpackungen ignoriert werden. Die Rechnung dient auch als Steuerbeleg.
</p>
<p><br>
 <br>
 <br>
	 • Der Besteller ist verpflichtet, die gelieferte Ware unverzüglich zu untersuchen und im Falle eines unvollständigen Konsignationsberichts spätestens drei Werktage nach Anlieferung an den Anbieter zu melden. Spätere Beschwerden werden ignoriert.
</p>
<p> 
<br>
<br>
	 • Ist der Besteller nicht in der Lage, die bestellte Ware innerhalb der festgelegten Frist zu erhalten, so wird der Kurier ihn erneut kontaktieren, um den Zeitpunkt der Nachlieferung zu vereinbaren.
</p>
<p>
<br>
	 • Ist der Besteller wiederholt nicht in der Lage, die Ware zu empfangen, ist er verpflichtet, die entstandenen Kosten des Verkäufers, d. H. Transportkosten, zu decken.
</p>
<p>
 <br><br>
 <br>
	 • Weigert sich der Besteller, die Ware bei Ablieferung zu empfangen oder er nicht auf der Kontaktadresse erreichbar ist, so beansprucht der Merchant die Kosten für die Lieferung der Ware.<br>
 </p>
 <br>
<br>
<br>
	 SLOWAKISCHE REPUBLIK
</p>
 <br>
<br>
<br>
<p>
 <br>
<br>
<br>	 Die Lieferung erfolgt innerhalb von 3 Werktagen. Die Lieferung an die Slowakische Republik erfolgt durch die Kurierfirma Geis.
</p>
<p>
 <br>
<br>
<br>	 LIEFERUNG DURCH KURIER
</p>
<p>
 <br> 
<br>
<br>	 • Der Kurier sendet Ihnen im Voraus eine Warnmeldung über die in Ihrer Bestellung angegebene Telefonnummer. Ohne Telefonnummer, wo Sie tagsüber erreichbar sind, wird Ihre Bestellung nicht bearbeitet.
</p>
<p>
 <br>

	 • Die Ware wird von einem Spediteur oder Kurierdienst an Ihre Adresse geliefert. So werden versandte Waren vor der ersten Haustür ausgeliefert. Der Fahrer hat in der Regel keine Co-Fahrer, daher ist er nicht in der Lage, Waren innerhalb des Hauses zu bewegen.
</p>
<p> 
 <br>
	 • Sie erhalten eine SMS / E-Mail-Benachrichtigung, die Sie über die Lieferzeit im Voraus informiert.
</p>
<p>
 <br>
	 • Wenn Sie per Nachnahme bestellen, müssen Sie den ganzen Betrag beim Kurier beim Kurier bezahlen.
</p>
<p>
 <br>
	 • Der Besteller ist verpflichtet, die Ware ordnungsgemäß zu erhalten, die Unversehrtheit der Verpackung und die Anzahl der Pakete zu prüfen und im Falle eines Defektes dies der Filiale des Verkäufers mitzuteilen und den Mangel auf der Liefe- rungsleistung zu vermerken. Der Käufer unterschreibt das Zertifikat und bestätigt, dass das Paket alle oben genannten Bedingungen erfüllt hat und dass spätere Beschwerden über beschädigte Verpackungen ignoriert werden. Die Rechnung dient auch als Steuerbeleg.
</p>
<p>
 <br>
	 • Der Besteller ist verpflichtet, die gelieferte Ware unverzüglich zu untersuchen und im Falle eines unvollständigen Konsignationsberichts spätestens drei Werktage nach Anlieferung an den Anbieter zu melden. Spätere Beschwerden werden ignoriert.
</p>
<p>
 <br>
	 • Ist der Besteller nicht in der Lage, die bestellte Ware innerhalb der festgelegten Frist zu erhalten, so wird der Kurier ihn erneut kontaktieren, um den Zeitpunkt der Nachlieferung zu vereinbaren.
</p>
<p>
 <br>
	 • Ist der Besteller wiederholt nicht in der Lage, die Ware zu empfangen, ist er verpflichtet, die entstandenen Kosten des Verkäufers, d. H. Transportkosten, zu decken.
</p>
<p>
 <br>
	 • Weigert sich der Besteller, die Ware bei Ablieferung zu empfangen oder er nicht auf der Kontaktadresse erreichbar ist, so beansprucht der Merchant die Kosten für die Lieferung der Ware.
</p>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>