<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Cookies");
?><h1>ERKLÄRUNG ZUR VERWENDUNG VON "COOKIES" TECHNOLOGIE </h1>
    <h1><br>
        <br>
        <p>
	 Wie die meisten Unternehmen verwendet Pelitt Group SE die "Cookies" -Technologie, um den Komfort der Besucher und Nutzer ihrer Internetseiten zu verbessern.
        </p>
        <p>
            <br>
        </p>
        <p>
	 "Cookies" sind Miniaturdateien, die aus Buchstaben und Ziffern bestehen, die in Ihrem Computer gespeichert werden, wenn Sie mit dieser Technologie eine Website eingeben. Unter anderem können Cookies der Website und E-Shop-Betreiber helfen, Ihre Person zu identifizieren und Sie von anderen Besuchern und Nutzern dieser Websites und E-Shops zu unterscheiden. "Cookies" können NICHT als Programmcode oder zur Verbreitung von Computerviren verwendet werden und können nicht zulassen, dass eine der oben genannten Einheiten auf Ihre Festplatte oder Ihre Daten zugreifen kann. Obwohl "Cookies" in Ihrem Computer gespeichert sind, können wir keine Daten von dort lesen / herunterladen.
        </p>
        <p>
            <br>
        </p>
        <p>
	 Daten, die von Websites der Pelitt Group SE unter Verwendung der "Cookies" -Technologie bezogen werden, können für folgende Zwecke verwendet werden:
        </p>
        <p>
            <br>
        </p>
        <ul>
	 <li>Besucher oder Nutzer von Websites und E-Shops zu ermöglichen, sich durch Seiten zu bewegen und ihre Funktionen zu nutzen, z.B. Eingeben der gesicherten Teile (Abschnitte) von Seiten oder Hinzufügen von Gegenständen zum Wagen usw., d. H. &amp; Ldquor; unbedingt erforderliche "Cookies;</li>

	 <li>Informationen über Besucher- und Nutzerverhalten auf Websites und im E-Shop zu sammeln, z.B. Welche Seiten am häufigsten besucht werden oder ob Besucher oder Benutzer Fehlermeldungen erhalten etc .; Diese "Cookies" werden vor allem zur Verbesserung der Funktionalität und Qualität der zukünftigen Website-Versionen verwendet;</li>

	 <li>Speichern von Informationen über Auswahlmöglichkeiten (oder Auswahlen) von Website-Besuchern und Benutzern, die z.B. Auswahl von Sprache oder Region, Paging-Werte, Originalfilter zum Durchsuchen des Produktkatalogs usw. werden diese "Cookies" meistens verwendet, um einen personalisierten Ansatz zu gewährleisten;</li>

	 <li>Für Promotions- und Marketingaktivitäten der Pelitt Group SE, ihrer Programme, Produkte und Dienstleistungen werden diese "Cookies" meistens verwendet, um einen personalisierten Ansatz im Marketing- und Geschäftsbereich zu garantieren.</li>
            <li><br>
            </li>
        </ul>
        <p>
	 Die Gesellschaft Pelitt Group SE gestattet es Dritten nicht, kommerzielle Texte auf ihren Websites zu platzieren. Die oben genannte Firma kann keine Gewähr und Haftung für jegliche Inhalte, Die auf den Webseiten Dritter liegen, sowie für jegliche Verletzung von gesetzlichen Bestimmungen, Rechten oder legitimen Interessen Dritter, die auf Webseiten Dritter aufgetreten sind oder entstehen könnten.
        </p>
        <p>
            <br>
        </p>
        <p>
	 Durch die Bestätigung der Vereinbarung mit diesem Dokument gewährt der Besucher oder der Nutzer der Website dem Unternehmen Pelitt Group SE, Sitz in Na strži 1702/65, 140 00 Prag 4, Gesellschaftsnummer: 03815854, eingetragen im Handelsregister des Stadtgerichts in Prag, Abschnitt H, Eintrag 1549 mit der Verwendung der "Cookies" -Technik einverstanden ist und die Daten von Webseiten verwendet, die von der Firma betrieben werden, die diese Technologie für jeden Zweck, wie hier angegeben, anwendet. Die Einwilligung ist freiwillig und wird auf unbestimmte Zeit gewährt und kann jederzeit kostenlos, in Form eines Schriftstückes, das an die Gesellschaft Pelitt Group SE übermittelt wird, widerrufen werden
        </p>
        <p>
            <br>
        </p>
        <p>
	 Weiterhin möchten wir Sie darüber informieren, dass die Nichtbeachtung der Einwilligung, der Widerruf und / oder die Deaktivierung von "Cookies" (in Ihrem Internet-Browser) Die die streng genommenen Cookies deaktivieren, können den Zugriff auf die Website oder die Funktionalität der Website oder ihrer Teile einschränken oder verhindern.
        </p>
        <br>
        <br>
    </h1>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>