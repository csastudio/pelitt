<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Terms-conditions");
?><h1></h1>
<h1>
<p>
	 HANDELSREGELN
</p>
<p>
 <br>
</p>
<p>
	 1. Allgemeine Bestimmungen
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vorliegende Handelsregeln gelten in Bezug auf Käufe, die im Elektronik-Onlineshop <a href="http://www.pelitt.com">www.pelitt.com</a> abgewickelt werden. Die Handelsregeln werden nachfolgend in Form der Pflichten und Rechten der Vertragsparteien festgehalten:
</p>
<p>
	 1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Verkäufer – Gesellschaft „Pelitt Group SE“, ID-Nr: 03815854, Anschrift Na strži 1702/65, 140 00 Prag 4, Tschechische Republik (Details s. unter: <a href="https://pelitt.com/contact">Kontakt</a> (<a href="https://pelitt.com/contact">Contact Us</a>)), eingetragen im Firmenregister im regionalen Kommunalgericht von Prag unter Register-Nr. H 1549,
</p>
<p>
	 2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; KÄUFER
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sämtliche Vertragsbeziehungen unterliegen dem Recht der Tschechischen Republik. Falls eine der Vertragsparteien keinen Auftraggeber darstellt, werden sämtliche, von diesem Vertrag nicht vorgesehene Beziehungen durch Gesetz Nr. 89/2012 Coll des Zivilgesetzbuches in der letzten Fassung (nachfolgend Zivilgesetzbuch genannt) und durch Gesetz Nr.634/1992 Coll über den Schutz der Verbraucherrechte in der letzten Fassung (nachfolgend Verbraucherschutzgesetz) geregelt. Juristische Handlungen mit ausländischen Auftraggebern, die durch den vorliegenden Vertrag nicht geregelt sind, unterliegen entsprechenden rechtlichen Verbraucherschutz-Vorschriften, die im Land des Auftraggebers gelten.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Falls eine der Vertragsparteien nicht als Auftraggeber handelt, unterliegen die Vertragsbeziehungen dem Gesetz Nr. 89/2012 Coll des Zivilgesetzbuches in der letzten Fassung (nachfolgend Zivilgesetzbuch genannt).
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 2. VERWENDETE BEGRIFFE UND IHRE INTERPRETATION
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vertrag mit dem Auftraggeber- Kaufvertrag, Dienstleistungsvertrag oder andere Verträge, falls nachstehende Personen als Vertragsparteien handeln: Auftraggeber einerseits und Lieferant oder Verkäufer andererseits.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lieferant/Verkäufer- Gesellschaft, die bei Abschluss oder Ausführung des Vertrags kommerzielle oder unternehmerische Tätigkeit ausübt. Der Unternehmer liefert an den Käufer Produkte oder Dienstleistungen direkt oder durch andere Unternehmer.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Käufer/Auftraggeber-Person, die bei der Unterzeichnung und Ausführung des Vertrags keine Kommerzielle -oder unternehmerische Tätigkeit ausübt. Natürliche oder juristische Person, die Produkte erwirbt oder Dienstleistungen für die Zwecke bezieht, die von denen der unternehmerischen Tätigkeit abweichen.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kaufvertrag-Einladung des Käufers, der nicht als Auftraggeber handelt, einen Kaufvertrag abzuschließen-Auftragsbestätigung des Käufers; dabei wird der Vertrag faktisch erst nach Empfang der obligatorischen Annahme der o.g. Einladung der Käufers durch den Verkäufer abgeschlossen.
</p>
<p>
	 e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kaufvertrag-Einladung des Käufers, der als Auftraggeber handelt, einen Kaufvertrag abzuschließen-Akte der Veröffentlichung der vorhandenen Waren in Elektronik-Onlineshops; dabei wird der Vertrag nach Versand des Auftrags durch den Käufer und Empfang der obligatorischen Annahme der o.g. Einladung bei dem Verkäufer abgeschlossen. Der Verkäufer bestätigt den Eingang des Auftrags umgehend durch den Versand einer elektronischen Bestätigung an die angegebene Adresse; dies hat jedoch keinen Einfluss auf die Entstehung des Vertrags. Der abgeschlossene Vertrag kann nach gegenseitigem Einvernehmen beider Seiten oder gemäß gültiger Gesetzgebung geändert oder aufgelöst werden.
</p>
<p>
	 f.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kaufvertrag-Einladung des Käufers, der nicht als Auftraggeber handelt, einen Kaufvertrag abzuschließen-Auftragsbestätigung des Käufers; dabei wird der Vertrag faktisch erst nach Empfang der obligatorischen Annahme der o.g. Einladung durch den Käufer abgeschlossen.
</p>
<p>
	 g.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Technischer Partner- ein Unternehmer, der für Qualität der Produkte und mangelfreien Zustand der Ware bei dem Verkauf zuständig ist. Mit dem Lieferanten ist er durch Vertrag für Bearbeitung der individuellen Reklamationen (bzgl. Fehlmengen) und Rücksendungen in gesetzlich geregelten Fristen in den Fällen, wenn der Auftraggeber von dem (im Fernverfahren abgeschlossenen Vertrag zurücktritt, verbunden. Im vorliegenden Vertrag steht der Begriff „Partner“ für den Dienstleistungslieferanten, der durch Lieferanten über eine bevollmächtigte Person vertreten werden kann.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 3. Angaben zum abgeschlossenen Vertrag und Handelsregeln
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mit der Unterzeichnung des Kaufvertrags bestätigt der Käufer, dass er die vorliegenden Handelsregeln inkl. Bestimmungen bzgl. Rücksendungen, Reklamationen, Datenschutzrichtlinien und „Cookie-Nutzungsbedingungen“ im genannten online-Shop gelesen und akzeptiert hat. Vorliegende Handelsregeln wurden dem Käufer ordnungsgemäß vor der Auftragsbestätigung vorgelegt; der Käufer hatte die Möglichkeit, ebendiese durchzulesen. Vorliegende Handelsregeln stellen einen integrierten Bestandteil des abgeschlossenen Vertrags dar.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Für die Auftraggeber aus der Tschechischen und Slowakischen Republik gilt der in tschechischer Sprache verfasster Kaufvertrag. Falls möglich, kann der Kaufvertrag für den Käufer und Verkäufer auch in einer anderen (für sie verständlichen) Sprache verfasst werden.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Der abgeschlossene Kaufvertrag (Auftrag des Käufers) wird bei dem Verkäufer zwecks erfolgreicher Ausführung aufbewahrt und archiviert; mit Ausnahme von dem Technischen Partner haben keine Drittpersonen Zugriff darauf. Angaben zu individuellen technischen Fortschritten, die zum Vertragsabschluss führen, sind dem Auftragsverfahren im genannten online-Shop zu entnehmen; der Kunde hat auch die Möglichkeit, seinen Auftrag zu überprüfen und bei Bedarf zu ändern.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vorliegende Handelsregeln sind auf der Webseite des genannten online-Shops veröffentlicht. Der Kunde hat also die Möglichkeit, sie zu speichern und erneut abzurufen.
</p>
<p>
	 e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Die Ausgaben für die Nutzung der Kommunikationsmittel (Telefon, Internet, usw.), die für Auftragsabwicklung verwendet werden, hängen von jeweiligen Dienstleistungstarifen des Kunden ab und werden durch den Verkäufer nicht zurückerstattet.
</p>
<p>
	 f.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Unmittelbar nach der Bezahlung der Waren überträgt der Verkäufer die Eigentums-und Nutzungsrechte auf die erworbenen Waren an den Käufer. Ebenfalls verpflichtet sich der Käufer, den vollen Kaufbetrag (gemäß verbindlichem Auftrag und Bestimmungen des vorliegenden Vertrags) zu bezahlen.
</p>
<p>
	 g.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jeder Bestellung wird ein Lieferschein/Steuerdokument beigelegt, das gleichzeitig einen Frachtbrief darstellt (falls der Beförderer kein Lieferzertifikat zustellt). Der Kunde enthält die im verbindlichen Auftrag festgelegten Waren. Der Kunde unterzeichnet das Dokument und bestätigt die Zustellung der Waren durch den Beförderer oder persönliche Entgegennahme der Bestellung an den Abholpunkten.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 4. INFORMATIONEN
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sämtliche Inforationen zu Ihrer Bestellung können Sie hauptsächlich per E-Mail oder telefonisch erhalten. Für Kontaktdaten siehe <a href="https://pelitt.com/contact">Uns kontaktieren</a> auf der Seite des genannten Internet-Shops (<a href="https://pelitt.com/contact">Contact Us</a>).
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dieser Internet-Shop verwendet auch Cookies-Technologie. Für Details zu den angesammelten Daten s.<a href="https://pelitt.com/cookies">Cookies-Technologie</a>» (<a href="https://pelitt.com/cookies">Cookies Technology</a>). Dieser Text stellt ebenfalls einen integrierten Bestandteil des vorliegenden Vertrags dar.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Der genannte Online-Shop verarbeitet auch einige persönliche Kunden-Daten. Für weitere Details, sowie Angaben über Verwaltung und Zugriff auf diese Daten s. <a href="https://pelitt.com/privacy-policy">Datenschutzrichtlinien</a> (<a href="https://pelitt.com/privacy-policy">Personal Details Protection</a>)Der genannte Text stellt ebenfalls einen integrierten Bestandteil des vorliegenden Vertrags dar.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Verkehrsunternehmen und Unternehmen, die Verwendung von Zahlungsinstrumenten zulassen, erhalten ein Minimum an Informationen, die für Zahlungsoperationen und Zustellung der Waren erforderlich sind.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 5. Bestellung
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bestellungen können in elektronischer Form im Online-Shop angegeben werden, der 24/7 im Betrieb ist.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vor dem Absenden der Bestellung empfehlen wir Ihnen, die Positionen in Ihrem Warenkorb nochmals zu überprüfen- dadurch können evtl. Probleme und Missverständnisse vermieden werden. Der Kunde hat die Möglichkeit, seine Bestellung vor dem Abschicken nochmals zu überprüfen und zu ändern.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nach dem Absenden der Bestellung erhält der Kunde eine Bestellbestätigung via E-Mail. Falls Sie in der Bestellbestätigung falsche Angaben entdecken, kontaktieren Sie uns bitte unverzüglich. Nach dem Empfang Ihrer Bestellung setzen wir uns in kürzester Zeit mit unserem Logistik-Partner in Verbindung, der für die Zustellung der Waren an die angegebene Adresse verantwortlich ist.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 6. Auflösung des Vertrags auf Initiative des Käufers
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sobald die verbindliche Bestellung&nbsp; abgeschickt ist (der Kunde klickt auf „Bestellung abschicken“)(Send Order)), und der Verkäufer sie empfangen und bestätigt hat (automatische elektronische Benachrichtigung mit der Bestellübersicht wird an die angegebene E-Mail-Adresse verschickt), wird mit dem Kunden ein verbindlicher Kaufvertrag abgeschlossen (Details s. unter: „Verwendete Begriffe und ihre Interpretation“
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Stornierungen sind nur in Ausnahmefällen und nach Absprache mit dem Verkäufer möglich. Eine Stornierung ist unverzüglich über <a href="https://pelitt.com/contact">Kontaktformular abzuschicken</a> (<a href="https://pelitt.com/contact">contact form</a>). Dies hat keine Auswirkungen auf das Recht des Auftraggebers, von dem Vertrag zurücktreten, das im Zivilgesetzbuch und der entsprechenden Europäischen Richtlinie verankert ist (s. unten).
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Falls der Auftraggeber die Waren nicht abholt, ohne vorherige &nbsp;Stornierungsbenachrichtigung verschickt zu haben, die durch den Verkäufer empfangen werden muss, so trägt der Käufer (der keinen Auftraggeber darstellt) sämtliche Kosten aus dieser Geschäftsoperation (Logistik, Bank, Transportkosten, usw.) In diesem Zusammenhang wird das Recht des Verkäufers auf den Schadenersatz nicht angewendet. Der Auftraggeber erklärt sich damit einverstanden, dass die Bestellung jeder beliebigen Person übergeben wird, die sich an der angegebenen Adresse befindet und sich mittels gültiger ID-Nr. ausweisen kann. Es gilt dabei, dass die übergebene Bestellung ordnungsgemäß entgegengenommen wurde.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Falls der Käufer, der keinen Auftraggeber darstellt, seine Bestellung NICHT ABHOLT ohne vorher eine Stornierungsbenachrichtigung verschickt zu haben, die durch Lieferanten ordnungsgemäß empfangen werden soll, trägt er sämtliche, wegen der Lieferung angefallene Kosten, falls für einen Rücktritt vom Vertrag (s. unten) die Waren nach juristischem Recht des Auftraggebers zurückgeschickt werden können. Dies hat jedoch keine Auswirkungen auf das Recht des Auftraggebers, die Waren bei Abweichungen vom Kaufvertrag nicht abzuholen (s. unten).
</p>
<p>
	 e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Die Ware bleibt weiterhin das Eigentum des Verkäufers. Falls keine Umstände vorliegen, bei welchen der Käufer die Bestellung ohne Begründung nicht abholen kann, wird das Eigentumsrecht nicht übertragen. In diesem Fall ist der Verkäufer berechtigt, eine Entschädigung für Lagerung oder erneute Zustellung der Waren zu verlangen.
</p>
<p>
	 f.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Wird die Bestellung im Laufe von 6 Monaten ab Abholdatum durch den Kunden nicht abgeholt, gilt es, dass die Vertragsparteien von dem Vertrag zurückgetreten sind. In diesem Fall hat der Verkäufer Anspruch auf Zurückerstattung der Kosten, die wegen Lieferung und Lagerung der Waren oder aus sonstigen Gründen angefallen sind. Der Verkäufer macht diese Ansprüche in Form der Anrechnung des Kaufpreises geltend.
</p>
<p>
	 g.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Werden die bestellten Waren nicht mehr hergestellt oder nicht mehr geliefert, oder ist mit uns keine Kontaktaufnahme während längerer Zeit möglich, oder liegen markante Änderungen des Verkaufspreises vor oder die Warenbeschreibung enthält Fehler (Bezeichnung, Foto usw.) und der Käufer stellt keinen Auftraggeber dar, so ist der Lieferant berechtigt, vom Vertrag zurückzutreten. Handelt der Käufer als Auftraggeber, so ist der Verkäufer verpflichtet, sich mit ihm für Absprachen bzgl. weiterer Vorgehen unverzüglich in Verbindung zu setzen.
</p>
<p>
	 i.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In so einer Situation kontaktieren wir Sie per E-Mail. Falls Sie die bestellten Waren bereits bezahlt haben, die Bestellung jedoch annulliert werden musste, wird der Betrag an Sie in kürzester Zeit überwiesen (in der Regel im Laufe von 5 Tagen nach der Bestätigung der Stornierung durch den Verkäufer).
</p>
<p>
	 ii.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Wird die Bestellung geändert und fällt ihr Gesamtwert anschließend höher aus, werden Sie gebeten, die Differenz zu bezahlen. Die Bestellung wird nach dem Eingang der Differenz verschickt. Ist der eingezahlte Betrag höher, so überweisen wir die Differenz auf Ihr Bankkonto zurück.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 7. Preise
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Alle Preise in unserem Online-Shop verstehen sich zzgl. MwSt. (unter Berücksichtigung der jwlg. Enddestination (Land, Region) und sonstiger Gebühren, z.B. Verpackung, Recycling (falls gesetzlich vorgesehen). Vor der Abwicklung des Bestellvorgangs kann sich der Kunde über die Gültigkeitsdauer der Angebote/der Preise informieren.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Die angegebenen Preise verstehen sich ohne Transport-und Lieferkosten und ohne administrativen Aufwand, der mit Postnachname bei bestimmten Zustellmethoden in einzelnen Ländern verbunden ist.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Die Preise gelten für den Zeitpunkt der Abwicklung der Bestellung.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Stellt der Käufer keinen Auftraggeber dar, so behalten wir uns das Recht vor, die Preise zu ändern. Bei einer Erhöhung des Verkaufspreises ist der Kunde berechtigt, auf den Kauf kostenfrei zu verzichten. Der Kunde wird über dieses Recht vorzeitig informiert.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 8. Zahlungsbestimmungen
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Banküberweisung: Unser Internet-Shop schickt Ihnen sämtliche Angaben über die Bankverbindung zu, damit Sie die Bezahlung innerhalb von 7 Arbeitstagen tätigen können. Die Bestellung wird unmittelbar nach dem Eingang des Betrags ausgeliefert. Falls Sie Ihre Bestellung nicht abholen können, informieren Sie sich bitte über weiteres Vorgehen unter „Stornierte Bestellungen“. Diese Zahlungsmethode steht Auftraggebern in den Republiken Tschechei und Slowakei zur Verfügung.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bezahlung mit der Karte: die Bezahlung im Internet-Shop erfolgt online und die Bestellung wird unmittelbar nach der Abwicklung der Zahlungsoperation und Autorisierung ihrer Karte versandt.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nachname: das Paket wird mit Kurier verschickt (per Post) oder wird zum Abholpunkt gebracht, wo es gegen Bezahlung ausgehändigt wird. Diese Zahlungsmethode steht nur Kunden in den Republiken Tschechei und Slowakei zur Verfügung.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Der Verkäufer behält sich das Recht vor, von dem Kaufvertrag zurückzutreten oder durch Banküberweisung bezahlte Bestellung zu stornieren, wenn es zu einer übermäßigen Anzahl Reservierungen in offenen Bestellungen kommt oder die Bonität des Käufers nicht belegt werden kann. Somit kann der Verkäufer aufgrund seiner Erfahrungen mit dem Kunden Einschränkungen für den Kauf von Waren einführen, die mittels Überweisung oder Nachname bezahlt werden. Falls sich der Betrag immer noch auf dem Bankkonto des Lieferanten befindet, wird er zurück an den Käufer überwiesen.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 9. Empfang der Waren und Transportkosten
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sämtliche (im vorliegenden Abschnitt nicht enthaltene) Bestimmungen/Bedingungen bzgl. Warenlieferungen und Preise, inkl. persönliches Warensortiment in Abholpunkten sind den <a href="https://pelitt.com/delivery-information">Lieferbestimmungen</a> zu entnehmen, die einen integrierten Bestandteil dieses Vertrags darstellen.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Der Käufer, der nicht als Auftraggeber handelt, ist verpflichtet, die Bestellung nach dem Empfang zu überprüfen. Entdeckt der Käufer bei dem Empfang der Bestellung Mängel (z.B. beschädigte Verpackung) so überprüft der Käufer gemeinsam mit dem Zulieferer die Waren und erstellt (bei aufgedeckten Mängeln) ein Schadensprotokoll (von welchem er anschließend eine Kopie erhält). Sind die gelieferten Waren beschädigt, ist der Käufer berechtigt, die Entgegennahme zu verweigern. Zu einem späteren Zeitpunkt eingereichte Reklamationen werden nicht bearbeitet. Der Käufer ist verpflichtet, den Verkäufer über beschädigte Waren zwecks Einleitung entsprechender Maßnahmen unverzüglich zu informieren.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Der Käufer/Auftraggeber kann nicht verpflichtet werden, die Bestellung bei dem Empfang zu überprüfen; wir empfehlen unseren Kunden jedoch, darauf gemäß o.g. P. nicht zu verzichten. Somit kann der Käufer evtl. Probleme, die mit Reklamationen wegen Transportschaden verbunden sind, erfolgreich verhindern. Diese Reklamationen basieren in erster Linie auf dem Transportdokument, in welchem der Käufer den Zustand der gelieferten Ware festhalten kann. Der Käufer ist berechtigt, die Annahme der äußerlich beschädigten Waren zu verweigern und eine erneute Lieferung anzufordern.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Die Waren werden in der Regel innerhalb von 2-4 Arbeitstagen nach der Bestellabfertigung an die angegebene Adresse verschickt.
</p>
<p>
	 e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Der Auftraggeber erklärt sich damit einverstanden, dass die Bestellung an jede beliebige Person übergeben wird, die sich an der angegebenen Adresse befindet und sich durch eine gültige Id-Nr. ausweisen kann. Es gilt jedoch dabei, dass die übergebene Bestellung ordnungsgemäß empfangen wurde.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 10. Rücktrittrecht des Auftraggebers
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Das Recht des Auftraggebers, vom (im Fernverfahren) abgeschlossenen Vertrag zurücktreten zu können, sowie das Verfahren zur Ausübung dieses Rechts sind dem Abschnitt „<a href="https://pelitt.com/returns">Rückgabe</a>“ zu entnehmen, dessen Text im vorliegenden Vertrag ebenfalls enthalten ist.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Werden die Waren innerhalb von 30 Tagen nicht geliefert, so hat der Auftraggeber das Recht, vom Vertrag zurückzutreten und Anspruch auf Rückerstattung des vollen Kaufpreises geltend zu machen.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 11. Reklamationen
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Reklamationen aus der Garantie sind an unseren Technischen Partner zu richten.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bleiben Ihre Anfragen an den Technischen Partner über 10 Arbeitstage lang unbeantwortet, so haben Sie das Recht, sich in direkte Verbindung mit dem Lieferanten zu setzen.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Genaue Ablauffolge bei der Einreichung der Reklamationen während der Garantiefrist und das Verfahren der Überprüfung der Reklamationen sind dem Abschnitt <a href="https://pelitt.com/complaints">Reklamationen</a> zu entnehmen, der im vorliegenden Vertrag enthalten ist.
</p>
<p>
	 d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sämtliche Unstimmigkeiten zwischen der Fa. Pelitt Group SE und dem Käufer können außergerichtlich durch alternative Regelungsmethoden online geregelt werden (AMS/USO). In diesem Fall kann sich der Käufer mit der entsprechenden Organisation für die Beilegung der Streitigkeiten <a href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&lng=EN" target="_blank">hier</a> in Verbindung setzen. Weitere Details zu alternativen Methoden der Regelung von Streitfällen befinden sich <a href="https://webgate.ec.europa.eu/odr/main/?event=main.about.show" target="_blank">hier</a>. Für die Beseitigung des einen oder des anderen Streitfalls empfiehlt die Pelitt Group SE dem Käufer, <a href="https://pelitt.com/contact">sich zuerst mit </a>&nbsp;Pelitt Group SE in Verbindung zu setzen.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 12. Zusätzliche entgeltliche Dienstleistungen
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Änderung der Angaben in den durch eine natürliche Person an eine &nbsp;juristische Person ausgestellten Eingangsrechnungen wird mit einer Gebühr von 300 tschechischen Kronen ohne MwSt. belegt.
</p>
<p>
 <br>
 <br>
 <br>
</p>
<p>
	 13. SCHLUSSBESTIMMUNGEN
</p>
<p>
	 a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Die Handelsregeln treten per 1. Mai 2016 in Kraft.
</p>
<p>
	 b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Der Lieferant behält sich das Recht vor, Änderungen ohne vorherige Benachrichtigung vorzunehmen.
</p>
<p>
	 c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Falls Sie bereits Kunde von unserem Internet-Shop sind und sich über die aktuellen Bestimmungen informieren möchten, kontaktieren Sie uns, wir werden froh sein, diese Ihnen bereitzustellen.
</p>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>