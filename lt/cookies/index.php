<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Cookies");
?><h1>PROHLÁŠENÍ O POUŽÍVÁNÍ TECHNOLOGIE „COOKIES“</h1>
<h1><br>
 <br>
<p>
	 Podobně jako většina subjektů i&nbsp;společnost Pelitt Group SE používá&nbsp;technologii „cookies“ pro zlepšení uživatelského komfortu&nbsp;návštěvníků a&nbsp;uživatelů svých internetových stránek.
</p>
<p>
	<br>
</p>
<p>
	 „Cookies“ jsou miniaturní soubory, složené z&nbsp;řetězu písmen a&nbsp;číslic, které server uloží do&nbsp;Vašeho počítače při vstupu na&nbsp;internetovou stránku využívající tuto technologii. „Cookies“ kromě jiného provozovateli internetových stránek a&nbsp;elektronických obchodů umožňují odlišení Vaší osoby od&nbsp;ostatních návštěvníků a uživatelů těchto stránek a&nbsp;obchodů. „Cookies“&nbsp;NEMOHOU být použité jako programový kód ani k šíření počítačových virů&nbsp;a&nbsp;nemohou umožnit přístup ani jedné výše uvedené společnosti na&nbsp;Váš pevný disk&nbsp;a&nbsp;k&nbsp;Vašim datům. I&nbsp;přesto, že&nbsp;se&nbsp;na&nbsp;Vašem pevném disku ukládají „cookies“, nemůžeme z&nbsp;něho číst jakékoliv jiné informace.
</p>
<p>
	<br>
</p>
<p>
	 Údaje získané prostřednictvím stránek společnosti Pelitt Group SE využívající technologii „cookies“&nbsp;mohou být použité především na&nbsp;následující účely:
</p>
<p>
	<br>
</p>
<ul>
	<li>umožnění pohybu návštěvníka,&nbsp;resp. uživatele stránek a&nbsp;elektronického obchodu po&nbsp;stránkách a&nbsp;využívání jejich funkcí, jako např. přístup do&nbsp;zabezpečených částí (sekcí) stránek či&nbsp;ukládání položek do&nbsp;nákupního košíku atd. (tzv. nevyhnutelně potřebné - "Strictly Necessary" - cookies);&nbsp;</li>
	<li>shromažďování informací o&nbsp;tom, jak návštěvníci a&nbsp;uživatelé stránek používají stránky a&nbsp;elektronický obchod, jako např. které stránky jsou navštěvované nejčastěji anebo jestli návštěvníci nebo uživatelé stránek přijímají zprávy o&nbsp;chybách atd.; tyto „cookies“ jsou používané hlavně pro vylepšení funkčnosti a funkcionality budoucích verzí stránek;&nbsp;</li>
	<li>uchovávaní údajů o&nbsp;volbách&nbsp;(resp. výběrech)&nbsp;uskutečněnými návštěvníky a&nbsp;uživateli stránek,&nbsp;jako např. výběr jazyka a&nbsp;regionu, zvolené hodnoty stránkování, vychozí omezující podmínky při prohlížení produktového katalogu atd., tyto „cookies“ jsou používané hlavně pro zabezpečení osobnějšího (resp. personalizovaného) přístupu;&nbsp;</li>
	<li>reklamní a&nbsp;marketingové účely&nbsp;společnosti Pelitt Group SE, jejích programů, produktů a&nbsp;služeb, tyto „cookies“ jsou používané hlavně pro zajištění osobnějšího (resp. personalizovaného) přístupu v&nbsp;oblasti reklamy a&nbsp;marketingu.</li>
	<li><br>
	</li>
</ul>
<p>
	 Společnost Pelitt Global SE&nbsp;neumožňuje&nbsp;kterýmkoliv třetím osobám umísťování reklamního obsahu na&nbsp;své stránky. Výše uvedená společnost neručí a&nbsp;nezodpovídá za jakýkoli obsah (včetně reklamního a&nbsp;marketingového) umístěný na&nbsp;internetových stránkách třetích osob, jako ani za&nbsp;jakékoli porušení právních předpisů, práv nebo oprávněných zájmů třetích osob, ke&nbsp;kterým dochází, resp. by&nbsp;mohlo docházet na&nbsp;internetových stránkách třetích osob.
</p>
<p>
	<br>
</p>
<p>
	 Potvrzení souhlasu s&nbsp;tímto dokumentem uděluje návštěvník, resp. uživatel stránek&nbsp;společnosti Pelitt Group SE, se&nbsp;sídlem Na strži 1702/65,140 00 Praha , IČO: 03815854, zapsané v&nbsp;obchodním rejstříku Městského soudu v Praze, oddíl H,&nbsp;vložka 1549, včetně jejich mateřských, sesterských a&nbsp;dceřiných společností a&nbsp;dalších subjektů společnosti Pelitt Group SE&nbsp;souhlas na&nbsp;používání technologie „cookies“a&nbsp;používání údajů získaných prostřednictvím stránek a&nbsp;elektronického obchodu uvedených společností využívajících tuto technologii na&nbsp;každý a&nbsp;všechny účely uvedené výše v&nbsp;tomto dokumentu. Souhlas se&nbsp;uděluje dobrovolně na&nbsp;předem neurčenou dobu a&nbsp;je&nbsp;možné jej kdykoli bezplatně, písemně, formou psaní doručeného společnosti Pelitt Group SE odvolat.
</p>
<p>
	<br>
</p>
<p>
	 Pro úplnost si&nbsp;dovolujeme upozornit, že&nbsp;neudělení souhlasu, jeho odvoláním a&nbsp;nebo zablokováním technologie „cookies“ (ve Vašem internetovém prohlížeči) včetně zablokování tzv. nevyhnutelně potřebných (Strictly Necessary) cookies, může omezit nebo vyloučit přístup na&nbsp;stránky nebo funkcionalitu stránek nebo jejich částí.
</p>
 <br>
 <br>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>