<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Terms-conditions");
?><h1>OBCHODNÍ&nbsp;PODMÍNKY</h1>
<h1><br>
 <br>
 </h1>
<h4>1&nbsp;OBECNÁ USTANOVENÍ</h4>
<h1>
<ol type="a">
	<li>Tyto obchodní podmínky platí pro nákup v&nbsp;elektronickém internetovém obchodě&nbsp;<a href="http://www.pelitt.com">www.pelitt.</a><a href="http://www.pelitt.com">com</a>. Podmínky blíže vymezují a&nbsp;upřesňují práva a&nbsp;povinnosti jednotlivých smluvních stran, kterými jsou:
	<ol type="i">
		<li>prodávající, kterým je&nbsp;společnost&nbsp;Pelitt Group SE,&nbsp;IČO 03815854, se&nbsp;sídlem Na strži 1702/65, 140 00 Prague 4,&nbsp;Česká republika (podrobně v&nbsp;sekci&nbsp;<a href="/cz/contact">Kontaktujte nás</a>),&nbsp;společnost zapsaná v&nbsp;obchodním rejstříku vedeném u&nbsp;Městského soudu v&nbsp;Praze pod spisovou značkou H&nbsp;1549,</li>
		<li>kupující.</li>
	</ol>
 </li>
	<li>Veškeré smluvní vztahy jsou uzavřeny v&nbsp;souladu s&nbsp;právním řádem České republiky. Je-li smluvní stranou spotřebitel, řídí se&nbsp;vztahy neupravené obchodními podmínkami zákonem č.&nbsp;89/2012&nbsp;Sb., Občanský zákoník, ve&nbsp;znění pozdějších předpisů (dále jen Občanský zákoník) a&nbsp;zákonem č.&nbsp;634/1992 Sb., o&nbsp;ochraně spotřebitele, ve&nbsp;znění pozdějších předpisů (dále jen Spotřebitelský zákon). Pro spotřebitele z&nbsp;jiných zemí než České republiky se&nbsp;řídí právní vztahy neupravené obchodními podmínkami dále subsidiárně příslušnou právní úpravou na&nbsp;ochranu spotřebitele v&nbsp;zemi původu spotřebitele.</li>
	<li>Je-li smluvní stranou nikoli spotřebitel, řídí se&nbsp;vztahy neupravené obchodními podmínkami pouze zákonem č.&nbsp;89/2012&nbsp;Sb., Občanský zákoník, ve&nbsp;znění pozdějších předpisů (dále jen Občanský zákoník).</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>2&nbsp;VYMEZENÍ POJMŮ</h4>
<h1>
<ol type="a">
	<li>Spotřebitelská smlouva&nbsp;je&nbsp;smlouva kupní, o&nbsp;dílo, případně jiné smlouvy, pokud smluvními stranami jsou na&nbsp;jedné straně spotřebitel a&nbsp;na&nbsp;druhé straně dodavatel, resp. prodávající.</li>
	<li>Dodavatel/prodávající&nbsp;je&nbsp;osoba, která při uzavírání a&nbsp;plnění smlouvy jedná v&nbsp;rámci své obchodní nebo jiné podnikatelské činnosti. Je&nbsp;to&nbsp;podnikatel, který přímo nebo prostřednictvím jiných podnikatelů dodává kupujícímu výrobky nebo poskytuje služby.</li>
	<li>Kupující/spotřebitel&nbsp;je&nbsp;osoba, která při uzavírání a&nbsp;plnění smlouvy nejedná v&nbsp;rámci své obchodní nebo jiné podnikatelské činnosti. Je&nbsp;to&nbsp;fyzická či&nbsp;právnická osoba, která nakupuje výrobky nebo užívá služby za&nbsp;jiným účelem než pro podnikání s&nbsp;těmito výrobky nebo službami.</li>
	<li>Kupní smlouva, není-li kupujícím spotřebitel&nbsp;&nbsp; návrhem na&nbsp;uzavření kupní smlouvy je&nbsp;odeslaná objednávka zboží kupujícím a&nbsp;samotná kupní smlouva je&nbsp;uzavřena momentem doručení závazného souhlasu prodávajícího kupujícímu s&nbsp;tímto jeho návrhem.</li>
	<li>Kupní smlouva, je-li kupujícím spotřebitel&nbsp;&nbsp; návrhem k&nbsp;uzavření kupní smlouvy je&nbsp;umístění nabízeného zboží dodavatelem na&nbsp;stránky elektronického obchodu, kupní smlouva vzniká odesláním objednávky kupujícím spotřebitelem a&nbsp;přijetím objednávky dodavatelem. Toto přijetí dodavatel neprodleně potvrdí kupujícímu informativním e-mailem na&nbsp;zadaný e-mail, na&nbsp;vznik smlouvy však toto potvrzení nemá vliv. Vzniklou smlouvu (včetně dohodnuté ceny) lze měnit nebo rušit pouze na&nbsp;základě dohody stran nebo na&nbsp;základě zákonných důvodů.</li>
	<li>Kupní smlouva, není-li kupujícím spotřebitel&nbsp;&nbsp; návrhem na&nbsp;uzavření kupní smlouvy je&nbsp;odeslaná objednávka zboží kupujícím a&nbsp;samotná kupní smlouva je&nbsp;uzavřena momentem doručení závazného souhlasu prodávajícího kupujícímu s&nbsp;tímto jeho návrhem.</li>
	<li>Technický partner&nbsp;je&nbsp;podnikatel odpovědný za&nbsp;kvalitu a&nbsp;vady prodávaných výrobků, který je&nbsp;smluvně odpovědný dodavateli za&nbsp;vyřízení jednotlivých případů reklamace vad zboží a&nbsp;vrácení zboží v&nbsp;zákonné lhůtě při&nbsp;odstoupení spotřebitele od&nbsp;smlouvy uzavřené na&nbsp;dálku. Tímto partnerem je&nbsp;výrobce produktů, za&nbsp;kterého může v&nbsp;zastoupení jednat dodavatel.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>3&nbsp;INFORMACE O&nbsp;UZAVŘENÉ SMLOUVĚ A&nbsp;OBCHODNÍCH PODMÍNKÁCH</h4>
<h1>
<ol type="a">
	<li>Uzavřením kupní smlouvy kupující stvrzuje, že&nbsp;se&nbsp;seznámil s&nbsp;těmito obchodními podmínkami, s&nbsp;podmínkami vrácení zboží, se&nbsp;způsobem provádění reklamace, s&nbsp;podmínkami ochrany osobních údajů a&nbsp;podmínkami využití technologie „cookies“ na &nbsp;tomto elektronickém obchodu, a&nbsp;že&nbsp;s&nbsp;nimi souhlasí. Na&nbsp;tyto obchodní podmínky je&nbsp;kupující dostatečným způsobem před vlastním uskutečněním objednávky upozorněn a&nbsp;má&nbsp;možnost se&nbsp;s&nbsp;nimi seznámit. Tyto obchodní podmínky jsou nedílnou součástí uzavřené smlouvy.</li>
	<li>Smlouva je pro spotřebitele z&nbsp;České a&nbsp;Slovenské republiky&nbsp;uzavírána v&nbsp;jazyce českém a v jazyce anglickém pro kupující/nikoli spotřebitele. Nebrání-li tomu okolnosti na&nbsp;straně prodávajícího či&nbsp;kupujícího, lze smlouvu uzavřít i&nbsp;v&nbsp;jiném pro strany srozumitelném jazyce.</li>
	<li>Uzavřená smlouva (objednávka kupujícího) je&nbsp;prodávajícím archivována za&nbsp;účelem jejího úspěšného splnění a&nbsp;není přístupná třetím stranám s&nbsp;výjimkou technického partnera. Informace o&nbsp;jednotlivých technických krocích vedoucí k&nbsp;uzavření smlouvy jsou patrné z&nbsp;procesu objednávání v&nbsp;tomto internetovém obchodě a&nbsp;kupující má&nbsp;možnost před vlastním odesláním objednávky objednávku&nbsp;zkontrolovat a&nbsp;případně opravit.</li>
	<li>Tyto obchodní podmínky jsou zobrazeny na&nbsp;webových stránkách tohoto internetového obchodu, a&nbsp;je&nbsp;tak umožněna jejich archivace a&nbsp;reprodukce kupujícím.</li>
	<li>Náklady na&nbsp;použití komunikačních prostředků na&nbsp;dálku (telefon, internet atd.) pro uskutečnění objednávky jsou v&nbsp;běžné výši, závislé na&nbsp;tarifu telekomunikačních služeb, které objednatel používá, a&nbsp;nejsou hrazeny dodavatelem.</li>
	<li>Zaplacením zboží prodávající převádí vlastnická a&nbsp;užívací práva k&nbsp;tomuto zboží na&nbsp;kupujícího. Kupující se&nbsp;zároveň zavazuje splatit celou kupní cenu způsobem uvedeným v&nbsp;závazné objednávce a&nbsp;těchto podmínkách.</li>
	<li>Ke&nbsp;každému zboží je&nbsp;vystavena a&nbsp;přiložena faktura/daňový doklad, která slouží současně jako dodací list, není-li současně doručován protokol o&nbsp;doručení zásilky ze&nbsp;strany přepravce zboží. Kupující přebírá zboží způsobem uvedeným v&nbsp;závazné objednávce. Kupující svým podpisem stvrzuje převzetí zboží od&nbsp;dopravce.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>4&nbsp;INFORMACE</h4>
<h1>
<ol type="a">
	<li>Veškeré informace týkající se&nbsp;objednávky je&nbsp;možno získat především e-mailem, popřípadě telefonicky. Kontakty jsou uvedeny v&nbsp;tomto internetovém obchodu v&nbsp;sekci&nbsp;<a href="/cz/contact">Kontaktujte nás</a>.</li>
	<li>V&nbsp;rámci tohoto internetového obchodu je&nbsp;využívána technologie „cookies“, více informací o&nbsp;sbíraných informacích naleznete v&nbsp;části nazvané „<a href="/cz/cookies/">Cookies</a>“,&nbsp;jejíž text je&nbsp;rovněž součástí obchodních podmínek.</li>
	<li>V&nbsp;rámci tohoto internetového obchodu jsou dále zpracovávány některé osobní údaje kupujících, více informací o&nbsp;způsobu správy a&nbsp;přístupu k&nbsp;těmto osobním údajům viz část „<a href="/cz/privacy-policy/">Ochrana osobních údajů</a>“,&nbsp;jejíž text je&nbsp;rovněž součástí obchodních podmínek.</li>
	<li>Přepravním společnostem a&nbsp;společnostem zajišťujícím autorizaci platebních nástrojů je&nbsp;předáváno minimum informací nezbytných pro zajištění autorizace platby a&nbsp;doručení objednávky.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>5&nbsp;OBJEDNÁVKA</h4>
<h1>
<ol type="a">
	<li>Zboží lze u&nbsp;dodavatele objednat pouze vyplněním elektronického formuláře internetového obchodu, a&nbsp;to&nbsp;nepřetržitě 24&nbsp;hodin denně.</li>
	<li>Před odesláním objednávky doporučujeme kupujícímu pečlivě zkontrolovat jednotlivé položky, předejdete tím případným pozdějším nedorozuměním. Kupující má&nbsp;možnost ještě neodeslanou objednávku zkontrolovat a&nbsp;případně opravit.</li>
	<li>Po&nbsp;odeslání objednávky Vám bude zasláno e-mailem její potvrzení. V&nbsp;případě, že&nbsp;v&nbsp;tomto potvrzení naleznete jakékoliv nesrovnalosti, okamžitě nás kontaktujte, abychom mohli zjednat nápravu. Ihned po&nbsp;obdržení Vaší objednávky bude předána informace logistickému partnerovi pro účely zajištění doručení zboží ve&nbsp;Vaší objednávce na&nbsp;Vámi udanou adresu.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>6&nbsp;ZRUŠENÍ OBJEDNÁVKY ZE&nbsp;STRANY KUPUJÍCÍHO</h4>
<h1>
<ol type="a">
	<li>Odesláním závazné objednávky (kliknutím na&nbsp;tlačítko „Odeslat objednávku“) kupujícím a&nbsp;jejím doručením prodávajícímu s&nbsp;následným potvrzením ze&nbsp;strany prodávajícího (automaticky zaslaný e-mail s&nbsp;kopií objednávky odeslaný na&nbsp;zadanou e-mailovou adresu) vzniká závazná kupní smlouva (blíže k&nbsp;procesu vzniku smlouvy viz Vymezení pojmů).</li>
	<li>Zrušit objednávku lze pouze výjimečně, a&nbsp;to&nbsp;vždy pouze se&nbsp;souhlasem prodávajícího. Žádost o&nbsp;zrušení objednávky je&nbsp;třeba sdělit prostřednictvím&nbsp;<a href="/cz/contact">kontaktního formuláře</a>&nbsp;bez zbytečného odkladu. Tím není dotčeno právo spotřebitele na&nbsp;odstoupení od&nbsp;smlouvy dle Občanského zákoníku a&nbsp;příslušné evropské směrnice upravující toto právo (viz níže).</li>
	<li>Pokud si&nbsp;zákazník neodebere objednané zboží bez předešlého zrušení objednávky (akceptované prodejcem), nese kupující, který není spotřebitelem, ke&nbsp;své tíži veškeré vzniklé náklady s&nbsp;tímto obchodním případem (logistické, bankovní, přepravní atd.). Právo na&nbsp;náhradu škody prodávajícího tím není dotčeno. Zákazník souhlasí s&nbsp;tím, že&nbsp;objednané zboží bude vydáno kterékoli osobě, která se&nbsp;bude nacházet na&nbsp;zákazníkem uvedené doručovací adrese, a&nbsp;která prokáže svoji totožnost předložením platného dokladu totožnosti. Takto vydané zboží se&nbsp;považuje za&nbsp;řádně odebrané.</li>
	<li>Kupující, který je&nbsp;spotřebitelem a&nbsp;který NEODEBERE objednané zboží bez předešlého zrušení objednávky (akceptované prodejcem), nese ke&nbsp;své tíži vzniklé náklady s&nbsp;doručením tohoto zboží, pokud se&nbsp;jedná o&nbsp;zboží, které lze vrátit v&nbsp;rámci zákonného práva spotřebitele na&nbsp;odstoupení od&nbsp;smlouvy (viz níže). Tím není dotčeno právo spotřebitele zboží nepřevzít z&nbsp;důvodu rozporu s&nbsp;kupní smlouvou (viz níže).</li>
	<li>Zboží zůstává majetkem prodávajícího, pokud jej kupující bezdůvodně neodebere, nedochází tak k&nbsp;přechodu vlastnictví. V&nbsp;takovém případě je&nbsp;prodávající oprávněn požadovat poplatek za&nbsp;uskladnění, případně náhradu nákladů za&nbsp;opakované náhradní doručení.</li>
	<li>Pokud kupující nepřevezme zaplacené zboží do&nbsp;šesti měsíců ode dne, kdy byl povinen věc převzít, má&nbsp;se&nbsp;za&nbsp;to, že&nbsp;strany kupní smlouvy od&nbsp;této odstoupily. V&nbsp;takovém případě má&nbsp;prodávající nárok na&nbsp;úhradu nákladů, které mu&nbsp;vznikly s&nbsp;doručením, uskladněním zboží, a&nbsp;případně i&nbsp;na&nbsp;úhradu jinak vzniklé škody, přičemž tyto pohledávky prodávající uplatní u&nbsp;kupujícího formou započtení oproti vracené kupní ceně.</li>
	<li>Jestliže se&nbsp;objednané zboží již nevyrábí nebo nedodává nebo je&nbsp;dlouhodobě nedostupné, popř. dojde-li k&nbsp;výrazné změně ceny, za&nbsp;kterou prodávající získává zboží od&nbsp;svých dodavatelů, či&nbsp;je-li zjištěna typografická chyba u&nbsp;zboží (popis, fotografie atd.), a&nbsp;není-li kupujícím spotřebitel, má&nbsp;právo prodávající od&nbsp;smlouvy odstoupit. Je-li kupujícím spotřebitel, zavazuje se&nbsp;prodávající neprodleně kontaktovat kupujícího za&nbsp;účelem dohody o&nbsp;dalším postupu.
	<ol type="i">
		<li>V&nbsp;případě, že&nbsp;tato situace nastane, budeme Vás kontaktovat e-mailem. Pokud jste již za&nbsp;zboží zaplatili a&nbsp;dojde-li ke&nbsp;zrušení objednávky, budou Vám po&nbsp;provedení storna peníze převedeny zpět na&nbsp;Váš účet v&nbsp;nejkratším možném termínu (běžně do&nbsp;pěti pracovních dnů od&nbsp;potvrzení storna ze&nbsp;strany prodávajícího).</li>
		<li>V&nbsp;případě, že&nbsp;dojde ke&nbsp;změně objednávky a&nbsp;výsledná hodnota objednávky bude po&nbsp;provedení změny vyšší, bude Vám dán k&nbsp;úhradě rozdíl těchto částek a&nbsp;zboží zasláno po&nbsp;obdržení doplatku. Bude-li zaplacená částka vyšší, rozdíl převedeme na&nbsp;Váš účet.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>7&nbsp;CENA ZBOŽÍ</h4>
<h1>
<ol type="a">
	<li>Všechny ceny zboží v&nbsp;tomto internetovém obchodě jsou uvedeny s&nbsp;koncovou cenou DPH pro Vaši zvolenou cílovou zemi (region), včetně příp. dalších poplatků (např. poplatek z&nbsp;obalů, recyklační poplatek apod., jsou-li tyto poplatky zákonem předepsány). Kupující má&nbsp;možnost se&nbsp;před provedením objednávky seznámit se&nbsp;skutečností, po&nbsp;jakou dobu zůstává nabídka nebo cena v&nbsp;platnosti.</li>
	<li>V&nbsp;nabízené ceně zboží nejsou zahrnuty náklady na&nbsp;přepravu a&nbsp;doručení zboží a&nbsp;dále náklady na&nbsp;administrativní zajištění úhrady zboží prostřednictvím tzv. dobírky v&nbsp;zemích, kde je&nbsp;tato služba nabízena.</li>
	<li>Ceny zboží jsou platné v&nbsp;momentě uskutečnění objednávky.</li>
	<li>Není-li kupujícím spotřebitel, vyhrazuje si&nbsp;naše společnost právo změny cen. Zvýší-li se&nbsp;cena objednávky, má&nbsp;kupující právo od&nbsp;smlouvy odstoupit bez jakýchkoliv úhrad. Na&nbsp;tuto skutečnost bude kupující předem upozorněn.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>8&nbsp;ZPŮSOB PLATBY</h4>
<h1>
<ol type="a">
	<li>Bankovním převodem – internetový obchod Vám poskytne platební informace pro zajištění zaplacení Vaší objednávky, na&nbsp;které můžete provést do&nbsp;7&nbsp;dnů platbu. Zboží nebude dodáno dříve, než bude platba připsána na&nbsp;náš účet. V&nbsp;případě neodebrání zboží se&nbsp;postupuje, jak je&nbsp;výše uvedeno v&nbsp;kapitole „Zrušení objednávky kupujícím“. Tato platební metoda je&nbsp;nabízena v&nbsp;České a&nbsp;Slovenské republice.</li>
	<li>Platební kartou – platba v&nbsp;internetovém obchodě probíhá online, přičemž zboží bude odesláno ihned po&nbsp;dokončení platebního procesu a&nbsp;potvrzení autorizace Vaší platební karty.</li>
	<li>Dobírkou – zboží je&nbsp;odesláno kurýrní službou, přepravní poštou nebo doručeno na&nbsp;odběrové místo, kde bude vydáno po&nbsp;uhrazení ceny objednávky z&nbsp;Vaší strany. Tato platební metoda je&nbsp;nabízena jen v&nbsp;České a&nbsp;Slovenské republice.</li>
	<li>Prodávající si&nbsp;vyhrazuje právo odstoupit od&nbsp;kupní smlouvy a&nbsp;zrušit objednávku provedenou bankovním převodem z&nbsp;důvodu nadměrné rezervace produktů v&nbsp;otevřených objednávkách nebo z&nbsp;důvodu pochybností o&nbsp;platební morálce kupujícího. Analogicky si&nbsp;prodávající vyhrazuje právo na&nbsp;základě předchozí zkušenosti s&nbsp;kupujícím neumožnit nákup s&nbsp;platební metodou bankovní převod nebo dobírka. Pokud přesto dojde k&nbsp;poukázání platby na&nbsp;účet dodavatele, bude celá uhrazená částka kupujícímu vrácena.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>9&nbsp;PŘEVZETÍ ZBOŽÍ A&nbsp;CENA DOPRAVY</h4>
<h1>
<ol type="a">
	<li>Veškeré podmínky nad rámec této kapitoly týkající se&nbsp;doručení zboží a&nbsp;ceny za&nbsp;tuto službu vč. možností osobního odběru na&nbsp;odběrových místech jsou popsány v&nbsp;dokumentu&nbsp;<a href="/cz/delivery-information/">Dodací podmínky</a>,&nbsp;jehož text je&nbsp;součástí těchto obchodních podmínek.</li>
	<li>Kupující, který není spotřebitelem, je&nbsp;povinen si&nbsp;zboží při převzetí prohlédnout. Zjistí-li tento kupující při převzetí zboží od&nbsp;dopravce nesrovnalosti, např. obal je&nbsp;viditelně poškozen, zkontroluje kupující společně s&nbsp;dopravcem zásilku a&nbsp;v&nbsp;případě poškození zboží provedou ihned zápis o&nbsp;poškození zásilky (kopii musí dostat kupující). V&nbsp;případě poškození zboží má&nbsp;kupující právo zboží nepřevzít. Pozdější reklamace mechanického poškození zboží již nebude uznána. O&nbsp;poškození zásilky bude kupující okamžitě informovat dodavatele pro zjednání nápravy.</li>
	<li>Kupujícímu spotřebiteli nelze uložit povinnost provést prohlídku zboží při převzetí, nicméně naše společnost v&nbsp;jeho vlastním zájmu doporučuje, aby tuto prohlídku vykonal dle předchozího odstavce. Zamezí tak případným komplikacím s&nbsp;vyřizováním reklamace zboží poškozeného přepravou. Při této reklamaci se&nbsp;bude vycházet zejména z&nbsp;přepravního dokladu, do&nbsp;kterého má&nbsp;spotřebitel možnost uvést stav doručené zásilky. Spotřebitel má&nbsp;právo dodané zboží, které bude jevit známky vnějšího poškození, nepřevzít a&nbsp;požadovat dodání nového.</li>
	<li>Expedice zboží probíhá zpravidla do&nbsp;dvou až&nbsp;čtyř pracovních dnů od&nbsp;provedení objednávky na&nbsp;dodací adresu uvedenou v&nbsp;objednávce.</li>
	<li>Zákazník souhlasí s&nbsp;tím, že&nbsp;objednané zboží bude vydáno kterékoli osobě, která se&nbsp;bude nacházet na&nbsp;zákazníkem uvedené doručovací adrese a&nbsp;která prokáže svoji totožnost předložením platného dokladu totožnosti. Takto vydané zboží se&nbsp;považuje za&nbsp;řádně odebrané.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>10&nbsp;PRÁVO SPOTŘEBITELE NA&nbsp;ODSTOUPENÍ OD&nbsp;SMLOUVY</h4>
<h1>
<ol type="a">
	<li>Právo spotřebitele na&nbsp;odstoupení od&nbsp;smlouvy uzavřené na&nbsp;dálku a&nbsp;postup k&nbsp;uplatnění tohoto práva je&nbsp;uveden v&nbsp;samostatné části označené „<a href="/cz/returns/">Vrácení zboží</a>“,&nbsp;jejíž text je&nbsp;rovněž součástí těchto obchodních podmínek.</li>
	<li>Pokud není zboží doručeno ve lhůtě 30 dní, má zákazník právo na odstoupení od&nbsp;smlouvy a&nbsp;vrácení celé zaplacené částky.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>11&nbsp;REKLAMAČNÍ ŘÁD</h4>
<h1>
<ol type="a">
	<li>Reklamace vad v&nbsp;záruční době (a&nbsp;uplatnění reklamace) provádí kupující u&nbsp;technického partnera.</li>
	<li>V&nbsp;případě, že&nbsp;technický partner neodpovídá na&nbsp;komunikaci s&nbsp;kupujícím déle než 10&nbsp;pracovních dní, má&nbsp;kupující právo pokračovat v&nbsp;komunikaci přímo s&nbsp;dodavatelem.</li>
	<li>Podrobnosti k&nbsp;uplatnění reklamace vad v&nbsp;záruční době a&nbsp;způsob vyřízení reklamace je&nbsp;popsán v&nbsp;části&nbsp;<a href="/cz/complaints/">Reklamace</a>, jejíž text je&nbsp;součástí těchto obchodních podmínek.</li>
	<li>Případné spory mezi Pelitt Group SE a Kupujícím lze řešit také mimosoudní cestou. V takovém případě Kupující – spotřebitel může kontaktovat subjekt mimosoudního řešení sporu, kterým je například Česká obchodní inspekce (<a href="http://www.coi.cz/" target="_blank">www.coi.cz</a>, se sídlem Štěpánská 567/15, 120 00 Praha 2) či spor řešit on-line prostřednictvím k tomu určené ODR platformy. Více informací o mimosoudním řešení sporů naleznete&nbsp;<a href="https://webgate.ec.europa.eu/odr/main/?event=main.about.show" target="_blank">zde</a>. Než-li bude přistoupeno k mimosoudnímu řešení sporu, pak Pelitt Group SE doporučuje Kupujícímu nejdříve využít náš&nbsp;<a href="http:///cz/contact/">kontakt</a>&nbsp;pro vyřešení nastalé situace.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>12&nbsp;PŘÍPLATKOVÉ SLUŽBY</h4>
<h1>
<ol type="a">
	<li>Změna fakturačních údajů na&nbsp;již vystavené faktuře z&nbsp;fyzické osoby na&nbsp;právnickou osobu je&nbsp;zpoplatněna částkou 300&nbsp;Kč&nbsp;bez DPH.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>13&nbsp;ZÁVĚREČNÁ USTANOVENÍ</h4>
<h1>
<ol type="a">
	<li>Tyto obchodní podmínky nabývají účinnosti dnem 1.&nbsp;5.&nbsp;2016.</li>
	<li>Dodavatel si&nbsp;vyhrazuje právo na&nbsp;změny bez předchozího upozornění.</li>
	<li>V&nbsp;případě, že&nbsp;jste nakupovali v&nbsp;tomto elektronickém internetovém obchodě v&nbsp;minulosti a&nbsp;chcete znát Obchodní podmínky platné v&nbsp;době Vaší objednávky, kontaktujte nás, a&nbsp;my&nbsp;Vám je zašleme.</li>
</ol>
 <br>
 <br>
 <br>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>