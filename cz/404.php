<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");
?>
<div class="container">
    <div class="row">
        <!-- Contact Page-->
        <div class="span12">
            <h1 class="title-without-breadcrumb">WE ARE <span>SORRY</span></h1>
        </div>
    </div>

<div class="row">
   <div class="span12"> 
		<div id="wishlist-page" data-price-format="">
		
		    <div class="no-product-found-image">
		    </div>
		
		    <div class="no-product-found-messages">
		        <div class="title"> Something went wrong! The page you were looking for cannot be found.</div>
		        <strong> What can you do about it?</strong>
		        <ul>
		            <li>
		                 Go back to the previous page.
		            </li>
		            <li>
		                 Refresh this page (try it again!).
		            </li>
		            <li>
		                 Check your URL address for errors.
		            </li>
		            <li>
		                 You can also use our <a href="/contact/">contact formular</a> and tell us about it!
		            </li>
		        </ul>
		
		        <hr>
		
		        <!-- BUTTON -->
		        <div class="main-checkout">
		        	<a class="btn bb-base-float-left" href="<?=SITE_DIR?>"> Continue shopping</a>
				</div>
		    </div>
		  </div>
        </div>
    </div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>