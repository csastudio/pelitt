<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Complaints");
?><h1>REKLAMACE</h1>
<h1><br>
 <br>
 </h1>
<h4>1&nbsp;VŠEOBECNÁ USTANOVENÍ A&nbsp;VYMEZENÍ POJMŮ</h4>
<h1>
<ol type="a">
	<li>Tento reklamační řád byl zpracován dle Občanského zákoníku a&nbsp;Spotřebitelského zákona v&nbsp;návaznosti na&nbsp;evropské předpisy, jak je&nbsp;uvedeno v&nbsp;<a href="http://dev2_pelitt.csabx.tmweb.ru/cz/terms-conditions/">Obchodních podmínkách</a>, a&nbsp;vztahuje se&nbsp;na&nbsp;spotřební zboží (dále jen „zboží“), u&nbsp;něhož jsou uplatňována práva kupujícího z&nbsp;odpovědnosti za&nbsp;vady (dále jen „reklamace“). Reklamační řád platí pro nákup v&nbsp;elektronickém internetovém obchodě&nbsp;<a href="http://www.pelitt.com">www.pelitt.</a><a href="http://www.pelitt.com">com</a>. Řád blíže vymezuje a&nbsp;upřesňuje práva a&nbsp;povinnosti jednotlivých smluvních stran, kterými jsou:</li>
	<ol type="i">
		<li>Prodávajícím je&nbsp;společnost Pelitt Group SE, IČO 03815854, se&nbsp;sídlem Na strži 1702/65,140 00&nbsp;Praha,&nbsp;Česká republika (podrobně v&nbsp;sekci&nbsp;<a href="http://dev2_pelitt.csabx.tmweb.ru/cz/contact/">Kontaktujte nás</a>),&nbsp;společnost zapsaná v&nbsp;obchodním rejstříku vedeném u&nbsp;Městského soudu v&nbsp;Praze pod spisovou značkou H&nbsp;1549.</li>
		<li>Kupujícím je&nbsp;subjekt, který s&nbsp;prodávajícím uzavřel smlouvu o&nbsp;koupi zboží.</li>
		<li>Na&nbsp;kupujícího, který není spotřebitelem, může prodávající aplikovat přiměřeně tento reklamační řád, jinak se&nbsp;práva plynoucí z&nbsp;odpovědnosti za&nbsp;vady řídí uzavřenou smlouvou a&nbsp;Občanským zákoníkem.</li>
	</ol>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>2&nbsp;ROZPOR S&nbsp;KUPNÍ SMLOUVOU</h4>
<h1>
<ol type="a">
	<li>V&nbsp;případě, že&nbsp;věc při převzetí kupujícím není ve&nbsp;shodě s&nbsp;kupní smlouvou (dále jen „rozpor s&nbsp;kupní smlouvou“), má &nbsp;kupující právo na&nbsp;to, aby prodávající bezplatně a&nbsp;bez zbytečného odkladu věc uvedl do&nbsp;stavu odpovídajícího kupní smlouvě, a&nbsp;to&nbsp;podle požadavku kupujícího buď výměnou věci nebo její opravou; není-li takový postup možný, může kupující požadovat přiměřenou slevu z&nbsp;ceny věci nebo od&nbsp;smlouvy odstoupit.</li>
	<li>Výše uvedené neplatí, pokud kupující před převzetím věci o&nbsp;rozporu s&nbsp;kupní smlouvou věděl nebo rozpor s&nbsp;kupní smlouvou sám způsobil. Rozpor s&nbsp;kupní smlouvou, který se&nbsp;projeví během šesti měsíců ode dne převzetí věci, se&nbsp;považuje za &nbsp;rozpor existující již při jejím převzetí, pokud to&nbsp;neodporuje povaze věci nebo pokud se&nbsp;neprokáže opak.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>3&nbsp;ZÁRUČNÍ PODMÍNKY</h4>
<h1>
<ol type="a">
	<li>V&nbsp;případě, že&nbsp;se&nbsp;po&nbsp;převzetí zboží kupujícím vyskytnou v&nbsp;záruční době vady zboží, může kupující uplatnit svou oprávněnou reklamaci.</li>
	<li>Délka záruční doby se&nbsp;řídí platnými ustanoveními zákona. Pokud není uvedeno výrobcem jinak (a&nbsp;jde-li o&nbsp;spotřebitele, není-li uvedena lhůta delší), činí záruční doba 24&nbsp;měsíců a&nbsp;začíná běžet dnem převzetí zboží.</li>
	<li>Prodávající odpovídá za&nbsp;vady, které má&nbsp;zboží při převzetí, a&nbsp;za&nbsp;materiálové nebo výrobní vady, které se&nbsp;vyskytnou po&nbsp;převzetí v&nbsp;záruční době.</li>
	<li>Záruku nelze uplatnit v&nbsp;následujících případech:
	<ol type="i">
		<li>Vypršela-li u&nbsp;reklamovaného zboží záruční doba přede dnem uplatnění reklamace – záruka zanikla.</li>
		<li>Vada vznikla nevhodným používáním zboží.</li>
		<li>Vada vznikla nedodržením pokynů stanovených výrobcem.</li>
		<li>Vada vznikla neodbornou instalací, zacházením, obsluhou, manipulací nebo zanedbáním péče o&nbsp;zboží.</li>
		<li>Vada vznikla provedením neoprávněného zásahu do&nbsp;zboží či&nbsp;jiných úprav bez svolení výrobce.</li>
		<li>Zboží bylo poškozeno živly.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>4&nbsp;PRÁVA PLYNOUCÍ ZE&nbsp;ZÁRUKY</h4>
<h1>
<ol type="a">
	<li>Záruční doba se&nbsp;prodlužuje o&nbsp;dobu, po&nbsp;kterou bylo zboží v&nbsp;záruční opravě. V&nbsp;případě výměny zboží získává kupující záruku novou v&nbsp;délce 24&nbsp;měsíců.</li>
	<li>Spotřebitel při uplatnění záruky má:
	<ol type="i">
		<li>jde-li o&nbsp;vadu odstranitelnou, právo na&nbsp;bezplatné, řádné a&nbsp;včasné odstranění vady, právo na&nbsp;výměnu vadného zboží nebo vadné součásti, není-li to&nbsp;vzhledem k&nbsp;povaze vady neúměrné, a&nbsp;není-li takový postup možný, právo na&nbsp;přiměřenou slevu z&nbsp;kupní ceny nebo odstoupit od&nbsp;kupní smlouvy,</li>
		<li>jde-li o&nbsp;vadu neodstranitelnou bránící řádnému užívání zboží, právo na&nbsp;výměnu vadného zboží nebo odstoupit od&nbsp;kupní smlouvy,</li>
		<li>jde-li o&nbsp;vady odstranitelné vyskytující se&nbsp;ve&nbsp;větším počtu nebo opakovaně a&nbsp;bránící řádnému užívání zboží, právo na&nbsp;výměnu vadného zboží nebo odstoupit od&nbsp;kupní smlouvy,</li>
		<li>jde-li o&nbsp;jiné vady neodstranitelné a&nbsp;nepožaduje-li výměnu věci, právo na&nbsp;přiměřenou slevu z&nbsp;kupní ceny nebo odstoupit od&nbsp;kupní smlouvy.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>5&nbsp;VYŘÍZENÍ REKLAMACE</h4>
<h1>
<ol type="a">
	<li>Místem uplatnění reklamace je&nbsp;provozovna technického partnera (výrobce) nebo provozovna prodávajícího.</li>
	<li>Reklamace se&nbsp;uplatňují na&nbsp;adrese:</li>
 <address>Pelitt Group SE<br>
	 U Vodárny 3032/2a<br>
	 616 00 Brno<br>
	 Česká republika<br>
 </address><br>
	<p>
 <a href="http://static-flamefox-catalog.bizboxlive.com/__files/FlameFox-Catalog/Reklama%C4%8Dn%C3%AD%20formul%C3%A1%C5%99_Pelitt.pdf?497e9" target="_blank">Stáhnout reklamační formulář</a>&nbsp;(česky)
	</p>
	<li>Rozhodne-li se&nbsp;kupující reklamovat zboží osobně v&nbsp;provozovně technického partnera nebo prodávajícího, doporučujeme, aby si&nbsp;domluvil předem termín návštěvy e-mailem nebo telefonicky.</li>
	<li>Jako komunikační kanál pro reklamace využijte&nbsp;<a href="http://dev2_pelitt.csabx.tmweb.ru/cz/contact/">kontaktní formulář</a>&nbsp;nebo zašlete e-mail na adresu&nbsp;<a href="mailto:support@pelitt.com">support@</a><a href="mailto:support@pelitt.com">pelitt.com</a>.</li>
	<li>Za&nbsp;situace, kdy prodávající rozhodl o&nbsp;zaslání zboží zpět k&nbsp;sobě, si&nbsp;kupující ve&nbsp;vlastním zájmu vede tak, aby bylo zboží zabaleno do&nbsp;vhodného a&nbsp;dostatečně chránícího obalového materiálu, vyhovujícího nárokům přepravy křehkého zboží, a&nbsp;označí zásilku příslušnými symboly.</li>
	<li>Pracovníci technického partnera nebo provozovny po&nbsp;řádném vyřízení reklamace vyzvou kupujícího k&nbsp;převzetí opraveného zboží.</li>
	<li>Vždy je&nbsp;třeba písemného vyhotovení protokolu o&nbsp;zjištěných závadách a&nbsp;formě jejich odstranění, doporučujeme kupujícímu si&nbsp;tato potvrzení uchovávat po&nbsp;dobu platnosti záruky.</li>
	<li>V&nbsp;případě oprávněné reklamace má&nbsp;zákazník právo na&nbsp;náhradu poštovného v&nbsp;adekvátní výši.</li>
	<li>V&nbsp;případě neoprávněné reklamace nemá spotřebitel nárok na&nbsp;náhradu svých nákladů spojených s&nbsp;vyřízením reklamace a&nbsp;současně ani prodejce nemá nárok na&nbsp;náhradu nákladů, které vznikly na&nbsp;jeho straně (pokud se&nbsp;ze&nbsp;strany spotřebitele nejednalo např. o&nbsp;opakovanou bezdůvodnou reklamaci, u&nbsp;které se&nbsp;již dá&nbsp;dovozovat, že&nbsp;se &nbsp;z&nbsp;jeho strany jednalo o&nbsp;zneužití práv).</li>
	<li>O&nbsp;reklamaci prodávající nebo technický partner rozhodne ihned, ve&nbsp;složitých případech do&nbsp; tří pracovních dnů. Do &nbsp;této lhůty se&nbsp;nezapočítává doba přiměřená podle druhu výrobku či&nbsp;služby potřebná k&nbsp;odbornému posouzení vady. Reklamace včetně vady bude vyřízena bez zbytečného odkladu, nejpozději do&nbsp;30&nbsp;kalendářních dnů ode dne uplatnění reklamace, pokud se&nbsp;prodávající s&nbsp;kupujícím nedohodne jinak.</li>
	<li>Prodávající vydá spotřebiteli písemné potvrzení o&nbsp;tom, kdy spotřebitel uplatnil reklamaci, co&nbsp;je&nbsp;jejím obsahem a&nbsp;jaký způsob jejího vyřízení požaduje. Dále prodávající vydá spotřebiteli potvrzení o&nbsp;datu a&nbsp;způsobu vyřízení reklamace, včetně potvrzení o&nbsp;provedení opravy a&nbsp;době jejího trvání. Pro případ zamítnuté reklamace prodávající spotřebiteli vydá písemné odůvodnění tohoto zamítnutí.</li>
	<li>Skladné a&nbsp;náklady spojené s&nbsp;neoprávněnou reklamací: Při nevyzvednutí reklamace do&nbsp;30&nbsp;dnů po&nbsp;termínu ukončení opravy nebo při nevyzvednutí produktu v&nbsp;případě neoprávněné reklamace může být kupujícímu účtováno skladné ve&nbsp;výši 30&nbsp;Kč&nbsp;bez DPH za&nbsp;každý započatý kalendářní den. Prodejce je&nbsp;oprávněn produkt nevydat, dokud nebude uhrazena celá částka za&nbsp;opravu, případně skladné. V&nbsp;případě, že&nbsp;tato částka převýší aktuální prodejní cenu produktu, je&nbsp;prodejce oprávněn produkt prodat, a&nbsp;vzniklé náklady takto pokrýt.</li>
</ol>
 <br>
 <br>
 <br>
 <br>
 </h1>
<h4>6&nbsp;ZÁVĚREČNÁ USTANOVENÍ</h4>
<h1>
<ol type="a">
	<li>Tento reklamační řád nabývá účinnosti 1.&nbsp;5.&nbsp;2014.</li>
	<li>Změny reklamačního řádu vyhrazeny, reklamační řád je&nbsp;součástí obchodních podmínek prodejce.</li>
</ol>
 <br>
 <br>
 <br>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>