<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Privacy-policy");
?><h1>OCHRANA OSOBNÍCH ÚDAJŮ</h1>
<h1><br>
 <br>
<p>
	 Informace o&nbsp;zákaznících jsou uchovávány v&nbsp;souladu s&nbsp;platnými zákony České republiky, zejména se&nbsp;zákonem &nbsp;č.&nbsp;101/2000&nbsp;Sb., o&nbsp;ochraně osobních údajů, ve&nbsp;znění pozdějších dodatků a&nbsp;předpisů. Kupující uzavřením smlouvy&nbsp;souhlasí se&nbsp;zpracováním a&nbsp;shromažďováním svých osobních údajů&nbsp;v&nbsp;databázi prodávajícího po úspěšném splnění smlouvy, a&nbsp;to&nbsp;až&nbsp;do&nbsp;doby jeho písemného vyjádření nesouhlasu s&nbsp;tímto zpracováním.
</p>
<p>
	 Kupující má&nbsp;právo&nbsp;přístupu ke&nbsp;svým osobním údajům&nbsp;a právo na&nbsp;jejich opravu&nbsp;včetně dalších zákonných práv k těmto údajům. Osobní údaje je&nbsp;možno na&nbsp;základě písemné žádosti zákazníka odstranit z&nbsp;databáze. Osobní údaje zákazníků jsou plně zabezpečeny proti zneužití. Osobní údaje zákazníků provozovatel nepředává žádné další osobě. Výjimku představují externí dopravci a&nbsp;poskytovatelé platebních bran, kterým jsou osobní údaje zákazníků předávány v minimálním rozsahu, jenž je&nbsp;nutný pro doručení zboží nebo autorizaci příslušného platebního nástroje.
</p>
<p>
	 Jednotlivé smlouvy jsou po&nbsp;svém uzavření provozovatelem archivovány, a&nbsp;to&nbsp;ve&nbsp;formě elektronické, a&nbsp;jsou přístupné pouze provozovateli elektronického obchodu.
</p>
<p>
	<br>
</p>
<p>
	 Pelitt Group SE<br>
	 Na strži 1702/65<br>
	 140 00 Praha<br>
	 Česká republika
</p>
<p>
	<br>
</p>
<p>
	 Písemný nesouhlas se&nbsp;zpracováním a&nbsp;udržováním osobních údajů o&nbsp;zákazníkovi&nbsp;je&nbsp;možné projevit na&nbsp;adrese provozovatele elektronického obchodu, společnosti Pelitt Group SE, uvedené výše
</p>
 <br>
 <br>
 </h1>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>