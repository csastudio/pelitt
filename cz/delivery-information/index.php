<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Delivery-information");
?><h1>DODACÍ&nbsp;PODMÍNKY</h1>
 <br>
<p>
</p>
 Cena a&nbsp;způsob dopravy veškerého zboží zakoupeného v&nbsp;elektronickém obchodě produktů Pelitt Group SE je&nbsp;rozdělena podle cílové země dodání uvedené v&nbsp;objednávce: &nbsp; <br>
 <br>
<h2>ČESKÁ REPUBLIKA<br>
 </h2>
 Zboží je v doručováno nejpozději do 2 pracovních dnů. Zboží je v České republice doručováno kurýrní společností Geis. Zboží si lze také nechat doručit na některé z výdejních míst&nbsp;Geis Point. <br>
 <br>
<h3>JAK PROBÍHÁ DORUČENÍ PROSTŘEDNICTVÍM KURÝRNÍ SLUŽBY?</h3>
 Kurýrní služba Vás předem vyrozumí na&nbsp;telefonním čísle ve&nbsp;Vaší objednávce o&nbsp;termínu dodání. Bez telefonického spojení dosažitelného přes den nebude Vaše objednávka vyřízena. <br>
 Zboží bude doručeno na&nbsp;Vaši adresu najatým dopravcem nebo kurýrní službou. Takto expedované zboží je&nbsp;doručováno před první vchodové dveře. Řidič zpravidla jezdí bez závozníka, proto není schopen zboží stěhovat po&nbsp;domě či&nbsp;patrech. <br>
 O&nbsp;čase doručení budete informováni pomocí smluveného avíza v&nbsp;SMS nebo na&nbsp;e-mail na&nbsp;údaje uvedené v&nbsp;objednávce v&nbsp;předstihu. <br>
 Objednáváte-li si&nbsp;objednávku na&nbsp;dobírku, musíte celou částku uhradit kurýrovi v&nbsp;okamžiku doručení. <br>
 Kupující má&nbsp;povinnost zboží řádně převzít, zkontrolovat neporušenost obalů, počet balíků a&nbsp;v&nbsp;případě jakýchkoliv závad toto neprodleně oznámit přepravci a&nbsp;učinit zápis o&nbsp;poškození na&nbsp;dodací list. Podpisem dodacího listu kupující stvrzuje, že&nbsp;zásilka splňovala všechny podmínky a&nbsp;náležitosti výše uvedené, a&nbsp;na&nbsp;pozdější reklamaci o&nbsp;porušenosti obalu zásilky nebude brán zřetel. Faktura slouží zároveň i&nbsp;jako daňový doklad. <br>
 Kupující je&nbsp;povinen dodané zboží bez odkladu prohlédnout a&nbsp;v&nbsp;případě nekompletnosti zboží nahlásit do&nbsp; tří pracovních dnů ode dne doručení. Pozdější reklamace nebudou vyřízeny. <br>
 Nebude-li kupující schopen dané zboží převzít ve&nbsp;stanoveném termínu, bude kurýrní služba znovu opakovat pokus o&nbsp;doručení včetně telefonického spojení za&nbsp;účelem sjednání nového termínu. <br>
 Nebude-li opakovaně kupující schopen dané zboží převzít, je&nbsp;kupující povinen uhradit prodávajícímu škodu vzniklou tímto jednáním, tzn. náklady spojené s&nbsp;přepravou. <br>
 Odmítne-li kupující převzít zboží při dodávce, nebo nebude-li k&nbsp;zastižení na&nbsp;kontaktní adrese, bude prodávající požadovat náklady spojené s&nbsp;dodáním zboží.<br>
 <br>
<h2>
SLOVENSKÁ REPUBLIKA</h2>
 <br>
<h4>
<p>
	 Zboží je v doručováno nejpozději do 3 pracovních dnů. Zboží je do Slovenské republiky doručováno kurýrní společností Geis.
</p>
 <br>
 </h4>
<h3>JAK PROBÍHÁ DORUČENÍ PROSTŘEDNICTVÍM KURÝRNÍ SLUŽBY?</h3>
<h4>
<ul>
	<li>Kurýrní služba Vás předem vyrozumí na&nbsp;telefonním čísle ve&nbsp;Vaší objednávce o&nbsp;termínu dodání. Bez telefonického spojení dosažitelného přes den nebude Vaše objednávka vyřízena.</li>
	<li>Zboží bude doručeno na&nbsp;Vaši adresu najatým dopravcem nebo kurýrní službou. Takto expedované zboží je&nbsp;doručováno před první vchodové dveře. Řidič zpravidla jezdí bez závozníka, proto není schopen zboží stěhovat po&nbsp;domě či&nbsp;patrech.</li>
	<li>O&nbsp;čase doručení budete informováni pomocí smluveného avíza v&nbsp;SMS nebo na&nbsp;e-mail na&nbsp;údaje uvedené v&nbsp;objednávce v&nbsp;předstihu.</li>
	<li>Objednáváte-li si&nbsp;objednávku na&nbsp;dobírku, musíte celou částku uhradit kurýrovi v&nbsp;okamžiku doručení.</li>
	<li>Kupující má&nbsp;povinnost zboží řádně převzít, zkontrolovat neporušenost obalů, počet balíků a&nbsp;v&nbsp;případě jakýchkoliv závad toto neprodleně oznámit přepravci a&nbsp;učinit zápis o&nbsp;poškození na&nbsp;dodací list. Podpisem dodacího listu kupující stvrzuje, že&nbsp;zásilka splňovala všechny podmínky a&nbsp;náležitosti výše uvedené, a&nbsp;na&nbsp;pozdější reklamaci o&nbsp;porušenosti obalu zásilky nebude brán zřetel. Faktura slouží zároveň i&nbsp;jako daňový doklad.</li>
	<li>Kupující je&nbsp;povinen dodané zboží bez odkladu prohlédnout a&nbsp;v&nbsp;případě nekompletnosti zboží nahlásit do&nbsp; tří pracovních dnů ode dne doručení. Pozdější reklamace nebudou vyřízeny.</li>
	<li>Nebude-li kupující schopen dané zboží převzít ve&nbsp;stanoveném termínu, bude kurýrní služba znovu opakovat pokus o&nbsp;doručení včetně telefonického spojení za&nbsp;účelem sjednání nového termínu.</li>
	<li>Nebude-li opakovaně kupující schopen dané zboží převzít, je&nbsp;kupující povinen uhradit prodávajícímu škodu vzniklou tímto jednáním, tzn. náklady spojené s&nbsp;přepravou.</li>
	<li>Odmítne-li kupující převzít zboží při dodávce, nebo nebude-li k&nbsp;zastižení na&nbsp;kontaktní adrese, bude prodávající požadovat náklady spojené s&nbsp;dodáním zboží.</li>
</ul>
 <br>
 </h4><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>