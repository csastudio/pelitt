<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Returns");
?><h1>VRÁCENÍ ZBOŽÍ</h1>
<h1><br>
 <br>
<p>
	 Spotřebitel má&nbsp;právo dle evropské směrnice 97/7/ES a&nbsp;dle Občanského zákoníku České republiky odstoupit od&nbsp;kupní smlouvy&nbsp;ve lhůtě 14&nbsp;dní od&nbsp;převzetí zboží, pokud byla smlouva uzavřena pomocí prostředků komunikace na&nbsp;dálku, tedy zejména pomocí elektronického obchodu. Toto právo neslouží primárně jako způsob řešení reklamace zboží, vykazuje-li dodané zboží vadu, postupuje kupující ve&nbsp;vlastním zájmu dle reklamačního řádu.
</p>
<p>
	 Lhůta 14&nbsp;dnů je&nbsp;stanovena jako příznivější lhůta na&nbsp;základě spotřebitelské ochrany v&nbsp;zemi prodejce a&nbsp;zemi kupujícího pro kupující ze zemí Evropské Unie.
</p>
<p>
	 Rozhodne-li se&nbsp;spotřebitel využít výše uvedeného práva, musí odstoupení od&nbsp;smlouvy doručit nejpozději do&nbsp;14. dne od&nbsp;převzetí věci. Doporučujeme tak učinit písemnou formou, která zajišťuje zejména prokazatelnost učiněného právního úkonu, kterým se smlouva ruší.
</p>
<p>
	 Žádáme Vás současně o&nbsp;uvedení čísla daňového dokladu, data nákupu a&nbsp;čísla Vašeho bankovního účtu.
</p>
<p>
	 Odstoupení od&nbsp;smlouvy je&nbsp;účinné jeho doručením v&nbsp;zákonné lhůtě. Vracené zboží doporučujeme pojistit. Při splnění zákonných podmínek pro odstoupení od&nbsp;smlouvy bude kupujícímu zaslán dobropis s&nbsp;částkou odpovídající kupní ceně zboží sníženou pouze o&nbsp;skutečně vynaložené náklady prodávajícího spojené s&nbsp;vrácením zboží. Dobropis kupující podepíše v&nbsp;rámci zákona o&nbsp;DPH v&nbsp;České republice a evropské směrnice o&nbsp;DPH a&nbsp;zašle zpět prodávajícímu.&nbsp;Na&nbsp;základě podepsaného dobropisu bude spotřebiteli vrácena částka za&nbsp;zboží na jeho účet&nbsp;nejpozději do&nbsp;třiceti dnů od&nbsp;odstoupení od&nbsp;smlouvy.
</p>
<p>
	 Podmínky pro možnost vracení zboží:
</p>
<ul>
	<li>zboží musí být vráceno kompletní a&nbsp;v&nbsp;původním stavu, pokud možno včetně originálních obalů,</li>
	<li>zboží ani obal nesmějí být jakkoliv poškozeny (vyjma běžného porušení obalu způsobeného vyjmutím zabaleného zboží),</li>
	<li>zboží nesmí jevit známky opotřebení a&nbsp;používání.</li>
</ul>
<p>
	 Při nesplnění uvedených podmínek může prodávající vůči spotřebiteli uplatnit nároky plynoucí z&nbsp;jeho bezdůvodného obohacení spočívající ve&nbsp;snížení hodnoty vraceného zboží.
</p>
<p>
	 Nezasílejte zboží zpět na&nbsp;dobírku. Zásilka bude odmítnuta.
</p>
<p>
	 Vracené zboží zašlete poštou na adresu:
</p>
 <address>Pelitt Group SE<br>
 U Vodárny 3032/2a<br>
 616 00 Brno<br>
 Česká republika<br>
 </address><br>
<p>
 <a href="http://static-flamefox-catalog.bizboxlive.com/__files/FlameFox-Catalog/Vzorov%C3%BD%20formul%C3%A1%C5%99%20pro%20vr%C3%A1cen%C3%AD%20zbo%C5%BE%C3%AD%20%28Pelitt%20Group%20SE%29.pdf?4e941" target="_blank">Stáhnout formulář pro vrácení zboží</a>&nbsp;(česky)
</p>
<p>
	 Po&nbsp;fyzickém obdržení a&nbsp;kontrole zboží je&nbsp;vystaven kupujícímu dobropis. Navrácena je&nbsp;zaplacená kupní cena, náklady na&nbsp;přepravu se&nbsp;nevrací. Prodávající má&nbsp;právo na&nbsp;náhradu skutečně vynaložených nákladu spojených s&nbsp;vrácením zboží. Vrácená suma bude snížena o opotřebování a poškození zboží.
</p>
<p>
	 Berte prosím na&nbsp;vědomí, že&nbsp;tato lhůta je&nbsp;určena k&nbsp;tomu, aby se&nbsp;spotřebitel mohl dostatečně seznámit s&nbsp;vlastnostmi zakoupeného zboží, a&nbsp;nelze ji&nbsp;chápat jako právo na&nbsp;výpůjčku zboží.
</p>
 <br>
 <br>
 </h1><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>