<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Delivery-information");
?><h1><span style="color: #ea512f;">DELIVERY</span>&nbsp;INFORMATION</h1>
 <br>
 <br>
<p>
	 The price of delivery is for all orders free. The method of delivery of all goods purchased in e-shop selling products by Pelitt Group SE is divided by the country of delivery as stated in the order:
</p>
<p>
	<br>
</p>
<h4>CZECH REPUBLIC</h4>
<p>
	 Goods are delivered within 2 working days. In the Czech Republic deliveries are made by courier company Geis. We can also deliver in dispensing sites of Geis called&nbsp;<a href="http://geispoint.cz/?#ss_vydejnimista">Geis Point</a>.
</p>
<h5>DELIVERY BY COURIER</h5>
<ul>
	<li>The courier will send you a delivery alert in advance on the phone number stated in your order. Without a phone number where you can be reached during the day your order will not be processed.</li>
	<li>The goods will be delivered to your address by a carrier or a courier. Thus dispatched goods are delivered in front of the first entrance door. The driver usually has no co-driver, therefore, he is unable to move goods inside the house.</li>
	<li>You will receive a text message/e-mail alert informing you of the time of delivery in advance.</li>
	<li>If you order cash on delivery, you need to pay the whole amount to the courier on delivery.</li>
	<li>The Purchaser is obliged to properly receive the goods, inspect intactness of packaging and number of parcels, and in case of any defects report this to the deliver's branch office and make a record of the defect on the cetificate of parcel delivery. The Purchaser signs the certificate and confirms that the parcel complied with all terms and conditions stated above, and that later complaints about damaged packaging will be ignored. The invoice also serves as a tax document.</li>
	<li>The Purchaser is obliged to immediately inspect the delivered goods and in case of incomplete consignment report this to the Provider no later than three working days on delivery. Later complaints will be ignored.</li>
	<li>If the Purchaser is unable to receive the ordered goods within the fixed period, the courier will contact him again to agree on time of repeat delivery.</li>
	<li>If the Purchaser is repeatedly unable to receive the goods, he is obligated to cover the Merchant's incurred expenses, i.e. transport expenses.</li>
	<li>If the Purchaser refuses to receive the goods on delivery or if he failes to be reached on the contact address, the Merchant will claim the expenses for delivery of the goods.</li>
</ul>
 <br>
 <br>
 <br>
<h4>SLOVAK REPUBLIC</h4>
<p>
	 Goods are delivered within 3 working days. Delivery to the Slovak Republic is made by the courier company Geis.
</p>
<h5>DELIVERY BY COURIER</h5>
<ul>
	<li>The courier will send you a delivery alert in advance on the phone number stated in your order. Without a phone number where you can be reached during the day your order will not be processed.</li>
	<li>The goods will be delivered to your address by a carrier or a courier. Thus dispatched goods are delivered in front of the first entrance door. The driver usually has no co-driver, therefore, he is unable to move goods inside the house.</li>
	<li>You will receive a text message/e-mail alert informing you of the time of delivery in advance.</li>
	<li>If you order cash on delivery, you need to pay the whole amount to the courier on delivery.</li>
	<li>The Purchaser is obliged to properly receive the goods, inspect intactness of packaging and number of parcels, and in case of any defects report this to the deliver's branch office and make a record of the defect on the cetificate of parcel delivery. The Purchaser signs the certificate and confirms that the parcel complied with all terms and conditions stated above, and that later complaints about damaged packaging will be ignored. The invoice also serves as a tax document.</li>
	<li>The Purchaser is obliged to immediately inspect the delivered goods and in case of incomplete consignment report this to the Provider no later than three working days on delivery. Later complaints will be ignored.</li>
	<li>If the Purchaser is unable to receive the ordered goods within the fixed period, the courier will contact him again to agree on time of repeat delivery.</li>
	<li>If the Purchaser is repeatedly unable to receive the goods, he is obligated to cover the Merchant's incurred expenses, i.e. transport expenses.</li>
	<li>If the Purchaser refuses to receive the goods on delivery or if he failes to be reached on the contact address, the Merchant will claim the expenses for delivery of the goods.</li>
</ul>
 <br>
 <br>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>