<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Returns");
?><h1>ZWROTY</h1>
 <br>
 <br>
<p>
	 Zgodnie z Dyrektywą Unii Europejskiej 97/7/ES oraz kodeksem cywilnym Czech, Kupujący może odstąpić od Umowy <b>w ciągu 14 dni od dnia odbioru towaru</b> w przypadku zawarcia Umowy na odległość, w tym w szczególności za pośrednictwem sklepu internetowego. Powyższe prawo nie może być jednakże rozumiane zwłaszcza jako sposób załatwiania reklamacji. W przypadku, gdy produkt okaże się wadliwy, w interesie Kupującego leży aby stosować się do Zasad dotyczących Reklamacji.
</p>
<p>
	 Ustalony powyższy 14-dniowy okres jest korzystniejszy w stosunku do ochrony konsumentów obowiązującej w kraju Sprzedawcy i Kupującego w przypadku odbiorców pochodzących z krajów UE.
</p>
<p>
	 W przypadku, gdy Kupujący postanowi skorzystać z powyższego prawa, będzie on zobowiązany doręczyć oświadczenie o odstąpieniu od Umowy najpóźniej 14-go dnia od odbioru towaru. Rekomendujemy, aby oświadczenie to miało formę pisemną, która umożliwi weryfikację aktu prawnego rozwiązującego Umowę.
</p>
<p>
	 Prosimy ponadto o podanie numeru dokumentu podatkowego, datę zakupu oraz numer Państwa rachunku bankowego.
</p>
<p>
	 Odstąpienie od Umowy staje się skuteczne z momentem jego doręczenia w formie pisemnej w ustawowo określonym terminie. Zalecamy, aby zwracany towar został przez Państwa ubezpieczony. Zakładając, że dotrzymane zostaną wynikające z przepisów prawa terminy odstąpienia od Umowy, Kupującemu zostanie przesłana nota uznaniową na kwotę odpowiadającą cenie zakupu towaru pomniejszoną wyłącznie o faktycznie poniesione przez Sprzedawcę koszty związane ze zwrotem towaru. Kupujący będzie zobowiązany podpisać notę uznaniową zgodnie z obowiązującą w Czechach Ustawą o VAT oraz Dyrektywą Unii Europejskiej dotyczącą VAT, a następnie odesłać ją Sprzedawcy. <b>Na podstawie podpisanej noty uznaniowej, Odbiorca otrzyma zwrot za towar na swoje konto bankowe</b> najpóźniej w ciągu trzydziestu dni od dnia odstąpienia od Umowy.
</p>
<p>
 <strong>Warunki zwrotu towaru:</strong>
</p>
<ul>
	<li>zwracany towar musi być kompletny i oryginalny, w miarę możliwości opakowany w oryginalne opakowanie,</li>
	<li>ani towar, ani opakowanie nie może być w żaden sposób uszkodzone, z wyjątkiem normalnego uszkodzenia opakowania powstałego podczas wyjmowania towaru,</li>
	<li>towar nie może nosić oznak użycia.</li>
</ul>
<p>
	 Niedotrzymanie powyższych warunków może skutkować dochodzeniem przez Sprzedawcę praw wobec Odbiorcy wynikających z nieusprawiedliwionego wzbogacenia się poprzez obniżenie ceny zwracanych towarów.
</p>
<p>
	 Proszę nie odsyłać towaru za pobraniem. Paczka w tym przypadku nie zostanie przyjęta.
</p>
 <strong>Zwroty towaru proszę kierować na następujący adres:</strong> <address style="background: #e8e8e8;"><strong>Pelitt Group SE</strong><br>
 U Vodárny 3032/2a<br>
 616 00 Brno<br>
 Czech Republic<br>
 </address><br>
<p>
 <strong><a href="/Reklamační formulář_Pelitt.pdf" target="_blank">Pobierz formularz zwrotu</a>&nbsp;(wersja czeska)</strong>
</p>
<p>
	 Po zwrocie i sprawdzeniu towaru, Sprzedawca wystawia notę uznaniową. Kupującemu zwrócona zostaje cena zakupu. Koszty transportu nie podlegają zwrotowi. Sprzedawcy przysługuje zwrot kosztów poniesionych w związku ze zwrotem towaru. Zwrot kosztów zostanie pomniejszony o wartość zużycia towaru.
</p>
<p>
	<strong>Zwracamy uwagę, że ten okres czasu ma służyć Odbiorcy do odpowiedniego zapoznania się z właściwościami zakupionego towaru i nie jest okresem, na który towar zostaje Odbiorcy wypożyczony.</strong>
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>