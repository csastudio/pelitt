<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Complaints");
?><h1>REKLAMACJE</h1>
 <br>
 <br>
<h4>1. POSTANOWIENIA OGÓLNE. DEFINICJE POJĘĆ.</h4>
<ol type="a">
	<li>Niniejsze Zasady dotyczące Reklamacji zostały opracowane zgodnie z Kodeksem Cywilnym oraz Ustawą o Ochronie Konsumentów w odniesieniu do regulacji Unii Europejskiej, które zostały określone w&nbsp;<a href="/pl/terms-conditions">Warunkach korzystania</a>. Niniejsze Zasady dotyczą towarów konsumpcyjnych (zwanych w dalszej części Towarami), co do których Kupującemu przysługują prawa wynikające z odpowiedzialności za wady produktów (dalej jako Reklamacje). Powyższe reklamacje obowiązują w przypadku zakupów dokonanych w sklepie internetowym&nbsp;<a href="http://www.pelitt.com/">www.pelitt.com</a>.&nbsp;W dalszej części warunki zostały zdefiniowane oraz określone prawami i obowiązkami stron umowny, którymi są:</li>
	<ol type="i">
		<li>Sprzedawca, którym jest spółka&nbsp;<strong>Pelitt Group SE</strong>, IČO 03815854, z siedzibą w&nbsp;Na Strži 1702/65, 140 00 Praha 4, Czechy&nbsp;(szczegółowe informacje w części&nbsp;Kontakt), spółka wpisana do Rejestru Spółek przez Sąd Grodzki w Pradze pod numerem H 1549,</li>
		<li>Kupujący jako podmiot, który zawarł Umowę Kupna ze Sprzedawcą.</li>
		<li>Sprzedawca może odpowiednio stosować niniejsze Zasady dotyczące Reklamacji do Kupującego nie będącego Odbiorcą. W przeciwnym razie, prawa wynikające z odpowiedzialności za wady produktu będą wynikały z zawartej Umowy lub Kodeksu Cywilnego.</li>
	</ol>
</ol>
 <br>
 <br>
<h4>2. NIEZGODNOŚĆ Z UMOWĄ KUPNA</h4>
<ol type="a">
	<li>W przypadku, gdy stan produktu w momencie odbioru będzie niezgodny z Umową Kupna (dalej jako Niezgodność z Umową Kupna), Kupujący będzie mógł żądać, by Sprzedawca nieodpłatnie i bezzwłocznie naprawił zaistniałą sytuację poprzez albo wymianę, albo poprzez naprawę produktu, w zależności od życzenia Kupującego. Jeżeli wymiana albo naprawa nie będą możliwa, Kupujący będzie mógł zażądać odpowiedniego upustu albo odstąpić od Umowy.</li>
	<li>Powyższe nie dotyczy sytuacji, gdy Kupujący wiedział o niezgodności z Umową Kupna przed odbiorem towaru lub jeżeli niezgodność ta została spowodowana przez samego Kupującego. Niezgodność z Umową Kupna wykryta w ciągu sześciu miesięcy po odbiorze towaru będzie uznawana za istniejącą w momencie odbioru, chyba że rodzaj produktów ją wyklucza lub w inny sposób zostanie udowodnione, że była ona niemożliwa.</li>
</ol>
<h4><br>
 </h4>
 <br>
<h4>3. WARUNKI GWARANCJI</h4>
<ol type="a">
	<li>Jeżeli po odbiorze, w okresie obowiązywania gwarancji Kupujący wykryje wady w produktach, będzie on mógł dochodzić przysługujących mu praw z tytułu gwarancji.</li>
	<li>Okres gwarancji wynika z obowiązujących przepisów prawa. O ile Producent nie poda odmiennie, bądź w przypadku Odbiorcy, o ile okres gwarancji nie będzie dłuższy, okres gwarancji wynosi 24 miesiące i rozpoczyna bieg z momentem odbioru towaru.</li>
	<li>Sprzedawca ponosi odpowiedzialność za wszelkie wady obecne przy odbiorze, a także za wszelkie wady materiałowe oraz błędy produkcyjne, jakie pojawią się po odbiorze w okresie obowiązywania gwarancji.</li>
	<li>Gwarancja nie obowiązuje w następujących przypadkach:<br>
	<ol type="a">
		<ol type="i">
			<li>Jeżeli okres gwarancji minie zanim złożona zostanie reklamacja objęta gwarancją, gwarancja przestanie obowiązywać.</li>
			<li>Wada będzie następstwem niewłaściwego użytkowania produktu.</li>
			<li>Wada będzie następstwem niestosowania się do instrukcji Producenta.</li>
			<li>Wada będzie następstwem nieprofesjonalnej instalacji, obsługi, eksploatacji, manewrowania czy zaniedbania konserwacji produktu.</li>
			<li>Wada będzie następstwem nieupoważnionej ingerencji w towar lub wprowadzania innego rodzaju zmian, na które Producent nie wyraził zgody.</li>
			<li>Towar zostanie zniszczony przez klęskę żywiołową.</li>
		</ol>
	</ol>
 </li>
</ol>
 <br>
 <br>
<h4>4. PRAWA WYNIKAJĄCE Z GWARANCJI</h4>
<ol type="a">
	<li>Okres gwarancji zostaje przedłużony o okres, przez jaki towar będzie poddawany naprawie gwarancyjnej. W przypadku wymiany towaru, Kupującemu udzielona zostaje gwarancja na nowy okres 24 miesięcy.</li>
	<li>Dochodząc przysługujących praw wynikających z gwarancji, Odbiorcy przysługuje:<br>
	<ol type="a">
		<ol type="i">
			<li>w przypadku usterki możliwej do usunięcia, jeżeli Pan/i zażąda właściwego, terminowego, nieodpłatnego usunięcia usterki, wymiany wadliwego towaru lub wadliwej części, chyba że nie będzie to właściwe zważywszy na rodzaj usterki i chyba że powyższe działania nie będą możliwe, jeżeli zażąda Pan/i obniżenia ceny zakupu w uzasadnionej wysokości lub będzie chciał/a odstąpić od Umowy Kupna,</li>
			<li>w przypadku usterki niemożliwej do usunięcia, która uniemożliwia korzystanie z towaru w sposób zgodny z jego przeznaczeniem, jeżeli zażąda Pan/i wymiany wadliwego towaru lub będzie chciał/a odstąpić od Umowy Kupna,</li>
			<li>w przypadku możliwych do usunięcia kilku lub powtarzających się usterek, które uniemożliwiają korzystanie z towaru w sposób zgodny z jego przeznaczeniem, jeżeli zażąda Pan/i wymiany wadliwego towaru lub będzie chciał/a odstąpić od Umowy Kupna,</li>
			<li>w przypadku innego rodzaju usterek niemożliwych do usunięcia i jeżeli nie będzie Pan/i żądać wymiany towaru, jeżeli zażąda Pan/i obniżenia ceny zakupu w uzasadnionej wysokości lub będzie chciał/a odstąpić od Umowy Kupna.<br>
 </li>
		</ol>
	</ol>
 </li>
</ol>
 <br>
 <br>
<h4>5. ROZPATRYWANIE REKLAMACJI</h4>
<ol type="a">
	<li>Miejscem składania reklamacji jest przedsiębiorstwo Partnerów Technicznych (Producenta) lub przedsiębiorstwo Sprzedawcy.</li>
	<li>Reklamację proszę składać na następujący&nbsp;adres:</li>
 <address style="background: #e8e8e8;"><strong>Pelitt Group SE</strong><br>
	 U Vodárny 3032/2a<br>
	 616 00 Brno<br>
	 Czech Republic<br>
 </address><br>
	<p>
 <strong><a href="http://pelitt.com/complaints/FORMULARZ%20ZG%C5%81OSZENIA%20REKLAMACYJNEGO-Pelitt.pdf" target="_blank">Pobierz&nbsp;formularz reklamacyjny</a></strong>
	</p>
	<li>W przypadku składania przez Kupującego reklamacji osobiście w przedsiębiorstwie Partnera Technicznego lub Sprzedawcy, rekomendujemy wcześniejsze umówienie się mailem lub telefonicznie.</li>
	<li>W przypadku zwrotów i innych związanych z nimi spraw&nbsp;prosimy o skorzystanie z&nbsp;<a href="http://pelitt.com/contact/">formularza kontaktoweg</a>o&nbsp;lub przesłanie zgłoszenia na adres:&nbsp;<a href="mailto:serwis@pelitt.com">serwis@pelitt.com</a>.</li>
	<li>W przypadku decyzji Sprzedawcy o zwrocie towaru do niego, w interesie Kupującego leży by upewnić się, że towar jest odpowiednio zapakowany w odpowiednie opakowanie ochronne spełniające potrzeby towarów wrażliwych w transporcie oraz oznaczone odpowiednimi symbolami.</li>
	<li>Gdy&nbsp;tylko reklamacja zostanie właściwie załatwiona, personel Partnera Technicznego lub oddziału zaprosi Kupującego do odbioru naprawionego towaru.</li>
	<li>Konieczne jest, by wykryte wady oraz sposób ich usuwania zostały za każdym razem zaprotokołowane. Stanowczo rekomendujemy, by Kupujący przechowywał ten protokół przez cały okres obowiązywania gwarancji.</li>
	<li>W przypadku uznanej reklamacji&nbsp;Odbiorcy będzie przysługiwała odpowiednia rekompensata za koszty przesyłki.</li>
	<li>W przypadku odrzucenia reklamacji, Odbiorca nie będzie mógł dochodzić zwrotu poniesionych przez siebie kosztów związanych z procesem reklamacyjnym; podobnie Sprzedawca nie będzie mógł dochodzić poniesionych przez siebie kosztów, chyba że Kupujący złoży kolejną nieuzasadnioną reklamację, w którym to przypadku można zakładać nadużycie prawa.</li>
	<li>Sprzedawca lub Partner Techniczny zobowiązani są podjąć decyzję w prawie reklamacji w trybie natychmiastowym, zaś w&nbsp; bardziej skomplikowanych przypadkach – w ciągu trzech dni roboczych. Powyższy okres nie obejmuje czasu potrzebnego do przeprowadzenia analizy technicznej wady, która będzie adekwatna do rodzaju produktu lub usługi. Reklamacja oraz wada będą musiały zostać załatwione bezzwłocznie, przy czym najpóźniej w ciągu 30 dni kalendarzowych od otrzymania reklamacji, chyba że zostanie uzgodnione inaczej.</li>
	<li>Sprzedawca będzie zobowiązany wystawić Odbiorcy pisemne zaświadczenie potwierdzające dzień złożenia reklamacji, jej treść oraz wnioskowany sposób załatwienia reklamacji. Następnie, Sprzedawca wystawia zaświadczenie potwierdzające dzień i sposób zakończenia załatwiania reklamacji, w tym zawierające informacje na temat przeprowadzonej naprawy oraz czasu naprawy. W przypadku odrzucenia reklamacji, Sprzedawca wystawia pisemne oświadczenie zawierające uzasadnienie odrzucenia przez niego reklamacji.</li>
	<li>Koszty przechowywania w przypadku reklamacji nie objętych gwarancją: jeżeli reklamowany towar nie zostanie odebrany w ciągu 30 dni od ukończenia naprawy lub jeżeli dany produkt nie zostanie odebrany w przypadku reklamacji nie objętej gwarancją, Kupujący może zostać obciążony opłatą za składowanie w wysokości 30 CZK (netto) za każdy dzień kalendarzowy. Sprzedawca będzie mógł wstrzymać się z przekazaniem produktu do momentu pełnej zapłaty kosztów naprawy oraz opłaty za składowanie. Jeżeli powyższa kwota będzie przekraczała obecną cenę sprzedaży produktu, Sprzedawca będzie mógł sprzedać produkt w celu pokrycia poniesionych przez siebie kosztów.</li>
</ol>
 <br>
 <br>
<h4>6. POSTANOWIENIA KOŃCOWE</h4>
<ol type="a">
	<li>Niniejsze Zasady dotyczące reklamacji wchodzą w życie z dniem 1 maja 2014 roku.</li>
	<li>Zastrzega się możliwość wprowadzania zmian do niniejszych Zasad. Zasady dotyczące reklamacji stanowią integralną część Warunków Handlowych Sprzedawcy.<br>
 </li>
</ol>
<ol type="a">
</ol>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>