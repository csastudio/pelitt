<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Privacy-policy");
?><h1>PRIVACY POLICY</h1>
 <br>
 <br>
<p>
	 Customers' personal details are stored in accord with the valid laws of the Czech Republic, especially the Act No. 101/2000 Coll., on the protection of personal data, as amended. By concluding a contract the Purchaser&nbsp;agrees with processing and collecting their personal data&nbsp;in the Vendor's database after successful performance of the contract until explicitly revoced in writing by the Purchaser.
</p>
<p>
	 The Purchaser is entitled&nbsp;to access their personal data&nbsp;and to request&nbsp;its modification, including other legal rights to this data. Based on the Customer's written request, their personal data can be removed from the database. Customers' personal data is fully protected against compromise. The Vendor does not pass the Customers' personal data to any third party. The only exceptions are the external carriers and payment gate providers to whom the Customers' personal data is passed in the necessary minimum in order to deliver the goods or to authorize the relevant payment tool.
</p>
<p>
	 On signing the individual contracts are archived by the Provider in electronic form and are accessible to the e-shop Provider.
</p>
<p>
	 The e-shop Provider&nbsp;is Pelitt Group SE., Na Strži 1702/65, 140 00 Prague, Czech Republic.
</p>
<p>
	 Written revocation of processing and storing Customer's personal data&nbsp;can be sent to the e-shop Provider's address, Pelitt Group SE., as above.
</p>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>