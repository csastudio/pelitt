<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Delivery-information");
?><h1><span style="color: #ea512f;">INFORMACJE DOTYCZĄCE DOSTAWY</span></h1>
 <br>
 <br>
<p>
 <span style="text-align: justify;">W przypadku wszystkich zamówień, dostawa jest darmowa. Sposób dostawy wszystkich produktów zakupionych w sklepach internetowych prowadzących sprzedaż produktów Pelitt Group SE różni się w zależności od podanego w zamówieniu kraju dostawy:</span>
</p>
<p style="text-align:justify">
</p>
<p>
 <br>
</p>
<h4>CZECHY</h4>
 <br>
<p>
	 Termin dostawy towaru wynosi 2 dni robocze. Na terenie Czech dostawy są realizowane przez firmę kurierską Geis. Dostawy mogą być również realizowane do punktów odbioru Geis zwanych Punktami Geis (<a href="http://geispoint.cz/?#ss_vydejnimista">Geis Point</a>).
</p>
<p>
	 DOSTAWA ZA POŚREDNICTWEM FIRMY KURIERSKIEJ
</p>
<ul>
	<li>Kurier przesyła Pani/u z wyprzedzeniem powiadomienie o dostawie na podany w zamówieniu numer telefonu. W przypadku braku telefonu, pod którym może Pan/i być dostępna w ciągu dnia, Pani/a zamówienie nie będzie realizowane.</li>
	<li>Towar jest dostarczany na Pani/a adres przez przewoźnika lub kuriera. Wysłany towar dostarczany jest do pierwszych drzwi wejściowych. Kierowca zwykle nie ma drugiego kierowcy, w związku z czym nie jest w stanie wnieść towaru do domu.</li>
	<li>Z wyprzedzeniem otrzyma Pan/i wiadomość tekstową/ e-mail informujący o godzinie dostawy.</li>
	<li>W&nbsp;przypadku zamówień za pobraniem, w momencie dostawy należy zapłacić kurierowi pełną wartość zamówienia.</li>
	<li>Kupujący zobowiązany jest dokonać należytego odbioru towaru, sprawdzić czy opakowanie jest w stanie nienaruszonym oraz sprawdzić liczbę paczek, zaś w przypadku jakichkolwiek wad zgłosić je w oddziale dostawcy i odnotować wadę na dowodzie dostawy. Kupujący podpisuje dowód i potwierdza, że paczka jest zgodna z wszystkimi podanymi powyżej warunkami, a także że późniejsze reklamacje dotyczące uszkodzonego opakowania nie będą uwzględniane. Faktura służy również jako dokument dla celów podatkowych.</li>
	<li>Kupujący jest zobowiązany natychmiast skontrolować dostarczony towar, a w przypadku niekompletnej dostawy zgłosić ten fakt Dostawcy najpóźniej w ciągu trzech dni roboczych od dnia dostawy. Reklamacje złożone w późniejszym terminie nie będą uwzględniane.</li>
	<li>W przypadku gdy Kupujący nie będzie mógł odebrać zamówionego towaru w ustalonym czasie, kurier skontaktuje się z nim ponownie w celu ustalenia godziny ponownej dostawy.</li>
	<li>W przypadku gdy Kupujący ponownie nie będzie mógł odebrać towaru, będzie on zobowiązany pokryć poniesione przez Sprzedawcę koszty, tj. koszty transportu.</li>
	<li>W przypadku gdy Kupujący odmówi przyjęcia towaru w momencie dostawy lub będzie nieosiągalny pod wskazanym adresem, Sprzedawca będzie mógł dochodzić zwrotu kosztów dostawy towaru.</li>
</ul>
 <br>
 <br>
 <br>
<h4>SŁOWACJA</h4>
<p>
	<br>
</p>
<p>
	Termin dostawy towaru wynosi 3 dni robocze. Na terenie Słowacji dostawy są realizowane przez firmę kurierską Geis.
</p>
<p>
	 DOSTAWA ZA POŚREDNICTWEM FIRMY KURIERSKIEJ
</p>
<ul type="disc">
	<li>Kurier przesyła Pani/u z wyprzedzeniem powiadomienie o dostawie na podany w zamówieniu numer telefonu. W przypadku braku telefonu, pod którym może Pan/i być dostępna w ciągu dnia, Pani/a zamówienie nie będzie realizowane. </li>
	<li>Towar jest dostarczany na Pani/a adres przez przewoźnika lub kuriera. Wysłany towar dostarczany jest do pierwszych drzwi wejściowych. Kierowca zwykle nie ma drugiego kierowcy, w związku z czym nie jest w stanie wnieść towaru do domu.</li>
	<li>Z wyprzedzeniem otrzyma Pan/i wiadomość tekstową/ e-mail informujący o godzinie dostawy.</li>
	<li>W przypadku zamówień za pobraniem, w momencie dostawy należy zapłacić kurierowi pełną wartość zamówienia.</li>
	<li>Kupujący zobowiązany jest dokonać należytego odbioru towaru, sprawdzić czy opakowanie jest w stanie nienaruszonym oraz sprawdzić liczbę paczek, zaś w przypadku jakichkolwiek wad zgłosić je w oddziale dostawcy i odnotować wadę na dowodzie dostawy. Kupujący podpisuje dowód i potwierdza, że paczka jest zgodna z wszystkimi podanymi powyżej warunkami, a także że późniejsze reklamacje dotyczące uszkodzonego opakowania nie będą uwzględniane. Faktura służy również jako dokument dla celów podatkowych.</li>
	<li>Kupujący jest zobowiązany natychmiast skontrolować dostarczony towar, a w przypadku niekompletnej dostawy zgłosić ten fakt Dostawcy najpóźniej w ciągu trzech dni roboczych od dnia dostawy. Reklamacje złożone w późniejszym terminie nie będą uwzględniane.</li>
	<li>W przypadku gdy Kupujący nie będzie mógł odebrać zamówionego towaru w ustalonym czasie, kurier skontaktuje się z nim ponownie w celu ustalenia godziny ponownej dostawy.</li>
	<li>W przypadku gdy Kupujący ponownie nie będzie mógł odebrać towaru, będzie on zobowiązany pokryć poniesione przez Sprzedawcę koszty, tj. koszty transportu.</li>
	<li>W przypadku gdy Kupujący odmówi przyjęcia towaru w momencie dostawy lub będzie nieosiągalny pod wskazanym adresem, Sprzedawca będzie mógł dochodzić zwrotu kosztów dostawy towaru.</li>
</ul>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>