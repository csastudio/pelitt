<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/cz/shop/([\\w\\d\\-]+)/([\\w\\d\\-]+)/lp/(\\\\?(.*))?#",
		"RULE" => "SECT_CODE=\$1&ELEM_CODE=\$2",
		"ID" => "",
		"PATH" => "/cz/shop/lp.php",
	),
	array(
		"CONDITION" => "#^/shop/([\\w\\d\\-]+)/([\\w\\d\\-]+)/lp/(\\\\?(.*))?#",
		"RULE" => "SECT_CODE=\$1&ELEM_CODE=\$2",
		"ID" => "",
		"PATH" => "/shop/lp.php",
	),
	array(
		"CONDITION" => "#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#",
		"RULE" => "alias=\$1",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/online/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/personal-account/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/index.php",
	),
	array(
		"CONDITION" => "#^/personal-account/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal-account/orders/index.php",
	),
	array(
		"CONDITION" => "#^([^/]+?)\\??(.*)#",
		"RULE" => "SECTION_CODE=\$1&\$2",
		"ID" => "bitrix:catalog.section",
		"PATH" => "/local/templates/main_cz/footer.php",
	),
	array(
		"CONDITION" => "#^([^/]+?)\\??(.*)#",
		"RULE" => "SECTION_CODE=\$1&\$2",
		"ID" => "bitrix:catalog.section",
		"PATH" => "/local/templates/main/footer.php",
	),
	array(
		"CONDITION" => "#^/cz/shop/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/cz/shop/index.php",
	),
	array(
		"CONDITION" => "#^/pl/shop/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/pl/shop/index.php",
	),
	array(
		"CONDITION" => "#^/shop/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/shop/index.php",
	),
);

?>