<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Cookies");
?><h1>STATEMENT ON USING "COOKIES" TECHNOLOGY</h1>
 <br>
 <br>
<p>
	 As most entities Pelitt Group SE use&nbsp;the "cookies" technology to improve the comfort&nbsp;of visitors and users of their Internet pages.
</p>
<p>
	 "Cookies" are miniature files consisting of letters and digits strings, which are saved in your computer as you enter a website using this technology. Among others, "cookies" can help the website and e-shop operators identify your person and differentiate you from other visitors and users of these sites and e-shops. "Cookies"&nbsp;CANNOT be used as program code or to spread computer virus&nbsp;and cannot allow either of the above stated entities to access your hard disc or your data. Even though "cookies" are saved in your computer, we are not able to read/download any data from there.
</p>
<p>
	 Data obtained from websites by Pelitt Group SE using "cookies" technology&nbsp;can be used for the following purposes:
</p>
<ul>
	<li>to allow visitors, or users of websites and e-shop&nbsp;to move&nbsp;through pages and make use of their functions, e.g. entering the secured parts (sections) of pages or adding items to cart, etc., i.e. "Strictly Necessary" cookies;</li>
	<li>to collect information about visitors' and users' behaviour on websites and in e-shop, e.g. which pages are visited most often or if visitors or users receive error messages, etc.; these "cookies“ are used mostly to improve the functionality and quality of the future website versions;</li>
	<li>to store information about choices&nbsp;(or selections)&nbsp;website visitors and users made, e.g. selecting language or region, paging values, original filters for looking through the products catalogue, etc., these "cookies“ are used mostly to guarantee more personalized approach;</li>
	<li>for promotion and marketing activities&nbsp;of Pelitt Group SE, its programmes, products and services, these "cookies“ are used mostly to guarantee more personalized approach in marketing and commercial area.</li>
</ul>
<p>
	 Company Pelitt Group SE&nbsp;do not allow&nbsp;any third parties to place commercial texts on their websites. The above stated company cannot guarantee and be held liable for any content, incl. of commercial and marketing nature, placed on the webpages of third parties, as well as for any breach of legal provisions, rights or legitimate interests of third parties that occurred, or potentially can occur, on webpages of third parties.
</p>
<p>
	 By confirming agreement with this document the visitor, or user of website, grants&nbsp;the company Pelitt Group SE, seated in Na strži 1702/65, 140 00 Prague 4, Company ID: 03815854, incorporated in Commercial Register of the Municipal Court in Prague, section H, entry 1549&nbsp;consent with using the "cookies" technology&nbsp;and using the data obtained from websites operated by the said company applying this technology for each and every purposes as stated herein. The consent is voluntary and is granted for an indefinite period of time, and can be revoked any time, free of charge, in the form of a written document sent to company Pelitt Group SE
</p>
<p>
	 Further on, we would like to inform you that failure to grant the consent, its revocation and/or disabling "cookies" (in your Internet browser), incl. disabling the Strictly Necessary cookies, can restrict or prevent access to website or functionality of the website or its parts.
</p>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>