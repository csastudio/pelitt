<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Terms-conditions");
?><h1>TERMS &amp; CONDITIONS</h1>
 <br>
 <br>
<h4>1. GENERAL PROVISIONS</h4>
<ol type="a">
	<li>
	<p>
		 These Trade Terms and Conditions are effective for purchases made in the electronic shop www. pelitt . com . Conditions are further defined and specified by the rights and duties of the contractual parties which are:
	</p>
	<ol>
		<li>Merchant which is the company Pelitt Group SE, Company ID 03815854, seated in Na strži 1702/65, 140 00 Prague 4, Czech Republic (details in section <a href="/contact">Contact Us</a>), a company incorporated in the Register of Companies at the Regional Municipal Court of Prague, under reference number H 1549.,</li>
		<li>Purchaser.</li>
	</ol>
	<ol type="i">
	</ol>
 </li>
	<li>
	<p>
		 All contractual relations are made in compliance with the legal order of the Czech Republic. If the contractual party is a Consumer, the relations not regulated herein abide by the Act No. 89/2012 Coll., the Civil Code, as amended (hereinafter Civil Code) and the Act No. 634/1992 Coll., Consumer Protection Act, as amended (hereinafter Consumer Protection Act). With Consumers from foreign countries, the legal relations not regulated herein abide by the relevant legal regulations on consumer protection in effect in the Consumer's country.
	</p>
 </li>
	<li>
	<p>
		 When the contractual party is not a Consumer, the relations not stipulated herein comply with Act No. 89/2012 Coll., Civil Code, as amended (hereinafter referred to as Civil Code).
	</p>
 </li>
</ol>
 <br>
 <br>
<h4>2. CONCEPT DEFINITIONS</h4>
<ol type="a">
	<li>Consumer Contract is a Purchase Contract, Contract for the Work Done, or different contracts, if the contractual parties are Consumer on one side and Provider, or the Merchant, on the other side.</li>
	<li>Supplier/Merchant is an entity which on concluding or performing a contract acts commercially or business-like. It is an entrepreneur which directly or through other entrepreneurs provides products or renders services to the Purchaser.</li>
	<li>Purchaser/ Consumer is a person who on conclusion and perfomance of the Contract does not act commerciallly or business-like. It is a natural person or legal entity which buys products or uses services for purposes other than to engage in business.</li>
	<li>Purchase Contract - Purchaser is not Consumer invitation to conclude a Contract is the act of sending the order by the Purchaser and the actual Contract is concluded as soon as the Merchant delivers his binding agreement with this invitation to the Purchaser.</li>
	<li>Purchase Contract - Purchaser is a Consumer invitation to conclude a Purchase Contract is the act of placing the offered goods on the e-shop websites; Purchase Contract is concluded as soon as the Purchaser sends his order and the Merchant accepts it. The Merchant immediately confirms acceptance by confirmation e-mail sent to the given e-mail address; however, this does not affect formation of the Contract. The concluded Contract, incl. the agreed price, is subject to change or termination based on mutual agreement of both parties or based on statutory grounds.</li>
	<li>Purchase Contract - Purchaser is not Consumer invitation to conclude a Contract is the act of sending the order by the Purchaser and the actual Contract is concluded as soon as the Merchant delivers his binding agreement with this invitation to the Purchaser.</li>
	<li>Technical Partner is a business man responsible for quality and defects of the products on sale, who is contractually bound to the Provider to attend to individual deficiency claims and returned goods in statutory period when the Consumer withdraws from the Contract concluded on distance. The Partner herein is the producer, which can by proxy be represented by the Provider.</li>
</ol>
 <br>
 <br>
<h4>3. INFORMATION ABOUT CONCLUDED CONTRACT AND TRADE TERMS AND CONDITIONS</h4>
<ol type="a">
	<li>By concluding the Purchase Contract the Purchaser confirms that he has read and understood these Trade Terms and Conditions, the returned goods conditions, the method of making a claim, the conditions for protection of personal data and the conditions for using "cookies" technology in this Internet shop, and that he agrees with them. These Trade Terms and Conditions are adequately advertised to the Purchaser prior to making an order and he is able to study them. These Trade Terms and Conditions are an integral part of the concluded Contract.</li>
	<li>The language of the Purchase Contract is Czech for Consumers from the Czech and Slovak Republics. Unless circumstances on the Merchant's or the Purchaser's side prevent it, the Purchase Contract can also be concluded in a different language, comprehensible to both parties.</li>
	<li>The concluded Purchase Contract (the Purchaser's order) is stored and archived by the Merchant for the purpose of its successful performance and is not accessible to third parties, except to the Technical Partner. Information about the individual technical stages leading to conclusion of the Contract are apparent from the process of ordering in this Internet shop and the Purchaser is welcome to check and, if necessary, correct the order prior to sending.</li>
	<li>These Trade Terms and Conditions are posted on the website of this Internet shop, therefore, they can be stored and reproduced by the Purchaser.</li>
	<li>The expenses incurred from using distant communication devices (phone, Internet, etc.) to make an order are of standard rate, depending on the telecommunication service tariff used by the Purchaser, and are not covered by the Provider.</li>
	<li>As soon as the goods have been paid for, the Merchant transfers the proprietary rights and the right to use and enjoy these goods to the Purchaser. The Purchaser also undertakes to pay the whole amount in a way as stipulated in the binding order and under the terms specified therein.</li>
	<li>For each order there is an invoice/ tax document issued and attached, which also serves as the delivery note, unless the carrier also sends certificate of parcel delivery. The Purchaser receives the goods as stipulated in the binding order. The Purchaser signs the document and confirms reception of goods from the carrier or personally at collection points.</li>
</ol>
 <br>
 <br>
<h4>4. INFORMATION</h4>
<ol type="a">
	<li>All information regarding your order can be obtained primarily by e-mail, or by phone. The contact details are available in this Internet shop in section <a href="/contact">Contact Us</a>.</li>
	<li>This Internet shop uses the "cookies" technology. For more information about the collected data see section called „<a href="/cookies">Cookies Technology</a>“, text of which is also an integral part herein.</li>
	<li>This Internet shop further processes some personal details of the Purchasers. For more information about the method of administration and access to this data see section „<a href="/privacy-policy">Personal Details Protection</a>“, text of which is also an integral part herein.</li>
	<li>Transportation companies and companies authorizing payment tools receive minimum information necessary just for payment authorization and parcel delivery.</li>
</ol>
 <br>
 <br>
<h4>5. ORDER</h4>
<ol type="a">
	<li>Order can be made only through electronic form in the Internet shop available 24/7.</li>
	<li>Before you send your order we strongly recommend that you check the items properly, this way you can avoid any misunderstandings later on. The Purchaser can check and modify his order before sending.</li>
	<li>When you send your order, you will receive its confirmation by e-mail. If you detect any inconsistency in the confirmation, please contact us immediately so that we can rectify the situation. As soon as we receive your order, we inform our logistics partner to ensure delivery of your goods to the given address.</li>
</ol>
 <br>
 <br>
<h4>6. PURCHASER CANCELLED ORDER</h4>
<ol type="a">
	<li>As soon as the Purchaser sends a binding order (clicks on "Send Order" button) and the Merchant receives and confirms it (automatic e-mail with the copy of the order attached sent to the given e-mail address), a binding Purchaser Contract has been concluded (for details on conclusion of Contract see Concept Definitions).</li>
	<li>Order can be cancelled only exceptionally, always with the Merchant's agreement. Request to cancel an order needs to be communicated through <a href="/contact">contact form</a> without delay. This does not affect the Consumer's right to withdraw from the Contract as suggested in the Civil Code and the relevant European Directive regulating this right (see below).</li>
	<li>If the Customer fails to collect the goods without prior cancellation of the order (accepted by the Merchant), the Purchaser who is not a Consumer bears all the expenses incurred in this business operation (logistics, bank, transport, etc.). The Merchant's right to damages is not thus affected. The Customer agrees that the ordered goods will be handed over to anyone reached on the address as given by the Customer and who identifies themselves with a valid ID. The goods thus handed over will be deemed properly collected.</li>
	<li>The Purchaser who is not a Consumer and who DOES NOT COLLECT his goods without prior cancellation of the order (accepted by the Provider) shall bear the expenses incurred from delivery if it is goods that can be returned under the Consumer's legal right to withdraw from the Contract (see below). However, this does not affect the Consumer's right not to collect the goods when inconsistent with the Purchase Contract (see below).</li>
	<li>The goods remain in possession of the Merchant. Unless the Purchaser groundlessly fails to collect them, the possession right is not transferred. In this case the Merchant is entitled to demand storage fee, or compensation for repeat delivery.</li>
	<li>If the Purchaser fails to collect the ordered goods within six months from the day he was obliged to collect them, the parties to the Contract are deemed to have withdrawn from the Contract. In such a case the Merchant is entitled to a refund of the expenses incurred during delivery, storage of goods, or to cover any damage. The Merchant recovers these claims in the form of set-off against the refunded purchase price.</li>
	<li>If the ordered goods are no longer produced or delivered, or are unavailable in the longterm, or there is a significant change of price at which the Provider buys the goods from its suppliers, or there is a typographic error with the goods (description, photo, etc.), and the Purchaser is not a Consumer, the Provider has a right to withdraw from the Contract. If the Purchaser is a Consumer, the Merchant undertakes to contact the Purchaser immediately in order to agree further steps.
	<ol type="i">
		<li>Should this situation occur, we'll contact you by e-mail. If you have already paid for the goods and the order is cancelled, your money will be transferred back to your account in the shortest time possible, usually within five working days since the Merchant confirms the cancellation.</li>
		<li>If the order is changed and the total price after change is higher, you will be asked to pay the difference and the goods will be sent after receiving the difference. If the paid amount is higher, we credit the difference to your account.</li>
	</ol>
 </li>
</ol>
 <br>
 <br>
<h4>7. PRICE FOR GOODS</h4>
<ol type="a">
	<li>All prices available in this Internet shop are stated plus VAT for your target country (region), including all other fees, e.g. packaging fee, recycling fee, etc., if such fees are stipulated by law. Prior to making an order the Purchaser can learn how long the offer/price remains valid.</li>
	<li>The offered price includes neither transport and delivery costs, nor expenses for administrative fees related to cash on delivery method in countries where this service is offered.</li>
	<li>Prices for goods are valid on making the order.</li>
	<li>Unless the Purchaser is a Consumer, our company reserves the right to change prices. If the order price increases, the Purchaser has the right to withdraw from the Contract without any payment. The Purchaser will be informed of this right in advance.</li>
</ol>
 <br>
 <br>
<h4>8. METHOD OF PAYMENT</h4>
<ol type="a">
	<li>By Bank Transfer - the Internet shop provides you with payment information in order to make your payment within 7 working days. Your order will be delivered as soon as your payment is credited to our account. If you fail to collect your order, the procedure is described in chapter Purchaser Cancelled Order. This method of payment is offered to customers in the Czech and Slovak Republics.</li>
	<li>By Payment Card - payment in the Internet shop is made on-line and the goods will be sent as soon as the payment transaction has been completed and your payment card authorized.</li>
	<li>Cash on Delivery - goods are sent by courier, post or delivered at the collection point where they will be handed over on payment. This payment method is available only in the Czech and Slovak Republics.</li>
	<li>The Merchant reserves the right to withdraw from the Purchase Contract and cancel the order paid for by bank transfer due to excessive reservation of products in open orders or due to uncertainty about the Purchaser's payment ethic. Similarly, based on prior experience with a Purchaser the Merchant reserves the right to prevent a purchase settled by bank transfer or cash on delivery. If the payment is still credited to the Provider's bank account, the entire amount will be returned to the Purchaser.</li>
</ol>
 <br>
 <br>
<h4>9. RECEPTION OF GOODS AND TRANSPORT PRICE</h4>
<ol type="a">
	<li>All conditions outside this clause regarding delivery of goods and price, incl. personal collection of goods at collection points are outlined in the document called <a href="/delivery-information">Terms of Delivery</a>, text of which is an integral part herein.</li>
	<li>The Purchaser who is not a Consumer is obliged to inspect the goods on reception. If the Purchaser on reception of the goods from the carrier detects discrepancies, e.g. packaging is visibly damaged, the Purchaser together with the carrier inspects the consignment and in case of damaged goods they make a record of damaged consignment (the Purchaser shall receive a copy). In case of damaged goods, the Purchaser has a right to refuse the goods. Later claim of mechanic damage of goods will not be acknowledged. The Purchaser shall immediately inform the Merchant of the damaged consignment in order to rectify the situation.</li>
	<li>The Purchaser-Consumer cannot be made to inspect the goods on reception, nevertheless, our company strongly recommends that the Purchaser carries out such an inspection in accord with the previous Clause. The Purchaser thus prevents any complications claiming goods damaged during transport. These claims will be based mostly on the bill of lading where the Purchaser can record the state of the delivered consignment. The Purchaser has a right not to receive the goods displaying signs of outside damage and to request delivery of new ones.</li>
	<li>Goods are usually dispatched within 2 to 4 working days from order made to the delivery address given in order.</li>
	<li>The Customer agrees that the ordered goods will be handed over to any person available at the given address who identifies himself with a valid ID. The goods thus handed over are deemed properly collected.</li>
</ol>
 <br>
 <br>
<h4>10. CONSUMER'S RIGHT TO WITHDRAW FROM THE CONTRACT</h4>
<ol type="a">
	<li>Consumer's right to withdraw from the Contract concluded on distance and the procedure to enforce this right are stated in a separate section „<a href="/returns">Returns</a>“, the text of which is also included herein.</li>
	<li>If the goods fail to be delivered within 30 days, the Customer has a right to withdraw from the Contract and to be fully refunded.</li>
</ol>
 <br>
 <br>
<h4>11. CLAIMS CODE</h4>
<ol type="a">
	<li>Deficiency claims under warranty are made at the Technical Partner.</li>
	<li>If the Technical Partner fails to respond to the Purchaser communication for more than 10 working days, the Purchaser has a right to continue to communicate directly with the Provider.</li>
	<li>Details on making a deficiency claim in the warranty period and on the method of handling claims are described in section <a href="/complaints">Claims</a>, text of which is included herein.</li>
	<li>Any disputes between Pelitt Group SE and the Purchaser they can settle out-of-court through an Alternative Dispute Resolution or Online Dispute Resolution (ADR / ODR) procedure. In this case the Purchaser can contact the ODR entity <a href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&lng=EN" target="_blank">on this page</a>. More information on alternative dispute resolution can be found <a href="https://webgate.ec.europa.eu/odr/main/?event=main.about.show" target="_blank">here</a>. Pelitt Group SE recommends the Purchaser to <a href="/contact">contact</a> Pelitt Group SE first to resolve the situation.</li>
</ol>
 <br>
 <br>
<h4>12. EXTRA CHARGE SERVICES</h4>
<ol type="a">
	<li>Change of invoice details on the issued invoice from natural person to legal entity is subject to fee of CZK 300 without VAT.</li>
</ol>
 <br>
 <br>
<h4>13. FINAL PROVISIONS</h4>
<ol type="a">
	<li>These Trade Terms and Conditions take effect as of 1 May 2016.</li>
	<li>Provider reserves the right to make changes without prior notice.</li>
	<li>If you were shopping in this Internet shop in the past and you want to know the Trade Terms and Conditions in effect at that time, please contact us and we'll be happy to send them to you.</li>
</ol>
 <br>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>