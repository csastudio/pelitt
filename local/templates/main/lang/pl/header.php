<?
$MESS ['CATALOG_PROD'] = "Katalog produktów";
$MESS ['UNDERSTAND'] = "Rozumiem";
$MESS ['COOKIES'] = "Ta strona stosuje pliki cookies w celu świadczenia usług oraz analizowania ruchu w serwisie. Korzystając z niej, wyrażacie Państwo na to zgodę.";
$MESS ['COOKIES_MORE'] = "Więcej informacji";