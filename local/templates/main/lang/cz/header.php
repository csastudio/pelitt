<?
$MESS ['CATALOG_PROD'] = "Katalog produktů";
$MESS ['UNDERSTAND'] = "Rozumím";
$MESS ['COOKIES_MORE'] = "Více informací";
$MESS ['COOKIES'] = "Tato stránka používá technologii cookies při poskytování služeb, zvláště pak při analýze provozu webových stránek. Používáním této stránky s tímto souhlasíte.";
