<?
$MESS ['CATALOG_PROD'] = "Produktkatalog";
$MESS ['UNDERSTAND'] = "Ich verstehe";
$MESS ['COOKIES'] = "Diese Website verwendet Cookies bei der Bereitstellung von Diensten, insbesondere bei der Analyse des Traffic der Website. Durch die Nutzung dieser Website erklären Sie sich damit einverstanden.";
$MESS ['COOKIES_MORE'] = "Mehr Informationen.";