<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
use Bitrix\Main\Page\Asset;

IncludeTemplateLangFile(__FILE__);
$CurUrl = $APPLICATION->GetCurPage();
$Folders = explode('/', $CurUrl);
$isMain = $APPLICATION->GetCurPage(true)==SITE_DIR."index.php";
$isBasket = in_array('basket', $Folders);
$isAccount = in_array('personal', $Folders);
$isListOrders = in_array('list-orders', $Folders);
$isDealers = in_array('dealers', $Folders);
$isCheckout = in_array('checkout', $Folders);
$isContacts = in_array('contact', $Folders);
$isPersonal = $APPLICATION->GetCurPage(true)==SITE_DIR."personal-account/basket.php";
// Получаем последний символный код ЧПУ
$url = basename($APPLICATION->GetCurPage()).PHP_EOL;
$url = trim($url);
// Определение языка по SITE_ID
$arCountries = [
	        's1' => ['code' => 'en', 'lang' => 'English'],
	        's2' => ['code' => 'cz', 'lang' => 'Český'],
	        's3' => ['code' => 'pl', 'lang' => 'Polski'],
	        's4' => ['code' => 'sl', 'lang' => 'Sl'],
	        's5' => ['code' => 'de', 'lang' => 'Deutsch'],
	        's6' => ['code' => 'lt', 'lang' => 'Lietuvių'],
	        's7' => ['code' => 'ru', 'lang' => 'Русский'],
];

$AddJs = [
    "/assets/scripts/app.min.js",
    "/assets/scripts/jquery-3.2.1.min.js",
    "/assets/scripts/jquery.cookie.js",
    "/assets/scripts/fancybox/jquery.fancybox.min.js"
];

$AddCss = [
    "/assets/styles/app.min.css",
    "/assets/scripts/fancybox/jquery.fancybox.min.css",
];

$AddString = [
	'<meta charset="utf-8">',
	'<meta name="yandex-verification" content="fa740650c35afd01" />',
    '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">',
    '<meta http-equiv="X-UA-Compatible" content="IE=edge">',
    '<meta name="imagetoolbar" content="no">',
    '<meta name="msthemecompatible" content="no">',
    '<meta name="cleartype" content="on">',
    '<meta name="HandheldFriendly" content="True">',
    '<meta name="format-detection" content="telephone=no">',
    '<meta name="format-detection" content="address=no">',
    '<meta name="google" value="notranslate">',
    '<meta name="theme-color" content="#ffffff">',
    '<meta name="apple-mobile-web-app-capable" content="yes">',
    '<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">',
    '<meta name="application-name" content="">',
    '<meta name="msapplication-tooltip" content="">',
    '<meta name="msapplication-TileColor" content="#ffffff">',
    '<meta name="msapplication-tap-highlight" content="no">'
];

foreach ($AddCss as $css) {
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . $css);
}

foreach ($AddJs as $js) {
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . $js);

}
foreach ($AddString as $string) {
    Asset::getInstance()->addString($string);
}

?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">

<head>
	<title><?$APPLICATION->ShowTitle()?></title>
    <link rel="canonical" href="https://pelitt.com<?=$CurUrl?>"/>
    <link rel="icon" href="<?=SITE_DIR?>icon.ico" type="image/x-icon">
	<?
	$APPLICATION->ShowHead();
	CAjax::Init();
	?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PR3F7S');</script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PR3F7S" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</head>

<body class="page">
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<header id="header" class="header">
	<div id="cookie-notice">
        <div class="container">
            <div class="b-message">
                <div class="b-message__text">
                    <?=GetMessage("COOKIES")?><a href="<?if(SITE_ID != 's1'){echo '/'.$arCountries[SITE_ID]['code'];}?>/cookies/"><?=GetMessage("COOKIES_MORE")?></a>.
                </div>
                <div class="b-message__button">
                    <span class="cookie-ok"><?=GetMessage("UNDERSTAND")?></span>
                </div>
            </div>
        </div>
	</div>
	<div class="container">
		<div class="header__wrap js-header-wrap">
			<div class="header__row">
				<div class="header__col-l">
					<div class="header__catalog">
						<div class="header__catalog-wrap js-header-catalog">
							<svg class="header__icon header__icon_menu"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_menu"></use></svg>
							<svg class="header__icon header__icon_close"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_close-btn"></use></svg>
							<div class="header__text-production"><?=GetMessage("CATALOG_PROD");?></div>
						</div>
					</div>
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"main_top",
						Array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "top_cat",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(0=>"",),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "top_cat",
							"USE_EXT" => "Y"
						)
					);?>
				</div>
				<div class="header__col-c">
					<a href="<?=SITE_DIR?>" class="header__logo">
						<img class="header__logo-img" src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo.png" />
					</a>
				</div>
				<div class="header__col-r">
					<div class="header__right-menu">
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"support_top",
							Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "support",
								"USE_EXT" => "N"
							)
						);?>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top_lang",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "lang",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "CUR_LANG" => $arCountries[SITE_ID]['lang'],
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "lang",
                                "USE_EXT" => "N"
                            )
                        );?>
						<div class="header__search">
							<div class="icons">
								<a data-popup="popupSearch" href="javascript:void(0);" class="icons__item icons icons_bg_search js-popup-open">
									<svg class="icons__icon icons__icon_search"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_search"></use></svg>
								</a>
							</div>
						</div>
						<div class="header__basket">
							<?$APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket.line",
                                "basket_line_top",
                                array(
                                    "HIDE_ON_BASKET_PAGES" => "N",
                                    "PATH_TO_BASKET" => SITE_DIR."basket/",
                                    "PATH_TO_ORDER" => SITE_DIR."list-orders/",
                                    "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                    "PATH_TO_PROFILE" => SITE_DIR."personal/",
                                    "PATH_TO_REGISTER" => SITE_DIR."login/",
                                    "POSITION_FIXED" => "N",
                                    "SHOW_AUTHOR" => "N",
                                    "SHOW_DELAY" => "N",
                                    "SHOW_EMPTY_VALUES" => "Y",
                                    "SHOW_IMAGE" => "Y",
                                    "SHOW_NOTAVAIL" => "N",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_PERSONAL_LINK" => "N",
                                    "SHOW_PRICE" => "Y",
                                    "SHOW_PRODUCTS" => "Y",
                                    "SHOW_SUBSCRIBE" => "N",
                                    "SHOW_SUMMARY" => "Y",
                                    "SHOW_TOTAL_PRICE" => "Y",
                                    "COMPONENT_TEMPLATE" => "basket_line_top"
                                ),
                                false
                            );?>
						</div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:system.auth.form",
                            "auth_user",
                            Array(
                                "FORGOT_PASSWORD_URL" => "",
                                "PROFILE_URL" => "/personal/",
                                "REGISTER_URL" => "",
                                "SHOW_ERRORS" => "N"
                            )
                        );?>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<?if(!$isMain && !$isBasket && !$isListOrders && !$isPersonal && !$isAccount && !$isContacts && !$isCheckout && !in_array('shop', $Folders)){?>
<div class="payment-delivery <?=$isDealers?'payment-delivery_dealers':''?> page__content content_in">
    <div class="payment-delivery__wrap">
        <div class="payment-delivery__payment description__schedule">
            <div class="payment-delivery__description">
<?}?>
