<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
	IncludeTemplateLangFile(__FILE__);
?>
<?if (!$isMain && !$isBasket && !$isListOrders && !$isPersonal && !$isAccount && !$isContacts && !$isCheckout && !in_array('shop', $Folders)){?>
    </div></div></div></div>
<?}?>
<div id="popupSearch" class="popup popup_type_search js-popup">
	<div class="popup__close js-popup-close"></div>
	<?$APPLICATION->IncludeComponent(
		"bitrix:search.form",
		"top_form_q_links",
		Array(
			"PAGE" => "#SITE_DIR#search/index.php",
			"USE_SUGGEST" => "N"
		)
	);?>
</div>
<?$APPLICATION->IncludeComponent(
    "csa:auth", "",
    Array()
);?>
<?
if (!$isMain && !$isBasket && !$isPersonal):
	$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"breadcrumb",
		Array(
			"PATH" => "",
			"SITE_ID" => SITE_ID,
			"START_FROM" => "0"
		)
	);
endif;
?>
<?
// Не показываем footer в корзине
if (!$isBasket && !$isPersonal):
	?>
<div class="footer">
	<div class="container">
		<div class="footer__wrap">
			<div class="footer__menu">
				<div class="footer-menu">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"main_bottom",
						Array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "top_cat",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(""),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "top_cat",
							"USE_EXT" => "Y"
						)
					);?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"tech_support",
						Array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(""),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "tech_support",
							"USE_EXT" => "N"
						)
					);?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"shop",
						Array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"COMPONENT_TEMPLATE" => "shop",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "shop",
							"USE_EXT" => "N"
						)
					);?>
					<div class="footer-menu__group">
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"company",
							Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "company",
								"USE_EXT" => "N"
							)
						);?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "press_center", array(
							"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"COMPONENT_TEMPLATE" => "press_center",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => "",
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "press_center",
								"USE_EXT" => "N"
							),
							false,
							array(
							"ACTIVE_COMPONENT" => "N"
							)
						);?>
					</div>
                    <div class="footer-menu__group"style="display: none">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "company-3",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "support",
                                "USE_EXT" => "N"
                            )
                        );?>
                    </div>
                    <div class="footer-menu__group"style="display: none">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "company-2",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "lang",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "CUR_LANG" => 'English',
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "lang",
                                "USE_EXT" => "N"
                            )
                        );?>
                    </div>
				</div>
			</div>
			<div class="footer__contacts">
				<div class="footer-contacts">
					<div class="footer-contacts__wrapper">
						<div class="footer-contacts__contacts">
                            <div class="footer-contacts__text">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "inc",
									"EDIT_TEMPLATE" => "",
									"PATH" => "/inc/phone_footer_" . $arCountries[SITE_ID]['code'] . ".php"
								)
							);?>
                            </div>
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "inc",
									"EDIT_TEMPLATE" => "",
									"PATH" => "/inc/mail_footer_" . $arCountries[SITE_ID]['code'] . ".php"
								)
							);?>

                            <div class="soc-line">
                                <a href="https://www.facebook.com/pelittEUROPE" class="share-link share-link_static share-link_fb">
                                    <svg class="i-share i_absolute-center"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/icons.svg#share_fb"></use></svg>
                                </a>
                                <a href="https://www.youtube.com/channel/UCdbIY6zGk4A8CsmkbcS0uMQ" class="share-link share-link_static share-link_yt">
                                    <svg class="i-share i_absolute-center"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/icons.svg#share_yt"></use></svg>
                                </a>
                                <!--                        <a href="https://twitter.com/LEXANDLab" class="share-link share-link_static share-link_twitter">-->
                                <!--                            <svg class="i-share i_absolute-center"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="--><?//=SITE_TEMPLATE_PATH?><!--/assets/img/icons.svg#share_twitter"></use></svg>-->
                                <!--                        </a>-->
                                <!---->
                                <!--                        <a href="https://vk.com/lexandlab" class="share-link share-link_static share-link_vk">-->
                                <!--                            <svg class="i-share i_absolute-center"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="--><?//=SITE_TEMPLATE_PATH?><!--/assets/img/icons.svg#share_vk"></use></svg>-->
                                <!--                        </a>-->
                                <a href="https://www.instagram.com/pelittgroup/" class="share-link share-link_static share-link_inst">
                                    <svg class="i-share i_absolute-center"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/icons.svg#share_instagram"></use></svg>
                                </a>
                            </div>
						</div>
						<div class="footer-contacts__wrap">
                                <div class="footer-contacts__system">
                                    <a href="javascript:void(0);" class="footer-contacts__link-image">
                                        <img class="footer-contacts__image" src="/local/components/csa/cards/templates/.default/img/PayPal_logo_new.png">
                                    </a>
                                    <a href="javascript:void(0);" class="footer-contacts__link-image">
                                        <img class="footer-contacts__image" src="<?=SITE_TEMPLATE_PATH?>/assets/images/visalogo10-189.png">
                                    </a>
                                    <a href="javascript:void(0);" class="footer-contacts__link-image">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/mastercard-logo.png">
                                    </a>
                                    <a href="javascript:void(0);" class="footer-contacts__link-image" >
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/visa-mastercard.gif" style="width: 130px">
                                    </a>
                                </div>
<!--							--><?//$APPLICATION->IncludeComponent(
//	"csa:cards",
//	".default",
//	array(
//		"MCARD" => "",
//		"VCARD" => "",
//		"WMONEY" => "",
//		"YAMONEY" => "",
//		"COMPONENT_TEMPLATE" => ".default"
//	),
//	false
//);?>
							<div class="footer-contacts__copyright">
								<div class="footer-contacts__text"><?=GetMessage("COPYRIGHT");?></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer__subscribe">
				<div class="footer-subscribe">
					<div class="footer-subscribe__subscribe">
						<div class="subscribe">
							<div class="subscribe__wrap">
								<!--div class="field field_style_subscribe js-field subscribe__input">
									<input type="text" placeholder="Ваш e-mail" class="field__input js-field-input" />
									<div class="field__validate"></div>
									<div class="field__answer"></div>
								</div>
								<button type="submit" data-popup="" class="btn btn_style_subscribe btn_shape_ellipse subscribe__button">
									<div class="btn__text">Подписаться</div>
								</button>
								<div class="subscribe__text">Подпишитесь на рассылку. Полезная информация и никакого спама.</div-->
							</div>
						</div>
					</div>
					<?/*$APPLICATION->IncludeComponent(
	"csa:eshop.socnet.links", 
	".default", 
	array(
		"FACEBOOK" => "https://www.facebook.com/pelittEUROPE",
		"GOOGLE" => "",
		"INSTAGRAM" => "https://www.instagram.com/pelittgroup/",
//		"TWITTER" => "https://www.youtube.com/channel/UCdbIY6zGk4A8CsmkbcS0uMQ",
		"VKONTAKTE" => "",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);*/?>
				</div>
			</div>
		</div>
	</div>
</div>
    <?
endif;
?>
<div class="shadow js-shadow"></div>
<?if (!$isMain){?>
    <!-- {literal} -->
    <script type='text/javascript'>
        window['l'+'iv'+'e'+'Te'+'x'] = true,
            window['l'+'iv'+'e'+'T'+'exI'+'D'] = 132372,
            window['live'+'Tex_o'+'bjec'+'t'] = true;
        (function() {
            var t = document['cr'+'e'+'ateE'+'lement']('script');
            t.type ='text/javascript';
            t.async = true;
            t.src = '//cs15.'+'livet'+'ex.ru/js'+'/client.js';
            var c = document['get'+'Elements'+'ByTag'+'Name']('script')[0];
            if ( c ) c['paren'+'tNode']['ins'+'ertBe'+'fore'](t, c);
            else document['docum'+'entEl'+'ement']['fir'+'stCh'+'ild']['app'+'en'+'dCh'+'ild'](t);
        })();
    </script>
    <!-- {/literal} -->
<?}?>
</body>

</html>
