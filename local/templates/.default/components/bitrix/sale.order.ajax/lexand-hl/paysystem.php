<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$isActive = (int)$_POST["PAYSYSTEM_ID_CHECK"];
?>
<div data-number="<?if($isActive):?>✓<?else:?>3<?endif;?>" data-key="3" class="order__item<?if($isActive):?> is-active<?endif;?>">
	<div class="order-card order-card_type_payment">
		<div class="order-card__row order__item_title">
			<div class="heading">Способ оплаты</div>
		</div>
		<div class="order-card__content">
			<div class="order-card__choose">
			<input type="hidden" name="PAYSYSTEM_ID_CHECK" id="PAYSYSTEM_ID_CHECK" value="<?=(int)$_POST["PAYSYSTEM_ID_CHECK"]?>" />
			<?
			uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value
			$arResult['PAYSYSTEM_CHECKED'] = false;
			foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
			{
				?>
				<div class="choose-card choose-card_theme_receipt<?if ($arPaySystem["CHECKED"]=="Y" && (int)$_POST["PAYSYSTEM_ID_CHECK"]) echo " checked";?>">
					<?
					if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
						$arFileTmp = CFile::ResizeImageGet(
								$arPaySystem["PSA_LOGOTIP"]['ID'],
								array("width" => "155", "height" =>"155"),
								BX_RESIZE_IMAGE_PROPORTIONAL,
								true
						);
						$imgUrl = $arFileTmp["src"];
					else:
						$imgUrl = $templateFolder."/images/logo-default-ps.gif";
					endif;
					?>
					<div class="choose-card__icon">
						<img src="<?=$imgUrl?>" alt="" role="presentation" />
					</div>
					<div class="choose-card__title"><?=$arPaySystem["PSA_NAME"];?></div>
					<div class="choose-card__subtitle"></div>
					<div class="choose-card__text"></div>
					<div class="choose-card__btn">
						<input type="radio"
							id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
							name="PAY_SYSTEM_ID"
							value="<?=$arPaySystem["ID"]?>"
							<?if ($arPaySystem["CHECKED"]=="Y") echo " checked=\"checked\"";?> 
							style="display:none;" 
						/>
						<button type="" class="btn btn_style_default btn_shape_choose<?if ($arPaySystem["CHECKED"]=="Y"):?> js-card-choose<?endif;?>" onclick="return setPayment(this, <?=$arPaySystem["ID"]?>);">
							<div class="btn__text">выбрать</div>
						</button>
					</div>
				</div>	
				<?

				if ($arPaySystem['CHECKED'] == 'Y'){
					$arResult['PAYSYSTEM_CHECKED'] = $arPaySystem;
				}
			}
			?>
			</div>
			<div class="order-card__final-info"<?if($step == 3):?> style="display:none"<?endif;?>>
				<div class="order-card__paragraph">
					<div class="order-card__final-text">
						<span class="order-card__final-text order-card__final-text_dark">Оплата</span>: <?=$arResult['PAYSYSTEM_CHECKED']['PSA_NAME']?>
					</div>
				</div>
				<div class="order-card__final-btn">
					<button onclick="showBlock(3);return false;" type="" data-popup="" class="btn btn_style_default btn_shape_min-rectangle">
						<div class="btn__text">Изменить</div>
					</button>
				</div>
			</div>
			<div class="order-card__pay">
				<div class="order-pay">
					<div class="order-pay__info">
						<div class="order-pay__title">Сумма заказа</div>
						<div class="order-pay__text">Доставка</div>
						<div class="order-pay__text">Стоимость товара</div>
					</div>
					<div class="order-pay__price">
						<div class="order-pay__title"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></div>
						<div class="order-pay__text"><?=$arResult["DELIVERY_PRICE_FORMATED"]?></div>
						<div class="order-pay__text"><?=$arResult["ORDER_PRICE_FORMATED"]?></div>
					</div>
					<div class="order-pay__btn">
						<button 
							type="" data-popup="" 
							<?if((int)$_POST["PAYSYSTEM_ID_CHECK"] > 0):?>
								onclick="submitForm('Y'); return false;" 
							<?else:?>
								onclick="return false;" 
							<?endif;?>
							id="ORDER_CONFIRM_BUTTON" 
							class="btn btn_style_key btn_shape_rectangle<?if((int)$_POST["PAYSYSTEM_ID_CHECK"] == 0):?> btn_state_disabled<?endif;?>">
							<div class="btn__text">Оформить заказ</div>
						</button>
					</div>
				</div>
                <div class="order-pay__text" style="color: white">Компания продавец: SIA PELITT LV.
                    Адрес: Rīga, Uriekstes iela 14A, LV-1005</div>
                <div class="order-pay__text" style="color: white">Ваш платеж при  оплате картой будет указан как платеж в пользу Pelitt.com/ru</div>
			</div>
		</div>
	</div>
</div>