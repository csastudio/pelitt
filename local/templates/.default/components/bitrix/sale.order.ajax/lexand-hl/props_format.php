<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default")
	{
		if (!empty($arSource))
		{
			foreach ($arSource as $arProperties)
			{
			    $arParams["TEMPLATE_LOCATION"] = [1,2,4,5,6,14];
			    if (in_array($arProperties["ID"], $arParams["TEMPLATE_LOCATION"])) {
                    ?>
                    <div class="<?
                    if ($arProperties["TYPE"] == "CHECKBOX"):?>order-card__form-accept<? else:?>order-card__form-row<?endif; ?>">
                        <div class="<?
                        if ($arProperties["TYPE"] == "CHECKBOX"):?>checkbox checkbox_style_personal-data<? else:?>field field_style_mine-shaft js-field<?endif; ?>">
                            <?
                            if ($arProperties["TYPE"] == "CHECKBOX" && $arProperties["FIELD_NAME"] == 'ORDER_PROP_14') {
                                ?>
                                <input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" value="">
                                <input type="checkbox" class="checkbox__input checkbox__input_style_personal-data"
                                       name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>"
                                       value="Y"<?
                                if ($arProperties["CHECKED"] == "Y") echo " checked"; ?>>
                                <label for="<?= $arProperties["FIELD_NAME"] ?>"
                                       class="checkbox__label">
                                    Отправляя заказ, Вы соглашаетесь принимать телефонные звонки,SMS-сообщения и электронные письма, связанные с Вашим заказом и подтверждаете, что ознакомились и&nbsp;согласны с
                                        <a href="/ru/agreement/" class="breadcrumbs__link">Пользовательским соглашением</a>,
                                        <a href="/ru/privacy-policy/" class="breadcrumbs__link">Политикой конфиденциальности</a> и
                                        <a href="/ru/rules/" class="breadcrumbs__link">Правилами обмена и возврата</a>
                                    </label>
                                <?
                            } elseif ($arProperties["TYPE"] == "CHECKBOX") {
                                ?>
                                <input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" value="">
                                <input type="checkbox" class="checkbox__input checkbox__input_style_personal-data"
                                       name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>"
                                       value="Y"<?
                                if ($arProperties["CHECKED"] == "Y") echo " checked"; ?>>
                                <label for="<?= $arProperties["FIELD_NAME"] ?>"
                                       class="checkbox__label"><?= $arProperties["NAME"] ?></label>
                                <?
                            } elseif ($arProperties["TYPE"] == "TEXT") {
                                ?>
                                <input
                                        type="text"
                                        placeholder="<?= $arProperties["NAME"] ?>"
                                        class="field__input js-field-input<?
                                        if ($arProperties["REQUIED_FORMATED"] == "Y"):?> js-field-requied<?endif; ?><?
                                        if ($arProperties["IS_EMAIL"] == "Y"):?> is_email<?endif; ?>"
                                        maxlength="250"
                                        size="<?= $arProperties["SIZE1"] ?>"
                                        value="<?= $arProperties["VALUE"] ?>"
                                        name="<?= $arProperties["FIELD_NAME"] ?>"
                                        id="<?= $arProperties["FIELD_NAME"] ?>"
                                />
                                <div class="field__validate"></div>
                                <div class="field__answer"></div>
                                <?
                            } elseif ($arProperties["TYPE"] == "SELECT") {
                                ?>
                                <select name="<?= $arProperties["FIELD_NAME"] ?>"
                                        id="<?= $arProperties["FIELD_NAME"] ?>" size="<?= $arProperties["SIZE1"] ?>">
                                    <?
                                    foreach ($arProperties["VARIANTS"] as $arVariants):?>
                                        <option value="<?= $arVariants["VALUE"] ?>"<?= $arVariants["SELECTED"] == "Y" ? " selected" : '' ?>><?= $arVariants["NAME"] ?></option>
                                    <?endforeach ?>
                                </select>
                                <?
                            } elseif ($arProperties["TYPE"] == "MULTISELECT") {
                                ?>
                                <select multiple name="<?= $arProperties["FIELD_NAME"] ?>"
                                        id="<?= $arProperties["FIELD_NAME"] ?>" size="<?= $arProperties["SIZE1"] ?>">
                                    <?
                                    foreach ($arProperties["VARIANTS"] as $arVariants):?>
                                        <option value="<?= $arVariants["VALUE"] ?>"<?= $arVariants["SELECTED"] == "Y" ? " selected" : '' ?>><?= $arVariants["NAME"] ?></option>
                                    <?endforeach ?>
                                </select>
                                <?
                            } elseif ($arProperties["TYPE"] == "TEXTAREA") {
                                $rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
                                ?>
                                <textarea placeholder="<?= $arProperties["NAME"] ?>" rows="<?= $rows ?>"
                                          cols="<?= $arProperties["SIZE1"] ?>"
                                          class="field__input field__input_textarea js-field-input<?
                                          if ($arProperties["REQUIED_FORMATED"] == "Y"):?> js-field-requied<?endif; ?>"
                                          name="<?= $arProperties["FIELD_NAME"] ?>"
                                          id="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["VALUE"] ?></textarea>
                                <?
                            } elseif ($arProperties["TYPE"] == "RADIO") {
                                if (is_array($arProperties["VARIANTS"])) {
                                    foreach ($arProperties["VARIANTS"] as $arVariants):
                                        ?>
                                        <input
                                                type="radio"
                                                name="<?= $arProperties["FIELD_NAME"] ?>"
                                                id="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"
                                                value="<?= $arVariants["VALUE"] ?>" <?
                                        if ($arVariants["CHECKED"] == "Y") echo " checked"; ?> />

                                        <label for="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"><?= $arVariants["NAME"] ?></label></br>
                                        <?
                                    endforeach;
                                }
                            } elseif ($arProperties["TYPE"] == "FILE") {
                                ?>
                                <?= showFilePropertyField("ORDER_PROP_" . $arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"]) ?>
                                <?
                            } elseif ($arProperties["TYPE"] == "DATE") {
                                global $APPLICATION;

                                $APPLICATION->IncludeComponent('bitrix:main.calendar', '', array(
                                    'SHOW_INPUT' => 'Y',
                                    'INPUT_NAME' => "ORDER_PROP_" . $arProperties["ID"],
                                    'INPUT_VALUE' => $arProperties["VALUE"],
                                    'SHOW_TIME' => 'N'
                                ), null, array('HIDE_ICONS' => 'N'));
                                ?>
                                <?
                                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
                                    <div class="bx_description"><?= $arProperties["DESCRIPTION"] ?></div>
                                <?endif ?>
                                <?
                            }
                            ?>
                        </div>

                        <?
                        if (CSaleLocation::isLocationProEnabled()):?>

                            <?
                            $propertyAttributes = array(
                                'type' => $arProperties["TYPE"],
                                'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
                            );

                            if (intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
                                $propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

                            if (intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
                                $propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

                            if ($arProperties['IS_ZIP'] == 'Y')
                                $propertyAttributes['isZip'] = true;
                            ?>

                            <script>

                                <?// add property info to have client-side control on it?>
                                (window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=CUtil::PhpToJSObject(array(
                                    'id' => intval($arProperties["ID"]),
                                    'attributes' => $propertyAttributes
                                ))?>);

                            </script>
                        <?endif ?>
                    </div>
                    <?
                }
			}
		}
	}
}
?>