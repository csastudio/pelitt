<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<script type="text/javascript">
	function fShowStore(id, showImages, formWidth, siteId)
	{
		var strUrl = '<?=$templateFolder?>' + '/map.php';
		var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

		var storeForm = new BX.CDialog({
					'title': '<?=GetMessage('SOA_ORDER_GIVE')?>',
					head: '',
					'content_url': strUrl,
					'content_post': strUrlPost,
					'width': formWidth,
					'height':450,
					'resizable':false,
					'draggable':false
				});

		var button = [
				{
					title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
					id: 'crmOk',
					'action': function ()
					{
						GetBuyerStore();
						BX.WindowManager.Get().Close();
					}
				},
				BX.CDialog.btnCancel
			];
		storeForm.ClearButtons();
		storeForm.SetButtons(button);
		storeForm.Show();
	}

	function GetBuyerStore()
	{
		BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
		//BX('ORDER_DESCRIPTION').value = '<?=GetMessage("SOA_ORDER_GIVE_TITLE")?>: '+BX('POPUP_STORE_NAME').value;
		BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
		BX.show(BX('select_store'));
	}

	function showExtraParamsDialog(deliveryId)
	{
		var strUrl = '<?=$templateFolder?>' + '/delivery_extra_params.php';
		var formName = 'extra_params_form';
		var strUrlPost = 'deliveryId=' + deliveryId + '&formName=' + formName;

		if(window.BX.SaleDeliveryExtraParams)
		{
			for(var i in window.BX.SaleDeliveryExtraParams)
			{
				strUrlPost += '&'+encodeURI(i)+'='+encodeURI(window.BX.SaleDeliveryExtraParams[i]);
			}
		}

		var paramsDialog = new BX.CDialog({
			'title': '<?=GetMessage('SOA_ORDER_DELIVERY_EXTRA_PARAMS')?>',
			head: '',
			'content_url': strUrl,
			'content_post': strUrlPost,
			'width': 500,
			'height':200,
			'resizable':true,
			'draggable':false
		});

		var button = [
			{
				title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
				id: 'saleDeliveryExtraParamsOk',
				'action': function ()
				{
					insertParamsToForm(deliveryId, formName);
					BX.WindowManager.Get().Close();
				}
			},
			BX.CDialog.btnCancel
		];

		paramsDialog.ClearButtons();
		paramsDialog.SetButtons(button);
		//paramsDialog.adjustSizeEx();
		paramsDialog.Show();
	}

	function insertParamsToForm(deliveryId, paramsFormName)
	{
		var orderForm = BX("ORDER_FORM"),
			paramsForm = BX(paramsFormName);
			wrapDivId = deliveryId + "_extra_params";

		var wrapDiv = BX(wrapDivId);
		window.BX.SaleDeliveryExtraParams = {};

		if(wrapDiv)
			wrapDiv.parentNode.removeChild(wrapDiv);

		wrapDiv = BX.create('div', {props: { id: wrapDivId}});

		for(var i = paramsForm.elements.length-1; i >= 0; i--)
		{
			var input = BX.create('input', {
				props: {
					type: 'hidden',
					name: 'DELIVERY_EXTRA['+deliveryId+']['+paramsForm.elements[i].name+']',
					value: paramsForm.elements[i].value
					}
				}
			);

			window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

			wrapDiv.appendChild(input);
		}

		orderForm.appendChild(wrapDiv);

		BX.onCustomEvent('onSaleDeliveryGetExtraParams',[window.BX.SaleDeliveryExtraParams]);
	}

	if(typeof submitForm === 'function')
		BX.addCustomEvent('onDeliveryExtraServiceValueChange', function(){ submitForm(); });

</script>
<?
$isActive = $_POST['STEP'] > 2 || (int)$_POST["DELIVERY_ID_CHECK"] || (int)$_POST["PAYSYSTEM_ID_CHECK"];
$arClassesDelivery = array(
	13 => 'choose-card_theme_handle',
	9 => 'choose-card_theme_courier',
	11 => 'choose-card_theme_postamat'
);

function mp($arr){
	echo "Array<pre>";
		print_r($arr);
	echo "</pre>";
}
//mp($step);
?>

<div data-number="<?if($isActive):?>✓<?else:?>2<?endif;?>" data-key="2" class="order__item<?if($isActive):?> is-active<?endif;?>">
	<div class="order-card order-card_type_delivery">
		<div class="order-card__row order__item_title">
			<div class="heading">Способ доставки</div>
		</div>
		<div class="order-card__content">

			<div class="order-card__choose"<?if($step != 2):?> style="display:none"<?endif;?>>
				<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult["BUYER_STORE"]?>" />
				<input type="hidden" name="DELIVERY_ID_CHECK" id="DELIVERY_ID_CHECK" value="<?=(int)$_POST["DELIVERY_ID_CHECK"]?>" />
				<?
				$arResult['DELIVERY_CHECKED'] = array();
				if(!empty($arResult["DELIVERY"]))
				{
					$width = ($arParams["SHOW_STORES_IMAGES"] == "Y") ? 850 : 700;
					foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
					{
						if (count($arDelivery["STORE"]) > 0)
							$clickHandler = "onClick = \"fShowStore('".$arDelivery["ID"]."','".$arParams["SHOW_STORES_IMAGES"]."','".$width."','".SITE_ID."')\";";
						elseif($arDelivery["ID"] == 11){
							if($arDelivery["CHECKED"]=="Y" && (int)$_POST["DELIVERY_ID_CHECK"])
								$clickHandler = "onClick = \"ckt.open_map('postamat'); return false;\"";
							else
								$clickHandler = "onClick = \"return setDelivery(this, ".$arDelivery["ID"].");\"";
						}
						elseif($arDelivery["ID"] == 10){
							if($arDelivery["CHECKED"]=="Y" && (int)$_POST["DELIVERY_ID_CHECK"])
								$clickHandler = "onClick = \"ckt.open_map('pvz'); return false;\"";
							else
								$clickHandler = "onClick = \"return setDelivery(this, ".$arDelivery["ID"].");\"";

						}
						else
							$clickHandler = "onClick = \"return setDelivery(this, ".$arDelivery["ID"].");\"";

						?>
						<div class="choose-card <?=$arClassesDelivery[$arDelivery["ID"]]?> js-card-choose<?if ($arDelivery["CHECKED"]=="Y" && (int)$_POST["DELIVERY_ID_CHECK"]) echo " is-active";?>">
							<?
							if (count($arDelivery["LOGOTIP"]) > 0):

								$arFileTmp = CFile::ResizeImageGet(
									$arDelivery["LOGOTIP"]["ID"],
									array("width" => "95", "height" =>"55"),
									BX_RESIZE_IMAGE_PROPORTIONAL,
									true
								);

								$deliveryImgURL = $arFileTmp["src"];
							else:
								$deliveryImgURL = $templateFolder."/images/logo-default-d.gif";
							endif;
							?>
							<div class="choose-card__icon">
								<img src="<?=$deliveryImgURL?>" alt="" />
							</div>
							<div class="choose-card__title">
								<?if(strlen($arDelivery["OWN_NAME"])):?>
									<?echo htmlspecialcharsbx($arDelivery["OWN_NAME"]);?>
								<?else:?>
									<?echo htmlspecialcharsbx($arDelivery["NAME"]);?>
								<?endif;?>
							</div>
							<div class="choose-card__subtitle"><?=$arDelivery["PRICE_FORMATED"]?></div>
							<div class="choose-card__text choose-card__period<?if($arDelivery["ID"] == 10):?> pvz<?endif;?><?if($arDelivery["ID"] == 11):?> postamate<?endif;?>">
							<?if($arDelivery["ID"] == 10 || $arDelivery["ID"] == 11):?>
								<div style="display:none;" class="choose-card__period_hide">
									<?=$arDelivery["PERIOD_TEXT"]?>
								</div>
							<?elseif($arDelivery["ID"] == 13):?>
								Самовывоз возможен только при предоплате Яндекс Деньги или банковской картой <br>
								Возможен в день оформления заказа
							<?else:?>
								<?=$arDelivery["PERIOD_TEXT"]?>
							<?endif;?>
							</div>
							<div class="choose-card__success">
								<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_check-mark"></use></svg>
							</div>
							<div class="choose-card__btn">
								<input type="radio"
									id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
									name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
									value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?>
									onclick="submitForm();"
									style="display:none;"
								/>
								<button type="" <?=$clickHandler?> class="btn btn_style_default btn_shape_choose js-card-choose" data-choose="courier">
									<div class="btn__text">выбрать</div>
								</button>
							</div>
						</div>

						<?
						if ($arDelivery['CHECKED'] == 'Y'){
							$arResult['DELIVERY_CHECKED'] = $arDelivery;
						}
						?>
					<?
					}
				}
				?>
			</div>
			<?if((int)$_POST["DELIVERY_ID_CHECK"]):?>

		<?//mp( $arResult['DELIVERY_CHECKED']["ID"]);?>
		<?//mp( $arResult["ORDER_PROP"]["RELATED"])?>


		<?/*if($arResult['DELIVERY_CHECKED']["ID"] == 14):?>
					<div style="display:block;">
						<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");?>
					</div>
		<?else:*/
		//  PVZ 15 16 17
		//kyrer 14 19 20
		?>
				<?if($arResult['DELIVERY_CHECKED']["ID"] == 10 || $arResult['DELIVERY_CHECKED']["ID"] == 11
				    || $arResult['DELIVERY_CHECKED']["ID"] == 24):
				    			?><div style="display:none;"><?endif;?>
								<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");?>
				<?if($arResult['DELIVERY_CHECKED']["ID"] == 10 || $arResult['DELIVERY_CHECKED']["ID"] == 11
				|| $arResult['DELIVERY_CHECKED']["ID"] == 24):
								?></div><?
				endif;?>
		<?//endif;?>

		<?if($arResult['DELIVERY_CHECKED']["ID"] == 24):?>
			    <style>
				    #YMapsID{
				    z-index:998;
				    display:block;
				    position:fixed;
				    top:-2000px;
				    width: 800px;
				    height: 450px;
				    margin:10px;
				    border:solid 3px #cccccc;
				    right:100px;
				    box-shadow: 0px 0px 12px rgba(0,0,0,0.8);
				    -moz-box-shadow: 0px 0px 12px rgba(0,0,0,0.8);
				    -webkit-box-shadow: 0px 0px 12px rgba(0,0,0,0.8);
				    }
			    </style>

			<?foreach ($arResult["LOCATION"]["VARIANTS"] as $loct ):?>
					<?if ($loct["SELECTED"] == "Y"):?>
						<input type="hidden" value="<?=$loct["CITY_NAME"]?>" name="city" class="geo_class">
					<?endif;?>
			<?endforeach;?>




			<div class="order-card__delivery-info" <?if($step != 2):?> style="display:none"<?endif;?>>

				<div class="order-card__paragraph">
						<div class="order-card__delivery-text">Поиск пунктов на карте:</div>
						<div class="order-card__delivery-descr"><a style="cursor:pointer; display:block; text-decoration:underline;" class="message-map-link">Выбрать на карте</a></div>
				</div>


				<br><span id="close_map" style="position:fixed; top:-2000px; cursor:pointer; z-index:999; right:75px; background: rgba(5, 111, 237, 0.75);color: white; display:inline-block; padding:4px 6px; text-decoration:underline;">закрыть</span>
				<div id="YMapsID"></div>
			</div>
		<?endif;?>





			<div class="order-card__delivery-info" <?if($step != 2):?> style="display:none"<?endif;?>>
				<?if($arResult['DELIVERY_CHECKED']["ID"] == 10 || $arResult['DELIVERY_CHECKED']["ID"] == 11):?>

				<?else:?>
					<?echo $arResult['DELIVERY_CHECKED']["DESCRIPTION"];?>
				<?endif;?>
				<div class="order-card__delivery-btn btn_shape_rectangle_h">
					<a type="" data-popup="" class="btn btn_style_key btn_shape_rectangle" onclick="showBlock(3);">
						<div class="btn__text">продолжить</div>
					</a>
				</div>




			</div>
			<?endif;?>

			<div class="order-card__final-info"<?if($step == 2):?> style="display:none"<?endif;?>>
				<div class="order-card__paragraph">
					<div class="order-card__final-text">
						<span class="order-card__final-text order-card__final-text_dark">Доставка</span>: !<?=$arResult['DELIVERY_CHECKED']['OWN_NAME']?>! (<?=$arResult['DELIVERY_CHECKED']['PRICE_FORMATED']?>)
					</div>
					<div class="order-card__final-text order-card__final-text_light choose-card__period">
						<?if($arResult['DELIVERY_CHECKED']["ID"] != 4):?>
							<?=$arResult['DELIVERY_CHECKED']["PERIOD_TEXT"]?> после оформления заказа
						<?endif;?>
					</div>
				</div>
				<div class="order-card__final-btn">
					<button onclick="showBlock(2);return false;" type="" data-popup="" class="btn btn_style_default btn_shape_min-rectangle">
						<div class="btn__text">Изменить</div>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>