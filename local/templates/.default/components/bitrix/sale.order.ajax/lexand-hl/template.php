<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?><style type='text'>.choose-card {width:225px !important;}</style><?
if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}

	}
}
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");
$step = isset($_POST['STEP']) ? $_POST['STEP'] : 1;

if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
{
	if(strlen($arResult["REDIRECT_URL"]) == 0)
	{
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
		return;
	}
}
?>

<?/*<script src="https://api.dostavka.guru/jquery.js"></script>*/?>
<script src="https://api.dostavka.guru/client/collection-search-provider.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?load=package.standard,package.geoObjects&lang=ru-RU" type="text/javascript"></script>
<?/*<script src="/guru_files/dostavka_guru_app_pvz.js"></script>*/?>
<div id="order_form_div" class="order-checkout">
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>

<?
if (!function_exists("getColumnName"))
{
	function getColumnName($arHeader)
	{
		return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
	}
}

if (!function_exists("cmpBySort"))
{
	function cmpBySort($array1, $array2)
	{
		if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
			return -1;

		if ($array1["SORT"] > $array2["SORT"])
			return 1;

		if ($array1["SORT"] < $array2["SORT"])
			return -1;

		if ($array1["SORT"] == $array2["SORT"])
			return 0;
	}
}
?>
<div class="order">
	<div class="order__head">
		<div style="z-index: -5" class="page-preview page-preview_no-bg page-preview_type_order">
			<div class="page-preview__title">Оформление заказа</div>
		</div>
		<div class="close close_style_compact">
			<a href="/ru/basket/" class="close__button">
				<svg class="close__icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_close-btn"></use></svg>
			</a>
		</div>
	</div>
	<div class="order__content">
	<?
	if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
	{
		if(!empty($arResult["ERROR"]))
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);
		}
		elseif(!empty($arResult["OK_MESSAGE"]))
		{
			foreach($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}

		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
	}
	else
	{
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
		{
		}
		else
		{
			?>
			<script type="text/javascript">

			<?if(CSaleLocation::isLocationProEnabled()):?>

				<?
				// spike: for children of cities we place this prompt
				$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
				?>

				BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
					'source' => $this->__component->getPath().'/get.php',
					'cityTypeId' => intval($city['ID']),
					'messages' => array(
						'otherLocation' => '--- '.GetMessage('SOA_OTHER_LOCATION'),
						'moreInfoLocation' => '--- '.GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
						'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.GetMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
							'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
							'#ANCHOR_END#' => '</a>'
						)).'</div>'
					)
				))?>);

			<?endif?>

			function validateInput(input){
				var field = input.closest('.js-field');
				var val = input.val();

				if(val.length == 0){
					field.addClass('field_answer');
					$('.field__answer', field).text('Обязательное поле');

					return false;
				}
				else{
					if(input.hasClass('is_email')){
						re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
						if (!re.test(val))
						{
							field.addClass('field_answer');
							$('.field__answer', field).text('Некорректный формат email');

							return false;
						}
						else{
							field.removeClass('field_answer').addClass('field_validate');
							$('.field__answer', field).text('');
						}
					}
					else{
						field.removeClass('field_answer').addClass('field_validate');
						$('.field__answer', field).text('');
					}
				}

				return true;
			}

			function validate(block){
				var inputs = $('.js-field-requied', block);
				var first = false;

				inputs.each(function(){
					var input = $(this);

					if(!validateInput(input)){
						if(!first)
							first = input;
					}
				});

				if(first.length){
					var block = first.closest('.order__item');
					var number = block.data('key');

					showBlock(number);

					$('html, body').animate({
						scrollTop: first.offset().top - 100
					}, 1000);

					return false;
				}

				return true;
			}

			var BXFormPosting = false;
			var BXFormDeliveryType = false;
			var BXFormSubmit = false;

			function submitForm(val, deliveryType)
			{
				if (BXFormPosting === true)
					return true;

				if(val != 'Y')
					BX('confirmorder').value = 'N';
				else{
					if(!validate($('#ORDER_FORM')))
						return true;
				}

				BXFormPosting = true;

				var orderForm = BX('ORDER_FORM');
				BX.showWait();

				<?if(CSaleLocation::isLocationProEnabled()):?>
					BX.saleOrderAjax.cleanUp();
				<?endif?>

				BXFormDeliveryType = deliveryType;

				BX.ajax.submit(orderForm, ajaxResult);

				return true;
			}

			function ajaxResult(res)
			{
				BXFormSubmit = true;
				var orderForm = BX('ORDER_FORM');
				try
				{
					// if json came, it obviously a successfull order submit

					var json = JSON.parse(res);
					BX.closeWait();

					if (json.error)
					{
						BXFormPosting = false;
						return;
					}
					else if (json.redirect)
					{
						window.top.location.href = json.redirect;
					}
				}
				catch (e)
				{
					// json parse failed, so it is a simple chunk of html

					BXFormPosting = false;
					BX('order_form_content').innerHTML = res;

					$('#order_form_content .js-field-requied').on('change', function(){
						validateInput($(this));
					});

					showBlock();
					updatePeriod();

					<?if(CSaleLocation::isLocationProEnabled()):?>
						BX.saleOrderAjax.initDeferredControl();
					<?endif?>

					if(BXFormDeliveryType == 'postamat')
						ckt.open_map('postamat');
					else if(BXFormDeliveryType == 'pvz')
						ckt.open_map('pvz');
				}

				BX.closeWait();
				BX.onCustomEvent(orderForm, 'onAjaxSuccess');

				updatePvz();
			}

			function SetContact(profileId)
			{
				BX("profile_change").value = "Y";
				submitForm();
			}

			function hideProperties(){
				var finalBlock = $('.order-card_type_personal .order-card__final-info');
				finalBlock.find('> div').not('.order-card__final-btn').remove();

				var propBlock = $('.order-card_type_personal .order-card__form');
				var controls = $('input, textarea', propBlock);

				var name = '';
				var last_name = '';
				var email = '';
				var phone = '';
				var check = '';

				$(controls.get().reverse()).each(function(){
					var input = $(this);
					var type = input.attr('type');

					if(type == 'text'){
						var value = input.val();

						if(value){
							if(input.prop('name') == 'ORDER_PROP_1')
								name = value;
							else if(input.prop('name') == 'ORDER_PROP_2')
								last_name = value;
							else if(input.prop('name') == 'ORDER_PROP_3')
								email = value;
							else if(input.prop('name') == 'ORDER_PROP_4')
								phone = value;
						}
					}
					else if(type == 'checkbox'){
						if(input.prop('checked')){
							var value = input.parent().find('label').html();
							check = value;
						}
					}
				});


				if(check);
					finalBlock.prepend('<div class="order-card__descr"><div class="order-card__final-text order-card__final-text_light">' + check + '</div></div>');

				if(email || phone);
					finalBlock.prepend('<div class="order-card__final-text">' + email + ' / ' + phone + '</div>');

				if(name || last_name);
					finalBlock.prepend('<div class="order-card__final-text">' + last_name + ' ' + name + '</div>');

				finalBlock.show();
				propBlock.hide();
			}

			function showProperties(){
				var finalBlock = $('.order-card_type_personal .order-card__final-info');
				var propBlock = $('.order-card_type_personal .order-card__form');

				finalBlock.hide();
				propBlock.show();
			}

			function hideDeliveries(){
				var block = $('.order-card_type_delivery');
				var finalBlock = $('.order-card__final-info', block);
				var otherBlocks = $('div.order-card__city, div.order-card__choose, div.order-card__delivery-info, .order-card__form', block);

				var delivery = $('#DELIVERY_ID_CHECK');
				var deliveryId = 0;

				if(delivery)
					deliveryId = parseInt(delivery.val());


				if(deliveryId){
					otherBlocks.hide();
					finalBlock.show();
					block.addClass('order-card_checked');
				}
				else{
					otherBlocks.hide();
					finalBlock.hide();
					block.removeClass('order-card_checked');
				}
			}
			function showDeliveries(){
				var block = $('.order-card_type_delivery');
				var finalBlock = $('.order-card__final-info', block);
				var otherBlocks = $('div.order-card__city, div.order-card__choose, div.order-card__delivery-info, .order-card__form', block);

				otherBlocks.show();
				finalBlock.hide();

				block.addClass('order-card_checked');
			}

			function hidePayments(){
				var block = $('.order-card_type_payment');
				var finalBlock = $('.order-card__final-info', block);
				var otherBlocks = $('.order-card__content > div', block).not('.order-card__final-info');

				var paysystem = $('#PAYSYSTEM_ID_CHECK');
				var paysystemId = 0;

				if(paysystem)
					paysystemId = parseInt(paysystem.val());


				if(paysystemId){
					otherBlocks.hide();
					finalBlock.show();
					block.addClass('order-card_checked');
				}
				else{
					otherBlocks.hide();
					finalBlock.hide();
					block.removeClass('order-card_checked');
				}
			}

			function showPayments(){
				var block = $('.order-card_type_payment');
				var finalBlock = $('.order-card__final-info', block);
				var otherBlocks = $('.order-card__content > div', block).not('.order-card__final-info');

				otherBlocks.show();
				finalBlock.hide();

				block.addClass('order-card_checked');
			}

			function showBlock(step){
				var blocks = $('.order__item');
				var curStep = $('#ORDER_FORM input[name=STEP]').val();
				curStep = parseInt(curStep);

				if(step == curStep)
					return;


				if(step){
					var is_valid = true;
					blocks.each(function(){
						var block = $(this);
						var number = block.data('key');

						if(number == curStep){
							if(!validate(block))
								is_valid = false;
						}
					});

					if(!is_valid)
						return false;
				}

				step = step || curStep;

				blocks.each(function(){
					var block = $(this);
					var number = block.data('key');

					if(number < step){
						block.attr('data-number', '✓').addClass('is-active');
					}
					//else
						//block.attr('data-number', number).removeClass('is-active');
				});

				if(step != 1){
					hideProperties();
				}
				if(step != 2){
					hideDeliveries();
				}
				if(step != 3){
					hidePayments();
				}

				if(step == 1){
					showProperties();
				}
				else if(step == 2){
					showDeliveries();
				}
				else if(step == 3){
					showPayments();
				}

				$('#ORDER_FORM input[name=STEP]').val(step);
			}

			function updatePeriod(){
				var period = $('.choose-card.is-active .choose-card__period_hide');

				if(period.length){
					var select = $('.choose-card__period.postamate select');

					if(select){
						var card = period.closest('.js-card-choose.is-active');

						if(card.length){
							var link = $('a', period);
							$('.order-card__delivery-info .order-card__paragraph').append(link);
						}

						changePostamate();

						select.on('change', function(){
							changePostamate();
						});
					}

					select = $('.choose-card__period.pvz select');

					if(select){
						var card = period.closest('.js-card-choose.is-active');

						if(card.length){
							var link = $('a', period);
							$('.order-card__delivery-info .order-card__paragraph').append(link);
						}

						changePvz();

						select.on('change', function(){
							changePvz();
						});
					}
				}
			}

			function updatePvz(){
				if($('#ID_DELIVERY_ID_24').is(':checked')){
					var address = $('.choose-card.is-active .choose-card__period').text();

					if(address == 'Не задана точка ПВЗ'){
						open_GURU_map();
					}
					else{
						var pvz = $('#ORDER_PROP_12').val();
						if(pvz)
							$('.order-card__delivery-text').html(pvz);
					}
				}
			}

			function changePostamate(){
				var period = $('.postamate .choose-card__period_hide');
				var checked = period.closest('.js-card-choose').hasClass('is-active');

				if(period.length){
					var option = $('select option:selected', period);

					var infoBlock = $('.order-card_type_delivery .order-card__delivery-info');
					var finalBlock = $('.order-card_type_delivery .order-card__final-info');

					var from = option.data('minterm');
					var to = option.data('maxterm');

					$('.choose-card__period.postamate > span').remove();
					if(from == to){
						$('.choose-card__period.postamate').append('<span>от ' + from + ' дней после оформления заказа</span>');
						if(checked)
							$('.choose-card__period', finalBlock).text('от ' + from + ' дней после оформления заказа');
					}
					else{
						$('.choose-card__period.postamate').append('<span>от ' + from + '-' + to + ' дней после оформления заказа</span>');
						if(checked)
							$('.choose-card__period', finalBlock).text('от ' + from + '-' + to + ' дней после оформления заказа');
					}

					if(checked){
						var name = option.data('name-point');
						var desc = option.data('desc-point');
						$('.order-card__delivery-text', infoBlock).text(name);
						//$('.order-card__delivery-descr', infoBlock).html(desc);
					}
				}
			}

			function changePvz(){
				var period = $('.pvz .choose-card__period_hide');
				var checked = period.closest('.js-card-choose').hasClass('is-active');

				if(period.length){
					var option = $('select option:selected', period);

					var infoBlock = $('.order-card_type_delivery .order-card__delivery-info');
					var finalBlock = $('.order-card_type_delivery .order-card__final-info');

					var from = option.data('minterm');
					var to = option.data('maxterm');

					$('.choose-card__period.pvz > span').remove();
					if(from == to){
						$('.choose-card__period.pvz').append('<span>от ' + from + ' дней после оформления заказа</span>');
						if(checked)
							$('.choose-card__period', finalBlock).text('от ' + from + ' дней после оформления заказа');
					}
					else{
						$('.choose-card__period.pvz').append('<span>от ' + from + '-' + to + ' дней после оформления заказа</span>');
						if(checked)
							$('.choose-card__period', finalBlock).text('от ' + from + '-' + to + ' дней после оформления заказа');
					}

					if(checked){
						var name = option.data('name-point');
						var desc = option.data('desc-point');
						$('.order-card__delivery-text', infoBlock).text(name);
					}
				}
			}

			function setDelivery(input, id){
				if(BXFormSubmit){
					$('.order-card_type_delivery .choose-card').removeClass('is-active');
					$(input).closest('.choose-card').addClass('is-active');
				}

				BX('DELIVERY_ID_CHECK').value = id;
				BX('ID_DELIVERY_ID_' + id).checked=true;
				
				$('#YMapsID').attr('open', 'open');

				submitForm();

				return false;
			}

			function setPayment(input, id){
				$('.order-card_type_payment .choose-card').removeClass('checked');
				$(input).closest('.choose-card').addClass('checked');
				BX('PAYSYSTEM_ID_CHECK').value=id;
				BX('ID_PAY_SYSTEM_ID_' + id).checked=true;
				submitForm();
				return false;
			}

			updatePeriod();
			showBlock();

			$('#order_form_content .js-field-requied').on('change', function(){
				validateInput($(this));
			});

			$(function(){
				var addressGuru = function(){
					var controlName = 'ORDER_PROP_7';
					var getResult = function(value){
						var key='508b0cc09bfaf495e5fcb308539db357';//Ваш ключ
						var id='2490';//Ваш id
						var control = $('input[name="' + controlName + '"]');
						if(control.length){
							var parent = control.parent();
							var response = $('.response_addr', parent);
							if(!response.length){
								response = $('<div class="response_addr"></div>');
								parent.append(response);
								
								control.on('blur', function(){
									setTimeout(function(){
										if(!control.is(':focus')){
											response.hide();
										}
									}, 300);
								}).on('focus', function(){
									if(response.text().length){
										response.show();
									}
								});
							}
							
							var loader = $('.loader', parent);
							if(!loader.length){
								loader = $('<div class="loader"></div>');
								parent.append(loader);
							}
							
							var city = $('input.bx-ui-sls-fake').val();
							if(city.length){
								value = city + ', ' + value;
							}
							
							loader.show();
							$.post("https://api.dostavka.guru/client/dd_2_ch.php", { str: value, key: key, id: id }, function(data){
								loader.hide();
								if(data.length){
									response.html(data).show();
									
									$('.addr_li', response).on('click', function() {
										var item = $(this);
										var address = item.attr('param_adres');
										control.val(address).focus();
										
										var obAddress = {
											'zip' : item.attr('param_zip'),
											'region' : item.attr('param_reg'),
											'area' : item.attr('param_rai'),
											'city' : item.attr('param_cit'),
											'type' : item.attr('param_type'),
											'street' : item.attr('param_stre'),
											'kor' : item.attr('param_kor'),
											'house' : item.attr('param_hou'),
											'flar' : item.attr('param_fla'),
											'kladr' : item.attr('param_kladr')
										}
										
										$('#GURU').val(JSON.stringify(obAddress));
										
										response.html('').hide();
										return false;
									});
								}
								else{
									response.hide();
								}
							});
						}
					}
					
					$(document).keyup(function(event){
						var focus_name=$("input:focus").attr('id');
						var focus_value=$("input:focus").val();
						if(focus_name == controlName){
							if(focus_value.length>3){
								getResult(focus_value);
							}
						}
					});
				}
				addressGuru();
			});
			</script>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
				<?=bitrix_sessid_post()?>
				<input type="hidden" name="STEP" value="1" />
				<input type="hidden" name="GURU" id="GURU" value="" />
				<div id="order_form_content">
				<?
			}
			else
			{
				$APPLICATION->RestartBuffer();
			}

			if($_REQUEST['PERMANENT_MODE_STEPS'] == 1)
			{
				?>
				<input type="hidden" name="PERMANENT_MODE_STEPS" value="1" />
				<?
			}

			if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
			{
				foreach($arResult["ERROR"] as $v)
					echo ShowError($v);
				?>
				<script type="text/javascript">
					top.BX.scrollToNode(top.BX('ORDER_FORM'));
				</script>
				<?
			}

			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");

			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
			?>

			<?if($_POST["is_ajax_post"] != "Y")
			{
				?>
					</div>
					<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
					<input type="hidden" name="json" value="Y">
				</form>
				<?
				if($arParams["DELIVERY_NO_AJAX"] == "N")
				{
					?>
					<div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
					<?
				}
			}
			else
			{
				?>
				<script type="text/javascript">
					top.BX('confirmorder').value = 'Y';
					top.BX('profile_change').value = 'N';
				</script>
				<?
				die();
			}
		}
	}
	?>
	</div>
</div>

<?if(CSaleLocation::isLocationProEnabled()):?>

	<div style="display: none">
		<?// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.location.selector.steps",
			".default",
			array(
			),
			false
		);?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.location.selector.search",
			".default",
			array(
			),
			false
		);?>
	</div>

<?endif?>

<script>
	function include( filename ) {
		if(this.isInclude)
			return;
		this.isInclude = true;
		
		var js = document.createElement('script');
		js.setAttribute('type', 'text/javascript');
		js.setAttribute('src', filename);
		js.setAttribute('defer', 'defer');
		document.getElementsByTagName('HEAD')[0].appendChild(js);

		// save include state for reference by include_once
		var cur_file = {};
		cur_file[window.location.href] = 1;

		if (!window.php_js) window.php_js = {};
		if (!window.php_js.includes) window.php_js.includes = cur_file;
		if (!window.php_js.includes[filename]) {
		window.php_js.includes[filename] = 1;
		} else {
		window.php_js.includes[filename]++;
		}

		return window.php_js.includes[filename];
	}

	BX.addCustomEvent('onAjaxSuccess', afterFormReload);
	
	function afterFormReload()
	{
		include('/guru_files/dostavka_guru_app_pvz.js?v=3');

	}
</script>