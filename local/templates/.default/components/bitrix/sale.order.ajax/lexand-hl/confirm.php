<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$notShowBuyButton  = $arResult["PAY_SYSTEM"]["ID"] == 3;
?>
<?
if (!empty($arResult["ORDER"]))
{
	?>
	<div class="payment-success page__content">
		<div class="paper-ticket paper-ticket_style_success">
			<div class="paper-ticket__icon-wrap">
				<svg class="paper-ticket__icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_check-mark"></use></svg>
			</div>
            <?if (array_key_exists('res', $_REQUEST)){?>
                <?if((int)$_REQUEST['res'] === 1){?>
                    <div class="paper-ticket__title">Заказ успешно оплачен</div>
                    <div class="paper-ticket__sub-title">Номер заказа:
                        <span class="paper-ticket__order-number"><?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></span>
                </div>
                <?}elseif((int)$_REQUEST['res'] === 0){?>
        <div class="paper-ticket__title">Заказ не оплачен, обратитесь к менеджеру.</div>
        <div class="paper-ticket__sub-title">Номер заказа:
            <span class="paper-ticket__order-number"><?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></span>
        </div>
                    <?}?>
            <?}else{?>
			<div class="paper-ticket__title">Заказ успешно оформлен</div>
			<div class="paper-ticket__sub-title">Номер заказа:
				<span class="paper-ticket__order-number"><?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></span>
			</div>
			 <?if(!$notShowBuyButton):?>
				<?
				if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
				{
					$service = \Bitrix\Sale\PaySystem\Manager::getObjectById($arResult["ORDER"]['PAY_SYSTEM_ID']);

					if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
					{
						?>
						<a href="#pay" onclick="window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>&PAYMENT_ID=<?=$arResult['ORDER']["PAYMENT_ID"]?>'); return false;" class="paper-ticket__link paper-ticket__link_style_goto">Оплатить заказ</a>
						<?
					}
					else
					{
						?>
						<div style="display:none;">
						<?
						if ($service)
						{
							/** @var \Bitrix\Sale\Order $order */
							$order = \Bitrix\Sale\Order::load($arResult["ORDER_ID"]);

							/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
							$paymentCollection = $order->getPaymentCollection();

							/** @var \Bitrix\Sale\Payment $payment */
							foreach ($paymentCollection as $payment)
							{
								if (!$payment->isInner())
								{
									$context = \Bitrix\Main\Application::getInstance()->getContext();
									$service->initiatePay($payment, $context->getRequest());
									break;
								}
							}
						}
						?>
						</div>
                        <a href="/all_sales.php?order=<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?>" class="price-badge__btn">Оплатить заказ</a>
<!--						<a href="#pay" onclick="$('.payment-success form').submit();return false;" class="paper-ticket__link paper-ticket__link_style_goto">Оплатить заказ</a>-->
						<?
					}
				}
				?>
			<?endif;?>
            <?}?>
		</div>
	</div>
	<?
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
