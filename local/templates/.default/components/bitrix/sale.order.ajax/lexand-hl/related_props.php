<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!empty($arResult["ORDER_PROP"]["RELATED"])){
	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props_format.php");
	if($step != 2)
		$style="display:none";
	?>
	<div class="order-card__form" style="<?=$style?>">
		<div class="order-card__form-head">Адрес доставки</div>
		<?=PrintPropsForm($arResult["ORDER_PROP"]["RELATED"], $arParams["TEMPLATE_LOCATION"])?>
	</div>
<?
}
?>