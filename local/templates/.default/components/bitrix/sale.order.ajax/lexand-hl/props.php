<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props_format.php");
if (count($arResult["ORDER_PROP"]["USER_PROFILES"]) == 1)
{
	foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
	{
		?>
		<input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?=$arUserProfiles["ID"]?>" />
		<?
	}
}
else
{
	foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
	{
		if ($arUserProfiles["CHECKED"]=="Y"){
		?>
		<input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?=$arUserProfiles["ID"]?>" />
		<?
		}
	}
}
$isActive = $_POST['STEP'] > 1 || (int)$_POST["DELIVERY_ID_CHECK"] || (int)$_POST["PAYSYSTEM_ID_CHECK"];
?>
<div data-number="<?if($isActive):?>✓<?else:?>1<?endif;?>" data-key="1" class="order__item<?if($isActive):?> is-active<?endif;?>">
	<div class="order-card order-card_type_personal">
		<div class="order-card__row order__item_title order-card__row_theme_highlight">
			<div class="heading">Личные данные</div>
		</div>
		<div class="order-card__content">
			<?if(!$USER->IsAuthorized()):?>
			<div class="order-card__row">
				<span class="order-card__text">Уже зарегистрированы?</span>
				<a href="javascript:void(0);" data-popup="popupUser" class="order-card__link js-popup-open">Войти в аккаунт</a>
			</div>
			<?endif;?>
			<div class="order-card__form">
				<?
				PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
				PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"]);
				?>
				<div class="order-card__form-submit js-form-submit">
					<a class="btn btn_style_key btn_shape_rectangle" onclick="showBlock(2);">
						<div class="btn__text">Продолжить</div>
					</a>
				</div>
			</div>
			<div class="order-card__final-info" style="display:none;">
				<div class="order-card__final-btn">
					<a class="btn btn_style_default btn_shape_min-rectangle" onclick="showBlock(1);">
						<div class="btn__text">Изменить</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?if(!CSaleLocation::isLocationProEnabled()):?>
	<div style="display:none;">

		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.ajax.locations",
			$arParams["TEMPLATE_LOCATION"],
			array(
				"AJAX_CALL" => "N",
				"COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
				"REGION_INPUT_NAME" => "REGION_tmp",
				"CITY_INPUT_NAME" => "tmp",
				"CITY_OUT_LOCATION" => "Y",
				"LOCATION_VALUE" => "",
				"ONCITYCHANGE" => "submitForm()",
			),
			null,
			array('HIDE_ICONS' => 'Y')
		);?>

	</div>
<?endif?>
