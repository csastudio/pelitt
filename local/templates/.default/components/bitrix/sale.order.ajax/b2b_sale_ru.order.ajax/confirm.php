<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y")
	$APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
?>
<? if (!empty($arResult["ORDER"])): ?>

	<div class="payment-success page__content">
		<div class="paper-ticket paper-ticket_style_success">
			<div class="paper-ticket__icon-wrap">
				<svg class="paper-ticket__icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_check-mark"></use></svg>
			</div>
			<div class="paper-ticket__title">
				<?=Loc::getMessage("SOA_ORDER_SUC", array(
					"#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"],
					"#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]
				))?>
				<? if (!empty($arResult['ORDER']["PAYMENT_ID"])): ?>
					<?=Loc::getMessage("SOA_PAYMENT_SUC", array(
						"#PAYMENT_ID#" => $arResult['PAYMENT'][$arResult['ORDER']["PAYMENT_ID"]]['ACCOUNT_NUMBER']
					))?>
				<? endif ?>
				<br /><br />
			</div>
			<div class="paper-ticket__sub-title">
				<?=Loc::getMessage("SOA_ORDER_SUC1", array("#LINK#" => $arParams["PATH_TO_PERSONAL"]))?>
			</div>
			<div class="paper-ticket__contact">



                <?if (SITE_DIR == '/ru/'){?>
                    <div style="padding: 20px"><label style="font-size: 12px"><input type="checkbox" checked>
                            Отправляя заказ, Вы соглашаетесь принимать телефонные звонки,SMS-сообщения и электронные письма, связанные с Вашим заказом и подтверждаете, что ознакомились и&nbsp;согласны с
                            <a href="/ru/agreement/" class="breadcrumbs__link">Пользовательским соглашением</a>,
                            <a href="/ru/privacy-policy/" class="breadcrumbs__link">Политикой конфиденциальности</a> и
                            <a href="/ru/rules/" class="breadcrumbs__link">Правилами обмена и возврата</a>
                        </label>
                    </div>
                    <a href="/all_sales.php" class="price-badge__btn">Оплатить заказ</a>
                <?}else{?>
				<?
				if (!empty($arResult["PAYMENT"]))
				{
					foreach ($arResult["PAYMENT"] as $payment)
					{
						if ($payment["PAID"] != 'Y')
						{
							if (!empty($arResult['PAY_SYSTEM_LIST'])
								&& array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
							)
							{
								$arPaySystem = $arResult['PAY_SYSTEM_LIST'][$payment["PAY_SYSTEM_ID"]];

							if (empty($arPaySystem["ERROR"]))
							{
								?>
							<br /><br />

								<div class="pay_name"><?= Loc::getMessage("SOA_PAY") ?></div>
							<?= CFile::ShowImage($arPaySystem["LOGOTIP"], 100, 100, "border=0\" style=\"width:100px\"", "", false) ?>
								<div class="paysystem_name"><?= $arPaySystem["NAME"] ?></div>
							<br/>

							<? if (strlen($arPaySystem["ACTION_FILE"]) > 0 && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y"): ?>
							<?
							$orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
							$paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
							?>
								<script>
									window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
								</script>
							<?=Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber))?>
							<? if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF']): ?>
							<br/>
							<? endif ?>
							<? else: ?>
								<?=$arPaySystem["BUFFERED_OUTPUT"]?>
							<? endif ?>
							<?
							}
							else
							{
							?>
								<span style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
							<?
							}
							}
							else
							{
							?>
								<span style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
								<?
							}
						}
					}
				}
				?>
        <?}?>
			</div>
		</div>
	</div>

<? else: ?>

	<div class="payment-error page__content">
		<div class="paper-ticket">
			<div class="paper-ticket__icon-wrap">
				<svg class="paper-ticket__icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_exclamation-mark"></use></svg>
			</div>
			<div class="paper-ticket__sub-title">
				<b><?=Loc::getMessage("SOA_ERROR_ORDER")?></b>
				<br /><br />

				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST", array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST1")?>
			</div>
		</div>
	</div>

<? endif ?>