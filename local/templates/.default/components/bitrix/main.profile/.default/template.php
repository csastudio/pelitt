<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div class="account__wrap">
	<div class="account__topic">
		<div class="topic"><?=GetMessage('PROFILE')?></div>
	</div>
	<div class="account__fields">
		<div class="account__data">
			<div class="account__personal">
				<div class="personal-data js-personal-data">
					<div class="heading"><?=GetMessage('PERSONAL_DATA')?></div>
					<?if($_POST['form'] == 'personal'):?>
					<?
					if (strlen($arResult["strProfileError"])){	
						ShowError($arResult["strProfileError"]);
						echo '<br />';
					}
					?>
					<?
					if ($arResult['DATA_SAVED'] == 'Y'){
						ShowNote(GetMessage('PROFILE_DATA_SAVED'));
						echo '<br />';
					}
					?>
					<?endif;?>
					<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
						<?=$arResult["BX_SESSION_CHECK"]?>
						<input type="hidden" name="lang" value="<?=LANG?>" />
						<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
						<input type="hidden" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" />
						<input type="hidden" name="form" value="personal" />
						
						<div class="personal-data__form-row">
							<div class="field field_style_mine-shaft js-field">
								<input type="text" placeholder="<?=GetMessage('NAME')?>" name="NAME" maxlength="50" class="field__input js-field-input" value="<?=$arResult["arUser"]["NAME"]?>" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						<div class="personal-data__form-row">
							<div class="field field_style_mine-shaft js-field">
								<input type="text" placeholder="<?=GetMessage('LAST_NAME')?>" name="LAST_NAME" maxlength="50" class="field__input js-field-input" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						<div class="personal-data__form-row">
							<div class="field field_style_mine-shaft js-field">
								<input type="text" placeholder="<?=GetMessage('EMAIL')?>" name="EMAIL" maxlength="50" class="field__input js-field-input" value="<?=$arResult["arUser"]["EMAIL"]?>" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						<div class="personal-data__form-row">
							<div class="field field_style_mine-shaft js-field">
								<input type="text" placeholder="<?=GetMessage('USER_PHONE')?>" name="PERSONAL_PHONE" maxlength="50" class="field__input js-field-input" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						<div class="personal-data__form-accept">
							<div class="checkbox checkbox_style_personal-data">
							<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
								<??>
								<?if($arUserField['FIELD_NAME'] == 'UF_SUBSCRIBE'):?>
									<?$APPLICATION->IncludeComponent(
										"bitrix:system.field.edit",
										"checkbox",
										array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), $component, array("HIDE_ICONS"=>"Y"));?>
									<label for="personal-data-accept" class="checkbox__label"><?=$arUserField["EDIT_FORM_LABEL"]?></label>
								<?endif;?>
							<?endforeach;?>
							</div>
						</div>
						<div class="personal-data__form-submit js-form-submit">
							<button type="submit" name="save" value="Y" data-popup="" class="btn btn_style_green-key btn_shape_ellipse">
								<div class="btn__text"><?=GetMessage('SAVE')?></div>
							</button>
						</div>
					</form>
				</div>
			</div>
			
<!--			<div class="account__delivery">-->
<!--				<div class="delivery-data js-delivery-data">-->
<!--					<div class="heading heading_style_delivery-data">--><?//=GetMessage('DELIVERY')?><!--</div>-->
<!--					--><?//if($_POST['form'] == 'delivery'):?>
<!--					--><?//
//					if (strlen($arResult["strProfileError"])){
//						ShowError($arResult["strProfileError"]);
//						echo '<br />';
//					}
//					?>
<!--					--><?//
//					if ($arResult['DATA_SAVED'] == 'Y'){
//						ShowNote(GetMessage('PROFILE_DATA_SAVED'));
//						echo '<br />';
//					}
//					?>
<!--					--><?//endif;?>
<!--					-->
<!--					<form method="post" name="form1" action="--><?//=$arResult["FORM_TARGET"]?><!--" enctype="multipart/form-data" class="change-password__form">-->
<!--						--><?//=$arResult["BX_SESSION_CHECK"]?>
<!--						<input type="hidden" name="lang" value="--><?//=LANG?><!--" />-->
<!--						<input type="hidden" name="ID" value=--><?//=$arResult["ID"]?><!-- />-->
<!--						<input type="hidden" name="LOGIN" value="--><?//=$arResult["arUser"]["LOGIN"]?><!--" />-->
<!--						<input type="hidden" name="EMAIL" value="--><?//=$arResult["arUser"]["EMAIL"]?><!--" />-->
<!--						<input type="hidden" name="form" value="delivery" />-->
<!--						<div class="delivery-data__form-row">-->
<!--							<div class="field field_style_mine-shaft js-field">-->
<!--								<input type="text" name="UF_DELIVERY_STREET" value="--><?//=$arResult["arUser"]["UF_DELIVERY_STREET"]?><!--" placeholder="--><?//=GetMessage('STREET')?><!--" class="field__input js-field-input">-->
<!--								<div class="field__validate"></div>-->
<!--								<div class="field__answer"></div>-->
<!--							</div>-->
<!--						</div>-->
<!--						<div class="delivery-data__form-row delivery-data__form-row_column">-->
<!--							<div class="field field_style_mine-shaft js-field field field_min-width">-->
<!--								<input type="text" name="UF_DELIVERY_HOME" value="--><?//=$arResult["arUser"]["UF_DELIVERY_HOME"]?><!--" placeholder="--><?//=GetMessage('HOUSE')?><!--" class="field__input js-field-input">-->
<!--								<div class="field__validate"></div>-->
<!--								<div class="field__answer"></div>-->
<!--							</div>-->
<!--							<div class="field field_style_mine-shaft js-field field field_min-width">-->
<!--								<input type="text" name="UF_DELIVERY_HOUSE" value="--><?//=$arResult["arUser"]["UF_DELIVERY_HOUSE"]?><!--" placeholder="--><?//=GetMessage('BLOCK')?><!--" class="field__input js-field-input">-->
<!--								<div class="field__validate"></div>-->
<!--								<div class="field__answer"></div>-->
<!--							</div>-->
<!--							<div class="field field_style_mine-shaft js-field field field_min-width">-->
<!--								<input type="text" name="UF_DELIVERY_APART" value="--><?//=$arResult["arUser"]["UF_DELIVERY_APART"]?><!--" placeholder="--><?//=GetMessage('FLAT')?><!--" class="field__input js-field-input">-->
<!--								<div class="field__validate"></div>-->
<!--								<div class="field__answer"></div>-->
<!--							</div>-->
<!--						</div>-->
<!--						<div class="delivery-data__form-row">-->
<!--							<div class="field field_style_mine-shaft js-field">-->
<!--								<textarea name="UF_DELIVERY_COMMENT" placeholder="--><?//=GetMessage('COMMENT')?><!--" class="field__input field__input_textarea js-field-input">--><?//=$arResult["arUser"]["UF_DELIVERY_COMMENT"]?><!--</textarea>-->
<!--							</div>-->
<!--						</div>-->
<!--						<div class="delivery-data__form-submit js-form-submit">-->
<!--							<button type="submit" name="save" value="Y" data-popup="" class="btn btn_style_green-key btn_shape_ellipse">-->
<!--								<div class="btn__text">--><?//=GetMessage('SAVE')?><!--</div>-->
<!--							</button>-->
<!--						</div>-->
<!--					</form>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
		
<!--		<div class="account__user">-->
<!--			<div class="account__item">-->
<!--				--><?//
//				if($arResult["SOCSERV_ENABLED"])
//				{
//					$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
//							"SHOW_PROFILES" => "Y",
//							"ALLOW_DELETE" => "Y"
//						),
//						$component
//					);
//				}
//				?>
<!--			</div>-->

			<div class="account__item">
				<div class="change-password js-change-password change-password_state_active">
					<div class="heading"><?=GetMessage('PASS_CHANGE')?></div>
					
					<?if($_POST['form'] == 'password'):?>
					<?
					if (strlen($arResult["strProfileError"])){	
						ShowError($arResult["strProfileError"]);
						echo '<br />';
					}
					?>
					<?
					if ($arResult['DATA_SAVED'] == 'Y'){
						ShowNote(GetMessage('PROFILE_DATA_SAVED'));
						echo '<br />';
					}
					?>
					<?endif;?>
					
					<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data" class="change-password__form">
						<?=$arResult["BX_SESSION_CHECK"]?>
						<input type="hidden" name="lang" value="<?=LANG?>" />
						<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
						<input type="hidden" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" />
						<input type="hidden" name="EMAIL" value="<?=$arResult["arUser"]["EMAIL"]?>" />
						<input type="hidden" name="form" value="password" />
						
						<div class="change-password__form-row">
							<div class="field js-field">
								<input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" placeholder="<?=GetMessage('NEW_PASSWORD')?>" class="field__input js-field-input" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						<div class="change-password__form-row">
							<div class="field js-field">
								<input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" placeholder="<?=GetMessage('NEW_PASSWORD_CONFIRM')?>" class="field__input js-field-input" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						
						<div class="change-password__form-submit js-form-submit">
							<button type="submit" name="save" value="Y" data-popup="" class="btn btn_style_green-key btn_shape_ellipse">
								<div class="btn__text"><?=GetMessage('SAVE')?></div>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>