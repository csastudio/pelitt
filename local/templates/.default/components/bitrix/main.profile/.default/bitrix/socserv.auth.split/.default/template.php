<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if($arResult['ERROR_MESSAGE'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="link-soc">
<?
$arServices = $arResult["AUTH_SERVICES_ICONS"];
if(!empty($arResult["AUTH_SERVICES"]))
{
?>
	<div class="heading">Привязать соцсети</div>
	<?
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
		array(
			"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
			"CURRENT_SERVICE"=>$arResult["CURRENT_SERVICE"],
			"AUTH_URL"=>$arResult['CURRENTURL'],
			"POST"=>$arResult["POST"],
			"SHOW_TITLES"=>'N',
			"FOR_SPLIT"=>'Y',
			"AUTH_LINE"=>'N',
		),
		$component,
		array("HIDE_ICONS"=>"Y")
	);
}
if(isset($arResult["DB_SOCSERV_USER"]) && $arParams["SHOW_PROFILES"] != 'N')
{
?>
	<?
	$arIcontToCode = array(
		'facebook' => 'facebook',
		'twitter' => 'facebook',
		'vkontakte' => 'vk',
		'odnoklassniki' => 'ok'
	);	
	
	foreach($arResult["DB_SOCSERV_USER"] as $key => $arUser)
	{
		
		if(!$icon = htmlspecialcharsbx($arIcontToCode[strtolower($arUser["EXTERNAL_AUTH_ID"])]))
			$icon = 'openid';
		$authID = ($arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"]) ? $arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"] : $arUser["EXTERNAL_AUTH_ID"];
		//p($arUser);
		?>
	<div class="subscribe-social">
		<div class="subscribe-social__social subscribe-social__social_<?=$icon?>">
			<svg class="subscribe-social__icon subscribe-social__icon_<?=$icon?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/images/icon.svg#icon_<?=$icon?>"></use></svg>
			<div class="subscribe-social__text"><?=$authID?></div>
		</div>
		<?if (in_array($arUser["ID"], $arResult["ALLOW_DELETE_ID"])):?>
		<a href="<?=htmlspecialcharsbx($arUser["DELETE_LINK"])?>" class="subscribe-social__link subscribe-social__link_<?=$icon?>" onclick="return confirm('<?=GetMessage("SS_PROFILE_DELETE_CONFIRM")?>')" title="Отвязать соцсеть">Отвязать соцсеть</a>
		<?endif;?>
	</div>	
	<?
	}
}
?>
</div>