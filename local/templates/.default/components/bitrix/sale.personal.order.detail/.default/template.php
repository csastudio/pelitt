<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

	<?$component = $this->__component;?>
	<?if($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])):?>
		<?$APPLICATION->AuthForm('', false, false, 'N', false);?>
	<?endif?>

<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>
	<div class="popup-product__order">
		<div class="popup-product__heading popup-product__heading_bold"><?=$arResult["NAMES"][$arParams["LANG"]]['ORDER'].' '.$arResult["ACCOUNT_NUMBER"]?></div>
		<div class="popup-product__heading popup-product__heading_date"><?=$arResult["NAMES"][$arParams["LANG"]]['OF'].' '.$arResult["DATE_STATUS_FORMATED"]?></div>
		<div class="popup-product__heading popup-product__heading_status"><?=$arResult["STATUSES"][$arResult["STATUS_ID"]]?></div>
	</div>
	<div class="popup-product__list">
		<?if (isset($arResult["BASKET"])):?>
			<?foreach($arResult["BASKET"] as $prod):?>
				<div class="popup-product__list-item">
					<div class="popup-product__item">
						<div class="popup-product__item-content">
							<img src="<?=$prod["PICTURE"]["SRC"]?>" class="popup-product__image" alt="<?=$prod["NAME"]?>" role="presentation" />
							<div class="popup-product__content">
								<div class="popup-product__name"><?=$prod["NAME"]?></div>
							</div>
						</div>
						<div class="popup-product__item-quantity">
							<div class="popup-product__quantity"><?=$prod["QUANTITY"].' '.$arResult["NAMES"][$arParams["LANG"]]['COUNT']?></div>
							<div class="popup-product__price"><?=$prod["PRICE_FORMATED"]?></div>
						</div>
					</div>
				</div>
			<?endforeach;?>
		<?endif;?>
		<div class="popup-product__list-item popup-product__list-item_style_delivery">
			<div class="popup-product__item">
				<div class="popup-product__item-content">
					<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/basket-list/delivery.jpg" class="popup-product__image" alt="" role="presentation" />
					<div class="popup-product__item-delivery">
						<?foreach ($arResult['SHIPMENT'] as $shipment):?>
							<div class="popup-product__section"><?=$arResult["NAMES"][$arParams["LANG"]]['DELIVERY']?></div>
							<div class="popup-product__name"><?=$shipment["DELIVERY"]["NAME"]?></div>
						<?endforeach;?>
					</div>
				</div>
				<div class="popup-product__price-delivery"><?=$arResult["PRICE_DELIVERY_FORMATED"]?></div>
			</div>
		</div>
	</div>
	<div class="popup-product__content-footer">
		<div class="popup-product__bg-wrap">
			<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/basket-list/bg-price.png" class="popup-product__bg" alt="" role="presentation" />
			<div class="popup-product__contents">
				<div class="popup-product__text-content"><?=GetMessage("SPOD_ALL_SUM");?></div>
				<div class="popup-product__text-content"><?=$arResult["PRICE_FORMATED"]?></div>
			</div>
		</div>
	</div>

<?endif?>