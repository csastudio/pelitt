<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<ul class="breadcrumbs">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '<li class="breadcrumbs__item"><a href="'.$arResult[$index]["LINK"].'" class="breadcrumbs__link">'.$title.'</a></li>';
	}
	else
	{
		$strReturn .= '<li class="breadcrumbs__item breadcrumbs__item_last"><span class="breadcrumbs__link">'.$title.'</span></li>';
	}
}

$strReturn .= '</ul></div></div>';

return $strReturn;
