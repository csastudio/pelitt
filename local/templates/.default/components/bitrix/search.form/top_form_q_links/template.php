<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

	<div class="popup__wrapper">
		<div class="popup-search">
			<form action="<?=$arResult["FORM_ACTION"]?>">
				<div class="popup-search__search-box">
					<?if($arParams["USE_SUGGEST"] === "Y"):?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:search.suggest.input",
							"",
							array(
								"NAME" => "q",
								"VALUE" => "",
								"INPUT_SIZE" => 15,
								"DROPDOWN_SIZE" => 10,
							),
							$component, array("HIDE_ICONS" => "Y")
						);?>
					<?else:?>
						<input id="title-search-input" type="text" name="q" value="" size="15" maxlength="50"  placeholder="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" <?php /*autofocus="autofocus"*/ ?> class="popup-search__field" />
					<?endif;?>
					<div class="popup-search__icon">
						<div class="search-preloader">
							<div class="search-preloader__layer">
								<div class="search-preloader__circle-clipper search-preloader__circle-clipper_side_left">
									<div class="search-preloader__circle"></div>
								</div>
								<div class="search-preloader__gap">
									<div class="search-preloader__circle"></div>
								</div>
								<div class="search-preloader__circle-clipper search-preloader__circle-clipper_side_right">
									<div class="search-preloader__circle"></div>
								</div>
							</div>
						</div>
						<svg class="popup-search__icon-search"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_search"></use></svg>
					</div>
				</div>
			</form>
			<div class="popup-search__wrap" id="title-search">
				<?$APPLICATION->IncludeComponent(
					"csa:search.title",
					"quick_search",
					Array(
						"CATEGORY_0" => array("iblock_shop"),
						"CATEGORY_0_TITLE" => "",
						"CATEGORY_0_iblock_catalog" => array(),
						"CATEGORY_0_iblock_shop" => array(),
						"CATEGORY_OTHERS_TITLE" => "Разное",
						"CHECK_DATES" => "N",
						"CONTAINER_ID" => "title-search",
						"INPUT_ID" => "title-search-input",
						"NUM_CATEGORIES" => "1",
						"ORDER" => "date",
						"PAGE" => "#SITE_DIR#search/index.php",
						"SHOW_INPUT" => "N",
						"SHOW_OTHERS" => "N",
						"TOP_COUNT" => "5",
						"USE_LANGUAGE_GUESS" => "Y"
					)
				);?>
				<div class="popup-search__row popup-search__row_type_links">
					<div class="popup-search__quick-links">
						<div class="popup-search__title"><?=GetMessage("QUICK_LINKS")?></div>
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"search_main",
							Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "top_cat",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(0=>"",),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "top_cat",
								"USE_EXT" => "Y"
							)
						);?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"search_q_links",
							Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"COMPONENT_TEMPLATE" => "search_q_links",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "q_links",
								"USE_EXT" => "N"
							)
						);?>
					</div>
				</div>
			</div>
		</div>
	</div>

