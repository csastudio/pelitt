<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//pr($templateFolder);
$this->setFrameMode(true);
if(CModule::IncludeModule('orion.infinitescroll')){
	COrionInfiniteScroll::SetOptions(
		array(

			  'btn_more_results'=>array('label' => 'Еще результаты', 'class' => 'show-more__text'),

		   ),
		   $arResult['NAV_RESULT']->NavNum
		);
	$sBeginMark = COrionInfiniteScroll::GetBeginMark($arResult['NAV_RESULT']->NavNum);
	$sEndMark = COrionInfiniteScroll::GetEndMark($arResult['NAV_RESULT']->NavNum);
}

if (!empty($arResult['ITEMS'])) {
	echo $sBeginMark;
	?><?
	foreach ($arResult['ITEMS'] as $key => $arItem) {

		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);

		$strMainID = $this->GetEditAreaId($arItem['ID']);
		?>
		<div class="cards-contact cards-contact_style_logo" id="<?=$strMainID?>"><?
			if ($arItem['PREVIEW_PICTURE']['SRC']):
				?>
				<div class="cards-contact__logo">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" height="44" class="cards-contact__image" alt="<?= $arItem["NAME"] ?>" role="presentation" />
				</div><?
			endif;
			?>
			<div class="cards-contact__heading"><?=$arItem["PROPERTIES"]["NAME"]["VALUE"]?></div><?
			if ($arItem["PROPERTIES"]["LINK_WHERE_BUY"]["VALUE"]):
				?>
				<a href="<?=$arItem["PROPERTIES"]["LINK_WHERE_BUY"]["VALUE"]?>" target="_blank" class="cards-contact__link"><?=$arItem["PROPERTIES"]["SITE_WHERE_BUY"]["VALUE"]?></a><?
			endif;
			if ($arItem["PROPERTIES"]["LINK_WHERE_BUY_2"]["VALUE"]):
				?>
				<a href="<?=$arItem["PROPERTIES"]["LINK_WHERE_BUY_2"]["VALUE"]?>" target="_blank" class="cards-contact__link"><?=$arItem["PROPERTIES"]["TEXT_WHERE_BUY_2"]["VALUE"]?></a><?
			endif;
			if ($arItem["PROPERTIES"]["LINK_WHERE_BUY_3"]["VALUE"]):
				?>
				<a href="<?=$arItem["PROPERTIES"]["LINK_WHERE_BUY_3"]["VALUE"]?>" target="_blank" class="cards-contact__link"><?=$arItem["PROPERTIES"]["TEXT_WHERE_BUY_3"]["VALUE"]?></a><?
			endif;
			if ($arItem["PROPERTIES"]["LINK_WHERE_BUY_4"]["VALUE"]):
				?>
				<a href="<?=$arItem["PROPERTIES"]["LINK_WHERE_BUY_4"]["VALUE"]?>" target="_blank" class="cards-contact__link"><?=$arItem["PROPERTIES"]["TEXT_WHERE_BUY_4"]["VALUE"]?></a><?
			endif;
			if ($arItem["PROPERTIES"]["ADRESS"]["VALUE"]):
				?>
				<div class="cards-contact__sait">
					<div class="cards-contact__title"><?=$arItem["PROPERTIES"]["ADRESS"]["VALUE"]?></div>
				</div><?
			endif;
			if ($arItem["PROPERTIES"]["PHONE_WHERE_BUY"]["VALUE"]):
				?>
				<div class="cards-contact__tel">
					<div class="cards-contact__title"><?=GetMessage('TELEPHONE')?>:
						<div class="cards-contact__phone"><?=$arItem["PROPERTIES"]["PHONE_WHERE_BUY"]["VALUE"]?></div>
					</div>
				</div><?
			endif;
			if ($arItem["PROPERTIES"]["TIME_WORK"]["VALUE"]):
				?>
				<div class="cards-contact__tel">
				<div class="cards-contact__title"><?=GetMessage('WORK_HOURS')?>:
					<div class="cards-contact__phone"><?=$arItem["PROPERTIES"]["TIME_WORK"]["VALUE"]?></div>
				</div>
				</div><?
			endif;
			?>
		</div><?
	}
	echo $sEndMark;?><?
}
?>
<?if (strlen($arResult['DESCRIPTION']) > 0) :?>
<div><?=$arResult['DESCRIPTION']?></div>
<? endif;?>
<!--div class="tabs__show-more-wrap">
	<div class="show-more">
		<div class="show-more__icon">
			<svg class="show-more__icon-item"><use xlink:href="<?//SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_preloader"></use></svg>
		</div>
		<div class="show-more__text">показать еще</div>
	</div>
</div-->
