<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
localRedirect('/dealers/online-shopping/');
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="where-buy__page-preview">
	<div style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/images/page-preview/support-bg-3.jpg)" class="page-preview page-preview_type_white page-preview_style_support">
		<div class="page-preview__title">Где купить</div>
		<div class="page-preview__text">Компания ЛЕКСАНД (бренд LEXAND) стремится к разработке <br> продуктов только высочайшего качества.</div>
<!--		<div class="page-preview__selects">-->
<!--			<div class="select select_type_city js-select">-->
<!--				<input style="display:none;" class="select__input js-select-input" type="text" />-->
<!--				<div class="select__current">-->
<!--					<div class="select__current-text js-select-current">Россия</div>-->
<!--					<svg class="select__icon"><use xlink:href="--><?//=SITE_TEMPLATE_PATH?><!--/assets/images/icon.svg#icon_country"></use></svg>-->
<!--				</div>-->
<!--				<div class="select__body js-select-body">-->
<!--					<div class="select__item js-select-item">Россия1</div>-->
<!--					<div class="select__item js-select-item">Россия2</div>-->
<!--					<div class="select__item js-select-item">Россия3</div>-->
<!--					<div class="select__item js-select-item">Россия4</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="select select_type_city js-select">-->
<!--				<input style="display:none;" class="select__input js-select-input" type="text" />-->
<!--				<div class="select__current">-->
<!--					<div class="select__current-text js-select-current">Санкт-Петербург</div>-->
<!--					<svg class="select__icon"><use xlink:href="--><?//=SITE_TEMPLATE_PATH?><!--/assets/images/icon.svg#icon_city"></use></svg>-->
<!--				</div>-->
<!--				<div class="select__body js-select-body">-->
<!--					<div class="select__item js-select-item">Санкт-Петербург1</div>-->
<!--					<div class="select__item js-select-item">Санкт-Петербург2</div>-->
<!--					<div class="select__item js-select-item">Санкт-Петербург3</div>-->
<!--					<div class="select__item js-select-item">Санкт-Петербург4</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
		<div class="page-preview__arrow-down js-arrow"></div>
	</div>
</div>

<div class="where-buy__wrap">
	<div class="where-buy__markets js-anchor">
		<div class="faq">
			<div class="faq__col-left">
				<div class="list-items js-list-items">
					<a href="javascript:void(0);" data-tab="markets_0" class="list-items__item js-list-item">
						<span class="list-items__item-text">Официальный <br> интернет&#8209;магазин</span>
					</a>
					<a href="javascript:void(0);" data-tab="markets_1" class="list-items__item list-items__item_state_active js-list-item">
						<span class="list-items__item-text">Интернет-магазины</span>
					</a>
					<a href="javascript:void(0);" data-tab="markets_2" class="list-items__item js-list-item">
						<span class="list-items__item-text">Розничные магазины</span>
					</a>
					<a href="javascript:void(0);" data-tab="markets_3" class="list-items__item js-list-item">
						<span class="list-items__item-text">Федеральные сети</span>
					</a>
					<a href="javascript:void(0);" data-tab="markets_4" class="list-items__item js-list-item">
						<span class="list-items__item-text">Региональные дистрибуторы</span>
					</a>
				</div>
				<div class="list-items js-list-items">
					<a href="javascript:void(0);" data-tab="" class="list-items__item">
						<span class="list-items__item-text">Оптовые продажи</span>
					</a>
					<a href="javascript:void(0);" data-tab="" class="list-items__item">
						<span class="list-items__item-text">Диллерам</span>
					</a>
				</div>
			</div>
			<div class="faq__col-right">
				<div class="tabs js-tabs">
					<div data-tab-content="markets_0" class="tabs__content js-tab-content">
						<div class="tabs__text">контент первого таба</div>
					</div>
					<div data-tab-content="markets_1" class="tabs__content js-tab-content">
						<div class="cards-contact cards-contact_style_logo">
							<div class="cards-contact__logo">
								<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/where-buy/cards-contact/m-video.png" class="cards-contact__image" alt="" role="presentation" />
							</div>
							<div class="cards-contact__heading">М.Видео</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.mvideo.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">+7 (495) 777-777-5</div>
								</div>
							</div>
						</div>
						<div class="cards-contact cards-contact_style_logo">
							<div class="cards-contact__logo">
								<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/where-buy/cards-contact/auto.png" class="cards-contact__image" alt="" role="presentation" />
							</div>
							<div class="cards-contact__heading">AUTO-GPS.ru</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.auto-gps.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">8-800-700-56 42</div>
								</div>
							</div>
						</div>
						<div class="cards-contact cards-contact_style_logo">
							<div class="cards-contact__logo">
								<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/where-buy/cards-contact/wikimart.png" class="cards-contact__image" alt="" role="presentation" />
							</div>
							<div class="cards-contact__heading">Wikimart</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.wikimart.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">8 (495) 641-58-85</div>
								</div>
							</div>
						</div>
						<div class="cards-contact cards-contact_style_logo">
							<div class="cards-contact__logo">
								<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/where-buy/cards-contact/techno.png" class="cards-contact__image" alt="" role="presentation" />
							</div>
							<div class="cards-contact__heading">Технопарк</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.technopark.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">8 (495) 755-88-88</div>
								</div>
							</div>
						</div>
						<!-- <div class="tabs__show-more-wrap">
							<div class="show-more">
								<div class="show-more__icon">
									<svg class="show-more__icon-item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_preloader"></use></svg>
								</div>
								<div class="show-more__text">показать еще</div>
							</div>
						</div> -->
					</div>
					<div data-tab-content="markets_2" class="tabs__content js-tab-content">
						<div class="cards-contact">
							<div class="cards-contact__heading">Компьютерный мир</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.dns-shop.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">+7 (812) 333-0033</div>
								</div>
							</div>
						</div>
						<div class="cards-contact">
							<div class="cards-contact__heading">Компьютерный центр КЕЙ</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.key.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">+7 (812) 074</div>
								</div>
							</div>
						</div>
						<div class="cards-contact">
							<div class="cards-contact__heading">NaviMarin.ru</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.navimarin.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">+7 (812) 495-65-11</div>
								</div>
							</div>
						</div>
						<div class="cards-contact">
							<div class="cards-contact__heading">SPBZONE</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.spbzone.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">+7 (812) 295-76-00</div>
								</div>
							</div>
						</div>
						<div class="cards-contact">
							<div class="cards-contact__heading">Полезный Попутчик</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.mega-poputchik.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">+7 (812) 934-18-19</div>
								</div>
							</div>
						</div>
						<div class="cards-contact">
							<div class="cards-contact__heading">ООО «Ремтроника»</div>
							<a href="javascript:void(0);" class="cards-contact__link">Каталог Lexand</a>
							<div class="cards-contact__sait">
								<div class="cards-contact__title">Сайт:
									<a href="javascript:void(0);" class="cards-contact__link">www.key.ru</a>
								</div>
							</div>
							<div class="cards-contact__tel">
								<div class="cards-contact__title">Телефон:
									<div class="cards-contact__phone">8-800-555-61-55</div>
								</div>
							</div>
						</div>
						<!-- <div class="tabs__show-more-wrap">
							<div class="show-more">
								<div class="show-more__icon">
									<svg class="show-more__icon-item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_preloader"></use></svg>
								</div>
								<div class="show-more__text">показать еще</div>
							</div>
						</div> -->
					</div>
					<div data-tab-content="markets_3" class="tabs__content js-tab-content">
						<div class="tabs__text">контент четвертого таба</div>
					</div>
					<div data-tab-content="markets_4" class="tabs__content js-tab-content">
						<div class="tabs__text">контент пятого таба</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
