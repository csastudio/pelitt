<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$noImgPreview = $this->GetFolder().'/img/none.png';

?><?
if (!empty($arResult['ITEMS'])) {
	switch ($arResult['UF_SECTION']) {
		// Дефолтный
		case "DEF":
			?>
			<div class="catalog-accessory__wrap">
				<div class="catalog-accessory__cards-list">
					<div class="cards"><?
						foreach ($arResult['ITEMS'] as $key => $arItem) {
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
							$strMainID = $this->GetEditAreaId($arItem['ID']);

							$productTitle = $arItem['NAME'];
							$imgTitle = (
							isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
								? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
								: $arItem['NAME']
							);
							?>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="card-product card-product_style_no-img" id="<?= $strMainID ?>">
								<span class="card-product__img">
									<img src="<?echo $arItem['PREVIEW_PICTURE']['SRC'] ? $arItem['PREVIEW_PICTURE']['SRC'] : $noImgPreview;?>" class="card-product__image"
										 alt="<?=$imgTitle ?>" role="presentation"/>
								</span>
								<span class="card-product__title">
									<span class="card-product__span-title js-card-product-title"><?= $productTitle ?></span>
								</span>
								<span class="card-product__descr">Совместимость: LEXAND LR-4500</span>
							</a><?
						}
						?>
					</div>
				</div>
			</div><?
			if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
				?><? // echo $arResult["NAV_STRING"]; ?><?
			}
		break;

		// Аксессуары
		case "ACCESSUARY":
			?>
			<div class="catalog-accessory__wrap">
			<div class="catalog-accessory__cards-list">
				<div class="cards"><?
					foreach ($arResult['ITEMS'] as $key => $arItem) {
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
						$strMainID = $this->GetEditAreaId($arItem['ID']);

						$productTitle = (
						isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
							? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
							: $arItem['NAME']
						);
						$imgTitle = (
						isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
							? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
							: $arItem['NAME']
						);
						?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="card-product card-product_style_no-img" id="<?= $strMainID ?>">
							<span class="card-product__img">
								<img src="<?echo $arItem['PREVIEW_PICTURE_ACC']['SRC'] ? $arItem['PREVIEW_PICTURE_ACC']['SRC'] : $noImgPreview;?>" class="card-product__image"
									 alt="<?= $imgTitle ?>" role="presentation"/>
							</span>
							<span class="card-product__title">
								<span class="card-product__span-title"><?= $productTitle ?></span>
							</span><?
							foreach($arItem["PROPERTIES"]["PROPS_LIST"]["VALUE"] as $arPropList):
								?>
								<span class="card-product__descr"><?=$arPropList?></span><?
							endforeach;
							?>
						</a><?
					}
					?>
				</div>
			</div>
			</div><?
			if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
				?><? // echo $arResult["NAV_STRING"]; ?><?
			}
		break;

		// Автонавигаторы
		case "NAVIG":
		// Смартфоны
		case "SMART":
			?>
			<div class="catalog-navigate__wrap">
				<div class="catalog-navigate__cards-list">
					<div class="cards"><?
					foreach ($arResult['ITEMS'] as $key => $arItem) {
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
						$strMainID = $this->GetEditAreaId($arItem['ID']);

						$productTitle = (
						isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
							? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
							: $arItem['NAME']
						);
						$imgTitle = (
						isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
							? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
							: $arItem['NAME']
						);
						?>
						<div class="card-navigate">
							<div class="card-navigate__wrap">
								<div class="card-navigate__head">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>lp/" class="card-navigate__heading"><?=$productTitle?></a>
									<div class="card-navigate__title"><?=$arItem["SECTION_NAME"]?></div>
									<div class="card-navigate__descr"><?=$arItem["PREVIEW_TEXT"]?></div>
									<div class="card-navigate__button">
										<a href="<?=$arItem["DETAIL_PAGE_URL"]?>lp/" class="btn btn_style_green-key btn_shape_ellipse">
											<div class="btn__text"><?=GetMessage("MORE");?>
												<svg class="btn__icon btn__icon_chevron-right">
													<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_chevron-right"></use>
												</svg>
											</div>
										</a>
									</div>
									<div class="card-navigate__list"><?
										foreach($arItem["PROPERTIES"]["PROPS_LIST"]["VALUE"] as $arPropList):
											?>
											<div class="card-navigate__item"><?=$arPropList?></div><?
										endforeach;
										?>
									</div>
								</div>
								<div class="card-navigate__image-wrap">
									<img src="<?echo $arItem['PREVIEW_PICTURE']['SRC'] ? $arItem['PREVIEW_PICTURE']['SRC'] : $noImgPreview;?>" class="card-navigate__image" alt="" role="presentation"/>
								</div>
							</div>
						</div><?
					}
					?>
					</div>
				</div>
			</div><?
			if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
				?><?// echo $arResult["NAV_STRING"]; ?><?
			}
		break;

		// Телефоны
		case "PHOENS":
			?>
			<div class="catalog-phone__wrap">
				<div class="catalog-phone__cards-list">
					<div class="cards"><?
					foreach ($arResult['ITEMS'] as $key => $arItem) {
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
						$strMainID = $this->GetEditAreaId($arItem['ID']);

						$productTitle = $arItem['NAME'];
						$imgTitle = (
						isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
							? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
							: $arItem['NAME']
						);
						?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>lp/" class="card-product card-product_style_phone">
							<span class="card-product__img">
								<img src="<?echo $arItem['PREVIEW_PICTURE_PHONE']['SRC'] ? $arItem['PREVIEW_PICTURE_PHONE']['SRC'] : $noImgPreview;?>" class="card-product__image" alt="<?=$imgTitle?>" role="presentation"/>
							</span>
							<span class="card-product__title">
								<span class="card-product__span-title js-card-product-title"><?=$productTitle?></span>
							</span>
							<span class="card-product__text"><?=$arItem["PREVIEW_TEXT"]?></span><?
							foreach($arItem["PROPERTIES"]["PROPS_LIST"]["VALUE"] as $arPropList):
								?>
								<div class="card-product__descr"><?=$arPropList?></div><?
							endforeach;
							?>
						</a><?
					}
					?>
					</div>
				</div>
			</div><?
			if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
				?><?// echo $arResult["NAV_STRING"]; ?><?
			}
		break;
	}
}
?>