<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

if(CSite::InDir('/')){
	$arResult["ACCESS_SIMILAR"] = 6;
}
if(CSite::InDir('/cz/')){
	$arResult["ACCESS_SIMILAR"] = 31;
}
if(CSite::InDir('/pl/')){
	$arResult["ACCESS_SIMILAR"] = 34;
}
if(CSite::InDir('/de/')){
	$arResult["ACCESS_SIMILAR"] = 59;
}

$resImg = CFile::GetFileArray($arResult["PROPERTIES"]["PHOTO_LP_BANER"]["VALUE"]);
$arResult["PROPERTIES"]["PHOTO_LP_BANER"]["VALUE"] = $resImg;

// Получаем пользовательские свойста раздела
if (CModule::IncludeModule('iblock')){
	$arFilter = Array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ACTIVE'=>'Y', 'ID' => $arResult['IBLOCK_SECTION_ID']);
	$uf_name = Array("UF_CONTANT_CARD", "UF_EMAIL_CARD", "UF_TEMPLATE");
	$db_list = CIBlockSection::GetList(Array(), $arFilter, false, $uf_name);
	if($uf_value = $db_list->GetNext()){
		$arResult["UF_CONTANT_CARD"] = htmlspecialcharsBack($uf_value["UF_CONTANT_CARD"]);
		$arResult["UF_EMAIL_CARD"] = htmlspecialcharsBack($uf_value["UF_EMAIL_CARD"]);
		$arResult["UF_TEMPLATE"] = $uf_value["UF_TEMPLATE"];
	}
}
$color = CIBlockElement::GetList([], ['IBLOCK_ID'=>Lexand\IBHelper::getIBID('color')],false,
    false,['ID','NAME','PROPERTY_COLOR_CODE']);
while ($col=$color->GetNext())
{
    $arResult["COLOR"][$col["ID"]] = $col;
}
$arResult["PROPERTIES"]['COLORS']['VALUE'][] = $arResult["ID"];
$colors_else = CIBlockElement::GetList([], ['ID'=>$arResult["PROPERTIES"]['COLORS']['VALUE'],"ACTIVE"=>'Y'],false,
    false,['PROPERTY_COLOR', 'DETAIL_PAGE_URL']);
while ($colors=$colors_else->GetNext())
{
    $arResult["COLORS"][]=$colors;
}
