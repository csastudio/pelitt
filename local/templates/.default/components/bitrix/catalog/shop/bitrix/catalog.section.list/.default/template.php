<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$isShop = $APPLICATION->GetCurPage(true)==SITE_DIR."shop/index.php";
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
//gg($arResult);
if ($arResult['UF_SECTION']["UF_TEMPLATE"] == 5){
    $class = "catalog-accessory__page-preview";
}
elseif ($arResult['UF_SECTION']["UF_TEMPLATE"] == 6){
    $class = "catalog-navigate__page-preview";
}
elseif ($arResult['UF_SECTION']["UF_TEMPLATE"] == 7){
    $class = "catalog-phone__page-preview";
}
?>
    <div class="<?=$class?>">
        <div style="" class="page-preview page-preview_no-bg page-preview_type_icon">
            <svg class="page-preview__icon"><use xlink:href="<?=$arResult['UF_SECTION']["UF_ICON"]?>#<?=$arResult['UF_SECTION']["UF_ICON_SELECT"]?>"></use></svg>
            <?if ($isShop):?>
                <div class="page-preview__title"><?=GetMessage("TITLE_CATALOG")?></div>
            <?else:?>
                <div class="page-preview__title"><?
                    echo (
                    isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
                        ? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
                        : $arResult['SECTION']['NAME']
                    );
                    ?>
                </div>
            <?endif;?>
            <?//if (strlen($arResult['SECTION']["DESCRIPTION"]) > 0):?><!--div class="page-preview__text"><?//=$arResult['SECTION']["DESCRIPTION"]?></div--><?//endif;?>
        </div>
        <?if ($arResult['UF_SECTION']["UF_IMG_SECTION"]["SRC"]):?>
            <div class="page-banner__section">
                <img src="<?=$arResult['UF_SECTION']["UF_IMG_SECTION"]["SRC"]?>" />
            </div>
        <?endif;?>
    </div>
<?if ($isShop):?>
    <div class="catalog-navigate__content">
        <div class="catalog-navigate__wrap">
            <div class="catalog-navigate__cards-list">
                <div class="cards">
                    <?foreach ($arResult["SECTIONS"] as $arSection):?>
                        <div class="card-navigate">
                            <div class="card-navigate__wrap">
                                <div class="card-navigate__head">
                                    <a href="<?=$arSection["SECTION_PAGE_URL"];?>" class="card-navigate__heading"><?=$arSection["NAME"];?></a>
                                    <div class="card-navigate__button">
                                        <a href="<?=$arSection["SECTION_PAGE_URL"];?>" class="btn btn_style_green-key btn_shape_ellipse">
                                            <div class="btn__text"><?=GetMessage("CATALOG_MORE");?>
                                                <svg class="btn__icon btn__icon_chevron-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_chevron-right"></use></svg>
                                            </div>
                                        </a>
                                    </div>
                                </div>
<!--                                <div class="card-navigate__image-wrap">-->
<!--                                    <img src="--><?//=$arSection["PICTURE"]["SRC"];?><!--" class="card-navigate__image" alt="--><?//=$arSection["PICTURE"]["ALT"];?><!--" role="presentation">-->
<!--                                </div>-->
                            </div>
                        </div>
                        <?
                        $ID=1;
                        foreach ($arSection["ELEMENTS"] as $arElements):
                            ?>
                            <a href="<?=$arElements["DETAIL_PAGE_URL"];?>" class="card-product card-product_style_no-img card-all-product <?if($ID%3 == 0):?>margin-right-no<?endif;?>">
								<span class="card-product__img">
									<img src="<?=$arElements["PREVIEW_PICTURE"]["SRC"];?>" class="card-product__image" alt="<?=$arElements["PREVIEW_PICTURE"]["ALT"];?>" role="presentation">
								</span>
                                <span class="card-product__title">
									<span class="card-product__span-title js-card-product-title" style="float: none; position: static;"><?=$arElements["NAME"];?></span>
								</span>
                            </a>
                            <?
                            $ID++;
                        endforeach;
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
<?endif;?>