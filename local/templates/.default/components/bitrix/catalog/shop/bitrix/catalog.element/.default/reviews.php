<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="card-products__average-rating-wrap">
    <div class="average-rating">
        <div class="average-rating__title">Отзывы</div>
        <div class="average-rating__rating-wrap">
            <div class="rating rating_size_max">
                <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-half"></use></svg>
            </div>
        </div>
        <div class="average-rating__average">Средняя оценка: 4.5</div>
    </div>
</div>
<div class="card-products__sort-wrap">
    <div class="sort js-sort">
        <div class="sort__list">
            <a href="javascript:void(0);" class="sort__link sort__link sort__link_state_active">Все</a>
            <a href="javascript:void(0);" class="sort__link">Хорошие</a>
            <a href="javascript:void(0);" class="sort__link">Средние</a>
            <a href="javascript:void(0);" class="sort__link">Плохие</a>
        </div>
    </div>
</div>
<div data-anchor="<?=$arResult["PROPERTIES"]["MENU_CARD"]["DESCRIPTION"][4]?>" class="card-products__slider-comments-wrap">
    <div class="slider-main slider-main_comments">
        <div id="slider-comments" class="slider-main__slider-wrap">
            <div style="" class="slider-main__item">
                <div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation">
                    <div class="slider-main__testimonial-wrap">
                        <div class="testimonial js-testimonial">
                            <div class="testimonial__heading">
                                <div class="testimonial__name">Родионов Илья</div>
                                <div class="testimonial__short">Отличная модель. Опыт использования: менее месяца.</div>
                                <div class="testimonial__rating">
                                    <div class="rating">
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                    </div>
                                </div>
                            </div>
                            <div class="testimonial__body js-testimonial-body">
                                <div class="testimonial__wrap js-testimonial-wrap">
                                    <div class="testimonial__plus">
                                        <div class="testimonial__title">Достоинства:</div>
                                        <div class="testimonial__text">1. Навител по-настоящему хорош!
                                            <br>2. Хорошее крепление к стеклу.
                                            <br>3. Сам аппарат быстро и устойчиво ловит спутники.</div>
                                    </div>
                                    <div class="testimonial__minus">
                                        <div class="testimonial__title">Недостатки:</div>
                                        <div class="testimonial__text">1. Когда на него светит Солнце, ничего не видно.
                                            <br>2. Шнур действительно коротковат - включен был внатяг.
                                            <br>3. Часто Навител предупреждал об ограничении в 60 км/ч или запрете поворота налево, когда их не было и в помине.</div>
                                    </div>
                                    <div class="testimonial__comment">
                                        <div class="testimonial__title">Комментарий:</div>
                                        <div class="testimonial__text js-testimonial-comment">Это мой первый навигатор, использовал для поездки по трассе. Общее впечатление - отлично!
                                            <br>Помимо собственно навигации, использовался для самоконтроля. Если бы еще и Навител не глючил, цены бы ему не
                                            было
                                            <br> Это мой первый навигатор, использовал для поездки по трассе. Общее впечатление - отлично!
                                            <br>Помимо собственно навигации, использовался для самоконтроля. Если бы еще и Навител не глючил, цены бы ему не
                                            было</div>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="testimonial__link js-testimonial-target">Показать полностью</a>
                            <div class="testimonial__date">9 ноября 2015, Саратов</div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="" class="slider-main__item">
                <div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap">
                    <div class="slider-main__testimonial-wrap">
                        <div class="testimonial js-testimonial">
                            <div class="testimonial__heading">
                                <div class="testimonial__name">Летов Владислав</div>
                                <div class="testimonial__short">Плохая модель. Опыт использования: менее месяца.</div>
                                <div class="testimonial__rating">
                                    <div class="rating">
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-empty"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-empty"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-empty"></use></svg>
                                    </div>
                                </div>
                            </div>
                            <div class="testimonial__body js-testimonial-body">
                                <div class="testimonial__wrap js-testimonial-wrap">
                                    <div class="testimonial__plus">
                                        <div class="testimonial__title">Достоинства:</div>
                                        <div class="testimonial__text">1. не плохой дизайн, навигация работает</div>
                                    </div>
                                    <div class="testimonial__minus">
                                        <div class="testimonial__title">Недостатки:</div>
                                        <div class="testimonial__text">брал как бюджетную модель с возможностью построения маршрутов с учетом пробок (пусть и через блютус). За 3 часа
                                            танцев с бубном смог только как громкую связь его использовать...</div>
                                    </div>
                                    <div class="testimonial__comment">
                                        <div class="testimonial__title">Комментарий:</div>
                                        <div class="testimonial__text js-testimonial-comment">Моё мнение что все отзывы на 5 звезд фейковые, он реально на слабую 3 тянет по функционалу. По скорости и качеству
                                            мой старый prestigio 5200 его обходит... На следующий день сдал его обратно в магазин.</div>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="testimonial__link js-testimonial-target">Показать полностью</a>
                            <div class="testimonial__date">7 января, Пермь</div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="" class="slider-main__item">
                <div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap">
                    <div class="slider-main__testimonial-wrap">
                        <div class="testimonial js-testimonial">
                            <div class="testimonial__heading">
                                <div class="testimonial__name">Родионов Илья</div>
                                <div class="testimonial__short">Отличная модель. Опыт использования: менее месяца.</div>
                                <div class="testimonial__rating">
                                    <div class="rating">
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                    </div>
                                </div>
                            </div>
                            <div class="testimonial__body js-testimonial-body">
                                <div class="testimonial__wrap js-testimonial-wrap">
                                    <div class="testimonial__plus">
                                        <div class="testimonial__title">Достоинства:</div>
                                        <div class="testimonial__text">1. Навител по-настоящему хорош!
                                            <br>2. Хорошее крепление к стеклу.
                                            <br>3. Сам аппарат быстро и устойчиво ловит спутники.</div>
                                    </div>
                                    <div class="testimonial__minus">
                                        <div class="testimonial__title">Недостатки:</div>
                                        <div class="testimonial__text">1. Когда на него светит Солнце, ничего не видно.
                                            <br>2. Шнур действительно коротковат - включен был внатяг.
                                            <br>3. Часто Навител предупреждал об ограничении в 60 км/ч или запрете поворота налево, когда их не было и в помине.</div>
                                    </div>
                                    <div class="testimonial__comment">
                                        <div class="testimonial__title">Комментарий:</div>
                                        <div class="testimonial__text js-testimonial-comment">Это мой первый навигатор, использовал для поездки по трассе. Общее впечатление - отлично!
                                            <br>Помимо собственно навигации, использовался для самоконтроля. Если бы еще и Навител не глючил, цены бы ему не
                                            было
                                            <br> Это мой первый навигатор, использовал для поездки по трассе. Общее впечатление - отлично!
                                            <br>Помимо собственно навигации, использовался для самоконтроля. Если бы еще и Навител не глючил, цены бы ему не
                                            было</div>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="testimonial__link js-testimonial-target">Показать полностью</a>
                            <div class="testimonial__date">9 ноября 2015, Саратов</div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="" class="slider-main__item">
                <div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap">
                    <div class="slider-main__testimonial-wrap">
                        <div class="testimonial js-testimonial">
                            <div class="testimonial__heading">
                                <div class="testimonial__name">Летов Владислав</div>
                                <div class="testimonial__short">Плохая модель. Опыт использования: менее месяца.</div>
                                <div class="testimonial__rating">
                                    <div class="rating">
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-full"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-empty"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-empty"></use></svg>
                                        <svg class="rating__item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_star-empty"></use></svg>
                                    </div>
                                </div>
                            </div>
                            <div class="testimonial__body js-testimonial-body">
                                <div class="testimonial__wrap js-testimonial-wrap">
                                    <div class="testimonial__plus">
                                        <div class="testimonial__title">Достоинства:</div>
                                        <div class="testimonial__text">1. не плохой дизайн, навигация работает</div>
                                    </div>
                                    <div class="testimonial__minus">
                                        <div class="testimonial__title">Недостатки:</div>
                                        <div class="testimonial__text">брал как бюджетную модель с возможностью построения маршрутов с учетом пробок (пусть и через блютус). За 3 часа
                                            танцев с бубном смог только как громкую связь его использовать...</div>
                                    </div>
                                    <div class="testimonial__comment">
                                        <div class="testimonial__title">Комментарий:</div>
                                        <div class="testimonial__text js-testimonial-comment">Моё мнение что все отзывы на 5 звезд фейковые, он реально на слабую 3 тянет по функционалу. По скорости и качеству
                                            мой старый prestigio 5200 его обходит... На следующий день сдал его обратно в магазин.</div>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="testimonial__link js-testimonial-target">Показать полностью</a>
                            <div class="testimonial__date">7 января, Пермь</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
