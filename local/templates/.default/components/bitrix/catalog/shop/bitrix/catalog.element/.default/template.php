<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arResult["NO_IMG"] = $this->GetFolder().'/img/none.png';
	?>
<style>
    <?foreach ($arResult['COLORS'] as $color){?>

    .switch-color__item_<?=$arResult['COLOR'][$color["PROPERTY_COLOR_VALUE"]]["NAME"]?>:before {
        background-color: #<?=$arResult['COLOR'][$color["PROPERTY_COLOR_VALUE"]]["PROPERTY_COLOR_CODE_VALUE"]?>;
    }
    <?}?>
    </style>
	<div class="product-mini js-product-mini page__content">
		<div class="product-mini__wrap">
			<div class="product-mini__row js-afix-row">
				<div class="product-mini__left-col">
					<div class="product-mini__content js-afix-content">
						<div class="product-mini__wrap-image"><?
							if ($arResult["DETAIL_PICTURE"]["SRC"]):
								?>
								<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" data-popup="popupGallery" class="product-mini__image js-popup-open" alt="<?=$arResult["NAME"]?>" role="presentation" /><?
							else:
								?>
								<img src="<?=$arResult["NO_IMG"]?>" width="88" height="68" alt="Нет фото" role="presentation" /><?
							endif;
							?>
						</div><?
						if (is_array($arResult["MORE_PHOTO"]) && count($arResult["MORE_PHOTO"]) > 1):?>
							<div class="product-mini__btn-gallery">
								<button type="" data-popup="popupGallery" class="btn btn_style_gallery js-popup-open">
									<div class="btn__text"><?=GetMessage("SEE_GAL");?>
										<svg class="btn__icon btn__icon_gallery"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_gallery"></use></svg>
									</div>
								</button>
							</div><?
						endif;
						?>
					</div>
				</div>
				<div class="product-mini__right-col">
					<div class="product-mini__head">
						<div class="product-mini__heading"><?=$arResult["NAME"]?></div>
						<div class="product-mini__title"><?=htmlspecialcharsBack($arResult["PROPERTIES"]["PARAMETRS"]["VALUE"]["TEXT"])?></div>
					</div>
                    <?if($arResult['COLOR'][$color["PROPERTY_COLOR_VALUE"]]["NAME"]){?>
                    <div class="product-mini__color">
                        <div class="product-mini__text">Color</div>
                        <div class="product-mini__switch-color">
                            <div class="switch-color">
                                <?foreach ($arResult['COLORS'] as $key=>$color){?>
                                        <input type="radio" name="colors" id="<?=$arResult['COLOR'][$color["PROPERTY_COLOR_VALUE"]]["NAME"]?>"
                                           class="switch-color__input" <?if($color["PROPERTY_COLOR_VALUE"]==$arResult["PROPERTIES"]["COLOR"]["VALUE"]){?>checked="checked"<?}?>>
                                    <label for="<?=$arResult['COLOR'][$color["PROPERTY_COLOR_VALUE"]]["NAME"]?>"
                                    data-href="<?=$color["DETAIL_PAGE_URL"]?>"
                                           class="switch-color__item switch-color__item_<?=$arResult['COLOR'][$color["PROPERTY_COLOR_VALUE"]]['NAME']?>">
                                    </label>
                                    <?}?>
                            </div>
                        </div>
                    </div>
                    <?}?>
					<div class="product-mini__body">
						<div class="product-mini__price">
<!--                            --><?//=$arResult["CATALOG_PURCHASING_PRICE"] . ' ' . $arResult["ORIGINAL_PARAMETERS"]["PRICE_CODE"][0]?>
                            <?=$arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE_VAT'];?></div>
						<div class="product-mini__button">
						<?if ($arResult["PROPERTIES"]["STATUS_PROD"]["VALUE_XML_ID"] == "NO"):?>
							<div class="btn__text"><?=$arResult["PROPERTIES"]["STATUS_PROD"]["VALUE"];?></div>
						<?else:?>
							<?if (is_array($arResult['OFFERS']) && count($arResult['OFFERS']) > 0):?>
								<a data-id="<?=$arResult['OFFERS'][0]["ID"]?>" href="javascript:void(0);" data-popup="popupSucces" class="btn_add btn btn_style_ascendant btn_shape_min-ellipse js-popup-open">
							<?else:?>
								<a data-id="<?=$arResult['ID']?>" href="javascript:void(0);" data-popup="popupSucces" class="btn_add btn btn_style_ascendant btn_shape_min-ellipse js-popup-open">
							<?endif;?>
								<div class="btn__text"><?=GetMessage("ADD_TO_BASKET");?></div>
							</a>
						<?endif;?>
						</div>
					</div>

<!--                    --><?//gg($arResult)?>
                    <?

					// Подключение компонента - аксессуары
					if ($arResult['PROPERTIES']['ACCESSORISE']['VALUE']):
						?>
						<div class="product-mini__accessory">
							<div class="product-mini__text"><?=GetMessage("ACC");?></div><?

								include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/accessories.php");

							?>
						</div><?
					endif;
					?>
				</div>
			</div>
		</div>
	</div>
	<div id="popupSucces" class="popup popup_type_basket js-popup">
		<div class="popup__close js-popup-close"></div>
		<div class="popup__wrapper">
			<div class="popup-succes js-popup-auto-close">
				<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/popup/succes.png" class="popup-succes__image" alt="" role="presentation" />
				<div class="popup-succes__heading"><?=GetMessage("ADDED_TO_CART");?></div>
				<div class="popup-succes__title"><?=$arResult["NAME"]?></div>
			</div>
		</div>
	</div><?

	// Подключение модального окна галереи
	if (is_array($arResult["MORE_PHOTO"]) && count($arResult["MORE_PHOTO"]) > 1):
		?>
		<div id="popupGallery" class="popup popup_type_gallery js-popup">
			<div class="popup__close js-popup-close"></div>
			<div class="popup__wrapper">
				<div class="popup-gallery">
					<div class="slider-main slider-main_news-page">
						<div id="slider-news" class="slider-main__slider-wrap"><?
							foreach ($arResult["MORE_PHOTO"] as $arItemMP):
								?>
								<div style="background-image: url(<?=$arItemMP["SRC"]?>)" class="slider-main__item slider-main__big">
									<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation"></div>
								</div><?
							endforeach;
							?>
						</div>
					</div>
					<div class="popup-gallery__slider-thumbs-wrap">
						<div class="slider-main slider-main_thumbs">
							<div id="slider-nav" class="slider-main__slider-wrap"><?
								foreach ($arResult["MORE_PHOTO"] as $arItemMP):
									?>
									<div style="background-image: url(<?=$arItemMP["SRC"]?>)" class="slider-main__item">
										<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation"></div>
									</div><?
								endforeach;
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?
	endif;
		?>
<script type="text/javascript">
	$(function(){
		$('.switch-color__item').click(function() {
			var element = $(this);
            document.location.href = 'https://pelitt.com' + element.attr('data-href');
		});
		$('a.btn_add').click(function() {

			var element = $(this);
			var ID = element.attr('data-id');

			$.ajax({
				type: "POST",
				url: "<?=$templateFolder?>/add_basket.php?lang=<?=LANG?>",
				data: {
					ID: ID,
				},
				success: function(msg){
					BX.onCustomEvent('OnBasketChange');
					setTimeout(function() {window.location.reload();}, 2000);
				}
			});

		});
	});
</script>
