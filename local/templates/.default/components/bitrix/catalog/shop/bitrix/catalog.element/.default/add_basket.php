<?
define('LANG', $_REQUEST['lang']);
define('SITE_ID', $_REQUEST['lang']);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog"))
{
    if (isset($_REQUEST['ID'])) {
        $patern = "/^[0-9]/";
        $ID = intval($_REQUEST['ID']);

        if (preg_match($patern, $ID)) {
			echo LANG;

		 Add2BasketByProductID(
                $ID,
                false
);

        }
    }

    else {

        echo "Нет параметров ";
    }
}

else {

    echo "Не подключены модули";
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
