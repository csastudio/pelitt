<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arOrder = Array("SORT"=>"­­ASC");
$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'ID' => $arResult["SECTION"]["ID"]);
$bIncCnt = Array('ELEMENT_SUBSECTIONS' => 'N');
$arSelect = Array('UF_ICON', 'UF_TEMPLATE', 'UF_ICON_SELECT', 'UF_IMG_SECTION');

$uf_arresult = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect);
if($uf_value = $uf_arresult->GetNext());
$arResult['UF_SECTION'] = $uf_value;

$arResult['UF_SECTION']['UF_IMG_SECTION'] = CFile::GetFileArray($uf_value['UF_IMG_SECTION']);

$DBElement = CIBlockElement::GetList(
    array("SORT"=>"ASC"),
    array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y"),
    false,
    false,
    array()
);
while($ob = $DBElement->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arFields["PREVIEW_PICTURE"] = CFile::GetFileArray($arFields["PREVIEW_PICTURE"]);
    $arElements[] = $arFields;
}

foreach ($arResult["SECTIONS"] as $ID => $arSection){
    foreach ($arElements as $element){
        if ($arSection["ID"] == $element["IBLOCK_SECTION_ID"]){
            $arResult["SECTIONS"][$ID]["ELEMENTS"][] = $element;
        }
    }
}
//pr($arResult["SECTIONS"]);