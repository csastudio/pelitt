<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>




<?
if($arResult["FORM_TYPE"] == "login"):
	?>
	<div class="header__support">
		<div class="support-header js-support-header">
			<div class="js-support-header-current js-support-header__style">
				<div class="icons">
					<a href="javascript:void(0);" class="icons__item icons icons_bg_basket">
						<svg class="icons__icon icons__icon_basket"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_user"></use></svg>
                    </a>
				</div>
			</div>
			<div class="support-header__body support-header__style">
				<div class="support-header__options-header support-header__options-header_type_empty"></div>
				<div class="support-header__options">
					<div class="support-header__option js-support-header-option">
						<a href="javascript:void(0);" data-popup="popupUser" class="support-header__link js-popup-open"><?=GetMessage('SIGN')?> | <?=GetMessage('LOGIN')?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?
	else:
	?>
	<div class="header__support">
		<div class="support-header js-support-header">
			<div class="js-support-header-current js-support-header__style">
				<div class="icons">
					<a href="javascript:void(0);" class="icons__item icons icons_bg_basket">
						<svg class="icons__icon icons__icon_basket"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_user"></use></svg>
					</a>
				</div>
			</div>
			<div class="support-header__body support-header__style">
				<div class="support-header__options-header support-header__options-header_type_empty"></div>
				<div class="support-header__options">
					<div class="support-header__option js-support-header-option">
						<a href="<?=(LANGUAGE_ID == 'en') ? false : '/' . LANGUAGE_ID?>/personal/" class="support-header__link"><?=GetMessage('PROFILE')?></a>
					</div>
					<div class="support-header__option js-support-header-option">
						<a href="<?=(LANGUAGE_ID == 'en') ? false : '/' . LANGUAGE_ID?>/list-orders/" class="support-header__link"><?=GetMessage('ORDERS')?></a>
					</div>
					<div class="support-header__option js-support-header-option">
						<a href="/?logout=yes" class="support-header__link"><?=GetMessage("LOGOUT")?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?
endif;
?>

