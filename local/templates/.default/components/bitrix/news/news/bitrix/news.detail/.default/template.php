<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddHeadString('<meta property="og:image" content="http://lexand.ru'.$arResult["PREVIEW_PICTURE"]["SRC"].'" />');
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />');
$APPLICATION->AddHeadString('<meta property="og:description" content="'.strip_tags($arResult["PREVIEW_TEXT"]).'" />');
$APPLICATION->AddHeadString('<meta property="og:url" content="'.$arResult["DETAIL_PAGE_URL"].'" />');
$APPLICATION->AddHeadString('<meta name="title" content="'.$arResult["NAME"].'" />');
$APPLICATION->AddHeadString('<meta name="description" content="'.strip_tags($arResult["PREVIEW_TEXT"]).'" />');
$APPLICATION->AddHeadString('<link rel="image_src" href="http://lexand.ru'.$arResult["PREVIEW_PICTURE"]["SRC"].'" />');
?>

<div style="" class="page-preview page-preview_no-bg page-preview_type_normal">
	<div class="page-preview__title"><?=$arResult["NAME"]?></div>
	<div class="page-preview__text"><?=GetMessage("TITLE_ITEM_NEWS");?> &nbsp;|&nbsp; <?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
</div>
<div class="news-content">
	<div class="news-content__content"><?
		echo htmlspecialcharsBack($arResult["DETAIL_TEXT"]);
		// Галерея в детальной новости
		if (is_array($arResult["MORE_PHOTO"]) && count($arResult["MORE_PHOTO"]) > 0):
			?>
			<div class="slider-main slider-main_news-page">
				<div id="slider-news" class="slider-main__slider-wrap"><?
					foreach ($arResult["MORE_PHOTO"] as $arPhoto):
						?>
						<div style="background-image: url(<?=$arPhoto["SRC"]?>)" class="slider-main__item">
							<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation"></div>
						</div><?
					endforeach;
					?>
				</div>
			</div>
			<div class="news-content__slider-thumbs-wrap">
				<div class="slider-main slider-main_thumbs">
					<div id="slider-nav" class="slider-main__slider-wrap"><?
						foreach ($arResult["MORE_PHOTO"] as $arPhoto):
							?>
							<div style="background-image: url(<?=$arPhoto["SRC"]?>)" class="slider-main__item">
								<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation"></div>
							</div><?
						endforeach;
						?>
					</div>
				</div>
			</div><?
		endif;
		if (strlen($arResult['PROPERTIES']['DOP_DESC']['VALUE']['TEXT']) > 0):
			?>
			<div class="news-content__text">
				<?=htmlspecialcharsBack($arResult['PROPERTIES']['DOP_DESC']['VALUE']['TEXT']);?>
			</div><?
		endif;
		?>
	</div>
	<!--div class="news-content__share">
		<div class="news-content__share-title">Поделитесь новостью в соцсетях</div>
		<div class="likely likely-light">
			<div class="twitter">Твитнуть</div>
			<div class="facebook">Поделиться</div>
			<div class="gplus">Плюсануть</div>
			<div class="vkontakte">Поделиться</div>
			<div data-media="i/pinnable.jpg" class="pinterest">Запинить</div>
		</div>
	</div-->
</div>