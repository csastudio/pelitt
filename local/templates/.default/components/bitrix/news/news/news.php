<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// Нужно получить список годов
$dbRes = CIBlockElement::GetList(array("DATE_ACTIVE_FROM"=>"DESC"),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]),false,false,array("DATE_ACTIVE_FROM"));

$arDates = array();
while($res = $dbRes->Fetch()) {
	$month["YEAR"] = date("Y",MakeTimeStamp($res["DATE_ACTIVE_FROM"]));
	$arDates[] = array_unique($month);
}
sort($arDates, SORT_NUMERIC);

foreach ($arDates as $year){

	$arYear[] = $year["YEAR"];
}
$arYear = array_unique($arYear);
arsort($arYear);
?>
<script>
	$(function() {
		$('.sort__link').click(function() {
			var _this = this;
			$.get('<?=$templateFolder.'/ajax.php'?>',
			{
				ACTIVE_FROM : $(this).data('active-from'),
				ACTIVE_TO : $(this).data('active-to')
			}, 'html')
			.success(function(res) {
				$('.news__news-list').html(res);
				$('.sort__link').removeClass('sort__link_state_active');
				$(_this).addClass('sort__link_state_active');
				console.log(res);
			})
			.error(function(res) {
				alert('Error! Please try again!');
			});
		});
	});
</script>
<div class="news page__content">
	<div class="news__page-review-wrap">
		<div style="" class="page-preview page-preview_no-bg">
			<div class="page-preview__title"><?=GetMessage("TITLE_NEWS");?></div>
			<div class="page-preview__text"></div>
		</div>
	</div>
	<div class="news__sort-wrap">
		<div class="sort js-sort">
			<div class="sort__list">
				<a href="javascript:void(0);" class="sort__link sort__link sort__link_state_active">Все</a>
				<?foreach ($arYear as $arItemYear):?>
					<a href="javascript:void(0);" class="sort__link" data-active-from="01.01.<?=$arItemYear?>" data-active-to="01.01.<?=$arItemYear+1?>"><?=$arItemYear?></a>
				<?endforeach;?>
			</div>
		</div>
	</div>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"NEWS_COUNT" => $arParams["NEWS_COUNT"],
			"SORT_BY1" => $arParams["SORT_BY1"],
			"SORT_ORDER1" => $arParams["SORT_ORDER1"],
			"SORT_BY2" => $arParams["SORT_BY2"],
			"SORT_ORDER2" => $arParams["SORT_ORDER2"],
			"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
			"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
			"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
			"SET_TITLE" => $arParams["SET_TITLE"],
			"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
			"MESSAGE_404" => $arParams["MESSAGE_404"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"SHOW_404" => $arParams["SHOW_404"],
			"FILE_404" => $arParams["FILE_404"],
			"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_FILTER" => $arParams["CACHE_FILTER"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE" => $arParams["PAGER_TITLE"],
			"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
			"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
			"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
			"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
			"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
			"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
			"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
			"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
			"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
			"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
			"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
			"CHECK_DATES" => $arParams["CHECK_DATES"],
		),
		$component
	);?>
</div>