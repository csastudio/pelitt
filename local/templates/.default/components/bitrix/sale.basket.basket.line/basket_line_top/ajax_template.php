<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y")
{
?>
	<div class="support-header__body" data-role="basket-item-list">
		<div class="support-header__options-header support-header__options-header_type_empty"></div>
		<div class="support-header__options">
			<div class="support-header__option support-header__option_style_first js-support-header-option">
				<div class="support-header__wrap-item js-scrollbar" id="<?=$cartId?>products">
                    <?if ($arResult['NUM_PRODUCTS'] == 0){?>
                        <div class="support-header__item">
                            <div class="support-header__descr"><?=GetMessage('CART_IS_EMPTY')?></div>
                        </div>
                    <?}?>
					<?foreach ($arResult["CATEGORIES"] as $category => $items):
						if (empty($items))
							continue;
						foreach ($items as $v):?>
							<div class="support-header__item">
								<div class="support-header__size">
									<img src="<?=$v["PICTURE_SRC"]?>" class="support-header__view" alt="<?=$v["NAME"]?>" role="presentation" />
								</div>
								<div class="support-header__descr"><?=$v["NAME"]?></div>
								<div class="support-header__info">
									<div class="support-header__price"><?=$v["FULL_PRICE"]?></div>
									<div class="support-header__quantity"><?echo $v["QUANTITY"].' '.GetMessage("PC_PCS");?></div>
								</div>
							</div>
						<?endforeach;?>
					<?endforeach;?>
				</div>
			</div>
			<div class="support-header__option js-support-header-option">
				<div class="price-badge">
					<div class="price-badge__bg">
						<div class="price-badge__container">
							<div class="price-badge__text"><?=GetMessage("TSB1_YOUR_CART");?></div>
							<div class="price-badge__price"><?=$arResult["TOTAL_PRICE"]?></div>
						</div>
						<a href="<?=$arParams["PATH_TO_BASKET"]?>" class="price-badge__btn"><?=GetMessage("TSB1_2ORDER");?></a>
					</div>
				</div>
			</div>
			<?if ($USER->IsAuthorized()):?>
				<div class="support-header__option js-support-header-option">
					<a href="<?=$arParams["PATH_TO_ORDER"]?>" class="support-header__link"><?=GetMessage("PRV_ORDERS");?></a>
				</div>
			<?else:?>
				<div class="support-header__option js-support-header-option">
					<a href="javascript:void(0);" data-popup="popupUser" class="support-header__link js-popup-open"><?=GetMessage("PRV_ORDERS");?></a>
				</div>
			<?endif?>
			<?if ($USER->IsAuthorized()):?>
				<div class="support-header__option js-support-header-option">
					<a href="<?=$arParams["PATH_TO_PROFILE"]?>" class="support-header__link"><?=GetMessage("TSB1_PERSONAL");?></a>
				</div>
			<?else:?>
				<div class="support-header__option js-support-header-option">
					<a href="javascript:void(0);" data-popup="popupUser" class="support-header__link js-popup-open"><?=GetMessage("TSB1_PERSONAL");?></a>
				</div>
			<?endif?>
		</div>
	</div>

	<script>
		BX.ready(function(){
			<?=$cartId?>.fixCart();
		});
	</script>
<?
}