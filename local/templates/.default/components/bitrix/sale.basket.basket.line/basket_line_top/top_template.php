<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<div class="support-header__current js-support-header-current">
	<div class="icons">
		<a href="javascript:void(0);" class="icons__item icons icons_bg_basket">
			<svg class="icons__icon icons__icon_basket"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_basket"></use></svg>
		</a>
	</div>
</div>