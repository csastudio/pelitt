<?
$MESS["TSB1_PERSONAL"] = "Zaloguj się";
$MESS["TSB1_EXPAND"] = "Expand";
$MESS["TSB1_COLLAPSE"] = "Hide";
$MESS["TSB1_CART"] = "Cart";
$MESS["TSB1_TOTAL_PRICE"] = "Total price";
$MESS["TSB1_YOUR_CART"] = "Wartość koszyka";
$MESS["TSB1_READY"] = "In stock";
$MESS["TSB1_DELAY"] = "On wish list";
$MESS["TSB1_NOTAVAIL"] = "Unavailable";
$MESS["TSB1_SUBSCRIBE"] = "Watch list";
$MESS["TSB1_SUM"] = "Total";
$MESS["TSB1_DELETE"] = "Delete";
$MESS["TSB1_2ORDER"] = "Zapłać";
$MESS["TSB1_LOGIN"] = "Log In";
$MESS["TSB1_LOGOUT"] = "Log Out";
$MESS["TSB1_REGISTER"] = "Register";
$MESS["PRV_ORDERS"] = "Poprzednie zamówienia";
$MESS["PC_PCS"] = "szt";
$MESS["CART_IS_EMPTY"] = "Koszyk jest pusty";