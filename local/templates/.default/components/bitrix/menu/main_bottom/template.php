<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

	<div class="footer-menu__group">
		<div class="footer-menu__list"><?
			foreach($arResult as $id => $arItem):
				if ($id == 0 && $arItem["ITEM_TYPE"] == "P"):

					?><a href="javascript:void(0);" class="footer-menu__title"><?=$arItem["TEXT"]?></a><?

				elseif ($id != 0 && $arItem["PARAMS"]["UF_MENU_CHECK"]):

					?><div class="footer-menu__item">

					<a href="<?=$arItem["LINK"]?>" class="footer-menu__link"><?=$arItem["TEXT"]?></a>

				</div><?
				endif;
			endforeach;?>
		</div>
	</div>

<?endif?>