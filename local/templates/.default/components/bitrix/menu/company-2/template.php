<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

	<div class="footer-menu__list">

				<a class="footer-menu__title">Language</a>
		<?foreach($arResult as $id => $arItem):?>
				<div class="footer-menu__item">
					<a href="<?=$arItem["LINK"]?>" class="footer-menu__link"><?=$arItem["TEXT"]?></a>
				</div>

		<?endforeach;?>

	</div>

<?endif?>

