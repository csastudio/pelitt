<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<div class="list-items"><?
		foreach($arResult as $key => $arItem):
			if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
			if ($arItem["UF_SETTINGS"] != 142):
				?>
				<a href="<?=$arItem["LINK"];?>" class="list-items__item <?if($arItem["SELECTED"]):?>active<?endif;?>">
					<span class="list-items__item-text"><?=GetMessage(explode(' ', trim($arItem["TEXT"]))[0])?></span>
				</a><?
			endif;
		endforeach;
		?>
	</div>
	<div class="list-items"><?
		foreach($arResult as $key => $arItem):
			if ($arItem["UF_SETTINGS"] == 142):
				?>
				<a href="<?=$arItem["LINK"]?>" class="list-items__item <?if($arItem["SELECTED"]):?>active<?endif;?>">
					<span class="list-items__item-text"><?=$arItem["TEXT"]?></span>
				</a><?
			endif;
		endforeach;
		?>
	</div>
<?endif?>
