<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (CModule::IncludeModule('iblock')){
    $arOrder = array("SORT"=>"ASC");
    $arFilter = array("IBLOCK_ID" => 27, "ACTIVE" => "Y");
    $arSelect = array("ID", "NAME", "UF_SETTINGS");
    // Получаем свойства разделов
    $dbList = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);

    while($arResultSection = $dbList->GetNext()){
        $arSection[] = $arResultSection;
    }
}
// Добавляем свойства разделов в меню
foreach($arResult as $key => $arItem){
    $arResult[$key]["UF_SETTINGS"] = $arSection[$key]["UF_SETTINGS"];
}