<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<div class="header__menu-products js-header-menu-products">
		<div class="menu-products menu-products_style_header">
			<div class="menu-products__wrap">
                <? foreach($arResult as $key=>$arItem){
				if ($arItem["PARAMS"]["UF_MENU_CHECK"]):
					if ($key == 5){
					?>
					<div class="menu-products__item-wrap"></div>
					<?}?>
					<div class="menu-products__item-wrap">
						<a href="<?=$arItem["LINK"]?>" class="menu-products__item">
							<span class="menu-products__icon-wrap">
								<svg class="menu-products__icon"><use xlink:href="<?=$arItem["PARAMS"]["UF_ICON"]?>#<?=$arItem["PARAMS"]["UF_ICON_SELECT"]?>"></use></svg>
							</span>
							<span class="menu-products__text"><?=$arItem["TEXT"]?></span>
						</a>
					</div>
				<?endif?>
			<?}?>
			</div>
		</div>
	</div>

<?endif?>