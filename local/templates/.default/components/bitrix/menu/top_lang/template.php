<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <div class="header__support">
        <div class="support-header js-support-header">
            <div class="support-header__current support-header__lang js-support-header-current">
                <?=$arParams['CUR_LANG']?>
            </div>
            <div class="support-header__body support-header-lang__body">
                <div class="support-header__options-header support-header__options-header_type_empty"></div>
                <div class="support-header__options">
                    <? foreach ($arResult as $item){ ?>
                    <div class="support-header__option support-header-s__option js-support-header-option">
                        <a href="<?=$item["LINK"]?>" class="support-header__link"><?=$item['TEXT']?></a>
                    </div>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
<?endif?>
