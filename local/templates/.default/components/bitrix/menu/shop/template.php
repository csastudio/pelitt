<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

	<div class="footer-menu__group footer-menu__group_style_border">
		<div class="footer-menu__list">
			<?foreach($arResult as $id => $arItem):?>
				<?if ($id == 0):?>
					<a href="javascript:void(0);" class="footer-menu__title"><?=$arItem["TEXT"]?></a>
				<?else:?>
					<div class="footer-menu__item">
						<a href="<?=$arItem["LINK"]?>" class="footer-menu__link"><?=$arItem["TEXT"]?></a>
					</div>
				<?endif;?>
			<?
				if ($id == 4){
					echo '</div><div class="footer-menu__list"><a href="javascript:void(0);" class="footer-menu__title"></a>';
				}
			endforeach;?>
		</div>
	</div>

<?endif?>