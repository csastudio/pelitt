<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

	<div class="header__support">
		<div class="support-header js-support-header">
			<div class="support-header__current support-header__dop js-support-header-current"><?=GetMessage("MENU_TITLE")?></div>
			<div class="support-header__body">
				<div class="support-header__options-header support-header__options-header_type_empty"></div>
				<div class="support-header__options">
					<?foreach($arResult as $arItem):?>
						<div class="support-header__option js-support-header-option">
							<a href="<?=$arItem["LINK"]?>" class="support-header__link"><?=$arItem["TEXT"]?></a>
						</div>
					<?endforeach;?>
				</div>
			</div>
		</div>
	</div>

<?endif?>