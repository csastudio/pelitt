<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arIcontToCode = array(
	'facebook' => 'facebook',
	'twitter' => 'twitter',
	'vkontakte' => 'vk',
	'odnoklassniki' => 'ok'
);	
	
foreach($arParams['~AUTH_SERVICES'] as $key => &$value){
	if(isset($arIcontToCode[$value['ICON']]))
		$value['CODE'] = $arIcontToCode[$value['ICON']];
}
unset($value);
?>