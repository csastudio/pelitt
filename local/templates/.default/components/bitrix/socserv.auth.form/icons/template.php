<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="icons icons_square">
<?foreach($arParams["~AUTH_SERVICES"] as $service):?>
	<a title="<?=htmlspecialcharsbx($service["NAME"])?>" href="javascript:void(0)" onclick="BxShowAuthFloat('<?=$service["ID"]?>', '<?=$arParams["SUFFIX"]?>')" class="icons__item icons icons_bg_<?=$service['CODE']?>">
		<svg class="icons__icon icons__icon_<?=$service['CODE']?>"><use xlink:href="/local/templates/main/assets/images/icon.svg#icon_<?=$service['CODE']?>"></use></svg>
	</a>
<?endforeach?>
</div>
