<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])) {
	?>
	<div class="home__cards-wrap"><?
		foreach ($arResult['ITEMS'] as $key => $arItem) {
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
			$strMainID = $this->GetEditAreaId($arItem['ID']);

			$productTitle = (
			isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
				: $arItem['NAME']
			);
			$imgTitle = (
			isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
				: $arItem['NAME']
			);
			?>
			<div class="cards cards_state_animate js-scale-card" data-animstart="bottom" data-animstop="400" id="<?=$strMainID?>">
				<div class="card-navigate">
					<div class="card-navigate__wrap">
						<div class="card-navigate__head">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>lp/" class="card-navigate__heading"><?=$productTitle?></a>
							<div class="card-navigate__title"><?=$arItem["SECTION_NAME"]?></div>
							<div class="card-navigate__descr"><?=$arItem["PREVIEW_TEXT"]?>
							</div>
							<div class="card-navigate__button">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>lp/" class="btn btn_style_green-key btn_shape_ellipse">
									<div class="btn__text"><?=GetMessage("CT_BCS_TPL_MESS_BTN_DETAIL");?>
										<svg class="btn__icon btn__icon_chevron-right">
											<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/images/icon.svg#icon_chevron-right"></use>
										</svg>
									</div>
								</a>
							</div>
							<div class="card-navigate__list"><?
								foreach($arItem["PROPERTIES"]["PROPS_LIST"]["VALUE"] as $arPropList):
									?>
									<div class="card-navigate__item"><?=$arPropList?></div><?
								endforeach;
								?>
							</div>
						</div>
						<div class="card-navigate__image-wrap">
							<img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" class="card-navigate__image" alt="<?= $imgTitle ?>" role="presentation"/>
						</div>
					</div>
				</div>
			</div><?
		}
		?>
	</div><?
}
?>