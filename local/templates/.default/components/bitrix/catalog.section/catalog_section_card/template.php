<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arResult["NO_IMG"] = $this->GetFolder().'/img/none.png';
?>
<?if (!empty($arResult['ITEMS'])) {
	?>
	<div class="product-mini__cards">
		<div class="cards"><?
			foreach ($arResult['ITEMS'] as $key => $arItem) {
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
				$strMainID = $this->GetEditAreaId($arItem['ID']);

				$productTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
					? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
					: $arItem['NAME']
				);
				$imgTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
					? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
					: $arItem['NAME']
				);
				$minPrice = false;
				if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
					$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
				?>
				<div class="card-accessory" id="<?=$strMainID?>">
					<div class="card-accessory__wrap">
						<?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
							<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="card-accessory__image" alt="<?=$imgTitle?>" role="presentation"/>
						<?else:?>
							<img src="<?=$arResult["NO_IMG"]?>" width="88" height="68" class="no__image" alt="<?=$imgTitle?>" role="presentation"/>
						<?endif;?>
						<div class="card-accessory__descr">
							<div class="card-accessory__title">
								<div class="card-accessory__span-title"><?=$productTitle?></div>
							</div>
							<div class="card-accessory__decr-wrap">
								<div class="card-accessory__button">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn btn_style_green-key btn_shape_ellipse">
										<div class="btn__text"><?=GetMessage("CT_BCS_TPL_MESS_BTN_DETAIL");?></div>
									</a>
								</div>
								<div class="card-accessory__price"><?=$minPrice['PRINT_DISCOUNT_VALUE']?></div>
							</div>
						</div>
					</div>
				</div><?
			}
			?>
		</div>
	</div><?
}
?>