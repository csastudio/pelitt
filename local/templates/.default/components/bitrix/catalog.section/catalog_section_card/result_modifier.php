<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);

	if($ar_res = $res->GetNext())
	$arResult['ITEMS'][$key]["SECTION_NAME"] = $ar_res['NAME'];

}

foreach ($arResult['ITEMS'] as $ID=>$arItem)
{
	$imgRes=CFile::ResizeImageGet(

		$arItem['PREVIEW_PICTURE']['ID'],

		array(
			'width'=>70,
			'height'=>70
		),

		BX_RESIZE_IMAGE_PROPORTIONAL,
		true,
		Array()

	);

	$arResult['ITEMS'][$ID]['PREVIEW_PICTURE'] = array(

		'SRC' => $imgRes['src'],
		'WIDTH' => $imgRes['width'],
		'HEIGHT' => $imgRes['height'],

	);
}