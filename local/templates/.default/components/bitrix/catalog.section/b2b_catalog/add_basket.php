<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog"))
{
    if (isset($_REQUEST['ID'])) {
        $patern = "/^[0-9]/";
        $ID = intval($_REQUEST['ID']);
        $COUNT = intval($_REQUEST['COUNT']);

        if (preg_match($patern, $ID)) {
            Add2BasketByProductID(
                $ID,
                $COUNT,
                false
            );
        }
    }

    else {

        echo "Нет параметров ";
    }
}

else {

    echo "Не подключены модули";
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");