<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="order-list js-main-modal page__content">
	<div class="container">
		<div class="order-list__wrap">
			<div class="order-list__content">
				<div class="order-list__topic">
					<div class="topic"><?=GetMessage("ORDER_NAME");?></div>
				</div>
				<a class="sale-order-history-link" href="/personal-account/orders/">Order list</a>
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket.line",
					"b2b_basket_line",
					Array(
						"HIDE_ON_BASKET_PAGES" => "Y",
						"PATH_TO_BASKET" => SITE_DIR."personal-account/basket.php",
						"PATH_TO_ORDER" => SITE_DIR."personal-account/order.php",
						"PATH_TO_PERSONAL" => SITE_DIR."personal/",
						"PATH_TO_PROFILE" => SITE_DIR."personal/",
						"PATH_TO_REGISTER" => SITE_DIR."login/",
						"POSITION_FIXED" => "N",
						"SHOW_AUTHOR" => "N",
						"SHOW_EMPTY_VALUES" => "N",
						"SHOW_NUM_PRODUCTS" => "Y",
						"SHOW_PERSONAL_LINK" => "N",
						"SHOW_PRODUCTS" => "N",
						"SHOW_TOTAL_PRICE" => "Y"
					)
				);?>
				<table class="table-100" border="1" cellpadding="0" cellspacing="0">
					<tbody>
					<tr id="<?=$strMainID?>">
						<td class="img-prod first-title">
							<div>Photo</div>
						</td>
						<td class="first-title">
							<div>Name</div>
						</td>
						<td class="first-title">
							<div>Price</div>
						</td>
						<td class="first-title">
							<div>Discount</div>
						</td>
						<td class="first-title">
							<div>Quantity</div>
						</td>
						<td class="first-title">
							<div></div>
						</td>
					</tr>
					<?
					foreach ($arResult['ITEMS'] as $key => $arItem) {
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
						$strMainID = $this->GetEditAreaId($arItem['ID']);
						$productTitle = (
						isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
							? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
							: $arItem['NAME']
						);
						$imgTitle = (
						isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
							? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
							: $arItem['NAME']
						);
						$minPrice = false;
						if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
							$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
						?>
						<tr id="<?=$strMainID?>" class="product-item" rel="1">
							<td class="img-prod">
								<div class="img-align"><img src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" /></div>
							</td>
							<td>
								<div><?=$productTitle?></div>
							</td>
							<td>
								<div><?echo $minPrice['PRINT_DISCOUNT_VALUE'];?></div>
								<?if ($minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']):?>
									<div class="old-price"><? echo $minPrice['PRINT_VALUE']; ?></div>
								<?endif;?>
							</td>
							<td>
								<div><? echo $minPrice['DISCOUNT_DIFF_PERCENT'] > 0 ? $minPrice['DISCOUNT_DIFF_PERCENT']." %" : ""; ?></div>
							</td>
							<td>
								<input type="hidden" id="count" name="count" value="1">
								<a href="#" class="minus_cols_button"><span>-</span></a>
								<input type="text" value="1" class="count" />
								<a href="#" class="plus_cols_button"><span>+</span></a>
							</td>
							<td>
								<a class="btn-buy" data-id="<?=$arItem["ID"]?>" href="javascript:void(0);">Add</a>
							</td>
						</tr>
						<?
					}
					?>
					</tbody>
				</table>
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket.line",
					"b2b_basket_line",
					Array(
						"COMPONENT_TEMPLATE" => "b2b_basket_line",
						"HIDE_ON_BASKET_PAGES" => "Y",
						"PATH_TO_BASKET" => SITE_DIR."personal-account/basket.php",
						"PATH_TO_ORDER" => SITE_DIR."personal-account/order.php",
						"PATH_TO_PERSONAL" => SITE_DIR."personal/",
						"PATH_TO_PROFILE" => SITE_DIR."personal/",
						"PATH_TO_REGISTER" => SITE_DIR."login/",
						"POSITION_FIXED" => "N",
						"SHOW_AUTHOR" => "N",
						"SHOW_EMPTY_VALUES" => "N",
						"SHOW_NUM_PRODUCTS" => "Y",
						"SHOW_PERSONAL_LINK" => "N",
						"SHOW_PRODUCTS" => "N",
						"SHOW_TOTAL_PRICE" => "Y"
					)
				);?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var parent_tag, attr_tag;

	$(".minus_cols_button").click(function(e){

		e.preventDefault();

		parent_tag = $(this).parent();
		attr_tag = parseInt($(this).closest(".product-item").attr("rel"));

		if($("input", parent_tag).val() > attr_tag){
			var res_val = $("input", parent_tag).val() - attr_tag;
			$("input", parent_tag).val(res_val);
		}
	});

	$(".plus_cols_button").click(function(e){

		e.preventDefault();

		parent_tag = $(this).parent();
		attr_tag = parseInt($(this).closest(".product-item").attr("rel"));

		var res_val = parseInt($("input", parent_tag).val());
		alert(res_val);
		res_val = res_val + attr_tag;
		$("input", parent_tag).val(res_val);
	});

	$('a.btn-buy').click(function() {

		var element = $(this);
		var ID = element.data('id');
		var parent = $(this).closest(".product-item");
		var CNT = $('#count', parent).attr('value');

		$.ajax({
			type: "POST",
			url: "<?=$templateFolder?>/add_basket.php",
			data: {
				ID: ID,
				COUNT: CNT,
			},
			success: function(msg){
				BX.onCustomEvent('OnBasketChange');
				element.html("Checkout");
			}

		});

	});
</script>

</div>