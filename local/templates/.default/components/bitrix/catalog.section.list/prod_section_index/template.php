<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (0 < $arResult["SECTIONS_COUNT"]):?>
	<div class="home__menu-products-wrap js-anchor">
		<div class="menu-products menu-products_style_header">
			<div class="menu-products__wrap">
				<?foreach ($arResult['SECTIONS'] as &$arSection):?>
					<?
					$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
					$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
					if ($arSection["UF_MENU_CHECK"]):
					?>
					<div class="menu-products__item-wrap" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
						<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="menu-products__item">
							<span class="menu-products__icon-wrap">
								<svg class="menu-products__icon"><use xlink:href="<?=$arSection["UF_ICON"]?>#<?=$arSection["UF_ICON_SELECT"]?>"></use></svg>
							</span>
							<span class="menu-products__text"><?=$arSection['NAME']?></span>
						</a>
					</div><?
					endif;
				endforeach;?>
			</div>
		</div>
	</div>
<?endif?>