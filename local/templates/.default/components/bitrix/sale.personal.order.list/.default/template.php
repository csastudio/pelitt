<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

	<?$component = $this->__component;?>
	<?if($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])):?>
		<?$APPLICATION->AuthForm('', false, false, 'N', false);?>
	<?endif?>

<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>


	<?if(!empty($arResult['ORDERS'])):?>

		<div class="order-list js-main-modal page__content">
			<div class="container">
				<div class="order-list__wrap">
					<div class="order-list__content">
						<div class="order-list__topic">
							<div class="topic"><?=GetMessage("HISTORY_ORDERS");?></div>
						</div>
						<div class="order-list__history">
							<?foreach($arResult["ORDER_BY_STATUS"] as $key => $group):?>

								<?
								foreach($group as $k => $order):
									$countItems = count($order["BASKET_ITEMS"]);
									?>
									<div class="order-list__card">
										<a href="javascript:void(0);" data-id="<?=$order["ORDER"]["ID"]?>" data-type="<?=LANGUAGE_ID?>" data-popup="popupOrder1" class="popup-id card-history js-popup-open">
											<span class="card-history__badge">
												<span class="badge">
													<span class="badge__content badge__content_style_l-<?=($arResult["COLORS"][$order["ORDER"]["STATUS_ID"]])?>">
														<span class="badge__text"><?=$arResult["STATUSES"][$order["ORDER"]["STATUS_ID"]] ?></span>
													</span>
												</span>
											</span>
											<span class="card-history__wrap">
												<span class="card-history__content">
													<span class="card-history__order"><?=GetMessage("SPOL_NUM_SIGN").' '.$order["ORDER"]["ACCOUNT_NUMBER"]?></span>
													<span class="card-history__date"><?=GetMessage("SPOL_FROM").' '.$order["ORDER"]["DATE_INSERT_FORMATED"]?></span>
												</span>
												<span class="card-history__content">
													<span class="card-history__price"><?=$order["ORDER"]["FORMATED_PRICE"]?></span>
													<span class="card-history__product"><?=$countItems.' '.GetMessage("SPOL_COUNT_ITEMS")?></span>
												</span>
											</span>
										</a>
									</div>

								<?endforeach;?>

							<?endforeach;?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popupOrder1" class="popup popup_type_order js-popup">
			<div class="popup__close-modal js-popup-close">
				<div class="close close_style_modal">
					<div class="close__button">
						<svg class="close__icon"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_close-btn"></use></svg>
					</div>
				</div>
			</div>
			<div class="popup__wrapper">
				<div class="popup-product"></div>
			</div>
		</div>

	<?else:?>
<div class="payment-delivery page__content content_in">
    <div class="payment-delivery__wrap">
        <div class="payment-delivery__payment description__schedule">
            <div class="payment-delivery__description">
		<?=GetMessage('SPOL_NO_ORDERS')?>
            </div>
        </div>
    </div>
</div>
	<?endif?>

<?endif?>

<script>
	$(function() {
		$('.popup-id').click(function() {

			$.get('<?=$templateFolder.'/ajax.php'?>',
				{
					ITEM_ID : $(this).data('id'),
                    LANG : $(this).data('type')
				}, 'html')
				.success(function(res) {
					$('#popupOrder1 .popup-product').html(res);

					console.log(res);
				})
				.error(function(res) {
					alert('Error! Please try again!');
				});
		});
	});
</script>
