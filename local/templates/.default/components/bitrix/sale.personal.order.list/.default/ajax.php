<?php
/**
 * @global CMain $APPLICATION
 * @global CDatabase $DB
 * @global CUser $USER
 */
require $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";

$APPLICATION->IncludeComponent(
    "bitrix:sale.personal.order.detail",
    "",
    Array(
        "LANG" => $_REQUEST['LANG'],
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CUSTOM_SELECT_PROPS" => array(""),
        "ID" => $_REQUEST["ITEM_ID"],
        "PATH_TO_CANCEL" => "",
        "PATH_TO_LIST" => "",
        "PATH_TO_PAYMENT" => "payment.php",
        "PICTURE_HEIGHT" => "110",
        "PICTURE_RESAMPLE_TYPE" => "1",
        "PICTURE_WIDTH" => "110",
        "PROP_1" => array(),
        "SET_TITLE" => "Y"
    )
);