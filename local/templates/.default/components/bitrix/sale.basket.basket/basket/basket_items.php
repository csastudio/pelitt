<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);
?>
<div class="basket__wrap">
	<div class="basket__topic">
		<div class="topic"><?=GetMessage("SALE_BASKET");?></div>
	</div>
	<div class="basket__row">
		<div class="basket__col-r">
			<div class="basket__card-basket">
				<div class="card-basket" id="basket_items"><?
					foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
						if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
						?>
						<div class="card-basket__item" id="<?=$arItem["ID"]?>">
							<div class="card-basket__info">
								<?
								if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
									$url = $arItem["PREVIEW_PICTURE_SRC"];
								elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
									$url = $arItem["DETAIL_PICTURE_SRC"];
								else:
									$url = $templateFolder."/images/no_photo.png";
								endif;
								?>
								<img src="<?=$url?>" class="card-basket__view" alt="" role="presentation" />
								<div class="card-basket__descr">
									<div class="card-basket__name">
										<div class="card-basket__category"><?=$arItem["SECTION_NAME"]?></div>
										<div class="card-basket__product"><?=$arItem["NAME"] ?></div>
									</div>
									<a class="card-basket__delete" href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>">
										<svg class="card-basket__icon"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_close-btn"></use></svg>
										<div class="card-basket__text"><?=GetMessage("SALE_DELETE");?></div>
									</a>
								</div>
							</div>
							<div class="card-basket__quantity">
								<div class="card-basket__counter">
									<div class="counter">
										<div class="counter__wrap">
											<?
											if (!isset($arItem["MEASURE_RATIO"]))
											{
												$arItem["MEASURE_RATIO"] = 1;
											}

											$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
											$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
											$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
											$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
											?>
											<div data-oper="-1" class="counter__dec" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"></div>
											<input
												type="text"
												class="counter__view"
												size="3"
												id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
												name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
												size="2"
												maxlength="18"
												min="0"
												<?=$max?>
												step="<?=$ratio?>"
												style="max-width: 50px"
												value="<?=$arItem["QUANTITY"]?>"
												onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
											>
											<div data-oper="-1" class="counter__inc" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"></div>
											<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
										</div>
									</div>
								</div>
								<div class="card-basket__price"><?=$arItem["SUM"]?></div>
							</div>
						</div><?
						endif;
					endforeach;
					?>
				</div>
                <?if (SITE_DIR == '/ru/'){?>
                <div style="padding: 20px"><label style="font-size: 12px"><input id="agree" type="checkbox">
                        Отправляя заказ, Вы соглашаетесь принимать телефонные звонки,SMS-сообщения и электронные письма, связанные с Вашим заказом и подтверждаете, что ознакомились и&nbsp;согласны с
                        <a href="/ru/agreement/" class="breadcrumbs__link">Пользовательским соглашением</a>,
                        <a href="/ru/privacy-policy/" class="breadcrumbs__link">Политикой конфиденциальности</a> и
                        <a href="/ru/rules/" class="breadcrumbs__link">Правилами обмена и возврата</a>
                    </label>
                </div>
                    <script>
                        $(document).ready(function(){

                            if ($("#agree").prop("checked")) {
                                $('#continue').prop('disabled', false);
                            }else{
                                $('#continue').prop('disabled', true);
                            }

                            $('#agree').change(function() {

                                $('#continue').prop('disabled', function(i, val) {
                                    return !val;
                                })
                            });
                        })
                        </script>
                <?}?>
				<div class="bx_ordercart_order_pay_left" id="coupons_block">
					<?
					if ($arParams["HIDE_COUPON"] != "Y")
					{
						?>
						<div style="background: #f2f6fb;" class="ui-kit__content">
							<div class="subscribe">
								<div class="subscribe__wrap" style="width: 100%;">
									<div class="field field_style_subscribe js-field subscribe__input">
										<input type="text" placeholder="<?=GetMessage('COUPON_CODE'); ?>" id="coupon" name="COUPON" value="" onchange="enterCoupon();" class="field__input js-field-input">
										<div class="field__validate"></div>
										<div class="field__answer"></div>
									</div>
									<a class="btn btn_style_subscribe btn_shape_ellipse subscribe__button" href="javascript:void(0)" onclick="enterCoupon();" title="<?=GetMessage('SALE_COUPON_APPLY_TITLE'); ?>">
										<div class="btn__text"><?=GetMessage('SALE_COUPON_APPLY'); ?></div>
									</a>
									<div class="subscribe__text">
										<?
										if (!empty($arResult['COUPON_LIST']))
										{
											foreach ($arResult['COUPON_LIST'] as $oneCoupon)
											{
												$couponClass = 'disabled';
												switch ($oneCoupon['STATUS'])
												{
													case DiscountCouponsManager::STATUS_NOT_FOUND:
														$oneCoupon['CHECK_CODE_TEXT'][0] = "Купон не найден";
														break;
													case DiscountCouponsManager::STATUS_FREEZE:
														$couponClass = 'bad';
														$oneCoupon['CHECK_CODE_TEXT'][0] = "Ваш купон истек.";
														$oneCoupon['CHECK_CODE_TEXT'][1] = "";
														$oneCoupon['JS_CHECK_CODE'] = "Купон не найден";
														break;
													case DiscountCouponsManager::STATUS_APPLYED:
														$couponClass = 'good';
														break;
												}
												?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span>&nbsp;&nbsp;&nbsp;<span class="bx_ordercart_coupon_notes coupon_<?=$couponClass?>"><?
													if (isset($oneCoupon['CHECK_CODE_TEXT']))
													{
														echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
													}
													?></span></div><?
											}
											unset($couponClass, $oneCoupon);
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<?
					}
					else
					{
						?>&nbsp;<?
					}
					?>
				</div>
			</div>
		</div>
		<div class="basket__col-l">
			<div class="basket__price-badge">
				<div class="price-badge price-badge_style_basket">
					<div class="price-badge__bg">
						<div class="price-badge__container">
							<div class="price-badge__text"><?=GetMessage("SALE_SUM");?></div>
							<div class="price-badge__price"><?=$arResult["allSum"]?><?=' ' . GetMessage("SUM_SYMBOL");?></div>
						</div>
						<input type="submit" href="javascript:void(0);" onclick="checkOut();" id="continue" class="price-badge__btn" value="<?=GetMessage("SALE_ORDER");?>" style="border:none">
						<div class="price-badge__shadow"></div>
					</div>
					<div class="price-badge__wrap">
						<div class="price-badge__delivery"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?
foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader){
	$arHeaders[] = $arHeader["id"];
}
?>
<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />