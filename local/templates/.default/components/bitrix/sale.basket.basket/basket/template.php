<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
$curPage = $APPLICATION->GetCurPage().'?'.$arParams["ACTION_VARIABLE"].'=';
$arUrls = array(
	"delete" => $curPage."delete&id=#ID#",
	"delay" => $curPage."delay&id=#ID#",
	"add" => $curPage."add&id=#ID#",
);
unset($curPage);

$arBasketJSParams = array(
	'SALE_DELETE' => GetMessage("SALE_DELETE"),
	'SALE_DELAY' => GetMessage("SALE_DELAY"),
	'SALE_TYPE' => GetMessage("SALE_TYPE"),
	'TEMPLATE_FOLDER' => $templateFolder,
	'DELETE_URL' => $arUrls["delete"],
	'DELAY_URL' => $arUrls["delay"],
	'ADD_URL' => $arUrls["add"]
);
?>
<script type="text/javascript">
	var basketJSParams = <?=CUtil::PhpToJSObject($arBasketJSParams);?>
</script>
<?
$APPLICATION->AddHeadScript($templateFolder."/script.js");

if (strlen($arResult["ERROR_MESSAGE"]) <= 0)
{
	?>
	<div class="basket page__content" id="basket_content">
		<?if($_POST['ajax'] == 'Y')$APPLICATION->RestartBuffer();?>

		<div id="warning_message">
			<?
			if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"]))
			{
				foreach ($arResult["WARNING_MESSAGE"] as $v)
					ShowError($v);
			}
			?>
		</div>
		
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">
			<?
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
			?>
			<input type="hidden" name="BasketOrder" value="BasketOrder" />
		</form>
		<?if($_POST['ajax'] == 'Y')die();?>
		<div class="basket__footer">
			<div class="footer-contacts footer-contacts_type_wrapper">
				<div class="footer-contacts__wrapper">
					<div class="footer-contacts__contacts">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/include/phone_footer.php"
							)
						);?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/include/mail_footer.php"
							)
						);?>
					</div>
					<div class="footer-contacts__wrap">
						<?$APPLICATION->IncludeComponent(
	"csa:cards", 
	".default", 
	array(
		"MCARD" => "",
		"VCARD" => "",
		"WMONEY" => "",
		"YAMONEY" => "",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
						<div class="footer-contacts__copyright">
							<div class="footer-contacts__text"><?=GetMessage("COPYRIGHT");?></div>
							<!--div class="footer-contacts__text">Дизайн:
								<a href="javascript:void(0);" class="footer-contacts__text-link">WowRussianDesigner</a>
							</div-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?
}
else
{
	?>
	<div class="basket page__content" id="basket_content">
		<div class="not-found">
			<div class="not-found__title"><?=GetMessage("SALE_NO_ITEMS");?></div>
			<div class="not-found__heading"></div>
			<div class="not-found__desc"></div>
			<a href="<?=SITE_DIR?>" class="btn btn_style_key btn_shape_rectangle">
				<div class="btn__text"><?=GetMessage("SALE_HOME_PAGE");?></div>
			</a>
		</div>
		<div class="basket__footer">
			<div class="footer-contacts footer-contacts_type_wrapper">
				<div class="footer-contacts__wrapper">
					<div class="footer-contacts__contacts">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/include/phone_footer.php"
							)
						);?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/include/mail_footer.php"
							)
						);?>
					</div>
					<div class="footer-contacts__wrap">
						<?$APPLICATION->IncludeComponent(
							"csa:cards",
							"",
							Array(
								"MCARD" => "icon_mastercard",
								"VCARD" => "icon_visa",
								"WMONEY" => "icon_webmoney",
								"YAMONEY" => "icon_yandexmoney"
							)
						);?>
						<div class="footer-contacts__copyright">
							<div class="footer-contacts__text"><?=GetMessage("COPYRIGHT");?></div>
							<!--div class="footer-contacts__text">Дизайн:
								<a href="javascript:void(0);" class="footer-contacts__text-link">WowRussianDesigner</a>
							</div-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><?
}
?>