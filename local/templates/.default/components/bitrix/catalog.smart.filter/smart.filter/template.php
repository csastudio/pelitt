<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="bx_filter">
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
		<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
		<?endforeach;?>
		<div class="catalog-accessory__filter-checkbox">
			<div class="filter-checkbox">
				<?foreach($arResult["ITEMS"] as $key=>$arItem) {
					if (
						empty($arItem["VALUES"])
						|| isset($arItem["PRICE"])
					)
						continue;

					if (
						$arItem["DISPLAY_TYPE"] == "A"
						&& (
							$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
						)
					)
					continue;

					$arCur = current($arItem["VALUES"]);
					switch ($arItem["DISPLAY_TYPE"]) {
						default://CHECKBOXES
						foreach($arItem["VALUES"] as $val => $ar):?>
						<span class="bx-filter-container-modef"></span>
						<div class="checkbox bx-filter-parameters-box-container" data-role="bx_filter_block">
							<input
								type="checkbox"
								value="<? echo $ar["HTML_VALUE"] ?>"
								name="<? echo $ar["CONTROL_NAME"] ?>"
								id="<? echo $ar["CONTROL_ID"] ?>"
								<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
								onclick="smartFilter.click(this)"
								class="checkbox__input"
							/>
							<label data-role="label_<?=$ar["CONTROL_ID"]?>" for="<? echo $ar["CONTROL_ID"] ?>" class="checkbox__label <? echo $ar["DISABLED"] ? 'disabled': '' ?>"><?=$ar["VALUE"];?></label>
						</div><?
						endforeach;
					}
				}
				?>
				<div class="bx-filter-parameters-box-container" style="display: inline-block;">
					<input
						class="btn btn-themes"
						type="submit"
						id="set_filter"
						name="set_filter"
						value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
					/>
					<input
						class="btn btn-link"
						type="submit"
						id="del_filter"
						name="del_filter"
						value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
					/>
					<div class="filter-checkbox__find bx-filter-popup-result" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
<!--						<div class="filter-checkbox__find-text">&nbsp;</div>-->
<!--						--><?//echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<div id="modef_num" class="filter-checkbox__eq js-filter-checkbox-eq">'.intval($arResult["ELEMENT_COUNT"]).'</div>'));?>
						<a class="btn__filter" href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>