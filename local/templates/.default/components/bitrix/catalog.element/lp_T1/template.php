<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strMainID = $this->GetEditAreaId($arResult['ID']);

use Bitrix\Main\Page\Asset;

$AddJs = [
//    "/assets_T1/web/assets/jquery/jquery.min.js",
    "/assets_T1/tether/tether.min.js",
    "/assets_T1/popper/popper.min.js",
    "/assets_T1/bootstrap/js/bootstrap.min.js",
    "/assets_T1/jarallax/jarallax.min.js",
    "/assets_T1/masonry/masonry.pkgd.min.js",
    "/assets_T1/imagesloaded/imagesloaded.pkgd.min.js",
    "/assets_T1/smooth-scroll/smooth-scroll.js",
    "/assets_T1/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js",
    "/assets_T1/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js",
    "/assets_T1/theme/js/script.js",
    "/assets_T1/mobirise-gallery/player.min.js",
    "/assets_T1/mobirise-gallery/script.js",
    "/assets_T1/mobirise-slider-video/script.js"
];

$AddCss = [
    "/assets_T1/web/assets/mobirise-icons-bold/mobirise-icons-bold.css",
    "/assets_T1/web/assets/mobirise-icons/mobirise-icons.css",
    "/assets_T1/tether/tether.min.css",
    "/assets_T1/bootstrap/css/bootstrap.min.css",
    "/assets_T1/bootstrap/css/bootstrap-grid.min.css",
    "/assets_T1/bootstrap/css/bootstrap-reboot.min.css",
    "/assets_T1/theme/css/style.css",
    "/assets_T1/mobirise-gallery/style.css",
    "/assets_T1/mobirise/css/mbr-additional.css",
];

foreach ($AddCss as $css) {
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . $css);
}

foreach ($AddJs as $js) {
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . $js);

}
?>
<div class="card-products page__content js-main-modal" id="<?=$strMainID?>">
	<div class="product-menu js-product-menu product-menu_scroll_up">
		<div class="product-menu__wrap"><?
			if ($arResult["PROPERTIES"]["HOME"]["VALUE"]):
				?>
				<div data-menu-link="<?=$arResult["PROPERTIES"]["HOME"]["DESCRIPTION"]?>" class="product-menu__item product-menu__item_state_active js-product-link"><?=$arResult["PROPERTIES"]["HOME"]["VALUE"]?></div><?
			endif;
			if ($arResult["PROPERTIES"]["FEATURES"]["VALUE"]):
				?>
				<div data-menu-link="<?=$arResult["PROPERTIES"]["FEATURES"]["DESCRIPTION"]?>" class="product-menu__item js-product-link"><?=$arResult["PROPERTIES"]["FEATURES"]["VALUE"]?></div><?
			endif;
			if ($arResult["PROPERTIES"]["CHARACTERISTICS"]["VALUE"]):
			?>
			<div data-menu-link="<?=$arResult["PROPERTIES"]["CHARACTERISTICS"]["DESCRIPTION"]?>" class="product-menu__item  js-product-link"><?=$arResult["PROPERTIES"]["CHARACTERISTICS"]["VALUE"]?></div><?
			endif;
			if ($arResult["PROPERTIES"]["OVERVIEW"]["VALUE"]):
			?>
			<div data-menu-link="<?=$arResult["PROPERTIES"]["OVERVIEW"]["DESCRIPTION"]?>" class="product-menu__item  js-product-link"><?=$arResult["PROPERTIES"]["OVERVIEW"]["VALUE"]?></div><?
			endif;
			if ($arResult["PROPERTIES"]["FAQ_LP"]["VALUE"]):
			?>
			<div data-menu-link="<?=$arResult["PROPERTIES"]["FAQ_LP"]["DESCRIPTION"]?>" class="product-menu__item  js-product-link"><?=$arResult["PROPERTIES"]["FAQ_LP"]["VALUE"]?></div><?
			endif;
			if ($arResult["PROPERTIES"]["ADDITIONALLY"]["VALUE"]):
			?>
			<div data-menu-link="<?=$arResult["PROPERTIES"]["ADDITIONALLY"]["DESCRIPTION"]?>" class="product-menu__item  js-product-link"><?=$arResult["PROPERTIES"]["ADDITIONALLY"]["VALUE"]?></div><?
			endif;
			?>
		</div>
	</div>
<!--    --><?//gg($arResult["PROPERTIES"])?>
    <section class="cid-qzIXCjzRMe" id="image1-1" data-rv-view="169">



        <figure class="mbr-figure container">
            <div class="image-block" style="width: 66%;">
                <img src="<?=CFile::getPath($arResult["PROPERTIES"]["PHOTO_1"]["VALUE"])?>" width="1400" alt="Mobirise" title="" media-simple="true">

            </div>
        </figure>
    </section>

    <section class="engine"><a href="https://mobirise.co/i">build your own website</a></section><section class="mbr-section content4 cid-qzIY8bEYg9" id="content4-2" data-rv-view="171">



        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">
                    <h2 class="align-center pb-3 mbr-fonts-style display-1"><?=$arResult["PROPERTIES"]["TITLE_1"]["VALUE"]?></h2>
                    <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["TEXT_1"]["VALUE"])?></h3>

                </div>
            </div>
        </div>
    </section>

    <section style="background-image: url('<?=CFile::getPath($arResult["PROPERTIES"]["PHOTO_1"]["VALUE"])?>');"
             class="header9 cid-qzIZO3EAja mbr-fullscreen mbr-parallax-background"
             id="header9-3" data-rv-view="173">





        <div class="container">
            <div class="media-container-column mbr-white col-md-8">
                <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-2"><span style="font-weight: normal;"><?=$arResult["PROPERTIES"]["_2"]["VALUE"]?></span></h1>

                <p class="mbr-text align-left pb-3 mbr-fonts-style display-7"><?=$arResult["PROPERTIES"]["TEXT_2"]["VALUE"]?></p>

            </div>
        </div>


    </section>

    <section class="header10 cid-qzJ6s6AL9k mbr-fullscreen mbr-parallax-background" id="header10-4" data-rv-view="176">

        <div class="container">
            <div class="media-container-column mbr-white col-lg-8 col-md-10 ml-auto">
                <h1 class="mbr-section-title align-right mbr-bold pb-3 mbr-fonts-style display-1"><span style="font-weight: normal;"><?=$arResult["PROPERTIES"]["TITLE_3"]["VALUE"]?></span></h1>

                <p class="mbr-text align-right pb-3 mbr-fonts-style display-5"><?=$arResult["PROPERTIES"]["TEXT_3"]["VALUE"]?><br></p>

            </div>
        </div>


    </section>

    <section class="features11 cid-qzJ8SMSlTk" id="features11-6" data-rv-view="179">





        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure" style="width: 50%;">
                        <img src="<?=CFile::getPath($arResult["PROPERTIES"]["PHOTO_4"]["VALUE"])?>" alt="Mobirise" title="" media-simple="true">
                    </div>
                    <div class=" align-left aside-content">
                        <h2 class="mbr-title pt-2 mbr-fonts-style display-2"><?=$arResult["PROPERTIES"]["TITLE_4"]["VALUE"]?></h2>
                        <div class="mbr-section-text">
                            <p class="mbr-text mb-5 pt-3 mbr-light mbr-fonts-style display-5"><?=$arResult["PROPERTIES"]["TEXT_3"]["VALUE"]?><br></p>
                        </div>

                        <div class="block-content">
                            <div class="card p-3 pr-3">
                                <div class="media">
                                    <div class=" align-self-center card-img pb-3">
                                        <span class="mbr-iconfont mbrib-touch-swipe" media-simple="true"></span>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="card-title mbr-fonts-style display-7">only 0.19 seconds</h4>
                                    </div>
                                </div>

                                <div class="card-box">

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="header9 cid-qzJaeq70hc mbr-fullscreen mbr-parallax-background" id="header9-7" data-rv-view="182">





        <div class="container">
            <div class="media-container-column mbr-white col-md-8">
                <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-1"><span style="font-weight: normal;"><?=$arResult["PROPERTIES"]["TITLE_5"]["VALUE"]?></span></h1>

                <p class="mbr-text align-left pb-3 mbr-fonts-style display-5"><?=$arResult["PROPERTIES"]["TEXT_3"]["VALUE"]?><br></p>

            </div>
        </div>


    </section>

    <section class="features11 cid-qzJc4w0Xzm" id="features11-a" data-rv-view="185">





        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure" style="width: 30%;">
                        <img src="<?=CFile::getPath($arResult["PROPERTIES"]["PHOTO_6"]["VALUE"])?>" alt="Mobirise" title="" media-simple="true">
                    </div>
                    <div class=" align-left aside-content">
                        <h2 class="mbr-title pt-2 mbr-fonts-style display-2"><?=$arResult["PROPERTIES"]["TITLE_6"]["VALUE"]?></h2>
                        <div class="mbr-section-text">
                            <p class="mbr-text mb-5 pt-3 mbr-light mbr-fonts-style display-5"><?=$arResult["PROPERTIES"]["TEXT_6"]["VALUE"]?><br></p>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section article content10 cid-qzJcGW7fK5" id="content10-b" data-rv-view="188">



        <div class="container">
            <div class="inner-container" style="width: 66%;">
                <hr class="line" style="width: 25%;">
                <div class="section-text align-center mbr-white mbr-fonts-style display-5"><?=$arResult["PROPERTIES"]["NOTE_LP_T1"]["VALUE"]?></div>
                <hr class="line" style="width: 25%;">
            </div>
        </div>
    </section>

    <section class="header9 cid-qzJcPKo7xY mbr-fullscreen mbr-parallax-background" id="header9-c" data-rv-view="190">



        <div class="mbr-overlay" style="opacity: 0.2; background-color: rgb(0, 0, 0);">
        </div>

        <div class="container">
            <div class="media-container-column mbr-white col-md-8">
                <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-1"><span style="font-weight: normal;"><?=$arResult["PROPERTIES"]["TITLE_7"]["VALUE"]?></h1>

                <p class="mbr-text align-left pb-3 mbr-fonts-style display-5"><?=$arResult["PROPERTIES"]["TEXT_7"]["VALUE"]?><br></p>

            </div>
        </div>


    </section>

    <section class="cid-qzJe8DlBi7" id="video2-f" data-rv-view="193">



        <figure class="mbr-figure align-center">
            <div class="video-block" style="width: 100%;">
                <div><iframe class="mbr-embedded-video" src="<?=$arResult["PROPERTIES"]["YT_SRC"]["VALUE"]?>" width="1280" height="720" frameborder="0" allowfullscreen></iframe></div>
            </div>
        </figure>
    </section>

    <section class="mbr-gallery mbr-slider-carousel cid-qzJepq6vHH" id="gallery3-g" data-rv-view="195">



        <div>
            <div><!-- Filter --><!-- Gallery -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <div>
                            <div>
                                <?
                                foreach ($arResult["MORE_PHOTO"] as $key=>$arItemMP):
                                    ?>

                                    <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                         data-tags="Animated">
                                        <div href="#lb-gallery3-g" data-slide-to="<?=$key?>" data-toggle="modal"><img
                                                    src="<?=$arItemMP["SRC"]?>"
                                                    alt=""><span class="icon-focus"></span></div>
                                    </div>
                                    <?
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div><!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1"
                     data-keyboard="true" data-interval="false" id="lb-gallery3-g">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="carousel-inner">
                                    <?
                                    foreach ($arResult["MORE_PHOTO"] as $key=>$arItemMP):
                                        ?>
                                        <div class="carousel-item <?if ($key==0){echo 'active';}?>"><img src="<?=$arItemMP["SRC"]?>"
                                                                        alt=""></div>
                                        <?
                                    endforeach;
                                    ?>
                                </div>
                                <a class="carousel-control carousel-control-prev" role="button" data-slide="prev"
                                   href="#lb-gallery3-g"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span
                                            class="sr-only">Previous</span></a><a
                                        class="carousel-control carousel-control-next" role="button" data-slide="next"
                                        href="#lb-gallery3-g"><span class="mbri-right mbr-iconfont"
                                                                    aria-hidden="true"></span><span
                                            class="sr-only">Next</span></a><a class="close" href="#" role="button"
                                                                              data-dismiss="modal"><span
                                            class="sr-only">Close</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <?
	if($arResult["PROPERTIES"]["CHARACTERISTICS"]["VALUE"]):
		?>
		<div data-anchor="<?=$arResult["PROPERTIES"]["CHARACTERISTICS"]["DESCRIPTION"]?>" class="card-products__characteristics-wrap">
			<div class="characteristics js-characteristics">
				<div class="characteristics__wrap">
					<div class="characteristics__title"><?=GetMessage("SPECIFICATIONS");?></div>
					<div class="characteristics__show-all js-show-all"><?=GetMessage("SHOW_ALL_SPECIFICATIONS");?></div>
					<div class="characteristics__tech-list">
						<div class="tech-features js-t-parent">
							<?
							$count=1;
							foreach ($arResult["PROPS_GOUP"] as $arPropsGroup):
								foreach ($arPropsGroup as $propname => $propsGroup):
									switch ($propname) {

							            # en
										case "Main":
											$icon = "icon_main";
											$clsas = "main";
											continue;
										case "Display":
											$icon = "icon_screen";
											$clsas = "screen";
											continue;
										case "Product Features":
											$icon = "icon_techs";
											$clsas = "techs";
											continue;
										case "Functions":
											$icon = "icon_func";
											$clsas = "func";
											continue;
										case "Battery":
											$icon = "icon_power";
											$clsas = "power";
											continue;
										case "Additional Information":
											$icon = "icon_info";
											$clsas = "info";
											continue;
										case "Комплектация":
											$icon = "icon_complect";
											$clsas = "complect";
											continue;

							            # ru
										case "Основные":
											$icon = "icon_main";
											$clsas = "main";
											continue;
										case "Дисплей":
											$icon = "icon_screen";
											$clsas = "screen";
											continue;
										case "Особенности":
											$icon = "icon_techs";
											$clsas = "techs";
											continue;
										case "Функции":
											$icon = "icon_func";
											$clsas = "func";
											continue;
										case "Аккумулятор":
											$icon = "icon_power";
											$clsas = "power";
											continue;
										case "Дополнительная информация":
											$icon = "icon_info";
											$clsas = "info";
											continue;
										case "Комплектация":
											$icon = "icon_complect";
											$clsas = "complect";
											continue;

											#  de
										case "Hauptinformationen":
											$icon = "icon_main";
											$clsas = "main";
											continue;
										case "Anzeige":
											$icon = "icon_screen";
											$clsas = "screen";
											continue;
										case "Produkteigenschaften":
											$icon = "icon_techs";
											$clsas = "techs";
											continue;
										case "Funktionen":
											$icon = "icon_func";
											$clsas = "func";
											continue;
										case "Akku":
											$icon = "icon_power";
											$clsas = "power";
											continue;
										case "Hauptinformationen":
											$icon = "icon_info";
											$clsas = "info";
											continue;

                                        # cz
                                        case "Hlavní":
                                            $icon = "icon_main";
                                            $clsas = "main";
                                            continue;
                                        case "Displej":
                                            $icon = "icon_screen";
                                            $clsas = "screen";
                                            continue;
                                        case "Vlastnosti produktu":
                                            $icon = "icon_techs";
                                            $clsas = "techs";
                                            continue;
                                        case "Funkce":
                                            $icon = "icon_func";
                                            $clsas = "func";
                                            continue;
                                        case "Baterie":
                                            $icon = "icon_power";
                                            $clsas = "power";
                                            continue;
                                        case "Další informace":
                                            $icon = "icon_info";
                                            $clsas = "info";
                                            continue;

                                        # pl
                                        case "Informacje główne":
                                            $icon = "icon_main";
                                            $clsas = "main";
                                            continue;
                                        case "Ekran":
                                            $icon = "icon_screen";
                                            $clsas = "screen";
                                            continue;
                                        case "Komponenty":
                                            $icon = "icon_techs";
                                            $clsas = "techs";
                                            continue;
                                        case "Funkcjonalności":
                                            $icon = "icon_func";
                                            $clsas = "func";
                                            continue;
                                        case "Bateria":
                                            $icon = "icon_power";
                                            $clsas = "power";
                                            continue;
                                        case "Dodatkowe informacje":
                                            $icon = "icon_info";
                                            $clsas = "info";
                                            continue;
                                        case "Комплектация":
                                            $icon = "icon_complect";
                                            $clsas = "complect";
                                            continue;
										default:
											$icon = "icon_main";
											$clsas = "main";
									}
									?>
									<div class="tech-features__item <?if ($count == 1):?>tech-features__item_state_active<?endif;?> js-t-item">
										<div class="tech-features__top-info">
											<div class="tech-features__title"><?=$propname?></div>
											<div class="tech-features__view tech-features__view_type_<?=$clsas?>">
												<svg class="tech-features__icon"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#<?=$icon?>"></use></svg>
											</div>
										</div>
										<div class="tech-features__card-wrap">
											<div class="tech-card js-tech-card tech-card tech-card_collapse">
												<div class="tech-card__content-wrap js-tech-wrap"><?
													foreach ($propsGroup as $arItemProp):
														if ($arItemProp["CODE"] == "COMMENT"):
															?>
															<div class="tech-comment"><?=$arItemProp["VALUE"]?></div><?
														else:
															?>
															<div class="tech-card__row">
																<div class="tech-card__row-type"><?=$arItemProp["NAME"]?></div>
																<div class="tech-card__row-text"><?=$arItemProp["VALUE"]?></div>
															</div><?
														endif;
													endforeach;
													?>
												</div>
											</div>
										</div>
									</div><?
									$count++;
								endforeach;
							endforeach;
							// Подключение комплектации отдельно в множественном свйостве
							if (is_array($arResult["PROPERTIES"]["COMPLECT"]["VALUE"]) && count($arResult["PROPERTIES"]["COMPLECT"]["VALUE"]) > 0):
								?>
								<div class="tech-features__item js-t-item">
									<div class="tech-features__top-info">
										<div class="tech-features__title">Комплектация</div>
										<div class="tech-features__view tech-features__view_type_complect">
											<svg class="tech-features__icon"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_complect"></use></svg>
										</div>
									</div>
									<div class="tech-features__card-wrap">
										<div class="tech-card js-tech-card tech-card tech-card_collapse">
											<div class="tech-card__content-wrap js-tech-wrap"><?
												foreach ($arResult["PROPERTIES"]["COMPLECT"]["VALUE"] as $arComplect):
													?>
													<div class="tech-card__describe-item"><?=$arComplect?></div><?
												endforeach;
												?>
											</div>
										</div>
									</div>
								</div><?
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div><?
	endif;
	if($arResult["PROPERTIES"]["OVERVIEW"]["VALUE"]):

		?>
		<div data-anchor="<?=$arResult["PROPERTIES"]["OVERVIEW"]["DESCRIPTION"]?>" class="card-products__overview"><?

		if ($arResult['PROPERTIES']['SLIDE_VIDEO_SELECT']['VALUE']){

			include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/slider_video_lp.php");

		}
		?></div><?
	endif;
	/*
	if($arResult["PROPERTIES"]["MENU_CARD"]["DESCRIPTION"][4] && $arResult["PROPERTIES"]["YANDEX_ID"]["VALUE"]):

		include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/reviews.php");

	else:

		echo '<div style="padding-top: 120px"></div>';

	endif;
	*/
	if($arResult["PROPERTIES"]["FAQ_LP"]["VALUE"] && !empty($arResult["SECTIONS"])):
		?>
		<div data-anchor="<?=$arResult["PROPERTIES"]["FAQ_LP"]["DESCRIPTION"]?>" class="card-products__faq-title"><?=GetMessage("FAQ");?></div>
		<div class="card-products__faq-wrap">
			<div class="faq">
				<div class="faq__col-left">
					<div class="list-items js-list-items"><?
						foreach ($arResult["SECTIONS"] as $key => $arSection):
							// Обычный динамический таб
							$rsGender = CUserFieldEnum::GetList(array(), array(
								"ID" => $arSection["UF_SET"],
							));
							if($arGender = $rsGender->GetNext())
								$arSection["UF_SET"] = $arGender["XML_ID"];
							if ($arSection["UF_SET"] == "MAIN_TAB"):
								?>
								<a href="javascript:void(0);" data-tab="faq_<?=$key?>" class="list-items__item <?if ($key == 0):?>list-items__item_state_active<?endif;?> js-list-item">
									<span class="list-items__item-text"><?=$arSection["NAME"]?></span>
								</a><?
							endif;
						endforeach;
						?>
					</div>
					<div class="list-items js-list-items"><?
						foreach ($arResult["SECTIONS"] as $key => $arSection):
							// Таб с подключением формы запроса в ТП
							$rsGender = CUserFieldEnum::GetList(array(), array(
								"ID" => $arSection["UF_SET"],
							));
							if($arGender = $rsGender->GetNext())
								$arSection["UF_SET"] = $arGender["XML_ID"];
							if ($arSection["UF_SET"] == "DOP_MENU"):
								?>
								<a href="javascript:void(0);" data-tab="" class="<?if ($arSection["UF_CALL_POPUP"]):?>js-support-modal-button<?endif;?> list-items__item">
									<span class="list-items__item-text list-items__item-text_green"><?echo $arSection["UF_CONTACT"] ? $arSection["DESCRIPTION"] : $arSection["NAME"]?></span>
								</a><?
							endif;
						endforeach;
						?>
					</div>
				</div>
				<div class="faq__col-right">
					<div class="tabs js-tabs"><?
						// Табы из разделов, статичные
						foreach ($arResult["SECTIONS"] as $key => $arSection):
							// Динамический таб
							$rsGender = CUserFieldEnum::GetList(array(), array(
								"ID" => $arSection["UF_SELECT_IB"],
							));
							if($arGender = $rsGender->GetNext())
								$arSection["UF_SELECT_IB"] = $arGender["XML_ID"];
							if ($arSection["UF_SELECT_IB"] == "FAQ"):
								?>
								<div data-tab-content="faq_<?=$key?>" class="overflow-scroll-tab tabs__content js-tab-content">
									<div class="accordeon js-accordeon"><?
										// Подключение компонента - вопросы и ответы
										if ($arResult['PROPERTIES']['FAQ_TAB']['VALUE']){

											include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/faq_tab.php");

										}
										?>
									</div>
								</div><?
							elseif ($arSection["UF_SELECT_IB"] == "DOC"):
								?>
								<div data-tab-content="faq_<?=$key?>" class="tabs__content js-tab-content">
									<div class="tabs__text"><?
										foreach ($arResult["DOCS"] as $arDocs):
											?>
											<a class="tabs-text__doc" href="<?=$arDocs["SRC"]?>" target="_blank"><?=$arDocs["ORIGINAL_NAME"]?></a><br />
											<?
										endforeach;
										?>
									</div>
								</div><?
							endif;
						endforeach;
						?>
					</div>
				</div>
			</div>
		</div><?
	endif;
	if($arResult["PROPERTIES"]["ADDITIONALLY"]["DESCRIPTION"]):
		?>
		<div data-anchor="<?=$arResult["PROPERTIES"]["ADDITIONALLY"]["DESCRIPTION"]?>" class="card-products__filter-products-wrap">
			<div class="filter-products">
				<div class="filter-products__filter-list">
					<?if ($arResult['PROPERTIES']['ACCESSORISE']['VALUE']):?><a href="javascript:void(0);" data-tab="prod1" class="filter-products__filter filter-products__filter_state_active js-fl-switch"><?=GetMessage("ACCESSORISE");?></a><?endif;?>
					<?if ($arResult['PROPERTIES']['SIMILAR']['VALUE']):?><a href="javascript:void(0);" data-tab="prod2" class="filter-products__filter js-fl-switch"><?echo GetMessage("SIMILAR_DEV").' '.mb_strtolower($arResult["SECTION_NAME"]);?></a><?endif;?>
				</div>
				<div data-tab="prod1" class="filter-products__cards-list filter-products__cards-list_state_active js-fl-pane"><?

					if ($arResult['PROPERTIES']['ACCESSORISE']['VALUE']){

						include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/accessories.php");

					}
					?>
				</div>
				<div data-tab="prod2" class="filter-products__cards-list js-fl-pane"><?

					if ($arResult['PROPERTIES']['SIMILAR']['VALUE']){

						include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/similar.php");

					}
					?>
				</div>
			</div>
		</div><?
	endif;
	?>
</div>