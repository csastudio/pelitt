<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="market_id" style="display:none"><?=$arResult["PROPERTIES"]["YANDEX_ID"]["VALUE"]?></div>

<?
global $revID;

$revID = $arResult["PROPERTIES"]["YANDEX_ID"]["VALUE"];

if ($USER->IsAdmin()) {
    $url = "https://api.content.market.yandex.ru/v1/model/" . $arResult["PROPERTIES"]["YANDEX_ID"]["VALUE"] . "/opinion.json?count=30";

    $headers = array(
        "Host: api.content.market.yandex.ru",
        "Accept: */*",
        "Authorization: 5Y5QkYoiZTOiE2G9wrzC4Hhr19RCK4"
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);

    curl_close($ch);

    $rev = json_decode($data, true);
    $grade_total = 0;
    $best = 0;
    $bad = 0;
    $etc = 0;
    if (isset($rev['errors'])) {
        $rev['modelOpinions']['opinion'] = array();
    } else if ($rev['modelOpinions']['total'] <= 0) {
        $rev['modelOpinions']['opinion'] = array();
    } else {
        $count = $rev['modelOpinions']['count'];
        foreach ($rev['modelOpinions']['opinion'] as $val) {
            $grade_total += $val['grade'] + 3;
            if ($val['author'] == 'undefined') {
                $val['author'] = '';
            }
            $val['grade'] += 3;
            if ($val['grade'] >= 4) {
                $best++;
            } else if ($val['grade'] < 3) {
                $bad++;
            } else {
                $etc++;
            }
        }
        $grade_total = (int)($grade_total / $count);
    }
}
/*
foreach ($rev['modelOpinions']['opinion'] as $val) {

    $el = new CIBlockElement;

    $PROP = array();
    $PROP[424] = $val['pro'];    // Достоинство
    $PROP[425] = $val['contra']; // Недостатки
    $PROP[426] = $val['text'];   // Комментарий
    $PROP[427] = $val['date'];   // Дата
    $PROP[428] = $val['author']; // Автор
    $PROP[429] = $val['grade'];  // Рейтинг
    $PROP[430] = $grade_total;   // Рейтинг (общий)

    $arLoadProductArray = Array(
        "MODIFIED_BY"    => $USER->GetID(),
        "IBLOCK_SECTION_ID" => $arResult["PROPERTIES"]["REV_SECTION"]["VALUE"],
        "IBLOCK_ID"      => 35,
        "PROPERTY_VALUES"=> $PROP,
        "NAME"           => $val['id'],
        "CODE"           => $val['id'],
        "ACTIVE"         => "Y"
    );

    $el->Add($arLoadProductArray);
}*/
?>

<div data-anchor="<?=$arResult["PROPERTIES"]["MENU_CARD"]["DESCRIPTION"][4]?>" id="otzivi" class="card-products__slider-comments-wrap">
    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "reviews",
        array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "FILTER_NAME" => "",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => "35",
            "IBLOCK_TYPE" => "content",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "Y",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "30",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Новости",
            "PARENT_SECTION" => $arResult["PROPERTIES"]["REV_SECTION"]["VALUE"],
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "PROPERTY_CODE" => array(
                0 => "AUTOR_REV",
                1 => "DATA_REV",
                2 => "PRO_REV",
                3 => "TEXT_REV",
                4 => "CONTRA_REV",
                5 => "RATING_REV",
                6 => "RATING_ALL_SUMM_REV",
                7 => "",
            ),
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SORT_BY1" => "PROPERTY_DATA_REV",
            "SORT_BY2" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "ASC",
            "COMPONENT_TEMPLATE" => "reviews"
        ),
        false
    );?>
</div>