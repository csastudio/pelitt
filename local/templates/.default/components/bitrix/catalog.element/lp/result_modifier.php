<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
if(CSite::InDir('/')){
	$arResult["GROUP_SELECT"] = 22;
	$arResult["TECH_FEA"] = 21;
	$arResult["TABS"] = 27;
	$arResult["ACCESS_SIMILAR"] = 6;
}
if(CSite::InDir('/cz/')){
	$arResult["GROUP_SELECT"] = 38;
	$arResult["TECH_FEA"] = 33;
	$arResult["TABS"] = 44;
	$arResult["ACCESS_SIMILAR"] = 31;
}
if(CSite::InDir('/pl/')){
	$arResult["GROUP_SELECT"] = 39;
	$arResult["TECH_FEA"] = 37;
	$arResult["TABS"] = 45;
	$arResult["ACCESS_SIMILAR"] = 34;
}
if(CSite::InDir('/de/')){
	$arResult["GROUP_SELECT"] = 64;
	$arResult["TECH_FEA"] = 62;
	$arResult["TABS"] = 63;
	$arResult["ACCESS_SIMILAR"] = 59;
}
if(CSite::InDir('/ru/')){
	$arResult["GROUP_SELECT"] = 95;
	$arResult["TECH_FEA"] = 62;
	$arResult["TABS"] = 63;
	$arResult["ACCESS_SIMILAR"] = 59;
}

$resImg = CFile::GetFileArray($arResult["PROPERTIES"]["PHOTO_LP_BANER"]["VALUE"]);
$arResult["PROPERTIES"]["PHOTO_LP_BANER"]["VALUE"] = $resImg;

$resImg_2 = CFile::GetFileArray($arResult["PROPERTIES"]["PHOTO_LP_BANER_2"]["VALUE"]);
$arResult["PROPERTIES"]["PHOTO_LP_BANER_2"]["VALUE"] = $resImg_2;

// Получаем CODE раздела из свойства привязки
if (strlen($arResult["PROPERTIES"]["SELECT_SECTION"]["VALUE"]) > 0){

	$arrFilter = array("IBLOCK_ID" => $arResult["GROUP_SELECT"], "ACTIVE" => "Y", "ID" => $arResult["PROPERTIES"]["SELECT_SECTION"]["VALUE"]);
	$arrSelect = array("ID", "NAME", "CODE");
	$DBList = CIBlockSection::GetList(array("SORT"=>"ASC"), $arrFilter, false, $arrSelect, false);
	if ($arSectionCode = $DBList->GetNext());
}
// Получаем символьный код текущего раздела
if ($arResult["IBLOCK_SECTION_ID"]){

	$arrOrder = array("SORT"=>"ASC");
	$arrFilter = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $arResult["IBLOCK_SECTION_ID"]);
	$arrSelect = array("ID", "NAME", "CODE");
	$DBList = CIBlockSection::GetList($arrOrder, $arrFilter, false, $arrSelect, false);
	if ($arCode = $DBList->GetNext());
}
// Переопределяем ID раздела из свойства привязки при необходимости
if (strlen($arSectionCode["CODE"]) > 0){
	$arCode["CODE"] = $arSectionCode["CODE"];
}

// Группировка свойст
$arSelect = Array();
$arFilter = Array("IBLOCK_ID"=> $arResult["GROUP_SELECT"], "ACTIVE"=>"Y", "SECTION_CODE" => $arCode["CODE"], "INCLUDE_SUBSECTIONS" => "N");
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$arFields["PROPERTIES"] = $ob->GetProperties();

	$arArray[] = array(
		"NAME" => $arFields["NAME"],
		"CODE" => $arFields["PROPERTIES"]["STRING_PROPS"]["VALUE"]
	);

}
// Формируем новый массив сгруппированных свойств
$ar_PropertyResult = array();
foreach ($arResult['PROPERTIES'] as $value)
{
	if (strlen($value['VALUE']) > 0)
	{
		foreach ($arArray as $cat_name => $ar_code)
		{

			if (in_array($value['CODE'], $ar_code["CODE"]))
			{
				$arResult["PROPS_GOUP"][$cat_name][$ar_code['NAME']][] = $value;
				break;
			}
		}
	}

}
// Сортируем по возрастаню ключа массива
ksort($arResult["PROPS_GOUP"]);

// Получаем пользовательские свойста раздела
if (CModule::IncludeModule('iblock')){
	$arFilter = Array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ACTIVE'=>'Y', 'ID' => $arResult['IBLOCK_SECTION_ID']);
	$uf_name = Array("UF_TEMPLATE");
	$db_list = CIBlockSection::GetList(Array(), $arFilter, false, $uf_name);
	if($uf_value = $db_list->GetNext()){
		$arResult["UF_TEMPLATE"] = $uf_value["UF_TEMPLATE"];
		// Получаем название раздела
		$arResult["SECTION_NAME"] = $uf_value["NAME"];
	}
	$rsGender = CUserFieldEnum::GetList(array(), array(
		"ID" => $uf_value["UF_TEMPLATE"],
	));
	if($arGender = $rsGender->GetNext())
		$arResult['UF_TEMPLATE'] = $arGender["XML_ID"];
}
// Табы из разделов ИБ Вопрсоы и ответы
if (CModule::IncludeModule('iblock')){
	$arOrder = array("SORT"=>"ASC");
	$arFilter = array("IBLOCK_ID" => $arResult["TABS"], "ACTIVE" => "Y", "ID" => $arResult["PROPERTIES"]["SELECT_FAQ"]["VALUE"]);
	$arSelect = array("ID", "NAME", "DESCRIPTION", "UF_SET", "UF_CONTACT", "UF_SELECT_IB", "UF_CALL_POPUP");
	// Получаем разделы
	$dbList = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);
	// Формируем массив табов
	while($arResultSection = $dbList->GetNext()){
		$arResult["SECTIONS"][] = $arResultSection;
	}
}

// Транслитерация названий разделов меню для LP
$trans = Cutil::translit($arResult["PROPERTIES"]["HOME"]["VALUE"],"ru",$arParams);
$arResult["PROPERTIES"]["HOME"]["DESCRIPTION"] = $trans;

$trans = Cutil::translit($arResult["PROPERTIES"]["FEATURES"]["VALUE"],"ru",$arParams);
$arResult["PROPERTIES"]["FEATURES"]["DESCRIPTION"] = $trans;

$trans = Cutil::translit($arResult["PROPERTIES"]["CHARACTERISTICS"]["VALUE"],"ru",$arParams);
$arResult["PROPERTIES"]["CHARACTERISTICS"]["DESCRIPTION"] = $trans;

$trans = Cutil::translit($arResult["PROPERTIES"]["OVERVIEW"]["VALUE"],"ru",$arParams);
$arResult["PROPERTIES"]["OVERVIEW"]["DESCRIPTION"] = $trans;

$trans = Cutil::translit($arResult["PROPERTIES"]["FAQ_LP"]["VALUE"],"ru",$arParams);
$arResult["PROPERTIES"]["FAQ_LP"]["DESCRIPTION"] = $trans;

$trans = Cutil::translit($arResult["PROPERTIES"]["ADDITIONALLY"]["VALUE"],"ru",$arParams);
$arResult["PROPERTIES"]["ADDITIONALLY"]["DESCRIPTION"] = $trans;

// Формируем массив для свойства - документы
foreach ($arResult['PROPERTIES']['DOC_FILE']['VALUE'] as $docs){
	$arResult["DOCS"][] = CFile::GetFileArray($docs);
}