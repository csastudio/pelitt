<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="card-products__slider-products-wrap js-anchor">
	<div class="slider-main slider-main_products">
		<div id="slider-products" class="slider-main__slider-wrap">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="slider-main__item">
					<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation"><?
						if (strlen($arItem["PREVIEW_TEXT"]) > 0):
							?>
							<div class="cards-news cards-news_products">
								<div class="card-news">
									<div class="card-news__content">
										<div class="card-news__title"><?=$arItem["NAME"]?></div>
										<div class="card-news__text"><?=$arItem["PREVIEW_TEXT"]?></div>
									</div>
								</div>
							</div><?
						endif;
						?>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</div>