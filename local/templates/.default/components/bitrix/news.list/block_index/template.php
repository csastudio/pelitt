<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="home__page-preview-wrap" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="page-preview page-preview_type_brand">
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
				<div class="page-preview__title">
					<?echo $arItem["PREVIEW_TEXT"];?>
				</div>
			<?endif;?>
			<a href="<?=$arItem["PROPERTIES"]["LINK_BLOCK"]["VALUE"]?>" class="link link_white"><?=GetMessage("MORE_BLOCK_INDEX");?></a>
		</div>
	</div>
<?endforeach;?>