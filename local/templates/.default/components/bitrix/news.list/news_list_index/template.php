<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$noPhoto = $templateFolder.'/img/none.png';
?>
<?if (!empty($arResult["ITEMS"])):?>
	<div class="home__slider-wrap">
		<div class="news-page__more-news-wrap">
			<div class="news-page__more-news"><?=GetMessage("NEWS_INDEX_TITLE");?></div>
		</div>
		<div class="slider-main slider-main_card-news">
			<div id="slider-cards-news" class="slider-main__slider-wrap">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					// Вариант размещения - предзаказ
					if($arItem["PROPERTIES"]["SPEC_NEWS"]["VALUE_ENUM_XML"] == "DEF"):
					?>
					<div style="" class="slider-main__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap">
							<div class="cards-news">
								<div class="card-pre-order">
									<div class="card-pre-order__subtitle"><?=$arItem["PROPERTIES"]["SUBTITLE_NEWS"]["VALUE"]?></div>
									<div class="card-pre-order__title"><?=htmlspecialcharsBack($arItem["NAME"])?></div>
									<a type="" data-popup="" class="btn btn_style_yellow-key btn_shape_ellipse" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
										<div class="btn__text"><?=GetMessage("MORE_NEWS_INDEX");?>
											<svg class="btn__icon btn__icon_chevron-right"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_chevron-right"></use></svg>
										</div>
									</a>
									<div class="card-pre-order__subscribe">
										<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
											<div class="card-pre-order__text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
										<?endif?>
										<div class="card-pre-order__image">
											<img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"] ? $arItem["PREVIEW_PICTURE"]["SRC"] : $noPhoto?>" class="card-pre-order__picture" alt="<?=$arItem["NAME"]?>" role="presentation" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><?
					// Остальные варианты размещения
					else:
					?>
					<div style="" class="slider-main__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation">
							<div class="cards-news">
								<div class="card-news">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="card-news__link"></a>
									<div class="card-news__image card-news__image_shadow">
										<img src="<?echo $arItem["PREVIEW_PICTURE_190"]["SRC"] ? $arItem["PREVIEW_PICTURE_190"]["SRC"] : $noPhoto?>" class="card-news__picture" alt="<?=$arItem["NAME"]?>" role="presentation" />
									</div>
									<div class="card-news__content">
										<div class="card-news__date"><?echo GetMessage("NEWS_TITLE").' '.$arItem["DISPLAY_ACTIVE_FROM"]?></div>
										<div class="card-news__title"><?=$arItem["NAME"]?></div>
										<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
											<div class="card-news__text js-card-news-text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
										<?endif?>
									</div>
								</div>
							</div>
						</div>
					</div><?
					endif;
				endforeach;?>
			</div>
		</div>
	</div>
<?endif;?>