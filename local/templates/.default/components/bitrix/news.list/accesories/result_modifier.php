<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['ITEMS'] as $ID=>$arItem)
{
    $imgRes=CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width'=>230, 'height'=>230), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());

    $arResult['ITEMS'][$ID]['PREVIEW_PICTURE'] = array(

        'SRC' => $imgRes['src'],
        'WIDTH' => $imgRes['width'],
        'HEIGHT' => $imgRes['height'],

    );
}
