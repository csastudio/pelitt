<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="cards">
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="card-product">
			<span class="card-product__img">
				<?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="card-product__image" alt="<?=$arItem["NAME"]?>" role="presentation" />
				<?else:?>
					<svg class="card-product__icon"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_none-image"></use></svg>
				<?endif;?>
			</span>
			<span class="card-product__title">
				<span class="card-product__span-title"><?=$arItem["NAME"]?></span>
			</span><?
			foreach($arItem["PROPERTIES"]["PROPS_LIST"]["VALUE"] as $arPropList):
				?>
				<span class="card-product__descr"><?=$arPropList?></span><?
			endforeach;
			?>
		</a>
	</div>
<?endforeach;?>