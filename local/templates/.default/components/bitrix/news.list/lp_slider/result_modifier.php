<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['ITEMS'] as $ikey => $arItem)
{
    $rs = CIBlockElement::GetList(
        array('ID' => 'ASC'),
        array(
            'IBLOCK_ID' => 1,
            'ID' => $arItem["PROPERTIES"]["CATALOG_SLIDER"]["VALUE"],
            'ACTIVE' => 'Y'
        ),
        false,
        false,
        array("ID", "NAME", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID")

    );
    if ($ar = $rs->GetNext()){
        $arResult['ITEMS'][$ikey]['ELEMENT_INFO'] = $ar;
    }
    $res = CIBlockSection::GetByID($ar["IBLOCK_SECTION_ID"]);
    if($ar_res = $res->GetNext()){
        $arResult['ITEMS'][$ikey]['SECTION_INFO'] = $ar_res["SECTION_PAGE_URL"];
    }

}

