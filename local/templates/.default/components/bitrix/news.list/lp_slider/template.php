<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="home__slider-main-wrap">
    <div class="slider-main">
        <div id="slider-main" class="slider-main__slider-wrap" style="background-color: white">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            // Если указано отправляем в LP если нет в обычную карточку
            if (strlen($arItem["PROPERTIES"]["LP_SELECT_CARDS"]["VALUE"]) > 0) {
                $lp = "lp/";
            }
            ?>
            <?if (!$arItem["PROPERTIES"]["VIDEO"]["VALUE"]){?>
            <? if ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TCPH"): ?>
            <div style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)"
                 class="slider-main__item sl-item"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <? elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TRPD"): ?>
                <div style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)"
                     class="slider-main__item sl-item slider-main__item_right slider-main__item_dark slider-main__item_small">
                    <? elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TCPD"): ?>
                    <div style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)"
                         class="slider-main__item sl-item slider-main__item_top slider-main__item_dark slider-main__item_small">
                        <? elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TCPL"): ?>
                        <div style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)"
                             class="slider-main__item sl-item slider-main__item_small">
                            <? elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TLPL"): ?>
                            <div style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)"
                                 class="slider-main__item sl-item slider-main__item_left">
                                <? elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "NOALL"): ?>
                                <div style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>); background-color: white"
                                     class="slider-main__item sl-item">
                                    <? endif; ?>
                                    <? if ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] != "NOALL"): ?>
                                    <? else: ?>
                                        <div style="background-image: url(<? echo ($arItem["DETAIL_PICTURE"]["SRC"]) ? $arItem["DETAIL_PICTURE"]["SRC"] : $arItem["PREVIEW_PICTURE"]["SRC"] ?>);opacity:0;"
                                             class="card-news__link">
                                        </div>
                                        <? if ($arItem["PREVIEW_TEXT"]) { ?>
                                            <div class="sl-block">
                                                <div class="sl-block__title"><?= $arItem["NAME"] ?></div>
                                                <div class="sl-block__text"><?= $arItem["PREVIEW_TEXT"]; ?></div>
                                            </div>
                                        <? } ?>
                                    <? endif; ?>
                                </div>
                                <?}else{?>
                                    <div
                                            style="background-image: url(<? echo $arItem["PREVIEW_PICTURE"]["SRC"] ?>);
                                        background-color: white; width: 1692px;"
                                            class="slider-main__item sl-item">

                                        <div style="background-image: url(<? echo ($arItem["DETAIL_PICTURE"]["SRC"]) ? $arItem["DETAIL_PICTURE"]["SRC"] : $arItem["PREVIEW_PICTURE"]["SRC"] ?>);opacity:0;" class="card-news__link"></div>

                                        <div class="sl-title">
                                            <div class="slider-main__title"><?= $arItem["NAME"] ?></div>
                                            <div class="slider-main__text">
                                                <p>
                                                    <?= $arItem["PREVIEW_TEXT"];?>
                                                </p>
                                            </div>
                                            <a class="slider-main__btn-wrap js-btn-video" href="<?=$arItem["PROPERTIES"]["VIDEO"]["VALUE"]?>" data-fancybox>
                                                <div class="slider-main__btn-video"></div>
                                                <svg class="slider-main__play">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="/local/templates/main/assets/images/icon.svg#icon_arrow-play"></use>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                <?}?>
                                <? endforeach; ?>
                            </div>
                            <div data-animstart="bottom" data-animstop="42"
                                 class="slider-main__arrow-down js-arrow-down js-arrow"></div>
                        </div>
                    </div>