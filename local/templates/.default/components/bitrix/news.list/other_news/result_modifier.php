<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['ITEMS'] as $ID=>$arItem)
{
    $imgRes=CFile::ResizeImageGet(

        $arItem['PREVIEW_PICTURE']['ID'],

        array(
            'width'=>190,
            'height'=>190
        ),

        BX_RESIZE_IMAGE_EXACT,
        true,
        Array()

    );

    $arResult['ITEMS'][$ID]['PREVIEW_PICTURE_190'] = array(

        'SRC' => $imgRes['src'],
        'WIDTH' => $imgRes['width'],
        'HEIGHT' => $imgRes['height'],

    );
}