<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-page__bottom-wrap">
	<div class="news-page__more-news-wrap">
		<div class="news-page__more-news"><?=GetMessage("NEWS_MAIN_TITLE_OTHER");?></div>
	</div>
	<div class="news-page__news-list"><?
		foreach($arResult["ITEMS"] as $arItem):

		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			// Вариант размещения - предзаказ
			if($arItem["PROPERTIES"]["SPEC_NEWS"]["VALUE_ENUM_ID"] == 16):
				?>
				<div class="news-page__news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="cards-news cards-news_horisontal">
						<div class="card-pre-order">
							<div class="card-pre-order__subtitle"><?=$arItem["PROPERTIES"]["SUBTITLE_NEWS"]["VALUE"]?></div>
							<div class="card-pre-order__title"><?=htmlspecialcharsBack($arItem["NAME"])?></div>
							<div class="card-pre-order__wrap-link">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="link link_white"><?=GetMessage("MORE_NEWS_OTHER");?></a>
							</div>
							<div class="card-pre-order__subscribe">
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<div class="card-pre-order__text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
								<?endif;?>
								<div class="card-pre-order__image">
									<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="card-pre-order__picture" alt="<?=$arItem["NAME"]?>" role="presentation" />
								</div>
							</div>
						</div>
					</div>
				</div><?
			// Остальные варианты размещения
			elseif($arItem["PROPERTIES"]["SPEC_NEWS"]["VALUE_ENUM_ID"] == 26):
				?>
				<div class="news-page__news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="cards-news cards-news_horisontal">
						<div class="card-news">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="card-news__link"></a>
							<div class="card-news__image card-news__image_shadow">
								<img src="<?=$arItem["PREVIEW_PICTURE_190"]["SRC"]?>" class="card-news__picture" alt="<?=$arItem["NAME"]?>" role="presentation" />
							</div>
							<div class="card-news__content">
								<div class="card-news__date"><?echo GetMessage("NEWS_TITLE_OTHER").' '.$arItem["DISPLAY_ACTIVE_FROM"]?></div>
								<div class="card-news__title"><?=$arItem["NAME"]?></div>
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<div class="card-news__text js-card-news-text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
								<?endif?>
							</div>
						</div>
					</div>
				</div><?
			elseif($arItem["PROPERTIES"]["SPEC_NEWS"]["VALUE_ENUM_ID"] == 25):
				?>
				<div class="news__news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="cards-news cards-news_horisontal">
						<div class="card-pre-order">
							<div class="card-pre-order__subtitle">Акция</div>
							<div class="card-pre-order__title"><?=$arItem["NAME"]?></div>
							<div class="card-pre-order__wrap-link">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="link link_white">Подробнее</a>
							</div>
							<div class="card-pre-order__subscribe">
								<div class="card-pre-order__text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
								<div class="card-pre-order__image card-pre-order__image_full">
									<img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"] ? $arItem["PREVIEW_PICTURE"]["SRC"] : $noPhoto?>" class="card-pre-order__picture" alt="" role="presentation" />
								</div>
							</div>
						</div>
					</div>
				</div><?
			endif;
		endforeach;
		?>
	</div>
	<!--div class="news-page__show-more-wrap">
		<div class="show-more">
			<div class="show-more__icon">
				<svg class="show-more__icon-item"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_preloader"></use></svg>
			</div>
			<div class="show-more__text">показать еще</div>
		</div>
	</div-->
</div>
