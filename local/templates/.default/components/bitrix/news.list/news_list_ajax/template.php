<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$noPhoto = $templateFolder.'/img/none.png';

foreach($arResult["ITEMS"] as $arItem):

	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	switch ($arItem["PROPERTIES"]["SPEC_NEWS"]["VALUE_ENUM_ID"]):
		// Обычное размещение
		case 26:
		default:
			?>
			<div class="news__news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="cards-news cards-news_horisontal">
					<div class="card-news">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="card-news__link"></a>
						<div class="card-news__image card-news__image_shadow">
							<img src="<?echo $arItem["PREVIEW_PICTURE_190"]["SRC"] ? $arItem["PREVIEW_PICTURE_190"]["SRC"] : $noPhoto?>" class="card-news__picture" alt="<?=$arItem["NAME"]?>" role="presentation" />
						</div>
						<div class="card-news__content">
							<div class="card-news__date">Новость &nbsp;|&nbsp; <?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
							<div class="card-news__title"><?=$arItem["NAME"]?></div>
							<div class="card-news__text js-card-news-text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
						</div>
					</div>
				</div>
			</div><?
			continue;
		// Предзаказ
		case 16:
			?>
			<div class="news__news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="cards-news cards-news_horisontal">
					<div class="card-pre-order">
						<div class="card-pre-order__subtitle">Открыт предзаказ</div>
						<div class="card-pre-order__title"><?=htmlspecialcharsBack($arItem["NAME"])?></div>
						<div class="card-pre-order__wrap-link">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="link link_white">Подробнее</a>
						</div>
						<div class="card-pre-order__subscribe">
							<div class="card-pre-order__text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
							<div class="card-pre-order__image">
								<img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"] ? $arItem["PREVIEW_PICTURE"]["SRC"] : $noPhoto?>" class="card-pre-order__picture" alt="" role="presentation" />
							</div>
						</div>
					</div>
				</div>
			</div><?
			continue;
		// Акция
		case 25:
			?>
			<div class="news__news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="cards-news cards-news_horisontal">
					<div class="card-pre-order">
						<div class="card-pre-order__subtitle">Акция</div>
						<div class="card-pre-order__title"><?=$arItem["NAME"]?></div>
						<div class="card-pre-order__wrap-link">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="link link_white">Подробнее</a>
						</div>
						<div class="card-pre-order__subscribe">
							<div class="card-pre-order__text"><?echo htmlspecialcharsBack($arItem["PREVIEW_TEXT"]);?></div>
							<div class="card-pre-order__image card-pre-order__image_full">
								<img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"] ? $arItem["PREVIEW_PICTURE"]["SRC"] : $noPhoto?>" class="card-pre-order__picture" alt="" role="presentation" />
							</div>
						</div>
					</div>
				</div>
			</div><?
			continue;
	endswitch;
endforeach
?>

