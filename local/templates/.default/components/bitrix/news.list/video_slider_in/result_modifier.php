<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

foreach($arResult["ITEMS"] as $id => $arItem)
{
    $arResult["ITEMS"][$id]["BACK_PHOTO"] = CFile::GetFileArray($arItem["PROPERTIES"]["BACK_IMG_VS"]["VALUE"]);
    $arResult["ITEMS"][$id]["PHOTO_SLIDER"] = CFile::GetFileArray($arItem["PROPERTIES"]["PHOTO_SLIDER"]["VALUE"]);
}
?>