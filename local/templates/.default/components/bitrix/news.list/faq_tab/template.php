<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="accordeon__item js-accordeon-item"><?=$arItem["NAME"]?></div>
	<div class="accordeon__content-wrap js-accordeon-wrap">
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<div class="accordeon__content js-accordeon-content">
				<?echo $arItem["PREVIEW_TEXT"];?>
			</div>
		<?endif?>
	</div>
<?endforeach;?>