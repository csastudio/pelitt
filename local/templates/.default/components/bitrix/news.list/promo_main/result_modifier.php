<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult["ITEMS"] as $ID => $arItem)
{
    $img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array("width"=>480, "height"=>320), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $arResult['ITEMS'][$ID]['PREVIEW_PICTURE'] = array(
        "SRC" => $img["src"],
        "WIDTH" => $img["width"],
        "HEIGHT" => $img["height"],
    );
}
