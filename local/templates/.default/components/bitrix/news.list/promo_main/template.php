<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="cell-advantages">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<a href="<?=$arItem["PROPERTIES"]["LINKS"]["VALUE"];?>" class="cell-advantages__elm">
			<div class="cell-advantages__image" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></div>
			<!-- <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>"/> -->
			<!-- <div class="cell-advantages__text-cont">
				<div class="advantages__title"><?=$arItem["NAME"]?></div>
				<p class="advantages__text"><?=$arItem["PREVIEW_TEXT"]?></p>
			</div> -->
		</a>
	<?endforeach;?>
</div>
