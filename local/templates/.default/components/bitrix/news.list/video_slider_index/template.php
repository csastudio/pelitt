<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="slider-main slider-main_video">
	<div id="slider-video" class="slider-main__slider-wrap">
		<?foreach($arResult["ITEMS"] as $id => $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div style="background-image: url(<?=$arItem["BACK_PHOTO"]["SRC"]?>)" class="slider-main__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation">
					<a href="<?=$arItem["PROPERTIES"]["LINK_VIEW_VS"]["VALUE"]?>" class="slider-main__subtitle"><?=$arItem["PROPERTIES"]["SUBTITLE_VS"]["VALUE"]?></a>
					<div class="slider-main__title"><?=$arItem["NAME"]?></div>
					<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
						<div class="slider-main__text"><?echo $arItem["PREVIEW_TEXT"];?></div>
					<?endif?>
					<div data-popup="popup-<?=$id?>" class="slider-main__btn-wrap js-btn-video">
						<div class="slider-main__btn-video"></div>
						<svg class="slider-main__play"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_arrow-play"></use></svg>
					</div>
					<?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
						<div class="slider-main__product">
							<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="72" height="158" alt="" role="presentation" />
						</div>
					<?endif;?>
					<?if ($arItem["PROPERTIES"]["LINK_IN"]["VALUE"]):?>
						<a href="<?=$arItem["PROPERTIES"]["LINK_IN"]["VALUE"]?>" class="btn btn_style_slider btn_shape_ellipse">
							<div class="btn__text"><?=GetMessage("MORE_VIDEO_SLIDER");?></div>
						</a>
					<?endif;?>
				</div>
			</div>
		<?endforeach;?>
	</div>
</div>
<?foreach($arResult["ITEMS"] as $id => $arItem):?>
	<div data-popup-link="popup-<?=$id?>" class="popup-video js-popup-video">
		<div class="popup-video__close js-popup-close-btn"></div>
		<div class="popup-video__content js-popup-content">
			<div class="popup-video__video">
				<iframe width="853" height="480" src="<?=$arItem["PROPERTIES"]["LINK_VIDEO_VS"]["VALUE"]?>" frameborder="0" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
<?endforeach;?>