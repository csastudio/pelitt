<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="home__slider-main-wrap">
	<div class="slider-main">
		<div id="slider-main" class="slider-main__slider-wrap">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				// Если указано отправляем в LP если нет в обычную карточку
				if (strlen($arItem["PROPERTIES"]["LP_SELECT_CARDS"]["VALUE"]) > 0){
					$lp = "lp/";
				}
				?>
				<?if ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TCPH"):?>
				<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="slider-main__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<?elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TRPD"):?>
				<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="slider-main__item slider-main__item_right slider-main__item_dark slider-main__item_small">
				<?elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TCPD"):?>
				<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="slider-main__item slider-main__item_top slider-main__item_dark slider-main__item_small">
				<?elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TCPL"):?>
				<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="slider-main__item slider-main__item_small">
				<?elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "TLPL"):?>
				<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="slider-main__item slider-main__item_left">
				<?elseif ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] == "NOALL"):?>
				<div style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)" class="slider-main__item">
				<?endif;?>
					<?if ($arItem["PROPERTIES"]["STYLE_SLIDER"]["VALUE_XML_ID"] != "NOALL"):?>
						<div data-animstart="bottom" data-animstop="420" class="slider-main__item-wrap js-animation">
							<a href="<?=$arItem["SECTION_INFO"]?>" class="slider-main__subtitle"><?=$arItem["PROPERTIES"]["SUBTITLE_SLIDE"]["VALUE"]?></a>
							<div class="slider-main__title"><?=$arItem["NAME"]?></div>
							<?if (strlen($arItem["PREVIEW_TEXT"]) > 0):?>
								<div class="slider-main__text"><?=$arItem["PREVIEW_TEXT"];?></div>
							<?endif?>
							<a href="<?=$arItem["ELEMENT_INFO"]["DETAIL_PAGE_URL"] ? $arItem["ELEMENT_INFO"]["DETAIL_PAGE_URL"] : $arItem["PROPERTIES"]["LINK"]["VALUE"].$lp?>" type="" data-popup="" class="btn btn_style_ascendant btn_shape_ellipse">
								<div class="btn__text"><?=GetMessage("MORE_SLIDER");?>
									<svg class="btn__icon btn__icon_chevron-right"><use xlink:href="/local/templates/assets/images/icon.svg#icon_chevron-right"></use></svg>
								</div>
							</a>
						</div>
					<?else:?>
						<a href="<?echo $arItem["PROPERTIES"]["LINK"]["VALUE"] ? $arItem["PROPERTIES"]["LINK"]["VALUE"] : $arItem["ELEMENT_INFO"]["DETAIL_PAGE_URL"].$lp?>"
                           style="background-image: url(<?echo ($arItem["DETAIL_PICTURE"]["SRC"]) ?  $arItem["DETAIL_PICTURE"]["SRC"] : $arItem["PREVIEW_PICTURE"]["SRC"]?>);opacity:0;" class="card-news__link"></a>
					<?endif;?>
				</div>
			<?endforeach;?>
		</div>
		<div data-animstart="bottom" data-animstop="42" class="slider-main__arrow-down js-arrow-down js-arrow"></div>
	</div>
</div>