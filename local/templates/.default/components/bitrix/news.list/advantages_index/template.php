<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="home__advantages-wrap">
	<div class="advantages">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="advantages__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="advantages__icon-wrap">
					<svg class="advantages__icon"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#<?=$arItem["PROPERTIES"]["NAME_ICON"]["VALUE"]?>"></use></svg>
				</div>
				<div class="advantages__title"><?=$arItem["NAME"]?></div>
				<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
					<div class="advantages__text"><?echo $arItem["PREVIEW_TEXT"];?></div>
				<?endif?>
			</div>
		<?endforeach;?>
	</div>
</div>