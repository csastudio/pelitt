<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="technical-features">
	<div class="technical-features__title"><?=GetMessage("TECH_FEA");?></div>
	<div class="technical-features__wrap"><?
		foreach($arResult["ITEMS"] as $arItem):
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="technical-features__feature" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<svg class="technical-features__icon technical-features__icon_<?=$arItem['PROPERTIES']['SELECT_ICON']['VALUE_XML_ID']?>">
					<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#<?=$arItem['PROPERTIES']['SELECT_ICON']['VALUE_XML_ID']?>"></use>
				</svg>
				<div class="technical-features__subtitle"><?=$arItem["NAME"]?></div>
				<div class="technical-features__feature-title"><?=$arItem["PREVIEW_TEXT"]?></div>
			</div><?
		endforeach;
		?>
	</div>
</div>
