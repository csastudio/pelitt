<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="contacts js-main-modal page__content">
	<div class="support-modal js-support-modal">
		<div class="support-modal__item">
			<div class="support-modal__close js-support-modal-close">
				<div class="close close_style_modal">
					<div class="close__button">
						<svg class="close__icon"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_close-btn"></use></svg>
					</div>
				</div>
			</div>
			<?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"feedback.service", 
	array(
		"ACTIVE_ELEMENT" => "Y",
		"ADD_LEAD" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ALX_CHECK_NAME_LINK" => "N",
		"BACKCOLOR_ERROR" => "#ffffff",
		"BBC_MAIL" => "",
		"BORDER_RADIUS" => "3px",
		"CAPTCHA_TYPE" => "default",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"CHANGE_CAPTCHA" => "N",
		"CHECK_ERROR" => "Y",
		"COLOR_ERROR" => "#8E8E8E",
		"COLOR_ERROR_TITLE" => "#A90000",
		"COLOR_HINT" => "#000000",
		"COLOR_INPUT" => "#727272",
		"COLOR_MESS_OK" => "#963258",
		"COLOR_NAME" => "#000000",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"FB_TEXT_NAME" => "",
		"FB_TEXT_SOURCE" => "PREVIEW_TEXT",
		"FORM_ID" => "12",
		"HIDE_FORM" => "Y",
		"IBLOCK_ID" => "24",
		"IBLOCK_TYPE" => "altasib_feedback",
		"IMG_ERROR" => "/upload/altasib.feedback.gif",
		"IMG_OK" => "/upload/altasib.feedback.ok.gif",
		"JQUERY_EN" => "N",
		"LOCAL_REDIRECT_ENABLE" => "N",
		"MASKED_INPUT_PHONE" => array(
		),
		"MESSAGE_OK" => "Сообщение отправлено!",
		"NAME_ELEMENT" => "ALX_DATE",
		"PROPERTY_FIELDS" => array(
			0 => "YOUR_NAME",
			1 => "YOUR_EMAIL"
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
		),
		"PROPS_AUTOCOMPLETE_EMAIL" => array(
			0 => "EMAIL",
		),
		"PROPS_AUTOCOMPLETE_NAME" => array(
			0 => "NAME",
		),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(
			0 => "PHONE",
		),
		"PROPS_AUTOCOMPLETE_VETO" => "N",
		"REWIND_FORM" => "N",
		"SECTION_FIELDS_ENABLE" => "N",
		"SECTION_MAIL_ALL" => "info@csa-studio.ru",
		"SEND_MAIL" => "N",
		"SHOW_MESSAGE_LINK" => "Y",
		"SIZE_HINT" => "10px",
		"SIZE_INPUT" => "12px",
		"SIZE_NAME" => "12px",
		"USERMAIL_FROM" => "Y",
		"USE_CAPTCHA" => "Y",
		"WIDTH_FORM" => "50%",
		"COMPONENT_TEMPLATE" => "feedback.service",
		"SECTION_MAIL28" => ""
	),
	false
);?>
		</div>
	</div>
	<div class="contacts__wrap" <?if (SITE_DIR == '/ru/'){?>style="
	padding-bottom: 100px;"<?}?>>
		<div class="contacts__row js-afix-row">
			<div class="contacts__map">
				<div class="contacts__content-map js-afix-content">
                    <?if (SITE_DIR == '/ru/'){?>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2173.6397890953367!2d24.100519815576398!3d56.98922920342165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46eecf9cedb4c4e3%3A0xeeeeaf26696070e1!2zVXJpZWtzdGVzIGllbGEgMTQsIFppZW1lxLx1IHJham9ucywgUsSrZ2EsIExWLTEwMDUsINCb0LDRgtCy0LjRjw!5e0!3m2!1sru!2sru!4v1504706639457" width="460" height="510" frameborder="0" style="border:0"></iframe>
                    <?}else{?>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2561.9223858112136!2d14.437017615912827!3d50.0502852239981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470b94759b1f6d5b%3A0x588df01abda2d398!2sNa+Str%C5%BEi+1702%2F65%2C+140+00+Praha-Praha+4!5e0!3m2!1scs!2scz!4v1447169472769" width="460" height="510" frameborder="0" style="border:0"></iframe>
					<br />
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4381.608068753931!2d16.5686177395145!3d49.232426748349624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4712940282851305%3A0x38370f32ae980f7d!2zSmlob21vcmF2c2vDqSBpbm92YcSNbsOtIGNlbnRydW0!5e0!3m2!1scs!2scz!4v1447169188094" width="460" height="510" frameborder="0" style="border:0"></iframe>
                    <?}?>
				</div>
			</div>
			<div class="contacts__content">
				<?=$arResult["DETAIL_TEXT"];
				if ($arResult["PROPERTIES"]["CHECK_TP"]["VALUE"]):
					?>
					<div class="contacts__form">
						<div class="contacts__title"><?=GetMessage("THIS_FORM");?></div>
						<div class="contacts__button js-support-modal-button">
							<button type="" data-popup="" class="btn btn_style_green-key btn_shape_ellipse">
								<div class="btn__text"><?=GetMessage("WRITE_TO_US");?>
									<svg class="btn__icon btn__icon_chevron-right"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_chevron-right"></use></svg>
								</div>
							</button>
						</div>
					</div><?
				endif;
				?>
				<?=$arResult["PREVIEW_TEXT"]?>
			</div>
		</div>
	</div>
</div>