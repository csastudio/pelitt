<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($arResult["CATEGORIES"])):
	?>
	<div class="popup-search__list">
		<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
			<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
				<a href="<?echo $arItem["URL"]?>" class="popup-search__list-item"><?echo $arItem["NAME"]?></a>
			<?endforeach;?>
		<?endforeach;?>
	</div><?
endif;
?>
