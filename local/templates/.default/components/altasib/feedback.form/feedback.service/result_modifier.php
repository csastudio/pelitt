<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$arFilter = Array('IBLOCK_ID'=> 10, 'ACTIVE'=>'Y');
$db_list = CIBlockSection::GetList(Array(), $arFilter, false, array('ID' => 28));
$arSect = $db_list->GetNext();

$arResult["SECTION_DESCRIPTION"] = $arSect["DESCRIPTION"];


$arFilter = Array('IBLOCK_ID' => 1, 'ACTIVE'=>'Y', "ID" => array(1, 4, 3, 7, 6, 5));
$db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false, array("ID", "NAME", "CODE"));
while($res_section = $db_list->GetNext()){

    $arResult["SECTIONS_NAME"][] = $res_section;
    $arIDS[] = $res_section["ID"];
}

$itemName =  CIBlockElement::GetList(
    array("SORT" => "ASC"),
    array("IBLOCK_ID"=> 1, "ACTIVE"=>"Y", "SECTION_ID" => $arIDS),
    false,
    false,
    array("ID", "NAME", "IBLOCK_SECTION_ID")
);
while($obs = $itemName->GetNextElement())
{
    $arField = $obs->GetFields();
    $arResult["ITEM_NAME"][$arField["IBLOCK_SECTION_ID"]][] = $arField;
}
