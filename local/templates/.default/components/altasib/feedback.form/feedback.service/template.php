<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CUtil::InitJSCore();
$this->addExternalJS($templateFolder."/is.mobile.js");
$this->addExternalJS($templateFolder."/jquery.maskedinput.min.js");
?>

<? $fVerComposite = (defined("SM_VERSION") && version_compare(SM_VERSION, "14.5.0") >= 0 ? true : false); ?>
<? if($fVerComposite) $this->setFrameMode(true); ?>

<?$ALX = "FID".$arParams["FORM_ID"];?>

<script type="text/javascript">
<!--
<?
if($arParams["REWIND_FORM"] == "Y" && ((count($arResult["FORM_ERRORS"]) > 0) || ($_REQUEST["success_".$ALX] == "yes"))):?>
$(document).ready(function(){
	document.location.hash="alx_position_feedback";
});
<?endif?>

if(typeof ALX_ReloadCaptcha!='function'){
	function ALX_ReloadCaptcha(csid,ALX){
		document.getElementById("alx_cm_CAPTCHA_"+ALX).src='/bitrix/tools/captcha.php?captcha_sid='+csid+'&rnd='+Math.random();
	}
	function ALX_SetNameQuestion(obj,ALX){
		var qw=obj.selectedIndex;
		document.getElementById("type_question_name_"+ALX).value=obj.options[qw].text;
		<?if($arParams["SECTION_FIELDS_ENABLE"] == "Y"):
		?>$('form[name=f_feedback_'+ALX+']').append('<input type="hidden" value="Y" name="REFRESH">');
		$('input[name=SEND_FORM_'+ALX+']').click();<?
		endif;?>
	}
}
if(typeof ALX_ChangeCaptcha!='function'){
	function ALX_ChangeCaptcha(ALX){
		$.getJSON('<?=$this->__folder?>/reload_captcha.php',function(data){
			$('#alx_cm_CAPTCHA_'+ALX).attr('src','/bitrix/tools/captcha.php?captcha_sid='+data);
			$('#alx_fb_captchaSid_'+ALX).val(data);
		});
	}
}
-->
</script>
<?
$errorField = array();

foreach($arResult["FORM_ERRORS"] as $error):
	foreach($error as $k => $v):
		$errorField[] = $k;
	endforeach;
endforeach;

?>
<?if($arParams["REWIND_FORM"] == "Y" && ((count($arResult["FORM_ERRORS"]) > 0) || ($arResult["success_".$ALX] == "yes"))):?>
	<a name="alx_position_feedback"></a>
<?endif?>



<?if($arParams["CHECK_ERROR"] == "Y"):?>
<?if(count($arResult["FORM_ERRORS"]) > 0):?>
	<div class="alx_feed_back_form_error_block">
		<table cellpadding="0" cellspacing="0" border="0" class="alx_feed_back_form_error_block_tbl">
		<tr>
			<td class="alx_feed_back_form_error_pic"><?=CFile::ShowImage($arParams["IMG_ERROR"])?></td>
			<td class="alx_feed_back_form_error_td_list">
			<div class="alx_feed_back_form_title_error"><?=GetMessage("ALX_TP_REQUIRED_ERROR")?></div>
				<ul class="alx_feed_back_form_error_list">
<?					foreach($arResult["FORM_ERRORS"] as $error):?>
<?						foreach($error as $v):?>
							<li><span>-</span> <?=$v?></li>
<?						endforeach?>
<?					endforeach?>
				</ul>
			</td>
		</tr>
		</table>
	</div>
<?endif?>
<?endif?>
<?
$hide = false;
if($arParams["HIDE_FORM"] == "Y" && ($_REQUEST["success_".$ALX] == "yes" || $arResult["success_".$ALX] == "yes"))
	$hide = true;

$actionPage = $APPLICATION->GetCurPage();
if(strpos($actionPage, "index.php") !== false)
	$actionPage = $APPLICATION->GetCurDir();

?>

	<div class="mess-ok" style="display: none;"><?=GetMessage("SENT_OK");?></div>
	<div class="support-modal__content" id="alx_feed_back_default_<?=$ALX?>">
		<div class="support-popup">
			<form id="f_feedback_<?=$ALX?>" name="f_feedback_<?=$ALX?>" action="<?=$actionPage?>" method="post" enctype="multipart/form-data" class="form_check support-popup__form">
				<input type="hidden" name="FEEDBACK_FORM_<?=$ALX?>" value="Y" />
				<?=bitrix_sessid_post()?>
				<div class="support-popup__form-heading"><?=GetMessage("CONTACT_US");?></div>
				<div class="support-popup__user">
					<div class="support-popup__input">
						<div class="field js-field rline" id="error_<?=$arResult["FIELDS"][0]["CODE"]?>">
							<input type="text" id="<?=$arResult["FIELDS"][0]["CODE"]?>" name="FIELDS[<?=$arResult["FIELDS"][0]["CODE"]?>]" value="" placeholder="<?=$arResult["FIELDS"][0]["NAME"]?>" class="rfield field__input js-field-input" />
							<div class="field__validate"></div>
							<div class="field__answer"></div>
						</div>
					</div>
					<div class="support-popup__input">
						<div class="field js-field rline" id="error_<?=$arResult["FIELDS"][1]["CODE"]?>">
							<input type="text" id="<?=$arResult["FIELDS"][1]["CODE"]?>" name="FIELDS[<?=$arResult["FIELDS"][1]["CODE"]?>]" value="" placeholder="<?=$arResult["FIELDS"][1]["NAME"]?>" class="rfield field__input js-field-input" />
							<div class="field__validate"></div>
							<div class="field__answer"></div>
						</div>
					</div>
				</div>
				<div id="main-select" class="support-popup__info-device">
					<div class="support-popup__textarea">
						<div class="field field_style_support-modal js-field rline">
							<textarea placeholder="<?=GetMessage("YOUR_MESS")?>" id="EMPTY_TEXT<?=$ALX?>" name="FEEDBACK_TEXT_<?=$ALX?>" class="rfield_2 field__input field__input_textarea js-field-input"></textarea>
						</div>
					</div>
					<div class="support-popup__input">
						<div class="field field_style_captcha js-field">
							<div class="field__wrap">
								<?if($arParams["USE_CAPTCHA"]):?>
									<?if($arParams["CAPTCHA_TYPE"] != 'recaptcha'):?>

										<?if(in_array("ALX_CP_WRONG_CAPTCHA", $errorField, true)):?><div><?endif?>
										<?if($fVerComposite) $frame = $this->createFrame()->begin('loading... <img src="/bitrix/themes/lp/start_menu/main/loading.gif">');?>
										<?$capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();?>
										<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsEx($capCode)?>">
										<img id="alx_cm_CAPTCHA_<?=$ALX?>" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsEx($capCode)?>" width="180" height="40" class="field__captcha">
										<?if($fVerComposite) $frame->end();?>
										<input type="text" class="field__input js-field-input" id="captcha_word<?=$ALX?>" name="captcha_word" placeholder="Введите символы" size="30" maxlength="50" value="">
										<?if(in_array("ALX_CP_WRONG_CAPTCHA", $errorField, true)):?></div><?endif?>

									<?endif;?>
								<?endif?>
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="support-popup__button">
					<button type="submit" data-popup="" id="fb_close_<?=$ALX?>" name="SEND_FORM_<?=$ALX?>" class="btnsubmit btn btn_style_support-modal">
						<div class="btn__text"><?=GetMessage("ALX_TP_MESSAGE_SUBMIT");?></div>
					</button>
				</div>
			</form>
		</div>
	</div>


<?if($arParams['ALX_CHECK_NAME_LINK']=='Y'):?>
<script type="text/javascript">
$(document).ready(function(){var a;$("a").click(function(){"alx_feedback_popup"==$(this).attr("class")&&(a=$(this).attr("id").split("_")[2]);$(".alx_feedback_popup").fancybox({ajax:{type:"POST",data:"OPEN_POPUP_"+a+"=Y"},titleShow:!1,type:"ajax",href:"",afterShow:function(){"undefined"!=typeof AltasibFeedbackOnload_<?=$ALX?>&&AltasibFeedbackOnload_<?=$ALX?>()},overlayShow:!1,autoDimensions:!1,helpers:{overlay:null}})})});
</script>
<?endif?>

