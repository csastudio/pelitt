<?
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $USER, $APPLICATION, $DB;

if (!is_object($USER)) $USER = new CUser;

if ($USER->IsAuthorized() || !check_bitrix_sessid() || $_SERVER['REQUEST_METHOD'] != 'POST')
{
	echo CUtil::PhpToJSObject(
		array(
			'type' => 'error',
			'message' => 'Invalid data!'
		)
	);
	die();
}

$GLOBALS['APPLICATION']->RestartBuffer();
$action = isset($_POST['action']) ? $_POST['action'] : '';
if(strlen($action) == 0)
{
	echo CUtil::PhpToJSObject(
		array(
			'type' => 'error',
			'message' => 'Invalid data!'
		)
	);
	die();
}

switch($action){
	case 'enter':
		$bPhone = false;
		$userLogin = trim($_POST['login']);
		
		if(check_email($userLogin, true)){
			$userEmail = $userLogin;
		}
		else
		{
			$phone = str_replace(array(' ', ')', '(', '+7', '+'), '', $userLogin);
			if(strlen($phone) > 10)
				$phone = substr($phone, -10, 10);
			
			if(intval($phone)){
				$userLogin = $phone;
				$bPhone = true;
			}
		}
		
		$filter = array('EMAIL' => $userLogin);
		
		if($bPhone){
			$filter = array('PERSONAL_PHONE' => $userLogin);
		}

        $by = 'last_login';
        $order = 'desc';
        $dbUser = CUser::getList($by, $order, array(), $filter);
		if ($arUser = $dbUser->Fetch())
		{
			$arAuthResult = $USER->Login($arUser['LOGIN'], $_POST['password'], "Y");
			$APPLICATION->arAuthResult = $arAuthResult;
			
			if ($USER->IsAuthorized())
			{
				echo CUtil::PhpToJSObject(
					array('type' => 'ok', 'action' => 'reload')
				);
			}
			else
			{
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'message' => $arAuthResult['MESSAGE']
					)
				);
			}
		}
		else
		{
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => 'Wrong login or password'
				)
			);
		}
	break;
	case 'registration':
		$bPhone = false;
		$userLogin = trim($_POST['login']);
		
		if(empty($userLogin)){
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => 'Wrong E-mail'
				)
			);
			
			die();
		}
		
		if(check_email($userLogin, true)){
			$userEmail = $userLogin;
		}
		else
		{
			$phone = str_replace(array(' ', ')', '(', '+7', '+'), '', $userLogin);
			if(strlen($phone) > 10)
				$phone = substr($phone, -10, 10);
			
			if(intval($phone)){
				$userPhone = $phone;
				$userLogin = $phone;
				$userEmail = $phone.'@'.'lexand.ru';
				
				$bPhone = true;
			}
			else{
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'message' => 'Wrong E-mail'
					)
				);
				
				die();
			}
		}
		
		$dbUserLogin = CUser::GetByLogin($userLogin);
		if ($arUserLogin = $dbUserLogin->Fetch())
		{
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => $bPhone ? 'Already exists' : 'Already exists'
				)
			);
			
			die();
		}

		$def_group = Bitrix\Main\Config\Option::get("main", "new_user_registration_def_group", "");
		if ($def_group != "")
		{
			$groupID = explode(",", $def_group);
			$arPolicy = $USER->GetGroupPolicy($groupID);
		}
		else
		{
			$arPolicy = $USER->GetGroupPolicy(array());
		}

		$password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
		if ($password_min_length <= 0)
			$password_min_length = 6;
		$password_chars = array(
				"abcdefghijklnmopqrstuvwxyz",
				"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
				"0123456789",
		);
		if ($arPolicy["PASSWORD_PUNCTUATION"] === "Y")
			$password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
		$userPassword = $userPasswordConfirm = randString($password_min_length+2, $password_chars);

		$arFields = array(
			'EMAIL' => $userEmail,
			'LOGIN' => $userLogin,
			'PASSWORD' => $userPassword,
			'CONFIRM_PASSWORD' => $userPasswordConfirm,
			'PERSONAL_PHONE' => $userPhone,
			'GROUP_ID' => $groupID,
			"ACTIVE" => "Y",
			"LID" => Bitrix\Main\Application::getInstance()->getContext()->getSite()
		);
		
		$user = new CUser;
		$arAuthResult = $user->Add($arFields);
		
		if ($arAuthResult != false && $arAuthResult["TYPE"] == "ERROR"){
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => $arAuthResult['MESSAGE']
				)
			);
			die();
		}
		else
		{
			if((int)$arAuthResult)
				$USER->Authorize($arAuthResult);
			
			if($bPhone)
			{
				SmsSend('+7'.$userPhone, 'Success. Login: '.$userLogin.'. Password: '.$userPassword.'.', false, 0, 3);
			}
			else
			{
				$event = new CEvent;
				$event->SendImmediate("CSA_NEW_USER", SITE_ID, $arFields);
			}
		
			if ($USER->IsAuthorized())
			{
				echo CUtil::PhpToJSObject(
					array('type' => 'ok', 'action' => 'reload')
				);
			}
			else
			{
				echo CUtil::PhpToJSObject(
					array('type' => 'ok', 'message' => 'Success. Password send on '.($bPhone ? 'sms' : 'E-mail'))
				);
			}
		}
	break;
	case 'checkword':
		$bPhone = false;
		$userLogin = trim($_POST['login']);
		
		if(empty($userLogin)){
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => 'No E-mail'
				)
			);
			
			die();
		}
		
		if(check_email($userLogin, true)){
			$userEmail = $userLogin;
		}
		else
		{
			$phone = str_replace(array(' ', ')', '(', '+7', '+'), '', $userLogin);
			if(strlen($phone) > 10)
				$phone = substr($phone, -10, 10);
			
			if(intval($phone)){
				$userPhone = $phone;
				$bPhone = true;
			}
			else
			{
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'message' => 'Wrong E-mail'
					)
				);
				
				die();
			}
		}
		
		$filter = array('EMAIL' => $userEmail);
		
		if($bPhone){
			$filter = array('PERSONAL_PHONE' => $userPhone);
		}

        $by = 'last_login';
        $order = 'desc';
        $dbUser = CUser::getList($by, $order, array(), $filter);
		if ($arUser = $dbUser->Fetch())
		{
			$salt = randString(2);
			$checkword = md5(CMain::GetServerUniqID().uniqid());
			$checkword = $salt.md5($salt.$checkword);
			$checkword = substr($checkword, 0, 6);
			
			$strSql = "UPDATE b_user SET ".
				"	CHECKWORD = '".($salt.md5($salt.$checkword))."', ".
				"	CHECKWORD_TIME = ".$DB->CurrentTimeFunction().", ".
				"	LID = '".$DB->ForSql($SITE_ID, 2)."', ".
				"   TIMESTAMP_X = TIMESTAMP_X ".
				"WHERE ID = '".$arUser['ID']."'".
				"	AND (EXTERNAL_AUTH_ID IS NULL OR EXTERNAL_AUTH_ID='') ";

			$DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);
			
			if($bPhone)
			{
				SmsSend('+7'.$userPhone, 'Код для смены пароля '.$checkword, false, 0, 3);
			}
			else
			{
				$arFields = array(
					'EMAIL' => $arUser['EMAIL'],
					'LOGIN' => $arUser['LOGIN'],
					'PERSONAL_PHONE' => $arUser['PERSONAL_PHONE'],
					'CHECKWORD' => $checkword
				);
				
				$event = new CEvent;
				$event->SendImmediate("CSA_SEND_CHECKWORD", SITE_ID, $arFields);
			}
			
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'ok',
					'action' => 'change_form',
					'form' => 'recovery',
					'user_id' => $arUser['ID'],
					'message' => 'Код для смены пароля вам выслан '.($bPhone ? 'в смс' : 'на email')
				)
			);
			
		}
		else
		{
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => $bPhone ? 'Already exists' : 'Already exists'
                )
			);
			
			die();
		}
	break;
	case 'recovery':
		$user_id = (int)$_POST['user_id'];
		$checkword = trim($_POST['checkword']);
		
		if(empty($checkword)){
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => 'Введите код смены пароля'
				)
			);
			
			die();
		}
		
		if($user_id){
			CTimeZone::Disable();
            $db_check = $DB->Query(
                "SELECT ID, LID, CHECKWORD, ".$DB->DateToCharFunction("CHECKWORD_TIME", "FULL")." as CHECKWORD_TIME ".
                "FROM b_user ".
                "WHERE ID='".$DB->ForSql($user_id, 0)."' AND (EXTERNAL_AUTH_ID IS NULL OR EXTERNAL_AUTH_ID='')");
            CTimeZone::Enable();

            if(!($res = $db_check->Fetch())){
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'message' => 'Wrong user'
					)
				);
				
				die();
			}
			
			$salt = substr($res["CHECKWORD"], 0, 2);
			$checkword = $salt.md5($salt.$checkword);
			
			if($checkword != $res["CHECKWORD"]){
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'checkword' => $checkword,
						'true' => $res["CHECKWORD"],
						'message' => 'Не верный код смены пароля'
					)
				);
				
				die();
			}
			
			$arPolicy = CUser::GetGroupPolicy($res["ID"]);

            $passwordErrors = CUser::CheckPasswordAgainstPolicy($_POST["password"], $arPolicy);
            if (!empty($passwordErrors))
            {
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'message' => implode("<br>", $passwordErrors)."<br>"
					)
				);
				
				die();
            }

            $site_format = CSite::GetDateFormat();
            if(mktime()-$arPolicy["CHECKWORD_TIMEOUT"]*60 > MakeTimeStamp($res["CHECKWORD_TIME"], $site_format))
			{
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'message' => "Срок действия кода истек, попробуйте получить код еще раз"
					)
				);
				
				die();
            }
            
			$obUser = new CUser;
            $res = $obUser->Update($res["ID"], array("PASSWORD" => $_POST["password"]));
            if(!$res && $obUser->LAST_ERROR <> ''){
				echo CUtil::PhpToJSObject(
					array(
						'type' => 'error',
						'message' => $obUser->LAST_ERROR."<br>"
					)
				);
				
				die();
			}
            
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'ok',
					'action' => 'change_form',
					'form' => 'success',
					'message' => 'Пароль успешно изменен'
				)
			);
		}
		else{
			echo CUtil::PhpToJSObject(
				array(
					'type' => 'error',
					'message' => 'Wrong data'
				)
			);
			
			die();
		}
	break;
}

die();