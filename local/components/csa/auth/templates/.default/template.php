<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$this->setFrameMode(true);
?>
<div id="popupUser" class="popup popup_type_user js-popup">
	<div class="popup__close js-popup-close"></div>
	<div class="popup__wrapper">
		<div id="userForm" class="user-form is-enter">
			<div class="user-form__wrapper">
				<div class="user-form__header js-uf-header">
					<div data-form="registration" class="user-form__item user-form__item_type_enter js-uf-switch"><?=GetMessage('SIGN')?></div>
					<div data-form="enter" class="user-form__item user-form__item_type_enter is-active js-uf-switch"><?=GetMessage('LOGIN')?></div>
				</div>
				<div class="user-form__recovery-heading"></div>
				<div class="user-form__container">
					<form class="user-enter">
						<input type="hidden" name="action" value="enter" />
						<?=bitrix_sessid_post()?>
						<div class="user-recovery__row message" style="display:none;">
							<div class="user-enter__text"></div>
						</div>
						<div class="user-enter__row">
							<div class="field field_style_mine-shaft js-field">
								<input name="login" type="text" value="" placeholder="E-mail" class="field__input js-field-input" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						<div class="user-enter__row">
							<div class="field field_style_mine-shaft js-field">
								<input name="password" type="password" value="" placeholder="<?=GetMessage('PASS')?>" class="field__input js-field-input" />
								<div class="field__validate"></div>
								<div class="field__answer"></div>
							</div>
						</div>
						<div class="user-enter__row">
							<button type="" data-popup="" class="btn price-badge__btn"><?=GetMessage('SUBMIT')?></button>
						</div>
					</form>
                    <form class="user-registration">
                        <input type="hidden" name="action" value="registration" />
                        <?=bitrix_sessid_post()?>
                        <div class="user-recovery__row message" style="display:none;">
                            <div class="user-registration__text"></div>
                        </div>
                        <div class="user-registration__row">
                            <div class="field field_style_mine-shaft js-field">
                                <input type="text" name="login" value="" placeholder="E-mail" class="field__input js-field-input" />
                                <div class="field__validate"></div>
                                <div class="field__answer"></div>
                            </div>
                        </div>
                        <div class="user-registration__row">
                            <button type="" data-popup="" class="btn price-badge__btn"><?=GetMessage('SIGN')?></button>
                        </div>
                    </form>
				</div>
				<div class="user-form__footer js-uf-footer">
					<div class="user-form__footer-text"><?=GetMessage("SOCIAL")?></div>
                    <div class="user-form__socials">
<!--                        <div class="icons icons_square">-->
<!--                            <a href="javascript:void(0);" class="icons__item icons icons_bg_facebook">-->
<!--                                <svg class="icons__icon icons__icon_facebook"><use xlink:href="--><?//=SITE_TEMPLATE_PATH?><!--/assets/images/icon.svg#icon_facebook"></use></svg>-->
<!--                            </a>-->
<!--                        </div>-->
                        <?
                        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons",
                            array(
                                "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                                "SUFFIX"=>"form",
                            ),
                            $component,
                            array("HIDE_ICONS"=>"Y")
                        );
                        ?>
                        <?
                        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
                            array(
                                "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                                "AUTH_URL"=>$arResult["AUTH_URL"],
                                "POST"=>$arResult["POST"],
                                "POPUP"=>"Y",
                                "SUFFIX"=>"form",
                            ),
                            $component,
                            array("HIDE_ICONS"=>"Y")
                        );
                        ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>