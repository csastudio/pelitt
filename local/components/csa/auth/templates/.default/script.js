$(function(){
	$.fn.removeClassWild = function(mask) {
        return this.removeClass(function(index, cls) {
            var re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };
	
	$('form.user-enter, form.user-registration, form.user-checkword, form.user-recovery').on('submit', function(){
		var form = $(this);
		var message = $('div.message', form);
		var data = form.serialize();
		$.post('/local/components/csa/auth/ajax.php', data, function(result){
			message.hide();
			
			result = BX.parseJSON(result);
			if(result['type'] == 'ok'){
				if(result['action'] == 'reload'){
					window.location.reload();
				}
				else if(result['action'] == 'change_form'){
					form = $('form.user-' + result['form']);
					if(form.length){
						$('#userForm').removeClassWild('is-*').addClass('is-' + result['form']);
						message = $('div.message', form);
						
						if(result['form'] == 'recovery'){
							$('.user-recovery__pass-upload', form).show();
							$('.user-send_checkword', form).hide();
							$('input[name="user_id"]', form).val(result['user_id']);
							
							var timer = $('.user-recovery__pass-upload span', form);
							var time = 30;
							timer.text(time);
							
							var interval = setInterval(function(){
								time -= 1;
								
								if(time > 0)
									timer.text(time);
								else{
									clearInterval(interval);
									$('.user-recovery__pass-upload', form).hide();
									$('.user-send_checkword', form).show();
								}
							}, 1000);
						}
					}
				}
				
				if(result['message']){
					$('div', message).html(result['message']);
					message.removeClass('error').addClass('ok').show();
				}
			}
			else{
				if(result['message']){
					$('div', message).html(result['message']);
					message.removeClass('ok').addClass('error').show();
				}
			}
		});
		
		return false;
	});
});