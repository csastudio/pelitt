<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arVarExcl = array("USER_LOGIN"=>1, "USER_PASSWORD"=>1, "backurl"=>1, "auth_service_id"=>1, "TYPE"=>1, "AUTH_FORM"=>1);
$arResult["GET"] = array();
$arResult["POST"] = array();
foreach($_POST as $vname=>$vvalue)
{
	if(!isset($arVarExcl[$vname]))
	{
		if(!is_array($vvalue))
		{
			$arResult["POST"][htmlspecialcharsbx($vname)] = htmlspecialcharsbx($vvalue);
		}
		else
		{
			foreach($vvalue as $k1 => $v1)
			{
				if(is_array($v1))
				{
					foreach($v1 as $k2 => $v2)
					{
						if(!is_array($v2))
							$arResult["POST"][htmlspecialcharsbx($vname)."[".htmlspecialcharsbx($k1)."][".htmlspecialcharsbx($k2)."]"] = htmlspecialcharsbx($v2);
					}
				}
				else
				{
					$arResult["POST"][htmlspecialcharsbx($vname)."[".htmlspecialcharsbx($k1)."]"] = htmlspecialcharsbx($v1);
				}
			}
		}
	}
}
		
if(defined("AUTH_404"))
	$arResult["AUTH_URL"] = htmlspecialcharsback(POST_FORM_ACTION_URI);
else
	$arResult["AUTH_URL"] = $APPLICATION->GetCurPageParam("login=yes", array_merge($arParamsToDelete, array("logout_butt", "backurl")));
		
$arResult["AUTH_SERVICES"] = false;
$arResult["CURRENT_SERVICE"] = false;
if(!$USER->IsAuthorized() && CModule::IncludeModule("socialservices"))
{
	$oAuthManager = new CSocServAuthManager();
	$arServices = $oAuthManager->GetActiveAuthServices($arResult);

	if(!empty($arServices))
	{
		$arResult["AUTH_SERVICES"] = $arServices;
		if(isset($_REQUEST["auth_service_id"]) && $_REQUEST["auth_service_id"] <> '' && isset($arResult["AUTH_SERVICES"][$_REQUEST["auth_service_id"]]))
		{
			$arResult["CURRENT_SERVICE"] = $_REQUEST["auth_service_id"];
			if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> '')
			{
				$arResult['ERROR_MESSAGE'] = $oAuthManager->GetError($arResult["CURRENT_SERVICE"], $_REQUEST["auth_service_error"]);
			}
			elseif(!$oAuthManager->Authorize($_REQUEST["auth_service_id"]))
			{
				$ex = $APPLICATION->GetException();
				if ($ex)
					$arResult['ERROR_MESSAGE'] = $ex->GetString();
			}
		}
	}
}

$this->IncludeComponentTemplate();
?>