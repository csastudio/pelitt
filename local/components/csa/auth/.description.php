<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SHOP_AUTH_TITLE"),
	"DESCRIPTION" => GetMessage("SHOP_AUTH_DESCR"),
	"PATH" => array(
		"ID" => "Lexand",
		"CHILD" => array(
			"ID" => "lexand-components",
			"NAME" => GetMessage("LEXAND")
		)
	),
);
?>