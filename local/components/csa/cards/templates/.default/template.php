<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$this->setFrameMode(true);

//if (is_array($arResult["CARDS"]) && !empty($arResult["CARDS"]))


?>
<div class="footer-contacts__system">
	<?foreach($arResult["CARDS"] as $cards):
		$icon_name = str_replace("icon_", "", $cards["LINK"]);
		?>
		<a href="javascript:void(0);" class="footer-contacts__link-image">
			<svg class="footer-contacts__image footer-contacts__image_<?=$icon_name?>"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#<?=$cards["LINK"]?>"></use></svg>
		</a>
	<?endforeach;?>
	<a href="javascript:void(0);" class="footer-contacts__link-image">
		<img class="footer-contacts__image" src="/local/components/csa/cards/templates/.default/img/PayPal_logo_new.png" />
	</a>
</div>
<?

?>