<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arResult["CARDS"] = array();

if (isset($arParams["MCARD"]) && !empty($arParams["MCARD"]))
	$arResult["CARDS"]["MCARD"] = array(
		"LINK" => $arParams["MCARD"],
	);

if (isset($arParams["VCARD"]) && !empty($arParams["VCARD"]))
	$arResult["CARDS"]["VCARD"] = array(
		"LINK" => $arParams["VCARD"],
	);

if (isset($arParams["WMONEY"]) && !empty($arParams["WMONEY"]))
	$arResult["CARDS"]["WMONEY"] = array(
		"LINK" => $arParams["WMONEY"],
	);

if (isset($arParams["YAMONEY"]) && !empty($arParams["YAMONEY"]))
	$arResult["CARDS"]["YAMONEY"] = array(
		"LINK" => $arParams["YAMONEY"],
	);

$this->IncludeComponentTemplate();
?>