<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

$arComponentParameters = array(
	"PARAMETERS" => array(
		"MCARD" => array(
			"NAME" => GetMessage("M_CARD"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "BASE",
		),
		"VCARD" => array(
			"NAME" => GetMessage("V_CARD"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "BASE",
		),
		"WMONEY" => array(
			"NAME" => GetMessage("W_MONEY"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "BASE",
		),
		"YAMONEY" => array(
			"NAME" => GetMessage("YA_MONEY"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "BASE",
		),
	),
);
?>