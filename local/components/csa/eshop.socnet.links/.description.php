<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("ESHOP_LINKS_TITLE"),
	"DESCRIPTION" => GetMessage("ESHOP_LINKS_DESCR"),
	"PATH" => array(
		"ID" => "Lexand",
		"CHILD" => array(
			"ID" => "lexand-components",
			"NAME" => GetMessage("ESHOP_LINKS_TITLE")
		)
	),
);
?>