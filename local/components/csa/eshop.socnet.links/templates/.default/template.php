<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$this->setFrameMode(true);

if (is_array($arResult["SOCSERV"]) && !empty($arResult["SOCSERV"]))
{
?>
<div class="footer-subscribe__social">
	<div class="icons icons_social">
		<?foreach($arResult["SOCSERV"] as $socserv):?>
			<a href="<?=htmlspecialcharsbx($socserv["LINK"])?>" class="icons__item icons icons_bg_<?=$socserv["CLASS"]?>">
				<svg class="icons__icon icons__icon_<?=$socserv["CLASS"]?>"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/images/icon.svg#icon_<?=$socserv["CLASS"]?>"></use></svg>
			</a>
		<?endforeach;?>
	</div>
</div>
<?
}
?>