<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}

if ($arResult['ACTION']['status']=='error') {
	ShowError($arResult['ACTION']['message']);
} elseif ($arResult['ACTION']['status']=='ok') {
	ShowNote($arResult['ACTION']['message']);
}
?>
<form action="<?= POST_FORM_ACTION_URI?>" method="post" id="asd_subscribe_form">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="asd_subscribe" value="Y" />
	<input type="hidden" name="charset" value="<?= SITE_CHARSET?>" />
	<input type="hidden" name="site_id" value="<?= SITE_ID?>" />
	<input type="hidden" name="asd_rubrics" value="<?= $arParams['RUBRICS_STR']?>" />
	<input type="hidden" name="asd_format" value="<?= $arParams['FORMAT']?>" />
	<input type="hidden" name="asd_show_rubrics" value="<?= $arParams['SHOW_RUBRICS']?>" />
	<input type="hidden" name="asd_not_confirm" value="<?= $arParams['NOT_CONFIRM']?>" />
	<input type="hidden" name="asd_key" value="<?= md5($arParams['JS_KEY'].$arParams['RUBRICS_STR'].$arParams['SHOW_RUBRICS'].$arParams['NOT_CONFIRM'])?>" />
	<div class="footer-subscribe__subscribe">
		<div class="subscribe">
			<div class="subscribe__wrap">
				<div class="field field_style_subscribe js-field subscribe__input">
					<input type="text" name="asd_email" placeholder="Ваш e-mail" class="field__input js-field-input" />
					<div class="field__validate"></div>
					<div class="field__answer"></div>
				</div>
				<button type="submit" name="asd_submit" id="asd_subscribe_submit" class="btn btn_style_subscribe btn_shape_ellipse subscribe__button">
					<div class="btn__text">Подписаться</div>
				</button>
				<div id="asd_subscribe_res" class="subscribe__text" style="display: none;"></div>
				<div class="subscribe__text">Подпишитесь на рассылку. Полезная информация и никакого спама.</div>
			</div>
		</div>
	</div>
</form>