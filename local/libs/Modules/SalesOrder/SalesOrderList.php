<?
namespace SalesOrder;

use HiloadHandler\HiloadHandlerProcess as HHP;
use Lexand\IBHelper as LIBH;


/**
 * Журнал запросов на выгрузку данных Highload information blocks: SalesOrderList
 * Class SalesOrderList
 * @package SalesOrder
 */
class SalesOrderList{


    /**
     * Делает запись в журнал запросов
     * @param $count
     * @param $organization_id
     * @param $last_id
     * @param $type
     * @param $json_data
     * @param $log
     * @return int
     */
    public static function WriteLog($count, $organization_id, $last_id, $type, $json_data, $log)
    {
        $list_journal_entity = HHP::GetHLEntityClass(LIBH::SALES_ORDER_LIST);
        $data = [
            'UF_DATE' => date('d/m/Y h:i:s a'),
            'UF_COUNT' => $count,
            'UF_ORGANIZATION_NAME' => $organization_id,
            'UF_LAST_ID' => $last_id,
            'UF_CODE' => $type,
            'UF_JSON_DATA' => $json_data,
            'UF_LOG' => $log
        ];
        $done = $list_journal_entity::add($data);
        return $data;
    }


    /**
     * Возвращает последнюю запись по организации с запросом last
     * @param $id
     * @return array
     */
    private static function GetLastRecordByOrg($id)
    {
        return HHP::GetHLItemsByID(LIBH::SALES_ORDER_LIST, ['*'], 1, ['UF_ORGANIZATION_NAME' => $id, 'UF_CODE' => 'last']);
    }


    /**
     * Возвращает журнал запросов по организации
     * @return array
     */
    public static function LogJournalData($id)
    {
        $arJournal = HHP::GetHLItemsByID(LIBH::SALES_ORDER_LIST, ['ID', 'UF_DATE', 'UF_CODE', 'UF_LAST_ID', 'UF_COUNT', 'UF_LOG'], '', ['UF_ORGANIZATION_NAME' => $id]);
        return $arJournal;
    }


    /**
     * Возвращает последнее id заказа по организации
     * @param $id
     * @return int
     */
    public static function GetLastId($id)
    {
        $last_record = self::GetLastRecordByOrg($id);
        if (count($last_record)>0)
        {
            return $last_record[0]['UF_LAST_ID'];  # id последнего заказа в последней отгрузке по организации
        }
        else
        {
            return 0;  # если журнал по запросам по организыции пуст
        }
    }
}