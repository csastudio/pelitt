<?

namespace SalesOrder;

use \Bitrix\Main\Loader;
use SuccessHandler\SuccessHandlerProcess as ScH;
use ErrorHandler\ErrorHandlerProcess as ErH;


/**
 * Записывает tracking_id в заказ по пост запросу
 * Class SalesOrderPostTracking
 * @package SalesOrder
 */
class SalesOrderPostTracking extends SalesOrderPostAbstract
{
    private $order_id;  # int
    private $tracking_number;  # str
    private $log;  # obj SuccessHandler/ErrorHandler
    private $success;  # bool

    const TRACKING_PROP_ID = 11;


    /**
     * SalesOrderPostTracking constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $prop_id = self::AddOrderTrackingValue($data->id, $data->tracking_number);
        $this->order_id = $data->id;
        $this->tracking_number = $data->tracking_number;

        if (self::OrderExist($data->id))
        {
            if ($prop_id)
            {
                $log = new ScH();
                $this->log = $log->SuccessAr(ScH::ORDER_TRACKING_ADDED);  # добавлен номер трекинга
                $this->success = True;
            }
            else
            {
                $update_prop = self::UpdateOrderProperties($data->id, $data->tracking_number);

                if ($update_prop)
                {
                    $log = new ScH();
                    $this->log = $log->SuccessAr(ScH::ORDER_TRACKING_UPDATED);  # апдейт номера трекинга
                    $this->success = True;
                }
                else
                {
                    $log = new ErH();
                    $this->log = $log->ErrorAr(ErH::PROPERTY_ORDER_UPDATE_FAILED);  # ошибка апдейта
                    $this->success = False;
                }
            }
        }
        else
        {
            $log = new ErH();
            $this->log = $log->ErrorAr(ErH::WRONG_ORDER_ID);  # неверный номер заказа
            $this->success = False;
        }
    }


    /**
     * Проверка есть ли заказ с таким id
     * @param $order_id
     * @return bool
     */
    private static function OrderExist($order_id)
    {
        return (\CSaleOrder::GetByID($order_id)) ? True : False;
    }


    /**
     * Апдейтит номер трекинга если он уже задан
     * @param $order_id
     * @param $tracking_number
     * @return bool|int
     */
    private static function UpdateOrderProperties($order_id, $tracking_number)
    {
        Loader::includeModule('sale');

        $db_props = \CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
                "ORDER_PROPS_ID" => self::TRACKING_PROP_ID,
            ),
            false,
            false,
            array()
        );

        while ($arProps = $db_props->Fetch())
        {
            $update_prop = \CSaleOrderPropsValue::Update($arProps['ID'], array(
                'NAME' => $arProps['NAME'],
                'CODE' => $arProps['CODE'],
                'ORDER_PROPS_ID' => $arProps['ORDER_PROPS_ID'],
                'ORDER_ID' => $arProps['ORDER_ID'],
                'VALUE' => $tracking_number,
            ));
            break;
        }

        return $update_prop;
    }


    /**
     * Добавляет номер трекинга если его еще нет
     * @param $order
     * @param $value
     * @return bool|int
     */
    private static function AddOrderTrackingValue($order, $value)
    {
        Loader::includeModule('sale');

        $prop_id =  \CSaleOrderPropsValue::Add(array(
            'NAME' => 'Tracking number',
            'CODE' => 'TRACKING_NUMBER',
            'ORDER_PROPS_ID' => self::TRACKING_PROP_ID,
            'ORDER_ID' => $order,
            'VALUE' => $value,
        ));
        return $prop_id;
    }


    /**
     * Для записи лога выполнения апдейта
     * @return string
     */
    public function GetLog()
    {
        return $this->log;
    }


    /**
     * Для проверки успешности апдейта
     * @return bool
     */
    public function GetSuccess()
    {
        return $this->success;
    }
}