<?

namespace SalesOrder;

use HiloadHandler\HiloadHandlerProcess as HHP;
use Lexand\IBHelper;


/**
 * Организация по хешу с определением доступа Highload information blocks: SalesOrderHash
 * Class SalesOrderHash
 * @package SalesOrder
 */
class SalesOrderHash
{
    private $id;  # int
    private $name;  # str
    private $hash;  # str
    private $access;  # bool


    /**
     * SalesOrderHash constructor.
     * Встроена проверка на доступ по хеш - access
     * @param $hash
     * @param bool $access
     */
    public function __construct($hash, $access = False)
    {
        $arHash = HHP::GetHLItemsByID(IBHelper::SALES_ORDER_HASH);

        $success = False;
        foreach ($arHash as $item) {
            if ($hash == $item['UF_CODE'])
            {
                $this->id = $item['ID'];
                $this->name = $item['UF_NAME'];
                $this->hash = $item['UF_CODE'];
                $this->access = True;
                $success = True;
                break;
            }
        }
        if (!$success)
        {
            $this->id = 0;
            $this->name = '';
            $this->hash = $hash;
            $this->access = False;
        }
    }


    /**
     * Ответ о доступе запроса к данным
     * @return bool
     */
    public function GetAccess()
    {
        return $this->access;
    }


    /**
     * Возвращает id организации
     * @return int
     */
    public function GetOrganizationId()
    {
        return $this->id;
    }
}