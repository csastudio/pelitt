<?

namespace SalesOrder;

use \Bitrix\Main\Loader;
use JsonHandler\JsonHandlerProcess as JP;
use SalesOrder\SalesOrderList as SL;
use ErrorHandler\ErrorHandlerProcess as ErH;


/**
 * Хранит данные в зависимости от типа запроса
 * Class SalesOrderData
 * @package SalesOrder
 */
class SalesOrderData
{
    private $type;  # str
    private $data;  # []
    private $count;  # int
    private $log = '';  # str
    private $last_id;  # int


    /**
     * SalesOrderData constructor.
     * @param $type
     * @param bool $access
     * @param int $id
     */
    public function __construct($type, $access = False, $id = 0)
    {
        $error = new ErH();
        $this->type = $type;
        if ($access)
        {
            self::GetDataFromSales($type, $id);
        }
        else{
            $this->data = JP::ArInJson($error->ErrorAr(ErH::ACCESS)[1]);
            $this->count = 0;
            $this->log .= ' Access denied.. ';
            $this->last_id = 0;
        }
    }


    /**
     * Забирает данные в зависимости от типа запроса all - все, last - только свежие
     * в поле count записывается количество отданных записей
     * В logs записываются данные по обработке записи заказов
     * @param $type
     * @param $org_id
     */
    private function GetDataFromSales($type, $org_id)
    {
        if (in_array($type, array('all', 'last'))) {
            switch ($type) {
                case 'all':
                    $arFilter = Array();  # возвращает все данные
                    break;
                case 'last':
                    $arFilter = Array('>ID' => SL::GetLastId($org_id));  # Фильтр по невыгруженым данным
                    break;
            }

            $orders = [];
            $rsOrders = self::GetOrders($arFilter);

            while ($order = $rsOrders->Fetch()) {
                $orders[] = $order;
            }

            if (count($orders)>0)
            {
                foreach ($orders as $key => $order)
                {
                    $arOrder[$key] = $order;
                    $properties = self::GetOrderProperties($order["ID"]);
                    $arOrder[$key]['PROPERTIES'] = $properties ? $properties : [];

                    $basket = self::GetOrderBasket($order["ID"]);
                    $arOrder[$key]['BASKET'] = $basket ? $basket : [];
                }

                $this->data = JP::ArInJson($arOrder);
                $this->count = count($orders);
                $this->log .= 'Orders downloaded.. ';
                $this->last_id = $order['ID'];
            }
            else
            {
                $this->data = JP::ArInJson(['Success' =>' No orders for request.. ']);
                $this->count = 0;
                $this->log .= 'No orders for request.. ';
                $this->last_id = SL::GetLastId($org_id);
            }
        }
        else
        {
            $this->data = JP::ArInJson(['Error' => ' Incorrect type of request.. ']);
            $this->count = 0;
            $this->log .= 'Incorrect type of request.. ';
            $this->last_id = 0;
        }
    }


    /**
     * Выгрузка заказов
     * @param $filter
     * @return bool|\CDBResult
     */
    private function GetOrders($filter)
    {
        Loader::includeModule('sale');

        if (\CModule::IncludeModule("sale"))
        {
            $rsSales = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $filter, false, false, array('*'));
            $this->log .= 'HL orders request success.. ';
            return $rsSales;
        }
        else
        {
            $this->log .= 'HL orders request failed.. ';
            return [];
        }
    }


    /**
     * Добавляет поля свойств к данным заказа
     * @param $order_id
     * @return array
     */
    private function GetOrderProperties($order_id)
    {
        $db_props = \CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
            ),
            false,
            false,
            array()
        );
        $arProperties = [];

        while ($arProps = $db_props->Fetch())
        {
            $arProperties[] = $arProps;
        }

        if (count($arProperties)>0)
        {
            $this->log .= count($arProperties) . ' Props for id#' . $order_id . ' success.. ';
        }
        else
        {
            $this->log .= 'No props for id#' . $order_id . '.. ';
        }
        return $arProperties;
    }


    /**
     * Добавляет поля корзины к данным заказа
     * @param $order_id
     * @return array
     */
    private function GetOrderBasket($order_id)
    {
        $db_basket = \CSaleBasket::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
            ),
            false,
            false,
            array()
        );
        $arBasket = [];

        while ($arBasketItems = $db_basket->Fetch())
        {
            $arBasket[] = $arBasketItems;
        }

        if (count($arBasket)>0)
        {
            $this->log .= count($arBasket) . ' items for id#' . $order_id . ' success.. ';
        }
        else
        {
            $this->log .= 'No items for id#' . $order_id . '.. ';
        }
        return $arBasket;
    }


    /**
     * @return int
     */
    public function GetCount()
    {
        return $this->count;
    }


    /**
     * @return int
     */
    public function GetLastOrderId()
    {
        return $this->last_id;
    }


    /**
     * @return string
     */
    public function GetLog()
    {
        return $this->log;
    }


    /**
     * @return string
     */
    public function GetJsonData()
    {
        return $this->data;
    }
}