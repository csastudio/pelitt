<?

namespace SalesOrder;

use JsonHandler\JsonHandlerProcess;
use ErrorHandler\ErrorHandlerProcess as ErH;


/**
 * Class SalesOrderPostProcess
 * @package SalesOrder
 */
class SalesOrderPostProcess
{
    private $organization;  # obj SalesOrderHash
    private $type;  # str
    private $data;  # str
    private $list;  # obj SalesOrderList


    /**
     * SalesOrderPostProcess constructor.
     * @param $hash
     */
    public function __construct($hash, $data)
    {
        $error = new ErH();
        $this->organization = new SalesOrderHash($hash);
        $data_id = 0;

        if ($this->organization->GetAccess())
        {
            $data = JsonHandlerProcess::JsonInAr($data);  # перевод пост данных в массив

            if ($data[1])  # переведено успешно из json
            {
                $correct = self::PostCheck($data[0]);
                if ($correct[0])
                {
                    $data_id = $data[0]->data->id;
                    $this->type = $data[0]->type;

                    switch ($data[0]->type)
                    {

                        #  пост на добавление номера отправления к заказу tracking_number
                        case 'delivery':
                            $process = new SalesOrderPostTracking($data[0]->data);
                            break;
                    }

                    #  Проверка на успешность выполнения оперрации
                    $log = $process->GetLog();
                    if ($process->GetSuccess())
                    {
                        $this->data = JsonHandlerProcess::ArInJson($log[1]);
                        $log = $log[1]['Success'];
                    }
                    else
                    {
                        $this->data = JsonHandlerProcess::ArInJson($log[1]);
                        $log = $log[1]['Error'];
                    }
                }
                else  # не переведено из json
                {
                    $this->data = JsonHandlerProcess::ArInJson($correct[1]);  # отдает ошибку в массиве данных
                    $log = $correct[1]['Error'];
                }
            }
            else # ошибка в формате json
            {
                $this->data = JsonHandlerProcess::ArInJson(['Error' => $data[0]]);
                $log = $data[0];
            }
        }
        else
        {
            $this->data = JsonHandlerProcess::ArInJson($error->ErrorAr(ErH::ACCESS)[1]);
            $log = 'Access denied';
        }
        $this->list = SalesOrderList::WriteLog
        (
            0,
            $this->organization->GetOrganizationId(),
            $data_id,
            $this->type,
            $this->data,
            $log
        );
        JsonHandlerProcess::JsonPost($this->data);
    }


    /**
     * Проверяет на корректность переданные данные пост
     * @param $data
     * @return array
     */
    private static function PostCheck($data)
    {
        $error = new ErH();
        if (array_key_exists('type', $data))
        {
            switch ($data->type)
            {

                #  пост на добавление номера отправления к заказу tracking_number
                case 'delivery':
                    if (array_key_exists('data', $data))
                    {
                        if (array_key_exists('id', $data->data))
                        {
                            if (array_key_exists('tracking_number', $data->data))
                            {
                                return [True, ['type' => 'delivery']];  # успешно
                            }
                            else
                            {
                                return $error->ErrorAr(ErH::JSON_DATA_TRACKING_NUMBER);  # не указан tracking_number заказа в пост
                            }
                        }
                        else
                        {
                            return $error->ErrorAr(ErH::JSON_DATA_ID);  # не указан id заказа в пост
                        }
                    }
                    else
                    {
                        return $error->ErrorAr(ErH::NO_JSON_DATA);  # не указан массив с данными в пост
                    }
                    break;

                #  при неопределенном типе запроса возвращает ошибку
                default:
                    return $error->ErrorAr(ErH::REQUEST_TYPE);  # неверный тип пост
                    break;
            }
        }
        else
        {
            return $error->ErrorAr(ErH::NO_REQUEST_TYPE);  # несуществующий тип пост
        }
    }
}