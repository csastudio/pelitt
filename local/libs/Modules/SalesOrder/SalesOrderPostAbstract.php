<?

namespace SalesOrder;


abstract class SalesOrderPostAbstract
{
    abstract protected function GetLog();  # return str
    abstract protected function GetSuccess();  # return bool
}