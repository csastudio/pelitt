<?

namespace SalesOrder;

use JsonHandler\JsonHandlerProcess;


/**
 * Обработчик запроса логов журнала
 * Class SalesOrderListProcess
 * @package SalesOrder
 */
class SalesOrderListProcess
{
    private $organization;  # obj SalesOrderHash
    private $data;  # str
    private $list;  # obj SalesOrderList


    /**
     * SalesOrderListProcess constructor.
     * @param $hash
     */
    public function __construct($hash)
    {
        $this->organization = new SalesOrderHash($hash);
        $org_id = $this->organization->GetOrganizationId();
        if ($this->organization->GetAccess())
        {
            $this->data = JsonHandlerProcess::ArInJson(SalesOrderList::LogJournalData($org_id));
            $log = ' Success.. ';
        }
        else
        {
            $this->data = JsonHandlerProcess::ArInJson(['Error' => ' Access denied.. ']);
            $log = ' Access denied.. ';
        }
        $this->list = SalesOrderList::WriteLog
        (
            0,
            $org_id,
            0,
            'log',
            $this->data,
            $log
        );
        JsonHandlerProcess::JsonPost($this->data);
    }
}