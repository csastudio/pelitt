<?

namespace SalesOrder;

use JsonHandler\JsonHandlerProcess;


/**
 * Обслуживает запросы на выгрузку заказов
 * Class SalesOrderProcess
 * @package SalesOrder
 */
class SalesOrderProcess
{
    private $organization;  # obj SalesOrderHash
    private $data;  # obj SalesOrderData
    private $list;  # obj SalesOrderList


    /**
     * SalesOrderProcess constructor.
     * @param $hash
     * @param $type
     */
    public function __construct($hash, $type)
    {

        $this->organization = new SalesOrderHash($hash);

        if ($this->organization->GetAccess())
        {
            $org_id = $this->organization->GetOrganizationId();
            $this->data = new SalesOrderData($type, True, $org_id);
            $this->list = SalesOrderList::WriteLog
            (
                $this->data->GetCount(),
                $org_id,
                $this->data->GetLastOrderId(),
                $type,
                $this->data->GetJsonData(),
                $this->data->GetLog()
            );
        }
        else
        {
            $this->data = new SalesOrderData($type);
            $this->list = SalesOrderList::WriteLog
            (
                $this->data->GetCount(),
                '',
                $this->data->GetLastOrderId(),
                $type,
                $this->data->GetJsonData(),
                $this->data->GetLog()
            );
        }
        JsonHandlerProcess::JsonPost($this->data->GetJsonData());
    }
}