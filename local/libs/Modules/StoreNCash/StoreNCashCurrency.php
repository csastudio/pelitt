<?
namespace StoreNCash;


use GuzzleHttp\Client;
use JsonHandler\JsonHandlerProcess as JH;


/**
 * Class StoreNCashCurrency
 * @package StoreNCash
 */
class StoreNCashCurrency implements \ArrayAccess
{
    private $id = [];


    /**
     * StoreNCashCurrency constructor.
     */
    public function __construct()
    {
        $url = StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::SEND_CURRENCY_IN_SNC;
        $client = new Client();
        $user = new StoreNCashUser();
        $response = $client->request('GET', $url,
            ["headers" => ['Content-Type' => 'application/x-www-form-urlencoded',
                'token' => $user->GetToken()]]);
        $currency = JH::JsonInAr($response->getBody());

        $id = [];
        foreach ($currency[0]->data->collection as $cur)
        {
            $id[$cur->currency_code] = $cur->id;
        }
        $this->id = $id;
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->id[] = $value;
        } else {
            $this->id[$offset] = $value;
        }
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) {
        return isset($this->id[$offset]);
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     */
    public function offsetUnset($offset) {
        unset($this->id[$offset]);
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset) {
        return isset($this->id[$offset]) ? $this->id[$offset] : null;
    }
}