<?
namespace StoreNCash;

use GuzzleHttp\Client;
use JsonHandler\JsonHandlerProcess as JH;

/**
 * For adding objects in snc using API
 * Class StoreNCashAddAbstract
 * @package StoreNCash
 */
abstract class StoreNCashAddAbstract
{
    /**
     * StoreNCashAddAbstract constructor.
     */
    public function __construct()
    {
        self::PelittData();
        self::PelittInSnc();
        self::SncAdd();
        self::WriteId();
    }

    /**
     * Gets data about obj from pelitt
     */
    private function PelittData(){}

    /**
     * Rewrite pelitt arr in snc arr
     */
    private function PelittInSnc(){}

    /**
     * Post data in snc
     */
    private function SncAdd(){}

    /**
     * Write in pelitt id-id table
     */
    private function WriteId(){}

    /**
     * Prints object data from snc
     * @param $url
     */
    public static function PrintSncObject($url)
    {
        $client = new Client();
        $user = new StoreNCashUser();
        $response = $client->request('GET',
            $url,
            ["headers" => ['Content-Type' => 'application/x-www-form-urlencoded',
                'token' => $user->GetToken()]]);
        gg(JH::JsonInAr($response->getBody()));
    }

    /**
     * Prints all objects from snc
     * @param $url
     */
    public static function PrintSncObjectList($url)
    {
        $client = new Client();
        $user = new StoreNCashUser();
        $response = $client->request('GET', $url,
            ["headers" => ['Content-Type' => 'application/x-www-form-urlencoded',
            'token' => $user->GetToken()]]);
        gg(JH::JsonInAr($response->getBody()));
    }
}