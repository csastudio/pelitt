<?
namespace StoreNCash;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use HiloadHandler\HiloadHandlerProcess as HH;
use Lexand\IBHelper;
use JsonHandler\JsonHandlerProcess as JH;


/**
 * Для подгрузки ключа из БД
 * Class StoreNCashUser
 * @package StoreNCash
 */
class StoreNCashUser
{
    private $key;  # str
    private $secret;  # str
    private $ticket;  # str
    private $token;  # md5 secret.ticket
    private $url;  # str



    /**
     * StoreNCashUser constructor.
     */
    public function __construct()
    {
        $user = HH::GetHLItemsByID(IBHelper::STORE_N_CASH_USER)[0];
        $this->key = trim($user['UF_PUBLIC_KEY']);
        $this->secret = trim($user['UF_PRIVATE_KEY']);
        $this->url = StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::AUTH . '/' . $this->key;
        $this->LogIn();
        $this->AddToken();
    }


    /**
     * Возвражает ключ доступа юзера
     * @return mixed
     */
    public function GetPubKey()
    {
        return $this->key;
    }


    /**
     * Получает ticket от snc
     */
    private function LogIn()
    {
        $client = new Client();
        $request = new Request('PUT', $this->url);
        $response = $client->send($request, ['timeout' => 7]);
        $data = JH::JsonInAr($response->getBody());  // TODO: Error 401 $data[0]->status != 'success'

        if ($data[0]->status == 'success')
        {
            $this->ticket = $data[0]->data->ticket;
        }
    }


    /**
     * Активирует token дял запросов к snc
     */
    private function AddToken()
    {
        $this->token = md5($this->secret.$this->ticket);
    }


    public function GetToken()
    {
        return $this->token;
    }
}