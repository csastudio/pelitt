<?
namespace StoreNCash;

use GuzzleHttp\Client;
use \Bitrix\Main\Loader;
use HiloadHandler\HiloadHandlerProcess as HH;
use JsonHandler\JsonHandlerProcess as JH;
use Lexand\IBHelper;


/**
 * For adding order in snc
 * Class StoreNCashOrderAdd
 * @package StoreNCash
 */
final class StoreNCashOrderAdd extends StoreNCashAddAbstract
{
    private $id_pelitt;  # int
    private $id_snc;  # str
    private $data_pelitt;  # arr
    private $data_snc;  # arr
    private $organization;  # int
    private $log;  # str
    private $url;  # str
    private $user;  # obj StoreNCashUser()

    #some snc API consts
    const MZ_ID_ADD = 0;
    const SVOP_PU = 1;


    /**
     * StoreNCashOrderAdd constructor.
     * @param $user
     * @param $id
     */
    public function __construct($user, $id)
    {
        $this->user = $user;
        $this->id_pelitt = $id;
        $this->log = '';
        $this->url = StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::SEND_ORDER_IN_SNC;
        self::PelittData();
        self::PelittInSnc();
        self::SncAdd();
        self::WriteId();
    }


    /**
     * StoreNCashAddAbstract
     */
    private function PelittData()
    {
        $arFilter = Array('ID' => $this->id_pelitt);
        $this->data_pelitt = self::GetOrder($arFilter)[0];
        $organizations = HH::GetHLItemsByID(IBHelper::STORE_N_CASH_ORGANIZATIONS);
        $this->organization = 0;

        # get existing org
        foreach ($organizations as $org)
        {
            if ($org['UF_PELITT_ORG_ID'] == $this->data_pelitt["USER_ID"])
            {
                $this->organization = $org['UF_SNC_ORG_ID'];
                break;
            }
        }

        # add org
        if (!$this->organization)
        {
            $new_org = new StoreNCashProcess(
                StoreNCashProcess::SEND_ORGANIZATION_IN_SNC_TYPE, ['id' => $this->data_pelitt["USER_ID"]]);
            $this->organization = $new_org->GetProcess()->GetOrgSncID();
        }
    }


    /**
     * StoreNCashAddAbstract
     */
    private function PelittInSnc()
    {

        $store = HH::GetHLItemsByID(IBHelper::STORE_N_CASH_STORE)[0];
        $store_id = $store['UF_ID'];

        $currency = new StoreNCashCurrency();
        $prod_id = new \StoreNCash\StoreNCashProducts();
        $products = [];
        foreach ($this->data_pelitt['BASKET'] as $prod)
        {
            $products[] =
                [
                    "mz_id" => self::MZ_ID_ADD,
                    "product_id" => (int) $prod_id[$prod['PRODUCT_ID']],
                    "count" => (int) round($prod['QUANTITY']),
                    "price" => $prod['PRICE'],
                    "currency_id" => $currency[$prod["CURRENCY"]]
                ];
        }

        $address = '';
        foreach ($this->data_pelitt['PROPERTIES'] as $prop)
        {
            $address .= $prop['VALUE']. ' ';
        }

        $this->data_snc['autoprihod'] = $this->data_pelitt[''];
        $this->data_snc['mz_id'] = self::MZ_ID_ADD;
        $this->data_snc['sabor_kid'] = $this->organization;
        $this->data_snc['descr'] = $address;
        $this->data_snc['address'] = $this->data_pelitt['USER_DESCRIPTION'];
        $this->data_snc['storage_id'] = $store_id;
        $this->data_snc['goods'] = JH::ArInJson($products);

        /*$this->data_snc['db_id'] = $this->organization;
        $this->data_snc['device_id'] = $this->data_pelitt[''];
        $this->data_snc['sabor_rid'] = $this->data_pelitt[''];
        $this->data_snc['contractor'] = $this->data_pelitt[''];
        $this->data_snc['payment_document'] = $this->data_pelitt[''];
        $this->data_snc['operation_id'] = $this->data_pelitt[''];
        $this->data_snc['svop_pu'] = self::SVOP_PU;
        $this->data_snc['svop_pvp'] = $this->data_pelitt[''];*/
    }


    /**
     * StoreNCashAddAbstract
     */
    private function SncAdd()
    {
        $body = '';
        foreach ($this->data_snc as $key => $val)
        {
            $val = trim($val);
            if ($val)
            {
                $body .= $key . '=' . $val . '&';
            }
        }
        $body = substr($body,0,-1);

        $client = new Client();
        $response = $client->request('POST',
            $this->url,
            ["headers" =>
                ['Content-Type'=>'application/x-www-form-urlencoded',
                    'token' => $this->user->GetToken()],
                'body' => $body]);
        $data = JH::JsonInAr($response->getBody());
//        gg($data);

        if ($data[0]->status == 'success')
        {
            $this->id_snc = $data[0]->data->transaction_id;
        }
    }


    /**
     * StoreNCashAddAbstract
     */
    private function WriteId()
    {
        $entity_class_ord = HH::GetHLEntityClass(IBHelper::STORE_N_CASH_ORDERS);
        $entity_class_ord::add(
            [
                'UF_SNC_ID' => $this->id_snc,
                'UF_PELITT_ID' => $this->id_pelitt
            ]
        );
    }


    /**
     * Bitrix api gets order data
     * @param $filter
     * @return array
     */
    private function GetOrder($filter)
    {
        Loader::includeModule('sale');

        if (\CModule::IncludeModule("sale"))
        {
            $rsSales = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $filter, false, false, array('*'));

            $this->log .= 'HL orders request success.. ';
            while ($order = $rsSales->Fetch())
            {
                $orders[] = $order;
            }

            if (count($orders)>0)
            {
                foreach ($orders as $key => $order)
                {
                    $arOrder[$key] = $order;
                    $properties = self::GetOrderProperties($order["ID"]);
                    $arOrder[$key]['PROPERTIES'] = $properties ? $properties : [];

                    $basket = self::GetOrderBasket($order["ID"]);
                    $arOrder[$key]['BASKET'] = $basket ? $basket : [];
                }
                $this->log .= 'Orders downloaded.. ';
            }
            else
            {
                $this->log .= 'No orders for request.. ';
            }
            return $arOrder;
        }
        else
        {
            $this->log .= 'HL orders request failed.. ';
            return [];
        }
    }


    /**
     * Добавляет поля свойств к данным заказа
     * @param $order_id
     * @return array
     */
    private function GetOrderProperties($order_id)
    {
        $db_props = \CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
            ),
            false,
            false,
            array()
        );
        $arProperties = [];

        while ($arProps = $db_props->Fetch())
        {
            $arProperties[] = $arProps;
        }

        if (count($arProperties)>0)
        {
            $this->log .= count($arProperties) . ' Props for id#' . $order_id . ' success.. ';
        }
        else
        {
            $this->log .= 'No props for id#' . $order_id . '.. ';
        }
        return $arProperties;
    }


    /**
     * Добавляет поля корзины к данным заказа
     * @param $order_id
     * @return array
     */
    private function GetOrderBasket($order_id)
    {
        $db_basket = \CSaleBasket::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
            ),
            false,
            false,
            array()
        );
        $arBasket = [];

        while ($arBasketItems = $db_basket->Fetch())
        {
            $arBasket[] = $arBasketItems;
        }

        if (count($arBasket)>0)
        {
            $this->log .= count($arBasket) . ' items for id#' . $order_id . ' success.. ';
        }
        else
        {
            $this->log .= 'No items for id#' . $order_id . '.. ';
        }
        return $arBasket;
    }


    /**
     * Returns pelitt data of order
     * @return mixed
     */
    public function GetOrderPelittData()
    {
        return $this->data_pelitt;
    }


    /**
     * StoreNCashAddAbstract
     * @param $id
     */
    public static function PrintSncObject($id)
    {
        $url = StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::SEND_ORDER_IN_SNC . '/' . $id;
        parent::PrintSncObject($url);
    }


    /**
     * StoreNCashAddAbstract
     */
    public static function PrintSncObjectList()
    {
        $url =  StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::SEND_ORDER_IN_SNC ;
        parent::PrintSncObjectList($url);
    }
}