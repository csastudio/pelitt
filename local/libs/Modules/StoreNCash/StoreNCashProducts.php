<?
namespace StoreNCash;

use HiloadHandler\HiloadHandlerProcess as HH;
use Lexand\IBHelper;


/**
 * id pelitt -> id snc
 * Class StoreNCashProducts
 * @package StoreNCash
 */
class StoreNCashProducts implements \ArrayAccess
{
    private $id = [];  # ar


    /**
     * StoreNCashProducts constructor.
     */
    public function __construct()
    {
        $id = [];
        $products = HH::GetHLItemsByID(IBHelper::STORE_N_CASH_PRODUCTS);

        foreach ($products as $prod)
        {
            $id[$prod['UF_PELITT_ID']] = $prod['UF_SNC_ID'];
        }
        $this->id = $id;
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->id[] = $value;
        } else {
            $this->id[$offset] = $value;
        }
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) {
        return isset($this->id[$offset]);
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     */
    public function offsetUnset($offset) {
        unset($this->id[$offset]);
    }


    /**
     * ArrayAccess
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset) {
        return isset($this->id[$offset]) ? $this->id[$offset] : null;
    }
}