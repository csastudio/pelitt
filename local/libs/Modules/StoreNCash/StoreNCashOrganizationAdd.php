<?
namespace StoreNCash;

use GuzzleHttp\Client;
use JsonHandler\JsonHandlerProcess as JH;
use HiloadHandler\HiloadHandlerProcess as HH;
use Lexand\IBHelper;


/**
 * For adding organizations in snc
 * Class StoreNCashOrganizationAdd
 * @package StoreNCash
 */
final class StoreNCashOrganizationAdd extends StoreNCashAddAbstract
{
    private $id_pelitt;
    private $id_snc;
    private $data_pelitt;
    private $data_snc;
    private $url;
    private $user;

    # some snc API consts
    const COMPANY_TYPE_ID = 1;


    /**
     * StoreNCashOrganizationAdd constructor.
     * @param $user
     * @param $id
     */
    public function __construct($user, $id)
    {
        $this->user = $user;
        $this->id_pelitt = $id;
        $this->url = StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::SEND_ORGANIZATION_IN_SNC;
        self::PelittData();
        self::PelittInSnc();
        self::SncAdd();
        self::WriteID();
    }


    /**
     * StoreNCashAddAbstract
     */
    private function PelittData()
    {
        $org = \CUser::GetByID($this->id_pelitt)->Fetch();
        $this->data_pelitt = $org;
    }


    /**
     * StoreNCashAddAbstract
     */
    private function SncAdd()
    {
        $body = '';
        foreach ($this->data_snc as $key => $val)
        {
            $val = trim($val);
            if ($val)
            {
                $body .= $key . '=' . $val . '&';
            }
        }
        $body = substr($body,0,-1);

        $client = new Client();
        $response = $client->request('POST',
            $this->url,
            ["headers" =>
                ['Content-Type'=>'application/x-www-form-urlencoded',
                    'token' => $this->user->GetToken()],
            'body' => $body]);
        $data = JH::JsonInAr($response->getBody());
//        gg($data);
//        die();

        if ($data[0]->status == 'success')
        {
            $this->id_snc = $data[0]->data->id;
        }
    }


    /**
     * StoreNCashAddAbstract
     */
    private function PelittInSnc()
    {
        $this->data_snc['company_name'] = $this->data_pelitt['LOGIN'];
        $this->data_snc['company_address'] = $this->data_pelitt['PERSONAL_COUNTRY'];
        $this->data_snc['company_name_official'] = $this->data_pelitt['NAME'] . ' ' .
            $this->data_pelitt['LAST_NAME'];
        $this->data_snc['phone'] = $this->data_pelitt['PERSONAL_PHONE'];
        $this->data_snc['fax'] = $this->data_pelitt['PERSONAL_FAX'];
        $this->data_snc['city'] = $this->data_pelitt['PERSONAL_CITY'];
        $this->data_snc['company_type_id'] = self::COMPANY_TYPE_ID;

        /*$this->data_snc['description'] = $this->data_pelitt[''];
        $this->data_snc['vat_number'] = $this->data_pelitt[''];
        $this->data_snc['swift'] = $this->data_pelitt[''];
        $this->data_snc['accountant_name'] = $this->data_pelitt[''];
        $this->data_snc['bank'] = $this->data_pelitt[''];
        $this->data_snc['ceo_name'] = $this->data_pelitt[''];
        $this->data_snc['kpp'] = $this->data_pelitt[''];
        $this->data_snc['ogrn'] = $this->data_pelitt[''];
        $this->data_snc['external_id'] = $this->data_pelitt[''];
        $this->data_snc['corr_account'] = $this->data_pelitt[''];
        $this->data_snc['account_number'] = $this->data_pelitt[''];*/
    }


    /**
     * Returns organization id for adding order
     * @return mixedStoreNCashAddAbstract
     */
    public function GetOrgSncID()
    {
        return $this->id_snc;
    }


    /**
     * StoreNCashAddAbstract
     */
    private function WriteID()
    {
        $entity_class_org = HH::GetHLEntityClass(IBHelper::STORE_N_CASH_ORGANIZATIONS);
        $entity_class_org::add(
            [
            'UF_SNC_ORG_ID' => $this->id_snc,
            'UF_PELITT_ORG_ID' => $this->id_pelitt
            ]
        );
    }


    /**
     * StoreNCashAddAbstract
     * @param $id
     */
    public static function PrintSncObject($id)
    {
        $url = StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::SEND_ORGANIZATION_IN_SNC . '/' . $id;
        parent::PrintSncObjectList($url);
    }


    /**
     * StoreNCashAddAbstract
     */
    public static function PrintSncObjectList()
    {
        $url = StoreNCashProcess::SNC_BASE_URL . StoreNCashProcess::SEND_ORGANIZATION_IN_SNC;
        parent::PrintSncObject($url);
    }
}