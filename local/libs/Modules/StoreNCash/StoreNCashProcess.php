<?
namespace StoreNCash;


/**
 * Class StoreNCashProcess
 * @package StoreNCash
 */
class StoreNCashProcess
{
    private $user;  # str
    private $process;  # obj StoreNCash**
    private $type;  # int
    private $log;  # str
    private $url;  # str

    const SNC_BASE_URL = 'https://my2can.com/api/';

    # files for API requests
    const AUTH = 'v1_ticket.json';
    const SEND_ORDER_IN_SNC = 'v1_orders.json';
    const SEND_ORGANIZATION_IN_SNC = 'v1_sabor.json';
    const SEND_PRODUCT_IN_SNC = 'v1_product.json';
    const SEND_CURRENCY_IN_SNC = 'v1_currency.json';

    # request types
    const SEND_ORDER_IN_SNC_TYPE = 1;
    const SEND_ORGANIZATION_IN_SNC_TYPE = 2;


    /**
     * StoreNCashProcess constructor.
     * @param int $type
     * @param array $args
     */
    public function __construct($type = 0, array $args)
    {
        if ($type)
        {
            $this->user = new StoreNCashUser();
            switch ($type)
            {
                case self::SEND_ORDER_IN_SNC_TYPE:
                    $this->type = self::SEND_ORDER_IN_SNC_TYPE;
                    $this->process = new StoreNCashOrderAdd($this->user, $args['id']);
                    break;
                case self::SEND_ORGANIZATION_IN_SNC_TYPE:
                    $this->type = self::SEND_ORGANIZATION_IN_SNC_TYPE;
                    $this->process = new StoreNCashOrganizationAdd($this->user, $args['id']);
                    break;
            }
        }
    }


    public function GetProcess()
    {
        return $this->process;
    }


    public function GetUser()
    {
        return $this->user;
    }
}