<?
namespace ApiHandler;

use JsonHandler\JsonHandlerProcess;
use SalesOrder\SalesOrderProcess;
use SalesOrder\SalesOrderListProcess;
use SalesOrder\SalesOrderPostProcess;


/**
 * Распределяет запросы
 * Class ApiHandlerProcess
 * @package ApiHandler
 */
class ApiHandlerProcess
{

    /**
     * ApiHandlerProcess constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        #  get
        if (array_key_exists('type', $fields) && array_key_exists('hash', $fields))
        {
            new SalesOrderProcess($fields['hash'], $fields['type']);  # Запрос уходит на отгрузку данных по заказам
        }
        elseif (array_key_exists('log', $fields) && array_key_exists('hash', $fields))
        {
            new SalesOrderListProcess($fields['hash'], $fields['log']);  # запрос уходит на отгрузку логов журнала
        }

        # post
        elseif (array_key_exists('data', $fields) && array_key_exists('hash', $fields))
        {
            new SalesOrderPostProcess($fields['hash'], $fields['data']);  # пост запрос
        }
        else
        {
            JsonHandlerProcess::ArReturnJson(['Error'=>' Wrong request ']);  # отдает ошибку несуществующий запрос к апи
        }
    }
}