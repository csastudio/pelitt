<?
namespace SuccessHandler;


class SuccessHandlerProcess
{
    private $Success;

    # апдейт tracking_number заказа
    const ORDER_TRACKING_UPDATED = 1;
    const ORDER_TRACKING_ADDED = 2;


    /**
     * SuccessHandlerProcess constructor.
     */
    public function __construct()
    {
        $this->Success = '';
    }


    /**
     * Возвращает массив логов удачных операций
     * @param string $success
     * @return array
     */
    public function SuccessAr($success = '')
    {
        switch ($success)
        {
            # апдейт tracking_number заказа
            case self::ORDER_TRACKING_UPDATED:
                $this->Success = 'Order tracking number updated';
                break;
            case self::ORDER_TRACKING_ADDED:
                $this->Success = 'Order tracking number added';
                break;

            #  дефолтный success
            default:
                $this->Success = 'Success';
                break;
        }
        return [True, ['Success' => $this->Success]];
    }
}