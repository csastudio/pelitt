<?
namespace ErrorHandler;


class ErrorHandlerProcess
{
    private $Error;  # str

    # глобальные
    const ACCESS = 6;

    # Ошибки в массиве переданных данных в пост json
    const JSON_DATA_TRACKING_NUMBER = 1;
    const JSON_DATA_ID = 2;


    # Ошибки в массиве переданных данных в пост json
    const NO_JSON_DATA = 3;
    const NO_REQUEST_TYPE = 4;

    # ошибка в поле типа запроса
    const REQUEST_TYPE = 5;

    # ошибка идентификации заказа
    const WRONG_ORDER_ID = 7;

    # ошибка апдейта свойства заказа
    const PROPERTY_ORDER_UPDATE_FAILED = 8;


    /**
     * ErrorHandlerProcess constructor.
     */
    public function __construct()
    {
        $this->Error = '';
    }


    /**
     * Возвращает массив ошибок
     * @param $error
     * @return array
     */
    public function ErrorAr($error = '')
    {
        switch ($error)
        {
            # глобальные
            case self::ACCESS:
                $this->Error = 'Access denied';  # Ошибка доступа
                break;

            # Ошибки в массиве переданных данных в пост data
            case self::JSON_DATA_TRACKING_NUMBER:
                $this->Error = 'No tracking number in data of type=delivery request';  # отсутствует номер доставки
                break;
            case self::JSON_DATA_ID:
                $this->Error = 'No id in data request';  # отсутствует id заказа
                break;

            # Ошибки в массиве переданных данных в пост json
            case self::NO_JSON_DATA:
                $this->Error = 'No data in request';  # нет массива данных data
                break;
            case self::NO_REQUEST_TYPE:
                $this->Error = 'No type specified';  # нет указания типа запроса type
                break;

            # ошибка в поле типа запроса
            case self::REQUEST_TYPE:
                $this->Error = 'Wrong type of request';
                break;

            # ошибка идентификации заказа
            case self::WRONG_ORDER_ID:
                $this->Error = 'Wrong order id';
                break;

            # ошибка апдейта свойства заказа
            case self::PROPERTY_ORDER_UPDATE_FAILED:
                $this->Error = 'Update of property of order failed';
                break;

            # дефолтный вывод
            default:
                $this->Error = 'Error';
                break;
        }
        return [False, ['Error' => $this->Error]];
    }
}