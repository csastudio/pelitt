<?

namespace HiloadHandler;

use Bitrix\Highloadblock as HL;
use \Bitrix\Main\Loader;


/**
 * Для работы с hiload инфоблоками
 * Class HiloadHandlerProcess
 * @package HiloadHandler
 */
class HiloadHandlerProcess
{

    /**
     * Возвращает все записи из Hl блока по id
     * При limit = 1 + filter по hash вернет самую свежую запись запроса по организации
     * @param $id
     * @param array $select
     * @param string $limit
     * @param array $filter
     * @return array
     */
    public static function GetHLItemsByID($id, $select = ['*'], $limit = '', $filter = [])
    {
        Loader::includeModule('highloadblock');

        $arItems = [];

        $entityClass = self::GetHLEntityClass($id);
        $rsData = $entityClass::getList(array(
            'select' => $select,
            'order' => array('ID' => 'DESC'),
            'limit' => $limit,
            'filter' => $filter,
        ));

        while ($arItem = $rsData->Fetch()) {
            $arItems[] = $arItem;
        }

        return $arItems;
    }


    /**
     * Возвращает класс сущности
     * @param $id
     * @return \Bitrix\Main\Entity\DataManager
     */
    public static function GetHLEntityClass($id)
    {
        Loader::includeModule('highloadblock');

        $hlblock = HL\HighloadBlockTable::getById($id)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

        return $entityClass;
    }
}