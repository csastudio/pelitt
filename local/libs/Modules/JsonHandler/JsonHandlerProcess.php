<?

namespace JsonHandler;


/**
 * Для выгрузки в формате Json
 * Class JsonHandlerProcess
 * @package JsonHandler
 */
class JsonHandlerProcess
{


    /**
     * Переводит данные в формат Json
     * @param $ar
     * @return string
     */
    public static function ArInJson($ar)
    {
        return json_encode($ar);
    }


    /**
     * Отдает Json в запрос
     * @param $json_ar
     */
    public static function JsonPost($json_ar)
    {
        header('Content-Type: application/json');
        echo $json_ar;
    }


    /**
     * @param $json
     * @return mixed
     */
    public static function JsonInAr($json)
    {
        $data = json_decode($json);
        $error = json_last_error();

        if ($error == JSON_ERROR_NONE)
        {
            return [$data, True];
        }else{
            return [json_last_error_msg(), False];
        }
    }


    /**
     * Для краткости кода - возврат json
     * @param $ar
     */
    public static function ArReturnJson($ar)
    {
        self::JsonPost(self::ArInJson($ar));
    }
}