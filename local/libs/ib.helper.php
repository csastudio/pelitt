<?php

namespace Lexand;

use \Bitrix\Main\Loader;

class IBHelper{

    const SALES_ORDER_HASH = 1;  # id хайлода с хешами организаций
    const SALES_ORDER_LIST = 2;  # id хайлода с журналом запросов
    const STORE_N_CASH_USER = 4;  # id хл с пользователями snc
    const STORE_N_CASH_PRODUCTS = 5;  # id хл с соответствиями id продуктов pelitt к id snc
    const STORE_N_CASH_STORE = 8;  # id хл с соответствиями id складов snc
    const STORE_N_CASH_ORDERS = 6;  # id хл с соответствиями id заказов pelitt к id snc
    const STORE_N_CASH_ORGANIZATIONS = 7;  # id хл с соответствиями id организаций pelitt к id snc
    const SNC_ID_IB_CODE = 'snc_products';  # iblock with snc id

    /**
     * Возвращает ID инфоблока по его коду
     *
     * @param string $code
     *
     * @return int|bool
     */
    public static function getIBID($code)
    {
        Loader::includeModule('iblock');

        $dbObj = \CIBlock::GetList(
            Array(),
            Array(
                "CODE"=>$code
            )
        );

        return ($result = $dbObj->Fetch()) ? $result['ID'] : false;
    }
}