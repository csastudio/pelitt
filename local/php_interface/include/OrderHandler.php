<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use HiloadHandler\HiloadHandlerProcess as HH;
use Bitrix\Main\Loader;

AddEventHandler("iblock", "OnAfterIBlockElementAdd", "IdTableUpdate");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "IdTableUpdate");
AddEventHandler("iblock", "OnIBlockElementUpdate", "IdSNCTableUpdate");
AddEventHandler("main", "OnBeforeEventAdd", "OnSaleOrderSendEmail");
AddEventHandler("sale", "OnSaleStatusOrderChange", "OnSaleOrderSendSNC");

/**
 * Adds some new fields in email when order is new
 * @param $event
 * @param $lid
 * @param $arFields
 * @param $message_id
 */
function OnSaleOrderSendEmail(&$event, &$lid, &$arFields, &$message_id)
{
    Loader::includeModule('sale');
    if ($event == "SALE_NEW_ORDER") {
        if ($arFields['ORDER_ID'] > 0) {
            $db_props = \CSaleOrderPropsValue::GetList(
                array("SORT" => "ASC"),
                array(
                    "ORDER_ID" => $arFields["ORDER_ID"],
                ),
                false,
                false,
                array()
            );

            $arFields['DELIVERY'] = "";
            while ($arProps = $db_props->Fetch()) {
                $arFields["DELIVERY"] .= $arProps["VALUE"] . ' ';
            }
            $arFields["ORDER_LIST"] = str_replace('шт:', ' ', $arFields['ORDER_LIST']);
            $arFields["ORDER_LIST"] = str_replace('m:', ' ', $arFields['ORDER_LIST']);
        }
    }
}

function OnSaleOrderSendSNC($entity)
{
    if ($entity->getField('STATUS_ID') == 'TT')
    {
        $proc = new \StoreNCash\StoreNCashProcess(StoreNCash\StoreNCashProcess::SEND_ORDER_IN_SNC_TYPE, ['id' => $entity->getId()]);
    }
}


/**
 * Alters table of id snc when product is added or updated
 * @param $arFields
 */
function IdTableUpdate(&$arFields)
{
    $ib = CIBlock::GetByID($arFields["IBLOCK_ID"])->Fetch();
    $ib_code = explode('_', $ib["CODE"]);
    if ($ib_code[0] == 'shop') {
        $properties = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields['ID'], array("sort" => "asc"), Array("CODE" => "SNC_ID"));
        if ($arProp = $properties->Fetch()) {
            if ($arProp["VALUE"]) {

                $entity_class_products = HH::GetHLEntityClass(\Lexand\IBHelper::STORE_N_CASH_PRODUCTS);
                $products = HH::GetHLItemsByID(\Lexand\IBHelper::STORE_N_CASH_PRODUCTS);

                foreach ($products as $prod) {
                    if ($prod['UF_PELITT_ID'] == $arFields["ID"]) {
                        $entity_class_products::delete($prod["ID"]);
                    }
                }
                $idSnc = CIBlockElement::GetProperty(\Lexand\IBHelper::getIBID(Lexand\IBHelper::SNC_ID_IB_CODE), $arProp["VALUE"], array("sort" => "asc"), Array("CODE" => "SNC_ID"));
                if ($id = $idSnc->Fetch()) {
                    if ($id["VALUE"]) {
                        $entity_class_products::add(
                            [
                                'UF_SNC_ID' => $id['VALUE'],
                                'UF_PELITT_ID' => $arFields["ID"],
                                'UF_NAME' => $arFields["NAME"]
                            ]
                        );
                    }
                }
            }
        }
    }
    if ($ib_code[0] == 'spsr'){
        $properties = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields['ID'], array("sort" => "asc"), Array());
        $fields = [];
        while ($prop = $properties->Fetch()){
            switch ($prop["CODE"]){
                case 'GOOD':
                    $fields['items'][0]['id'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
                case 'QUANTITY':
                    $fields['items'][0]['quantity'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
                case 'PRICE':
                    $fields['items'][0]['price'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
                case 'DATE':
                    $fields['date'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
                case 'DATE_OF_DELIVERY':
                    $fields['date_of_delivery'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
                case 'ACTION':
                    $fields['action'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
                case 'ORDER':
                    $fields['order_id'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
                case 'ADD_GOOD':
                    $fields['add_good_id'] = current($arFields["PROPERTY_VALUES"][$prop["ID"]])["VALUE"];
                    break;
            }
        }
        $res = ' ';
        switch ($fields["action"]){
            case '348': # PutItems
                $good_fields = CIBlockElement::GetByID($fields["add_good_id"])->Fetch();
                $properties = CIBlockElement::GetProperty($good_fields["IBLOCK_ID"], $good_fields['ID'], array("sort" => "asc"), Array("CODE" => "BARCODE"))->Fetch();
                $fields["id"] = $fields['add_good_id'];
                $fields['article'] = $good_fields["CODE"];
                $fields['name'] = $good_fields["NAME"];
                $fields['fullname'] = $good_fields["NAME"];
                $fields['barcodes'][0] = $properties["VALUE"];
                $action = new \SPSR\PutItems($fields);
                break;
            case '349': # PurchaseOrder
                $fields['id'] = rand();
                $action = new \SPSR\PurchaseOrder($fields);
                break;
            case '350': # SalesOrder
                \CModule::IncludeModule("sale");

                $db_props = \CSaleOrderPropsValue::GetList(
                    array("SORT" => "ASC"),
                    array(
                        "ORDER_ID" => $fields['order_id'],
                    ),
                    false,
                    false,
                    array()
                );
                while ($arProps = $db_props->Fetch()) {
                    switch ($arProps["CODE"]){
                        case 'FIRST_NAME':
                            $fields['name'] .= ' ' . $arProps["VALUE"];
                            $fields['full_name'] .= ' ' . $arProps["VALUE"];
                            break;
                        case 'LAST_NAME':
                            $fields['name'] .= ' ' . $arProps["VALUE"];
                            $fields['full_name'] .= ' ' . $arProps["VALUE"];
                            break;
                        case 'ZIP':
                            $fields['postcode'] = $arProps["VALUE"];
                            break;
                        case 'ADDRESS_1':
                            $fields['full_address'] .= ' ' . $arProps["VALUE"];
                            break;
                        case 'ADDRESS_2':
                            $fields['full_address'] .= ' ' . $arProps["VALUE"];
                            break;
                        case 'USER_PHONE':
                            $fields['phone'] = $arProps["VALUE"];
                            break;
                    }
                }
                $order = \CSaleOrder::GetByID($fields['order_id']);
                $fields["date"] = explode(' ', $order["DATE_INSERT"])[0];
                $fields["sum_of_delivery"] = 0;
                $fields["is_payed"] = 'true';
                $fields["items"] = [];
                $dbBasketItems = \CSaleBasket::GetList(
                    array(
                        "NAME" => "ASC",
                        "ID" => "ASC"
                    ),
                    array(
                        "ORDER_ID" => $fields['order_id'],
                    ),
                    false,
                    false,
                    []
                );
                $i = 0;
                $fields["total_sum"] = 0;
                while ($basket = $dbBasketItems->Fetch()){
                    $fields["items"][$i] = [
                        'id' => $basket['PRODUCT_ID'],
                        'quantity' => $basket['QUANTITY'],
                        'price' => $basket['PRICE'],
                        'VAT' => 0,
                        'vat_sum' => 0,
                        'total_without_vat' => $basket['PRICE'],
                        'total_with_vat' => $basket['PRICE'],
                        'cancel_item' => 'false'
                    ];
                    $i++;
                    $fields["total_sum"] += $fields["PRICE"];
                }

                $fields['id'] = rand();
//                gg($fields);
                $contractor = new \SPSR\PutContractors($fields);
                $spsr = new \SPSR\SPSR_Client('', '', True);
                $res = $spsr->Execute($contractor->GetBody());

                $fields["consumer_id"] = $fields["id"];
                $fields['id'] = rand();
                $action = new \SPSR\SalesOrder($fields);
                break;
            case '351': # StockReport
                break;
        }
        $spsr = new \SPSR\SPSR_Client('', '', True);
        $res .= $spsr->Execute($action->GetBody());

        $entity_class = HH::GetHLEntityClass(9);
        $entity_class::add(
            [
                'UF_PELITT_ID' => $arFields['ID'],
                'UF_SPSR' => $res,
            ]
        );
    }
}


/**
 * Alters table of id snc when id is changed
 * @param $arFields
 */
function IdSNCTableUpdate($newFields, $ar_wf_element)
{
    $ib_code = CIBlock::GetByID($newFields["IBLOCK_ID"])->Fetch();
    if ($ib_code["CODE"] == \Lexand\IBHelper::SNC_ID_IB_CODE) {
        $properties = CIBlockElement::GetProperty(
            $newFields["IBLOCK_ID"],
            $newFields['ID'],
            array("sort" => "asc"),
            Array("CODE" => "SNC_ID")
        );

        if ($arProp = $properties->Fetch()) {
            if ($arProp["VALUE"]) {
                $entity_class_products = HH::GetHLEntityClass(\Lexand\IBHelper::STORE_N_CASH_PRODUCTS);
                $products = HH::GetHLItemsByID(\Lexand\IBHelper::STORE_N_CASH_PRODUCTS);

                foreach ($products as $prod) {
                    if ($prod['UF_SNC_ID'] == $arProp["VALUE"]) {
                        $pelitt_id = $prod['UF_PELITT_ID'];
                        $name = $newFields["NAME"];
                        $entity_class_products::delete($prod["ID"]);

                        foreach ($newFields['PROPERTY_VALUES'][$arProp['ID']] as $val) {
                            $new_id = $val;
                            break;
                        }
                        $entity_class_products::add(
                            [
                                'UF_SNC_ID' => $new_id['VALUE'],
                                'UF_PELITT_ID' => $pelitt_id,
                                'UF_NAME' => $name
                            ]
                        );
                    }
                }
            }
        }
    }
}
