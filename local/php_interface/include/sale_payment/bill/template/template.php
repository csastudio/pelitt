<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<?

$basketItems = array();

/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
$paymentCollection = $payment->getCollection();

/** @var \Bitrix\Sale\Order $order */
$order = $paymentCollection->getOrder();

/** @var \Bitrix\Sale\Basket $basket */
$basket = $order->getBasket();

if (count($basket->getBasketItems()) > 0)
{
	$arCells = array();
	$arProps = array();

	$n = 0;
	$sum = 0.00;
	$vat = 0;
	$vats = array();

	/** @var \Bitrix\Sale\BasketItem $basketItem */
	foreach ($basket->getBasketItems() as $basketItem)
	{
		// @TODO: replace with real vatless price
		if ($basketItem->isVatInPrice())
			$vatLessPrice = roundEx($basketItem->getPrice() / (1 + $basketItem->getVatRate()), SALE_VALUE_PRECISION);
		else
			$vatLessPrice = $basketItem->getPrice();

		$productName = $basketItem->getField("NAME");
		if ($productName == "OrderDelivery")
			$productName = "Shipping";
		else if ($productName == "OrderDiscount")
			$productName = "Discount";

		$arCells[++$n] = array(
			1 => $n,
			htmlspecialcharsbx($productName),
			roundEx($basketItem->getQuantity(), SALE_VALUE_PRECISION),
			$basketItem->getField("MEASURE_NAME") ? htmlspecialcharsbx($basketItem->getField("MEASURE_NAME")) : 'pcs',
			SaleFormatCurrency($vatLessPrice, $basketItem->getCurrency(), false),
			roundEx($basketItem->getVatRate()*100, SALE_VALUE_PRECISION) . "%",
			SaleFormatCurrency(
				$vatLessPrice * $basketItem->getQuantity(),
				$basketItem->getCurrency(),
				false
			)
		);

		$arProps[$n] = array();

		/** @var \Bitrix\Sale\BasketPropertyItem $basketPropertyItem */
		foreach ($basketItem->getPropertyCollection() as $basketPropertyItem)
		{
			if ($basketPropertyItem->getField('CODE') == 'CATALOG.XML_ID' || $basketPropertyItem->getField('CODE') == 'PRODUCT.XML_ID')
				continue;
			$arProps[$n][] = htmlspecialcharsbx(sprintf("%s: %s", $basketPropertyItem->getField("NAME"), $basketPropertyItem->getField("VALUE")));
		}

		$sum += doubleval($vatLessPrice * $basketItem->getQuantity());
		$vat = max($vat, $basketItem->getVatRate());
		if ($basketItem->getVatRate() > 0)
		{
			if (!isset($vats[$basketItem->getVatRate()]))
				$vats[$basketItem->getVatRate()] = 0;

			if ($basketItem->isVatInPrice())
				$vats[$basketItem->getVatRate()] += ($basketItem->getPrice() - $vatLessPrice) * $basketItem->getQuantity();
			else
				$vats[$basketItem->getVatRate()] += ($basketItem->getPrice()*(1 + $basketItem->getVatRate()) - $vatLessPrice) * $basketItem->getQuantity();
		}
	}

	/** @var \Bitrix\Sale\ShipmentCollection $shipmentCollection */
	$shipmentCollection = $order->getShipmentCollection();

	$shipment = null;

	/** @var \Bitrix\Sale\Shipment $shipmentItem */
	foreach ($shipmentCollection as $shipmentItem)
	{
		if (!$shipmentItem->isSystem())
		{
			$shipment = $shipmentItem;
			break;
		}
	}

	if ($shipment && (float)$shipment->getPrice() > 0)
	{
		$sDeliveryItem = "Shipping";
		if ($shipment->getDeliveryName())
			$sDeliveryItem .= sprintf(" (%s)", $shipment->getDeliveryName());
		$arCells[++$n] = array(
			1 => $n,
			htmlspecialcharsbx($sDeliveryItem),
			1,
			'',
			SaleFormatCurrency(
				$shipment->getPrice() / (1 + $vat),
				$shipment->getCurrency(),
				false
			),
			roundEx($vat*100, SALE_VALUE_PRECISION) . "%",
			SaleFormatCurrency(
				$shipment->getPrice() / (1 + $vat),
				$shipment->getCurrency(),
				false
			)
		);

		$sum += roundEx(
			$shipment->getPrice() / (1 + $vat),
			SALE_VALUE_PRECISION
		);

		if ($vat > 0)
			$vats[$vat] += roundEx(
				$shipment->getPrice() / (1 + $vat),
				SALE_VALUE_PRECISION
			);
	}

	$items = $n;

	if ($sum < $payment->getSum())
	{
		$arCells[++$n] = array(
			1 => null,
			null,
			null,
			null,
			null,
			"Subtotal:",
			SaleFormatCurrency($sum, $order->getCurrency(), false)
		);
	}

	if (!empty($vats))
	{
		// @TODO: remove on real vatless price implemented
		$delta = intval(roundEx(
			$payment->getSum() - $sum - array_sum($vats),
			SALE_VALUE_PRECISION
		) * pow(10, SALE_VALUE_PRECISION));

		if ($delta)
		{
			$vatRates = array_keys($vats);
			rsort($vatRates);

			$ful = intval($delta / count($vatRates));
			$ost = $delta % count($vatRates);

			foreach ($vatRates as $vatRate)
			{
				$vats[$vatRate] += ($ful + $ost) / pow(10, SALE_VALUE_PRECISION);

				if ($ost > 0)
					$ost--;
			}
		}

		foreach ($vats as $vatRate => $vatSum)
		{
			$arCells[++$n] = array(
				1 => null,
				null,
				null,
				null,
				null,
				sprintf(
					"Tax (%s%%):",
					roundEx($vatRate * 100, SALE_VALUE_PRECISION)
				),
				SaleFormatCurrency(
					$vatSum,
					$order->getCurrency(),
					false
				)
			);
		}
	}
	else
	{
		$taxes = $order->getTax();

		$taxesList = $taxes->getTaxList();
		if ($taxesList)
		{
			foreach ($taxesList as $tax)
			{
				$arCells[++$n] = array(
						1 => null,
						null,
						null,
						null,
						null,
						htmlspecialcharsbx(sprintf(
								"%s%s%s:",
								($tax["IS_IN_PRICE"] == "Y") ? "Included " : "",
								$tax["TAX_NAME"],
								sprintf(' (%s%%)', roundEx($tax["VALUE"], SALE_VALUE_PRECISION))
						)),
						SaleFormatCurrency(
								$tax["VALUE_MONEY"],
								$order->getCurrency(),
								false
						)
				);
			}
		}
	}

	$sumPaid = $paymentCollection->getPaidSum();
	if (DoubleVal($sumPaid) > 0)
	{
		$arCells[++$n] = array(
			1 => null,
			null,
			null,
			null,
			null,
			"Payment made:",
			SaleFormatCurrency(
				$sumPaid,
				$order->getCurrency(),
				false
			)
		);
	}

	if (DoubleVal($order->getDiscountPrice()) > 0)
	{
		$arCells[++$n] = array(
			1 => null,
			null,
			null,
			null,
			null,
			"Discount:",
			SaleFormatCurrency(
				$order->getDiscountPrice(),
				$order->getCurrency(),
				false
			)
		);
	}

	$arCells[++$n] = array(
		1 => null,
		null,
		null,
		null,
		null,
		"Total:",
		SaleFormatCurrency(
			$payment->getSum(),
			$order->getCurrency(),
			false
		)
	);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Invoice</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET?>">
<style>
	table { border-collapse: collapse; }
	table.it td { border: 1pt solid #000000; padding: 0pt 3pt; }
	table.inv td, table.sign td { padding: 0pt; }
	table.sign td { vertical-align: top; }
	table.header td { padding: 0pt; vertical-align: top; }
</style>
</head>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=x-mac-cyrillic">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List
href="Invoice%20-%20Tax%20Document%20No.%20(template%20B2C,%20EURO,%20RB).fld/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\,";
	mso-displayed-thousand-separator:\00A0;}
@page
	{margin:.79in .71in .79in .71in;
	mso-header-margin:.31in;
	mso-footer-margin:.31in;}
.style16
	{color:blue;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-style-name:Гиперссылка;
	mso-style-id:8;}
a:link
	{color:blue;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;}
a:visited
	{color:purple;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Calibri, sans-serif;
	mso-font-charset:238;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:238;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Обычный;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:238;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl65
	{mso-style-parent:style0;
	vertical-align:top;}
.xl66
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl67
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl68
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl69
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;}
.xl70
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;}
.xl71
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	vertical-align:top;}
.xl72
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl73
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;}
.xl74
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;}
.xl75
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;}
.xl76
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:1.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl77
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;}
.xl78
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl79
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:1.5pt solid windowtext;
	border-left:none;}
.xl80
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:1.5pt solid windowtext;}
.xl81
	{mso-style-parent:style0;
	color:black;
	font-size:10.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;}
.xl82
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;}
.xl83
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl84
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:1.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl85
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;}
.xl86
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl87
	{mso-style-parent:style0;
	color:black;
	font-size:8.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl88
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;}
.xl89
	{mso-style-parent:style0;
	font-size:10.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	vertical-align:top;}
.xl90
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:0;}
.xl91
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	text-align:center;}
.xl92
	{mso-style-parent:style0;
	color:black;
	font-size:12.0pt;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl93
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl94
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:1.5pt solid windowtext;
	border-left:none;}
.xl95
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;}
.xl96
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;}
.xl97
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;}
.xl98
	{mso-style-parent:style0;
	color:#000090;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl99
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl100
	{mso-style-parent:style0;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:238;}
.xl101
	{mso-style-parent:style0;
	color:black;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl102
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl103
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;}
.xl104
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl105
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;}
.xl106
	{mso-style-parent:style0;
	color:#222222;
	font-family:Arial, sans-serif;
	mso-font-charset:238;}
.xl107
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;}
.xl108
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:right;
	vertical-align:top;}
.xl109
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl110
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;}
.xl111
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl112
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl113
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl114
	{mso-style-parent:style0;
	color:windowtext;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl115
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:0%;}
.xl116
	{mso-style-parent:style0;
	color:black;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl117
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";}
.xl118
	{mso-style-parent:style0;
	color:black;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;}
.xl119
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	vertical-align:top;}
.xl120
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	white-space:normal;}
.xl121
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\,\#\#0\.00\\ \[$¤-1\]";
	text-align:center;}
.xl122
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:right;}
.xl123
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:center;}
.xl124
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	white-space:normal;}
.xl125
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	vertical-align:top;
	white-space:normal;}
.xl126
	{mso-style-parent:style0;
	color:windowtext;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl127
	{mso-style-parent:style0;
	color:windowtext;
	font-weight:700;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;}
.xl128
	{mso-style-parent:style16;
	color:blue;
	font-size:9.0pt;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;}
.xl129
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:right;
	vertical-align:top;
	white-space:normal;}
.xl130
	{mso-style-parent:style0;
	color:black;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:right;}
.xl131
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"0\.000";
	text-align:right;
	vertical-align:top;}
.xl132
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"0\.000";
	vertical-align:top;}
.xl133
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:0;
	text-align:right;
	vertical-align:top;}
.xl134
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:Fixed;
	text-align:right;
	vertical-align:top;
	white-space:normal;}
.xl135
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:Standard;
	text-align:right;
	vertical-align:top;
	white-space:normal;}
.xl136
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:Standard;
	vertical-align:top;}
.xl137
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:right;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	white-space:normal;}
.xl138
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:Fixed;
	text-align:right;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	white-space:normal;}
.xl139
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	text-align:left;}
.xl140
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"Short Date";
	text-align:right;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl141
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"Short Date";
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl142
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:center;}
.xl143
	{mso-style-parent:style0;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\[$EUR\]\\ \#\,\#\#0";
	text-align:center;}
.xl144
	{mso-style-parent:style0;
	color:#000090;
	font-size:12.0pt;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:right;
	vertical-align:top;}
.xl145
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\@";
	text-align:right;
	vertical-align:top;}
.xl146
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:right;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl147
	{mso-style-parent:style0;
	color:#000090;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:left;
	vertical-align:top;
	white-space:normal;}
.xl148
	{mso-style-parent:style0;
	color:windowtext;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:center;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
.xl149
	{mso-style-parent:style0;
	color:black;
	font-family:Arial, sans-serif;
	mso-font-charset:238;
	mso-number-format:"\#\\ \#\#0\.00";
	text-align:center;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;}
-->
</style>
</head>

<body link=blue vlink=purple>

<table border=0 cellpadding=0 cellspacing=0 width=1257 style='border-collapse:
 collapse;table-layout:fixed;width:935pt'>
 <col width=27 style='mso-width-source:userset;mso-width-alt:853;width:20pt'>
 <col class=xl88 width=27 span=2 style='mso-width-source:userset;mso-width-alt:
 853;width:20pt'>
 <col class=xl88 width=36 style='mso-width-source:userset;mso-width-alt:1152;
 width:27pt'>
 <col class=xl88 width=44 style='mso-width-source:userset;mso-width-alt:1408;
 width:33pt'>
 <col class=xl88 width=27 span=3 style='mso-width-source:userset;mso-width-alt:
 853;width:20pt'>
 <col class=xl88 width=44 style='mso-width-source:userset;mso-width-alt:1408;
 width:33pt'>
 <col class=xl88 width=27 span=5 style='mso-width-source:userset;mso-width-alt:
 853;width:20pt'>
 <col class=xl88 width=40 style='mso-width-source:userset;mso-width-alt:1280;
 width:30pt'>
 <col class=xl88 width=44 style='mso-width-source:userset;mso-width-alt:1408;
 width:33pt'>
 <col class=xl88 width=27 span=2 style='mso-width-source:userset;mso-width-alt:
 853;width:20pt'>
 <col class=xl88 width=41 style='mso-width-source:userset;mso-width-alt:1322;
 width:31pt'>
 <col class=xl88 width=32 style='mso-width-source:userset;mso-width-alt:1024;
 width:24pt'>
 <col class=xl88 width=27 span=3 style='mso-width-source:userset;mso-width-alt:
 853;width:20pt'>
 <col class=xl88 width=39 style='mso-width-source:userset;mso-width-alt:1237;
 width:29pt'>
 <col class=xl88 width=27 span=13 style='mso-width-source:userset;mso-width-alt:
 853;width:20pt'>
 <col class=xl88 width=20 style='mso-width-source:userset;mso-width-alt:640;
 width:15pt'>
 <col class=xl88 width=43 style='mso-width-source:userset;mso-width-alt:1365;
 width:32pt'>
 <col class=xl88 width=39 style='mso-width-source:userset;mso-width-alt:1237;
 width:29pt'>
 <col class=xl88 width=52 style='mso-width-source:userset;mso-width-alt:1664;
 width:39pt'>
 <tr height=23 style='height:17.0pt'>
  <td height=23 class=xl65 dir=LTR width=27 style='height:17.0pt;width:20pt'></td>
  <td class=xl92 colspan=4 dir=LTR width=134 style='mso-ignore:colspan;
  width:100pt'>Pelitt Group SE</td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=44 style='width:33pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=40 style='width:30pt'></td>
  <td class=xl72 dir=LTR width=44 style='width:33pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td class=xl72 dir=LTR width=41 style='width:31pt'></td>
  <td class=xl72 dir=LTR width=32 style='width:24pt'></td>
  <td class=xl72 dir=LTR width=27 style='width:20pt'></td>
  <td colspan=20 class=xl144 dir=LTR width=598 style='width:444pt'>INVOICE -
  <?=htmlspecialcharsbx($params["ACCOUNT_NUMBER"]); ?>.<span style='mso-spacerun:yes'>&nbsp;</span></td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl72 dir=LTR style='height:16.0pt'></td>
  <td class=xl74 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl74 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl76 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl98 colspan=3 dir=LTR style='mso-ignore:colspan'>Supplier:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=4 rowspan=5 height=101 class=xl71 dir=LTR width=121
  style='mso-ignore:colspan-rowspan;height:76.0pt;width:90pt'><!--[if gte vml 1]><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="_x0420__x0438__x0441__x0443__x043d__x043e__x043a__x0020_2"
   o:spid="_x0000_s1026" type="#_x0000_t75" style='position:absolute;
   margin-left:7pt;margin-top:10pt;width:67pt;height:61pt;z-index:2;
   visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQDJGQD2CwEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhutcaI4K9vbk6xDQtUE4hjbn///d1brgx3ZBIGMw5rflhVn
gMppg33NP7YvxQNnFCVqOTqEmh+B+Lq5vlptjx6IJRqp5kOM/lEIUgNYSaXzgKnTuWBlTM/QCy/V
TvYg7qrqXiiHETAWMe/gzaqFTu7HyJ4PqTw7SThnT/Nclqq59H40SsZkVOSuuMh57BecsVk31y8T
AUZaID+lJtSLPMU5S5nIkx0ajKebs6e3dMxgNLCNDPFV2uRc6EDCGxX3AdJU+Xu2LGipcF1nFJRt
oM1M/iWg3ScGmP67vU3YO0zf28XpX5svAAAA//8DAFBLAwQUAAYACAAAACEAC6ruOdQAAACTAQAA
CwAAAF9yZWxzLy5yZWxzpJCxagMxDIb3Qt7BaO/5kqGUEl+2QtaQQFdj6+5Mz5aRnEvy9nWGQK9k
66BBv9D3CW131zipGVkCJQPrpgWFyZEPaTBwOn6+voOSYpO3EyU0cEOBXbd62R5wsqUuyRiyqEpJ
YmAsJX9oLW7EaKWhjKlOeuJoS2150Nm6bzug3rTtm+bfDOgWTLX3Bnjv16COt1zNf9gxOCahvjSO
oqa+D+4ZVefgypnxK04VZHnAYsCzPPJ6XlML9HP75p92T5d0wHmpvoeM88OrF6/sfgAAAP//AwBQ
SwMEFAAGAAgAAAAhAO0fwSyNAgAA9gUAABIAAABkcnMvcGljdHVyZXhtbC54bWysVF2O0zAQfkfi
DpHfu3HSZJNGm65Ku4uQFqgQHMDrOFtLjh3Z7s8K8YC4CMdASHCG7o0YO0mraoVALE+ZzHi+b+ab
sS8ud40INkwbrmSJojOMAiapqri8K9GH99ejHAXGElkRoSQr0T0z6HL6/NnFrtIFkXSldAAQ0hTg
KNHK2rYIQ0NXrCHmTLVMQrRWuiEWfvVdWGmyBfBGhDHG56FpNSOVWTFmF10ETT223ao5E2LWUbCK
25kpEdTgvP2ZWqumO02VmEbRReiqcraHAONtXU/zcR7jQ8h5fFSr7TTu3M4cfC4exZM06VMg5lM8
9JHQqgPHNEoO6Aenh8nxJEsPsRPm898wj8/TuE85YR74Wk47DrlZcrrUPeGbzVIHvCrRGAWSNDCo
/df9t4fPD1/2P/Y/99+DGIXHoy4Rfn1LJzi3grfXXIB+pHB2P1ryD4NtCJeoz/+rxVB1zSlbKLpu
mLTddmgmiIXNNCveGhTogjW3DLrUr6oIBRQW00KrrebSwkqQgu3sjbG9Faw1L9HHOJ9hPIlfjOYp
no8SnF2NZpMkG2X4KktwkkfzaP7JZUdJsTbsRlEiFi0fWo+SR803nGplVG3PqGrCru5hr6HuCIdd
8xsiSoSd8KEvbfj6EsHlFHa1GquZpauB8RHfn2+R53NQNQzvHaO24+yB+0kfh+tuiWlhY263r1UF
CpK1VV7AXa2b/1EHiBLsShRPsijH8Kbclwj2Osu9Gl6MgEI8y+GmpTBJiGfjPMVpr5arw/XTamNf
MvXkmgIHBHsD0vg+yQaG0Ik0UDg6qdz2P1UAwCWFkE+FORbUFSpkP0g3ut48PAdUcLg1C2KJO+wG
fPKA9r7uwZ7+AgAA//8DAFBLAwQUAAYACAAAACEAqiYOvrwAAAAhAQAAHQAAAGRycy9fcmVscy9w
aWN0dXJleG1sLnhtbC5yZWxzhI9BasMwEEX3hdxBzD6WnUUoxbI3oeBtSA4wSGNZxBoJSS317SPI
JoFAl/M//z2mH//8Kn4pZRdYQde0IIh1MI6tguvle/8JIhdkg2tgUrBRhnHYffRnWrHUUV5czKJS
OCtYSolfUma9kMfchEhcmzkkj6WeycqI+oaW5KFtjzI9M2B4YYrJKEiT6UBctljN/7PDPDtNp6B/
PHF5o5DOV3cFYrJUFHgyDh9h10S2IIdevjw23AEAAP//AwBQSwMEFAAGAAgAAAAhANKZ4qgTAQAA
igEAAA8AAABkcnMvZG93bnJldi54bWxUkM1OwzAQhO9IvIO1SNyokxQ1aYhTVVAEJ1AKClcrcX4g
tottmtCnZ6NSRT3OrL/x7CarQXZkL4xttWLgzzwgQhW6bFXN4P3t8SYCYh1XJe+0Egx+hYVVenmR
8LjUvcrEfutqgiHKxpxB49wuptQWjZDczvROKJxV2kjuUJqalob3GC47GnjegkreKvyh4Ttx34ji
a/sjGeT1Q+42G7M5vH5+D2FAnzKdfzB2fTWs74A4Mbjp8T/9XDKYw7gKrgEp9hu6tSoabUiVCdse
sPzRr4yWxOieQQCk0B36PozGS1VZ4VAGoefhIXB2sqJoiQ4dY50+wosTfHsOz5dIn8F+GEZHmk6t
0gTFdML0DwAA//8DAFBLAwQKAAAAAAAAACEAEqfliPsJAAD7CQAAFAAAAGRycy9tZWRpYS9pbWFn
ZTEucG5niVBORw0KGgoAAAANSUhEUgAAAL0AAACyCAMAAAFXWA5BAAAAAXNSR0IArs4c6QAAAARn
QU1BAACxjwv8YQUAAAHdUExURf////b29mppaSMhIiIfICkmJ7a2tunp6VNRUnt6evT09HNxcigm
JkVDRMbGxsPCwj07OywpKpeWlv39/ZaVlTMxMcHBwcPDw15dXbe2tjMwMeHh4d3d3WFfYPz8/ERC
Qzs6OmVjYyspKqmoqFRTU6yrq1ZVVeXl5X18fNDQ0H9/f5KRkvj4+P7+/vPz8/Ly8nV0dPn5+fv7
++/v70E/QIGBgS0qK0JAQNjY2HNzcyYjJEI/QOvq6mBfXyMgIWloaPX19d/f32tpatfW1q+urq6t
riUiI9vb29HQ0KWlpdnZ2dTU1K2srLKxsvr6+oF/gEZDRDAtLltYWfDw8GJhYkVCQ0RBQiYkJW9s
bfLx8Y+Oj+3t7URCQisoKVVSU4+Pj5WVlYWEhD47PGNiYionKDAuL6inp1RSU9XV1ebm5ujn56em
ps/Pz9HR0YiHh+vr6zc0Nerq6nl4eMTDw728vGBeXzw5Ovf3905MTD88PT07PFJRUcvKyrm4uExL
S0VERCQhImtqa0E/P5eVlZqYmePj4zYzNOzs7DMxMnJycnd3d29ubrS0tO7u7jk3OEdGRnd2dllX
V2VkZKSjo7y7vG5tbYB/gGVjZExJSsLCws7OzlxbW7KxsZiXlwAAAB31E9cAAACfdFJOU///////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////APJ+7zIAAAAJcEhZcwAAIdUA
ACHVAQSctJ0AAAb8SURBVHhe7Zy9jts4EMcFUAYWV1y6Aw5wShtp9xnuinTBprq9R7hiS2MNLKAi
PsAQ4MbZYh838yWJlERJlkiZsvmLQlPD0eTv0Vim9ZGkg1SAnrZ0DlBrWNnErbHeMpBKZLHIi76w
RV70hS3yoi9skRd9YYu86Atb5EVf0CI0B6hFmgNvSNuANRR2SpOscyfLMt0Kiwx0hSp75Tp20lQp
zcoDr0myReAlSfbUwjIGUoRg6HWa5CrZ0b8h5mogTfA94QC2aO0aAKxbwDqbhoWaf0CwbQE0Bqh6
qHxqA9ZQ6RFf2wZUlh1bB6yhdthp3UKplMpHH8i3XD1UNc9Z8vUhOYwsHzssVoON0KypzandsR0R
lwJeo1bfIKu87BswF2ywxs+YgnElLdMj6Qv47pIzeEn++za4SBK1C9nAhI3QDPsXGA8boBaBjdD0
bICfSeqUresNStgITc8GRW0RXjYoYSM0lg2CQ3SX4Ac3TZ+gVSke0bS3RIhbCfuvqB3uz+13atkL
W6TqIVsj/qX+ObVoH+LP+PNX6kxHSLWhdk0OHf6UT0zZT2pb/V/r/vBtsD9hwR1P5FDzb8SX8cor
bP/G++3xv7f3mw3wN3HvLx2N6/vz3JIgW4//Sd4sMOj9jvDntYH1MIM//N7Cn1ze3m/BZfqZEPwV
ID8FBvkX9PnjTwhsS2DtvMVvrl32SRvHNjIIybwd3CeFo/Rhusb7B4BJW7l/4Pu02lcCmjrR4+Oe
Byg+zl8APT584trjS7/BFsZ8xv+AsSvo15kU/xXGfMa36T8B0qP4vHai+L/JiICmS+NX+ccOsdH6
MjlkyCL9BsPj//QT38/+9V2fMb4Q92+Mj33u2uPjSUAbLuJ34UT/ygqe6SwdXedfB/3uO742kdZw
l/9Z4qtPGmTgQVfxjbOVbo8Ps8SXE8CMh/gNFhZfQjTRBp3lXwcHFxDfd34aBF4/kchI3rGculBy
jgQdi0sYteu9qqhH9Dc/+73hi1Mwf0O3ukJihC/LXe8zMXxf+I2sNMDBSeH/QMvQ8NXFLC/hPavX
w7eF1PvMqOTooNmxeh10WUb4uZJTns17fESXlM/m8de1cTZvVHi6mICdNbwi+JPyz6Kv6x+VHOxS
3X/FHpEkG+kZ6RmlHs2ew1MQt+Gbxxy83AXsH2DP7uhEf5qdaYSZpr6jz0zLvfQdhdeTszz1nnPf
rByn4WPl6ODgfOpj5ejg4Hy5j5Wjg4OW5MTKwUFLci5Q/yIrdWhWN129BZ7UTc99J5bkTFfPzKd+
bO5xRt3KanWUsqon5wL11sLUmKB+THgP6l0fczRmUO8599Mq5yQ3H5rID8np6o9S7CbTP7VFeBqu
M/2YMzS8Z/UTc4/9XPYo8h0NluSMU1+IYnDQqfrB4T2on14511TvOffTKwcfhCvBdbfqmzjNfRNL
chaiXk4uucm9Mm8qrKgnZ6R67jZxpJ67TdzkvjO862OOxgzqHeT+P+43qCdnnHqEb5WXBZ97Qtyo
b3LHxxx6cEAOBBt6fKDOOw+So3Q/73f7TPrb7cODyCMXfDwmEoksnnPXLbiDUMXBgzl9SER8Oknj
LBcuM/Pg8XaU65kHMQgHsqbpsetY8yZOU5DbpYTy9oonY+JEN1YR+qU/ucmKEIPAs2hEDG14VF88
18sEq96UU6n/XyxEpf6zWJi8rFwxCHuL3eB3cZlC8U3J9Oa+pl6sdZXycQDE0EaVe1NDP+9leFvl
DMu9WEepp9kOUcwpBoP3/RIX1v1C1C879zdUOclbnudf8m957VvpWurxeUtiUO5tLKNybCykcizE
ygF8V06Oz2OpldrJT2QApp0y8USgz7vrtFXgJ1a0r8y5SIXP3De+rVhr6d6YYykWcTDeFFG9Y4PZ
ct9yvG+ZIYp6Wav4izZoMFvdT1Nv+YniM/d985zqrsgSElHdIVkSYOUkybM8IE88PosVPrfJSTvm
2I+8E9Tjs8XEePXAMr6tFqc+6NxPPuYQsXKAmHvkVuo+HnMAMbRxt+p7v2tj5QBiaONu1cdjTsC5
X3bdx3kOIIY27rbul105Qf2uVR8vhwv+vFR3xYSgfjwhVM54bjP3y1Bvu1Y+Z+Vka/wPLMcgoYTr
5P7iI6aFW1Ef9LeVhVg5QKycEYSQ+y1etxuI+RxqAOpP2vXSXkKY55jqnxyoX3buY+UAYmijt3Ks
/0VhkhUuAefe+A86DcL9Ve5EfQCVY1cf7vmc26+ccM8hV5Wzst3/XqoMOfd0R1MrMhy4+l4CrpwB
3GbuQ1C/Oxz+PfzQ/1bAGmBUSBCVM+CIaSGo3N+t+lg5gBjaiOpNgqoclZ5z+PMP/LUt5o2qQeW+
HxXnOYAYhKur95/76i5yU321cT9+1dtPakQikSWQJL8Aog1qLrM+U3QAAAAASUVORK5CYIJQSwEC
LQAUAAYACAAAACEAyRkA9gsBAAAYAgAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNd
LnhtbFBLAQItABQABgAIAAAAIQALqu451AAAAJMBAAALAAAAAAAAAAAAAAAAADwBAABfcmVscy8u
cmVsc1BLAQItABQABgAIAAAAIQDtH8EsjQIAAPYFAAASAAAAAAAAAAAAAAAAADkCAABkcnMvcGlj
dHVyZXhtbC54bWxQSwECLQAUAAYACAAAACEAqiYOvrwAAAAhAQAAHQAAAAAAAAAAAAAAAAD2BAAA
ZHJzL19yZWxzL3BpY3R1cmV4bWwueG1sLnJlbHNQSwECLQAUAAYACAAAACEA0pniqBMBAACKAQAA
DwAAAAAAAAAAAAAAAADtBQAAZHJzL2Rvd25yZXYueG1sUEsBAi0ACgAAAAAAAAAhABKn5Yj7CQAA
+wkAABQAAAAAAAAAAAAAAAAALQcAAGRycy9tZWRpYS9pbWFnZTEucG5nUEsFBgAAAAAGAAYAhAEA
AFoRAAAAAA==
">
   <v:imagedata src="/upload/pay/image001.png"
    o:title=""/>
   <o:lock v:ext="edit" aspectratio="f"/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td width=9 height=13></td>
   </tr>
   <tr>
    <td></td>
    <td><img width=90 height=82
    src="/upload/pay/image002.png"
    v:shapes="_x0420__x0438__x0441__x0443__x043d__x043e__x043a__x0020_2"></td>
    <td width=22></td>
   </tr>
   <tr>
    <td height=6></td>
   </tr>
  </table>
  </span><![endif]></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl99 colspan=6 dir=LTR style='mso-ignore:colspan'>Variable symbol:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=5 class=xl133 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl101 colspan=4 dir=LTR style='mso-ignore:colspan'>Pelitt Group SE</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl99 colspan=5 dir=LTR style='mso-ignore:colspan'>Constant Code:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=5 class=xl108 dir=LTR>0308</td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl101 colspan=5 dir=LTR style='mso-ignore:colspan'>Na Str_i 1702/65</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl99 colspan=2 dir=LTR style='mso-ignore:colspan'>Date:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=4 class=xl145><?=$params["DATE_INSERT"]; ?></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl101 colspan=4 dir=LTR style='mso-ignore:colspan'>140 00 Praha 4</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl72 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl72 dir=LTR style='height:16.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl101 colspan=4 dir=LTR style='mso-ignore:colspan'>Czech Republic</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl72 dir=LTR style='height:16.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl72 dir=LTR></td>
  <td class=xl74 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl75 dir=LTR>&nbsp;</td>
  <td class=xl76 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl98 colspan=8 dir=LTR style='mso-ignore:colspan'>Registered
  number: 03815854</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl98 colspan=4 dir=LTR style='mso-ignore:colspan'>Customer:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl98 colspan=7 dir=LTR style='mso-ignore:colspan'>VAT number:
  CZ03815854</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl100></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl88></td>
  <td class=xl71></td>
  <td class=xl71></td>
  <td class=xl71></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl100></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl78 colspan=8 dir=LTR style='mso-ignore:colspan'>Telephone: +420
  222 191 985</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl93 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl100></td>
  <td class=xl101 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl72 dir=LTR style='height:15.0pt'></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl100></td>
  <td class=xl101 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl72 dir=LTR style='height:16.0pt'></td>
  <td class=xl80 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl94 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl98 colspan=6 dir=LTR style='mso-ignore:colspan'>Registered
  number:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 dir=LTR style='height:16.0pt'>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95 colspan=3 style='mso-ignore:colspan'>Issuing Bank:</td>
  <td class=xl95 colspan=6 style='mso-ignore:colspan'>Raiffeisenbank, a.s.</td>
  <td class=xl95></td>
  <td class=xl95></td>
  <td class=xl95></td>
  <td class=xl95></td>
  <td class=xl95></td>
  <td class=xl95></td>
  <td class=xl72 dir=LTR></td>
  <td class=xl72 dir=LTR></td>
  <td class=xl72 dir=LTR></td>
  <td class=xl72 dir=LTR></td>
  <td class=xl72 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl98 colspan=5 dir=LTR style='mso-ignore:colspan'>VAT number: C</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl67 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95 colspan=3 style='mso-ignore:colspan'>SWIFT:</td>
  <td class=xl88 colspan=4 style='mso-ignore:colspan'>RZBCCZPP</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl101 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl67 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95 colspan=2 style='mso-ignore:colspan'>IBAN:</td>
  <td class=xl88></td>
  <td class=xl88 colspan=9 style='mso-ignore:colspan'>CZ7955000000008606705001</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl67 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95 colspan=3 style='mso-ignore:colspan'>Account No.:</td>
  <td colspan=5 class=xl139>8606705001</td>
  <td class=xl95></td>
  <td class=xl95></td>
  <td class=xl95 colspan=3 style='mso-ignore:colspan'>Bank code:</td>
  <td colspan=2 class=xl139>5500</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
  <td class=xl77 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl89></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl67 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 dir=LTR style='height:16.0pt'>&nbsp;</td>
  <td class=xl80 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl96 colspan=3 style='mso-ignore:colspan'>Address:</td>
  <td class=xl97 colspan=14 style='mso-ignore:colspan'>Hv_zdova 1716/2b, Praha
  4, 140 78, Czech Republic</td>
  <td class=xl81 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl79 dir=LTR>&nbsp;</td>
  <td class=xl80 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl73 dir=LTR>&nbsp;</td>
  <td class=xl79 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl66 dir=LTR style='height:16.0pt'>&nbsp;</td>
  <td class=xl82 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl83 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl83 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl83 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl83 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl83 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl83 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl83 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl84 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl82 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl75 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl84 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99 colspan=3 dir=LTR style='mso-ignore:colspan'>Invoice date:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl103 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=5 class=xl140 dir=LTR><?=$params["DATE_INSERT"]; ?></td>
  <td class=xl104 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl98 colspan=6 dir=LTR style='mso-ignore:colspan'>Delivery address:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99 colspan=3 dir=LTR style='mso-ignore:colspan'>Due date:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl103 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=5 class=xl140 dir=LTR><?=$params["DATE_INSERT"]; ?></td>
  <td class=xl104 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl106></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl101 dir=LTR></td>
  <td class=xl100></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl98 colspan=7 dir=LTR style='mso-ignore:colspan;border-right:.5pt solid black'>Date
  of a taxable supply:</td>
  <td class=xl103 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=5 class=xl140 dir=LTR><?=$params["DATE_INSERT"]; ?></td>
  <td class=xl104 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl106></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl100></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl106></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl106></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99 colspan=3 dir=LTR style='mso-ignore:colspan'>Payment:</td>
  <td colspan=10 class=xl108 dir=LTR>payment order</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
  <td class=xl105 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl106></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl108 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl105 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl106></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl85 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl110 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl78 dir=LTR></td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl111 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl69 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl112 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl85 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl109 colspan=3 dir=LTR style='mso-ignore:colspan'>Description</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl113>Q_ty</td>
  <td class=xl113>&nbsp;</td>
  <td class=xl114>&nbsp;</td>
  <td colspan=2 class=xl148>Unit price</td>
  <td colspan=5 class=xl146 dir=LTR>Discount</td>
  <td colspan=4 class=xl146 dir=LTR>Price</td>
  <td colspan=4 class=xl146 dir=LTR>%VAT</td>
  <td colspan=5 class=xl149>VAT</td>
  <td colspan=2 class=xl146 dir=LTR>Total</td>
  <td class=xl111 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl69 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl112 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td colspan=38 class=xl147 dir=LTR width=1151 style='width:856pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
<?
$rowsCnt = count($arCells);
for ($n = 1; $n <= $rowsCnt-3; $n++)
{	$accumulated = 0;
		 if (!is_null($arCells[$n][1])) { 		
		 } else {
			$accumulated++;
		} ?>
	
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td colspan=13 class=xl120 width=407 style='width:303pt'><?=$arCells[$n][2]; ?>
			<? if (isset($arProps[$n]) && is_array($arProps[$n])) { ?>
			<? foreach ($arProps[$n] as $property) { ?>
			<br>
			<small><?=$property; ?></small>
			<? } ?>
			<? } ?>

  <td class=xl88 align=right><?=$arCells[$n][3]; ?></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td colspan=2 class=xl143><?=$arCells[$n][5]; ?></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115 align=right>0%</td>
  <td class=xl88></td>
  <td colspan=4 class=xl122><span style='mso-spacerun:yes'>&nbsp;</span><?=$arCells[$n][5]; ?></td>
  <td class=xl88 colspan=3 style='mso-ignore:colspan'></td>
  <td class=xl88 align=right>21</td>
  <td colspan=5 class=xl123><span style='mso-spacerun:yes'>&nbsp;</span>1,05</td>
  <td colspan=2 class=xl122><span style='mso-spacerun:yes'>&nbsp;</span><?=$arCells[$n][7]; ?></td>
  <td class=xl116></td>
 </tr>
 <?}?>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td colspan=4 class=xl122></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td colspan=5 class=xl123></td>
  <td colspan=2 class=xl122></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td colspan=4 class=xl122></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td colspan=5 class=xl123></td>
  <td colspan=2 class=xl122></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td class=xl123></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td class=xl123></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td class=xl123></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td class=xl123></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td class=xl123></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=19 style='mso-height-source:userset;height:14.5pt'>
  <td height=19 class=xl66 dir=LTR style='height:14.5pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=36 style='width:27pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=44 style='width:33pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=27 style='width:20pt'></td>
  <td class=xl120 width=40 style='width:30pt'></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl121></td>
  <td class=xl121></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl115></td>
  <td class=xl88></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl90></td>
  <td class=xl90></td>
  <td class=xl88></td>
  <td class=xl123></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl91></td>
  <td class=xl122></td>
  <td class=xl88></td>
  <td class=xl116>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl85 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl111 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl69 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl112 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95 colspan=3 style='mso-ignore:colspan'>Total amount</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td colspan=5 class=xl122><span style='mso-spacerun:yes'>&nbsp;</span>284,00</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl117></td>
  <td class=xl88 align=right>0</td>
  <td colspan=5 class=xl142><span style='mso-spacerun:yes'>&nbsp;</span>59,64</td>
  <td colspan=2 class=xl122><span style='mso-spacerun:yes'>&nbsp;</span>343,64</td>
  <td class=xl116>EURO</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95 colspan=3 style='mso-ignore:colspan'>TOTAL DUE</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td colspan=15 class=xl122>Currency</td>
  <td class=xl118 colspan=3 style='mso-ignore:colspan'>EURO</td>
  <td class=xl88></td>
  <td colspan=4 class=xl130><span style='mso-spacerun:yes'>&nbsp;</span>343,64</td>
  <td class=xl127>EURO</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl85 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl111 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl69 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl112 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl126 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl99 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl95 colspan=2 style='mso-ignore:colspan'>Issuer:</td>
  <td class=xl88 colspan=5 style='mso-ignore:colspan'>Alexander Pavlov</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl128 colspan=7 dir=LTR style='mso-ignore:colspan'><a
  href="mailto:alexander.pavlov@pelitt.com">alexander.pavlov@pelitt.com</a></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl85 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl111 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl69 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl112 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=15 class=xl108 dir=LTR>Recapitulation in CZK:</td>
  <td colspan=8 class=xl108 dir=LTR>Tax base in CZK</td>
  <td class=xl71 dir=LTR></td>
  <td colspan=2 class=xl108 dir=LTR>% VAT</td>
  <td colspan=6 class=xl108 dir=LTR>VAT in CZK</td>
  <td colspan=4 class=xl108 dir=LTR>Total in CZK</td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99 colspan=7 dir=LTR style='mso-ignore:colspan'>Foreign exchange
  rate used:</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td colspan=4 class=xl131 dir=LTR>27,012</td>
  <td class=xl71></td>
  <td class=xl119>CZK</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=6 class=xl135 dir=LTR width=174 style='width:129pt'>0,00</td>
  <td class=xl71 dir=LTR></td>
  <td colspan=2 class=xl129 dir=LTR width=54 style='width:40pt'>0%</td>
  <td colspan=6 class=xl129 dir=LTR width=162 style='width:120pt'><span
  style='mso-spacerun:yes'>&nbsp;</span>0,00</td>
  <td colspan=4 class=xl129 dir=LTR width=129 style='width:96pt'><span
  style='mso-spacerun:yes'>&nbsp;</span>0,00</td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99 colspan=3 dir=LTR style='mso-ignore:colspan'>Quantity:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td colspan=4 class=xl133 dir=LTR>1</td>
  <td class=xl71></td>
  <td class=xl119 colspan=2 style='mso-ignore:colspan'>EURO</td>
  <td class=xl71 dir=LTR></td>
  <td colspan=6 class=xl134 dir=LTR width=174 style='width:129pt'>0,00</td>
  <td class=xl71 dir=LTR></td>
  <td colspan=2 class=xl129 dir=LTR width=54 style='width:40pt'>10%</td>
  <td colspan=6 class=xl129 dir=LTR width=162 style='width:120pt'>0,00</td>
  <td colspan=4 class=xl129 dir=LTR width=129 style='width:96pt'>0,00</td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=6 class=xl134 dir=LTR width=174 style='width:129pt'>0,00</td>
  <td class=xl71 dir=LTR></td>
  <td colspan=2 class=xl129 dir=LTR width=54 style='width:40pt'>15%</td>
  <td colspan=6 class=xl129 dir=LTR width=162 style='width:120pt'>0,00</td>
  <td colspan=4 class=xl129 dir=LTR width=129 style='width:96pt'>0,00</td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td colspan=6 class=xl135 dir=LTR width=174 style='width:129pt'>7&nbsp;671,41</td>
  <td class=xl71 dir=LTR></td>
  <td colspan=2 class=xl129 dir=LTR width=54 style='width:40pt'>21%</td>
  <td colspan=6 class=xl129 dir=LTR width=162 style='width:120pt'>1 611,00</td>
  <td colspan=4 class=xl129 dir=LTR width=129 style='width:96pt'>9 282,40</td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl85 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td colspan=6 class=xl138 dir=LTR width=174 style='width:129pt'><span
  style='mso-spacerun:yes'>&nbsp;</span></td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td colspan=2 class=xl137 dir=LTR width=54 style='width:40pt'><span
  style='mso-spacerun:yes'>&nbsp;</span></td>
  <td colspan=6 class=xl137 dir=LTR width=162 style='width:120pt'><span
  style='mso-spacerun:yes'>&nbsp;</span></td>
  <td colspan=4 class=xl137 dir=LTR width=129 style='width:96pt'><span
  style='mso-spacerun:yes'>&nbsp;</span></td>
  <td class=xl111 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl69 dir=LTR style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl112 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99 dir=LTR></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=36 style='width:27pt'></td>
  <td class=xl125 width=44 style='width:33pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=44 style='width:33pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=40 style='width:30pt'></td>
  <td class=xl125 width=44 style='width:33pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=41 style='width:31pt'></td>
  <td class=xl125 width=32 style='width:24pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=39 style='width:29pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=20 style='width:15pt'></td>
  <td class=xl125 width=43 style='width:32pt'></td>
  <td class=xl125 width=39 style='width:29pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl119></td>
  <td class=xl125 width=36 style='width:27pt'></td>
  <td class=xl125 width=44 style='width:33pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=44 style='width:33pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=40 style='width:30pt'></td>
  <td class=xl125 width=44 style='width:33pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=41 style='width:31pt'></td>
  <td class=xl125 width=32 style='width:24pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=39 style='width:29pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=27 style='width:20pt'></td>
  <td class=xl125 width=20 style='width:15pt'></td>
  <td class=xl125 width=43 style='width:32pt'></td>
  <td class=xl125 width=39 style='width:29pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl124 width=43 style='width:32pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl124 width=43 style='width:32pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl99></td>
  <td class=xl124 width=36 style='width:27pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=40 style='width:30pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=41 style='width:31pt'></td>
  <td class=xl124 width=32 style='width:24pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=20 style='width:15pt'></td>
  <td class=xl124 width=43 style='width:32pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl88></td>
  <td class=xl124 width=36 style='width:27pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=40 style='width:30pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=41 style='width:31pt'></td>
  <td class=xl124 width=32 style='width:24pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=20 style='width:15pt'></td>
  <td class=xl124 width=43 style='width:32pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl70 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl88></td>
  <td class=xl124 width=36 style='width:27pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=40 style='width:30pt'></td>
  <td class=xl124 width=44 style='width:33pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=41 style='width:31pt'></td>
  <td class=xl124 width=32 style='width:24pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=27 style='width:20pt'></td>
  <td class=xl124 width=20 style='width:15pt'></td>
  <td class=xl124 width=43 style='width:32pt'></td>
  <td class=xl124 width=39 style='width:29pt'></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl66 dir=LTR style='height:15.0pt'>&nbsp;</td>
  <td class=xl85 dir=LTR style='border-left:none'>&nbsp;</td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl88></td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl109 dir=LTR>&nbsp;</td>
  <td class=xl111 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 style='height:15.0pt'></td>
  <td class=xl69 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'><!--[if gte vml 1]><v:shape
   id="_x0420__x0438__x0441__x0443__x043d__x043e__x043a__x0020_1" o:spid="_x0000_s1025"
   type="#_x0000_t75" style='position:absolute;margin-left:15pt;margin-top:4pt;
   width:164pt;height:66pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQDJGQD2CwEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhutcaI4K9vbk6xDQtUE4hjbn///d1brgx3ZBIGMw5rflhVn
gMppg33NP7YvxQNnFCVqOTqEmh+B+Lq5vlptjx6IJRqp5kOM/lEIUgNYSaXzgKnTuWBlTM/QCy/V
TvYg7qrqXiiHETAWMe/gzaqFTu7HyJ4PqTw7SThnT/Nclqq59H40SsZkVOSuuMh57BecsVk31y8T
AUZaID+lJtSLPMU5S5nIkx0ajKebs6e3dMxgNLCNDPFV2uRc6EDCGxX3AdJU+Xu2LGipcF1nFJRt
oM1M/iWg3ScGmP67vU3YO0zf28XpX5svAAAA//8DAFBLAwQUAAYACAAAACEAC6ruOdQAAACTAQAA
CwAAAF9yZWxzLy5yZWxzpJCxagMxDIb3Qt7BaO/5kqGUEl+2QtaQQFdj6+5Mz5aRnEvy9nWGQK9k
66BBv9D3CW131zipGVkCJQPrpgWFyZEPaTBwOn6+voOSYpO3EyU0cEOBXbd62R5wsqUuyRiyqEpJ
YmAsJX9oLW7EaKWhjKlOeuJoS2150Nm6bzug3rTtm+bfDOgWTLX3Bnjv16COt1zNf9gxOCahvjSO
oqa+D+4ZVefgypnxK04VZHnAYsCzPPJ6XlML9HP75p92T5d0wHmpvoeM88OrF6/sfgAAAP//AwBQ
SwMEFAAGAAgAAAAhAF2m5x8XAgAA8wQAABIAAABkcnMvcGljdHVyZXhtbC54bWysVFuO0zAU/Udi
D5b/mbz6SKMmo2qqQUgjqBAswOPcNBaJHdmm7XwiNsIyEBKsobMjruM0VT+QEOXPuY9zrs+5zvL2
0DZkB9oIJXMa3YSUgOSqFHKb048f7l+llBjLZMkaJSGnT2DobfHyxfJQ6oxJXitNEEKaDAM5ra3t
siAwvIaWmRvVgcRspXTLLH7qbVBqtkfwtgniMJwFptPASlMD2LXP0KLHtnt1B02z8hRQCrsyOcUZ
XHSoqbRqfTVXTREvloGbyp17CDy8q6oiWoTTMBxzLtSntdoX88jH3fkUdAWT2WISj6m+pcc+M1o1
khRJOqKPQdczTZLJn4inI/oFcRQn2DXmzswnvk5w3yB3G8E3eiB8u9toIsqcxpRI1qJTx2/H789f
nr8efx5/HX+QiAbnUt/IMgR7UPyTGSxk/2Bgy4RESnVXM7mFlemAW1wkx+bdwMk8Xf95MfVjI7p7
0aBdLHPnq8fwm/hXe6iqSnBYK/65BWn9MmpomMWHYGrRGUp0Bu0joKb6TdlfiGXGarC8vnZQd+EK
L/4exXJCjcCDaGdh3EKbztnMskOl2//BjFcnh5zO58kkCfHBP6FfSRKHizR2trEMDpZwrIjS2Wzh
KjiWpGEaRd5XP4qr7LSxr0FdPRZxQCg06oFvm2Vs92AGZU4UgzRejH6XxqfAG4Eerpllp627+HsM
nf5vVfwGAAD//wMAUEsDBBQABgAIAAAAIQCqJg6+vAAAACEBAAAdAAAAZHJzL19yZWxzL3BpY3R1
cmV4bWwueG1sLnJlbHOEj0FqwzAQRfeF3EHMPpadRSjFsjeh4G1IDjBIY1nEGglJLfXtI8gmgUCX
8z//PaYf//wqfillF1hB17QgiHUwjq2C6+V7/wkiF2SDa2BSsFGGcdh99GdasdRRXlzMolI4K1hK
iV9SZr2Qx9yESFybOSSPpZ7Jyoj6hpbkoW2PMj0zYHhhiskoSJPpQFy2WM3/s8M8O02noH88cXmj
kM5XdwVislQUeDIOH2HXRLYgh16+PDbcAQAA//8DAFBLAwQUAAYACAAAACEA9fJ0yRUBAACLAQAA
DwAAAGRycy9kb3ducmV2LnhtbFyQW0sDMRCF3wX/QxjBN5tsde3FZsuiCFqk0CroY9idveAmWZK0
u/bXO7WVio9zJt/JmTOb97phW3S+tkZCNBDA0GQ2r00p4e318WoMzAdlctVYgxK+0MM8OT+bqWlu
O7PC7TqUjEyMnyoJVQjtlHOfVaiVH9gWDe0K67QKNLqS5051ZK4bPhTilmtVG/qhUi3eV5h9rjda
wvsujTt8UBuv2/T5ZXkjPha9kPLyok/vgAXsw+nxkX7KJQxhfwqdAQnl65vUZJV1rFihr3cU/qAX
zmrmbCdhFAHLbEPgBPbKsig8BgmxGAsqgla/SjQRMUl87xvskY6P9DV19IeOhiPxDz8YEs1PsX6G
U4fJNwAAAP//AwBQSwMECgAAAAAAAAAhAGzrAIqaYgEAmmIBABQAAABkcnMvbWVkaWEvaW1hZ2Ux
LnBuZ4lQTkcNChoKAAAADUlIRFIAAAI2AAAA9QgCAAABuzAYxwAAAAFzUkdCAK7OHOkAAAAEZ0FN
QQAAsY8L/GEFAAAACXBIWXMAAA7EAAAOxAGVKw4bAAD/pUlEQVR4Xuz9BXdVS9s2iH5/5pzRo0ef
7h6nT3/dPd73+d4tbCTu7gkJkJBAgmtwd3d3dwse3AMJMUJcl681XepcNWtmsaIECGzYTy4mKzVr
ltxVdevU/0YMaJrm/f0HwBxV/9B13Ux9MwalKdaIb1NI++4OaFT94O9d3r56/9ZR/QB8xcT9AqPq
ic+O89OofPly4PgBHGiXtYHQNphyBXzddAwEikT+Sl/m7HvefLuWJMm7a45qgFOOYt97cbyUiTqp
ayO80jdtOnnZINvc9Cgr031UA4Ri/u0FONTPUV/cvnc/MCxKIXpwRExi5ngz1wcgUSS6TSGiSmQi
+2euU1UVY1BRp5Nu9DVz9enb99t8l8uLz4zKPyQiLjkdCV1T8RuZNLq6thHpBSvWyzp5+q7aKEWw
9h5ZPHryLDrbsnOvrKtpoyfzihYYlShqOq+oIwLDRgSEsMKbt+5++LRUJmRUYLSCVrpC02WikVpR
VXVV1WSnZDCISkrq2jEAc00k8ryEX7HzgndILN+Lz4zqwtWiEf7RY3MnW11cq8Xxu1+oR1JUWQpP
SMEAlq7YzIpNnVWIXUyBoBJFUS5evurgpKDwaL+gKFFTRI2sWr85b9I0VtgjiB5JA+2Xb9yua25j
mZ3QFJc+ofAUUaVmDrNHp5IQYeKqF3S5KNMpRFYdRHhS0oqR9LpQQHe56lbu+Nlzkir9OSKwocMl
KtrwwCjQrSryiOBwQSfhMcmySssrmj48KDIoOFJS0YCO3v4aMcrDCw6XZ/nK9ZKixCalcaIpzbKi
ojON6CiL2t4eQQKvO9fsei4T5f5rTiV2TZNVldt0yi6otC6WT1dsb9oUl9KkGf3SznzA2gF6WSvf
1WSiItEMZGsQXZYDjpQETqJT5wWmjjeTXRsB6lssZqoPoLyuoIpF1PnFhYcwaIaXFc2EM1tTBcep
yxKRUFpgRzESdqhbd31yYLdyDI8ePWIJWZZdLhcabWtr27t3LxJHjhzxeDycRxk/3lQAq1evxu/d
u3cfPH2JRGhUXFnVx5KSEllTSiuaOFkrL6tZv359csYYcNf/GBmraKCXK5i5B3yG3oHMmVc1nq4A
0os3X6lT3JKuIk1b7wPsaK+j0hPTMp++eReTloGdydPnRcWlc7Ian5LhHxJeV1eHuifOXaZDsrna
bS67R0RaUDToNPBnTEwMdlFx/a5DR0+dSx+Tg7rIiYpP3rBlP6pIunbn/mtR1Zs77KhSUVMfk7kq
KDIL6567/LBBtmYjpElRwRBIcwqpsnFOCGKnIDHSAbbrC3ao11FJr0vL9x858eRlCSq+KPtw4uxF
KIkWi33S9Nk3i+6j7tLVGxITE9FEXFJaSESMqKswgsVPnoIhHZwQn5RitmSAkcD681LGwKt6i0g4
cLIuTll2AEc1ok1ctE/RnRgdSq7bfV/RtA6RVqQHjRZYwhfdcsxRdRs0CkGVsTRLeKuxkshcunQp
tDnL9BbGUYDpKwC5AlUNxOFw4JfVZUCDp+9WOnTd7uEVnUyct6n0Az88ZYOiq4QnVkEViGvm6luK
6gYPeiv6tgB02/WiT7nqHwHBIYULl0qQAKNpt0jVN1ZywdKVksh3uKXQqFiXoMyatyhn4mSYpvCY
BJT0ndGw1G1WhSpPKKApi0+On3MW7dDZ1N1OYrfIqlskULMNtu7rMBB85aiCQsOtLp6X6ZqgVwcn
tttdASERECTsPn369ODBg5Kmj/APh9VCzsyZM1lFwEOUVqKWt3GYFLBZ/oyVWXOPdpLuaOxQi0sd
bhmyqNRZwX1fNiSUB75yVIKkvK/+aO50cgL4jREBWwTnBmnBMMHQFtAizDzsPnBHVfhp68/Ah8C2
dPMJtkQAvIh9Nz5mTNtjmA/1/JMWlg/QPnrDZziwn5rd8OfIAI8o1za1gbWYOEXHJ/M8b4PzYXUs
XLYK3IgBgBvXbNwqE318/pRFy1aBG4f7hQQnLlUVdcO2ItqdKs5dfdyg2YCqz9x8IXnqHl3jNeLa
eeoh644Bx83UwEBHhRH3NWgG71G0DYqhD0MiYwVVlw1NEBoeKcpqUHQcSF+zdbug6FaPuGXPYfgZ
cA2CwuNHhkRaZN0/PN8ic07oakWXFb2kxoG/tE3oOo1UN7oLV57DcsN9mDXvBHIGAl/KvQngCzgQ
3WNpwFHwSJEARbBRyIdOx3gIPEDswNEmYC242Ka5lIjy3sNDrU1b+wbyT9shGlWItLQxJNg6op28
2aThLyFZOZvRxDfiK+Wqf3inDSo+Pe8A/AP/9EXMbHl0InY66mzYHOHaFQ5pIG/W6U6X7JvwXUbF
ACpPXKyApfXlDRWq3AfwvrH4bEgzV5/Ar3ngS9Cz1tePqn8K4Cgyb57t9gqrzl2tMwd55ko5S/hO
wVfDHNVn20KBgfcn65yLdAmcWF3fFiB6mbMuM61gk2mc+43w7cIc1UCW/uvYo1egqagZVzTEtN8H
XThwEOn+e/FdtMXAefU74buM6m+BL6P9qqPqX1i6jOqHcc737ugX5kAsV18r9guMqn9m6xX/HG3h
i//mnYmvmJKvqPKlgAQGhi4ydwaMz6zVwMX6OygAnZPJzDUndd1Dd3q0jxzfTN/0T8eB3vUvbeU2
772p6U626wujjG4l5Pfg9UQzQ09vReDLRtWPCyrpA4qM4MsHhUcvXLoG0XRSVq6Z2w06KXpWmjdz
ExrkePK4vAN59LS7eT4OgyB2jybpto4el1QY+hvVX35BIwNDEflpqqLIUnl5ua5p4/OnvCmvQn8F
sxd54HgDujosJE6mZ2DIvsPHj548+/zN23WbtqEOpjEsKm3ekgWhwUGSQQBmNDQyRVC1+OSxfqOC
aJYPjPlW86ZuzC08jbZz5t3XVRvydU39fwftNdaDLsi9Zx30rHznLvv1RX+j8gsOf1NaqxB9+54D
GJ5HkjGqgNDIl6WVGMCBY8fMcrq6Yc+xorsPtu7aFxoVB/6eOquwvqVD0GRRk0cEhDx6/hpzOmHy
dJQFBVYXFxgaExYdb3N/utoAUOJUsWBVET2Vq0kPbzeyfJ3Th4050km6smDN0b0X32K3M6cX9Duq
oEhV1wRVffiq9G1VvYcXJFkG5zS1tr3/2LB9z77XJe+MgnpYTKJ/eNyCRSsio2MxqvDI6ODQcI3o
/oHh+ZOnPnv5KmPsOHpCE0QpSlVNLQJKbCDKV8RVSf499ZKmaIouPn/brqk6Amdd1TPmnWdnp7Fi
j2uFNdteSroZX9NMA6wFL/obFWNaQ1pAIZ0YQZNEI8QVeQ/yBS+jA5Qbu5xG8c4lRiJR9uwO38lG
OmbMXnQG6bQTF10IHJXIxReywzgKrN5xk6gC4c2K7LdXfLJXn8WVK1fMlIHHjx9zHAepc7lcLOfj
x4/xsaksDbCWr926VzBtFhLgt5jEFIfDAQF7W16Vk50fHh7u4CSwYmJi4qotF+k06YRT6HUQwOIk
V16Xi0REGr1MXvXGOHFqjocl+kIvayUJmltUdh46KggCJ4vN7fak9KzohORRQaG7DxxGgaJ79CpW
VHxydV2jk5fGjRuH4Tk4gZ7ulM3+8J+T1VXrNyVnjAmLjkMOVj5/2pSmdhsna5JGHj579b661im4
d+w+Pjx6vCRJNjCBoQJkXS+YdnnVllt0R9MSJix5UNFBZc1nSABL47cnehmVwMsLlqyAVjhw4hx4
OSsn7/GLN2C2lIw8gycV0eBMcHNkXJKDEwPDQ1euX4uc2w8eXCm6003Zolvfnr100ISkJk7aefLy
W53IaFNXqIS0OnSXrGZN2UoLCEppG7l976OumGcXe0XPQ3RU3aQNJoXlsNLdjgLIwaENW81L3QwQ
HmoDNHoICezyxrlZLDiO9mxKFT0HTr57UdGgqTJPYATU1cfKX32waZpt1boi1H9r4SRZ/fjBTkS3
l25vCyzRkzaG/rRFPxA14hT5O3fuIL18zYbg8GhIhUtQUjLGzF6wFDx27vK1wLAoi5ODrk9Izeim
KzBSSSbVHRgOTKvNI5J2F10N4PlzIhMpbNxuFKuCqyRTMWO1Bo6vGRVmqNXqhGwYV1PJstXrgiNi
eEWrbWr71x/DW1tbsVCRsYkjAyLdolpYWIjyXso0Xc4rPCspmo0aPw3u0P8atkZUTQ3h5LFynjsv
2zBWSBLLZBX7Qc8yX7lWze027+qzRvELp4nlwP5iF+sDGYNA4hdri1mA4r/+hheJ7DLuDRHhDhkV
GeAUeaSONg/NAdeyTIC12Q0sv6+jdFR9HeuJjNwpDa2Wm/cegq9YDkh///69KIr+weE79x0C9WlZ
2VDWMGsZY/L9QyKwhtEJKfD92iVy5/5jjgogtJnqoRr7E0SiWhy6oGExBU4wh8S66In+jwL/rS+B
6xXjJs6A3tu4bRfUAHUO6GUnepsEeoCpgYDhd+feo5NmL+BEaXzepNHj8mEbhoWF+wWli4pQMG+z
RF1tqUOAz0LdBbRACVCJRSL0JhENvoS55v3QjSq0Vh/aAplfxoGUqYxfWE+vAuAVymbQ9xxYTaPu
gfdeE5qWyX9kl0DJbdlXpetuZG7aeZ2on9QaEqt3gzjqxUJ52klDP+MZIL5SrvqBL7kuVWlXiIuo
DsG8NWTFhjOGb9UJhSQWHIVnhENgY063m/nfBuoxAeZevxhgMS9uP2uErkMCXMZyukMh0TO2eIxV
MswdyzU7+tLufPHFazXAzpIzNxta8BO6VUTMNjL9pJnWNLdxZ9K3jMQXAxoVE8oBdonCEyaeMXd8
0FWy1e0nnvGG2wGUNtFbgroW+Az6L/wFPvsgosFJODP5XfD5tfpbhv1Z9E/VYOpA1tPPMAt0VD/n
anwL+lurLxrtzzk1Pw9VA6GElemr5GCK1c+Pn2Tlvo6MX3WpfpJJ/5H4exyLIXwF/r0U4PeCgPiR
3u/4XfGtSzUoQvlrSjbi+xZRJxeuPfszbuOni4UDGw7K9BVHyTI9Odfz6M8oVb/CyikyTxdp2sqj
iPXpvYD0uvSXAYvR60iVzkc4uuHfQgGqKn0qh+fNS5NOp9Nut5eUlDQ2mldnBw5NVfZcfCDr6rJ1
FwUFXOUhYpfnXgcAWeTIhu3vLBo/dvI6XdHRxEC4c6BL1bOttrY2Jqq9cgFI37B1p2RUevLkCX6N
E9AaqiDdV6329va4pDFhUSnpWRO6Xf/qCcaV7BfNYj2QBszDncDRgKCQxjYLj0kR6SMeoVFxkkYf
EEIbZqHPAY1Acg5dfGyRtfEzV2IMusbj//MKR+LUA7wgWzTzYiFRqdX6LWKhQDjduDLCqgOCKlW1
eh5XWnRRevzeVtciK2hkwPi0VKzRftCtwF9+QU3tNsx3QGhkR0cHpj49p+CvoMh9x8/hqCiKfwVG
SpL427CRGWPHq7L0x6igiIS0kYGhokZGhMb6RSSEJppXHgBdVWYWLjxyoUjSNLckcLImqrrdI4RE
xqK8W5TbrFxYdAL4eOuufRMnT+c5bkRINKYMKxoYmigo+pZd2+YtXYl2lq1ev233frNdAxrROUUa
5jcyfUw2yv85IqTDztNz+J9bKQwZlLsEkjF15/ErHxo6BJVOgioqZFT8Aln10La9M6PJy4+++99j
Tiik+wK4PXzevN0iIS5Zy8hfJftMJOoyeNMsvye6SFU/5XoiKDz6j+F+o4JCl6xcDWYNjUr81x/D
/6//+C8sYVnlBycnYSqxVG5RHREQomtqbXPHqNAoLK2s69v37J04uRCN+AiBVtfU5h+dImrgRbJk
xZqciZM9khablMaoqm9qAU/wsnrs9PmYxFQIx6jAqCcv3n6ob/ljhL+Tl4ruPkhIHb37wBG/4PA7
D554xwJpmzhpSnBEjF9Q2IEjp1ERbQ73D/IPiWBPhPUURAaQ4dDUpMKz4xaeEjQi6dQe1bSIx09W
aroEE4W6DByR1u5/kr/yGaQEu6jL8jFqjPfC7YoOSR+ff81Fb7D4NMPo11uY/fru9sRnlqqvakBE
TMaJs2dZAQzjXcUHrNJf/iGBETGYHWikkQFh76vqBJU+6I1igiBk504MjozzSHQAS1aujU/JQJq1
xgDqnYLy8PkbSZJV42K9r57ELstB+ywHCeQw1VdZWTkmJ1+kNzN0N9c9c7rtdgPEGOpuZOL+Ko7X
OKMvjezc88SpmUYFv4BOn11S35V71h69oRvX9gHzqIJ1dd145fgfo69bHUhLzO8wivRCSbfMXvHz
uhUG9ZimLmvZ68DAKC5BotcB2a6mn710DZltNufChQuRYzyFSGISxmzesQfq1Nss2qn82GBz8+x2
JTATtO6jGn7MtFPtznbwA2y+qClzlu/hjQLe3hVdAZNNW11Tz0n0yTMfQPk1CuRf0adaFRnVzdxO
0FZ6Qz+HvKBLNYByuizqDg4mRK2oqoWKh6IHq+IA9AlmAb9NHa2YHaa+kBg9Lhdpl6CA2VFMEvi7
j55funEHNiwzM3O4XxCYbM3GrTA80Gn1Le2hoaHGWLSd+w6hhcCwSEHV2+3uOQuWwGEzaDBR39iU
mjMJXaBAREzC69KK2MRUTFxccjoTwczsPOxGxKRfLroOmq/fvi/pGoxfWmZ+fHK6W5DOXboGFyMw
LGq4f7DNKbVZnTv27xSIPDIoa1RImsfjoc9hY/lFdfzCXbIhLkbPGLHC6+217dqlO80nb773Sjyl
W1UrmpRJy88+qfRwxpVUVoEluv2yRM+jLNEXBmirJM4jvCgpnTF3wZmLV4+eOgct/6ykAt2BxoDI
BKuLO3ziDHgTa9NisWMej526anXxMFT37jyiS0k0TtG27TmI4d24cQMLBq11/MyF+uZ2LExUfLJH
FiVNjUqgt0mCuy9ff7Bx6wHReDoSngUaHJ83kd0uBEycOJGpPrS2dOnSey/K4jLGb9hF7xUDDh06
NGzYMCRu3boVGBjIMgGOo08WIoG1R8XKDx0S+tbIyVulm242bjp48+LtG9QfwEIJ2phF+9hKUM4j
Hk0gJ27VnL9VD9cgOXsj2gGd9PyEJjRzZMrcs6LO3SvrcHeddwbs+oLlsEMAy+wLvgUG6laA7tra
WgQQZ86cOXz4cHZ2dkFBAfJBcVpaWlVVFRKzZs3yWhGr1cpuAOrZJnIwB947Z/qDqah6GZgEiugd
D3Qzs3rAt7wvBF3RJKFWEuLHHm8RYPYfe1QXbUZTYKOmr9zXjnVVZOK2trvJqDEH39PbmTRBU/Pn
nZHRLRSCRJyKc+TohR26IujkRaPAdz6OafZhwCs3g4JPSzWIjfYKLHZ1dTUW++PHj1t27eBVw852
Ar3n5eVhgR89ehQcHAwJQOao0OANO+iNgYjM4KcgMzk5md1COm3W0pj4MbxEVq7ZwV7AAISFhXl5
pa/hoMD8zWfqncQjkjfvHDIRENZi6hEG5S65qkrc8xr33QcNmstDtA6sH+IpkWjnb9+z6W76Yh5N
3X3+bUmtgz78TUh5B/emFYvvoA8ODGwOvWW+dCF/nFsBskLCIlat3+xGVAIzp5OKCqhQCiqR8xbC
pCM4xe+23fsQPqMY/PLIuCRdU968r26yuiCLsE+I0prarVCezHfAgiMge1tenZaVPTIwBNaUtdkT
YJQzD+oRodkFqQ2aTkf8Tt/vxCl61Jj9wUvfBI25iKjVY9yFBGoBrGvZB4+FVzWZNNnkzMLrTqgD
VQPf1XXwTU6dqG5b5+yzX190Wwza4pesTTd8wVJ9SzcAqgeHhsMBmzF3PkKfV+/Kb9++zewBgCmG
l8EWo+hOMX5h+eua23jjDiFYKVSHRNK0Sh0ZQZFlei8adWHoBHTCaMyXVB6CkzB+t1OFxhVO3Xjn
4iFCKlV3Kr2DfML8naIkdeg6MrWuvriVkDtPLC6l4/LzdgFhrUY7RkhsJ+TZe4du3JeJxWDljb7M
iiz9WQxQqrxl+lyqXltBJiNugGDqCFVYLbQIl6SlA5PQBVBukDCXy4XEGwOY1627d+7evRs9Qh8e
P34cdVNH50bEpLCmlq1ZhQWLiE2MNl7ZhQR8TiQQgD989op65LoaEp31og4EOHhJnzl/pwdeKxUI
jEIVOTJ7zXEEO2ifAW2aKRq6kglL9kK+kwu2uxHsKnAD4fTALpGdp++zSr7l6RgGgH5Keg/1U2ag
bsVn0WvdkLg0zBrUGlxk6LSU0WPhFmJCsSSwK6wMdsOi41kagIa0Ojk40E9elty6/8jJi9CEcDgh
dvEp6ckZWWjQIykJKeNQES4inPXquib6pEq7bd/h43DY7jx9N3HKhvC4zIDo1CPn7tfWQ8nRJyFA
oaLpUFp7jt2lT2EYQCZbewZeF05eq3CKel7hNoHY6cJSGSaySE5dfip0Ph7yLRPlBfpFOwNvii7V
F1UYODDEGbOXrN6wZdL02XVNrUtWrn3w9CXipPiUDPj03luPQTFECosREBqJACsgPGpkUBgWA+uX
PZG+mS9v0vQdew5B7z19U7b7oPkkFFQi1rup3Y6ACd6XKDlEqzMoc3MHnXbl4kvHvFXXPLJ5jpie
SoCtahZKKtyK6mTjBVDUTFGnlLOJ7rvVYtiYVVTRdR6VdffCJVd4wvwGE+zol8LbJtvtC70WQOaP
cyu+ByAPHl2cvOhhWasNPhzYNG3WBgd9AyRVUrSEKMLvyJq+2klDKGrqus0X/AVFU+AqBGff5xX6
ajaWD4i6K2PsJkGVwBMo763SE/0cGkT8qkul6ZxD1MIzNhv3mmOqybaN5506B5/BC7ghuy8/Qdzj
veWcgc07snVUk9yLd96oc3dZQl1R8+buYeme6Cv/29F/y4O5VOjJlyu/E2Rdyl10VqVWo8sC+MI7
5l4HT0nUSLvL8V+xm7wuKCsp6cr2Iy9YDvBjRjRAfC+p6nWOvgkSVsbil3p9gHPXBwEQOo+u6knL
Xi1edh0xgpkNaNamRsnuVuG0071Bp/+r4EtGl6X6RvpQnc0jEt/YVDegtbJGvcYCF8x8qrVXeDvt
s3ddu3PjfdbqFy6Nw8rTk7Kd0BFvaa2wcSKNKb4jPjszfRX4AqnqtYmBZ/4MuH7vjcie0OtbMn8q
4n2J+VXdin9D0KViS/fTikJP/EKkDiIGU6owgz+Pv/TPQ+9uxb8n2/4YfN3c9ilSX9rcz7m0PwlV
IGMgyoZR2yvNfa7Tlyqxn1Pp+VL1N1KIrgdCCcvv9ehgmqifHD+PxH8FJX/nOoFxfp65+8HAwL9o
7L+kPGGEfamOfyr+jfTed8KXSsbXYWidfg0MrdOPwLcr6qF1+lZQnWc8vmXufx/QZ+i79fEDtC1A
lboBc//XBKZOkkjerC2zZ882s74Z3uuZvvgmecIsDwof/YKrhVFLRNLb3OofWTe37LtnZhsYyHD6
KcPuzDF3OvFN60SlfTDWybeRQWnwu0Onb8K7+qLtSaO25cQD407gTxjIEOjE9VFMNR4FM3c68dPZ
p19DtnSyY/OVZp7sPnyNvqu3z7s3+kNfI+11/bqs0yDOEWsKXf4a8943utKvKzr9MNqUBZd5nUwr
3Cqr9L5r82C/6DIVOrESsuVY2c4LZb6ffUIBVuZTyU78u/h7oijKxvP6AHu/QXV1tcfT5d7Kz0Pl
XRpZefwu7yRz9tykD3Rh3Qjh+S949h2LdO1iSWmdu0MR2q2qptEHufrSgV58r3UCk2AbEKf5QIb7
pGvsGwdfWrcfRMQk+AeH+wfFodm6prbAsMiw6Pi49HGHz1w2S/QLyt0a9b1beXLqxuNWD9l/4rGo
gEiJl8nGw3d/j9xmPvxDZ1tSiQV73WXCWAiPqsxfd8HKqxhp/sy9Zq0BYKDr1K1XsCd7SUavTiRw
406x1cVLilZTU9PtiQHs9pRrAPw+fsKMmITM+OQxZZWNfT9KQ8FaQzuscfYLYnqOvL6+PiQyFlSK
4H5C2u3ucbn5xtMiIOYzXMyAXmRddKvk1PWXb2qbr95vUnUB7h4aCcxYble16qpO7UcfiBf2nKvP
X7gRnbFM/FLoikvRxk5fS2/HVpSk7OX0S7kDxlfKk90jjgwMDQyLCgqPunrzrrkS9JVBtG8s4Yjw
BDcvaUT3D4mgb+YgJCg2VVR1XtYw4GFBURzRwVPeicKkB8Uk33lYDHlyCaLxen99576D7yo+LFi6
0iNpHpmfu2B1fEpGUzt0uzZt9rzN+09g3qfOmnv4+AWPLAwLjg4IDB4VFLZ8zQZfLkDLfw4PDYtK
BTVQfIJC/hwRGh6dOm5s3mcfgaTTi5nXdKeHPK90Fyx54sIQFZkovFXV0ybu01TziR0nlkjQJN39
v8dfWHP0HeMhdogOEV3LfHzWJU1ts4v6hOXnMEXsKINv+V7xaZ36KcTgWwDrlJUzAdNkcXj+8gtC
N42NjaPCYjPzprjdbixbcHyG3cW1W6x/jQos/1B3+/Ztv8jEP0cG7Np3aOXm3X8GRoyKDuNk9i1k
CohCVHIWNB6nSOx92JjTlNFjC6bNQl+Y4oiYVDvnkXX69IciCYtXrFm78xCK3bh9f878FbwiDw+N
U8C0hIB7uo1E0JTnb179/teoNpurttGWMSYfxaj97uy9L2DI4KWqNqXCoiZlrUQVVePgke89/bLo
UQcVns5pUXn6iMj/FnfmtUUk9EkqH+iSG1Zt13WU33PmuQOVjFt8AdRlwAR6d9mhbvhKeXKL6v/4
c0RVTfvKtTtGBcZgTn//K8QlyIuWr4aEYTckYXR1bQPkKbdgyq37j7ByfhGJmE1MDvTBX37BZkOd
AH1+EQk4Kun0MXe/4HBolYwxOdV1DWwMIwNC2GteouKTG9qbrt++n5yWJ+nK7Qd3ZxYuFBRt2KhA
+nodSUVd9uEXL3hFFVQ9JDx5845dLkGhD8e7hcZWS30L/dBQrzDmS4B6e/TS8qCWHL/2AB4DLKdH
c4+feVHmICGdz+6jK00SpZb/NeFJrcWOkXrnWqfPl2pO3bnnWImFOOOy1jmM1/EwsDIDRJd1Gnhl
MPh/DRs53D8wNCoG3H/t1q2gsMTf/ho1IiAEG6Y7PHlsVU2dJCsz5i64UnQHLY8Ii3OJsLvwFBT/
4EizoU5geCFxaVChWGMIDeYaei8oLOryjduoC3MIG1Pb1Ir1CI2Kg4Kprmv0C4qzOPkJk2cgBysR
FB4NPZkzcXJgaKTo8zEQJNAaqAoIjgdhoka/LQ31GB4dP2b8xH6GDL7/6FRWXmh/X+/GIoGBHJqn
YNIFt2F46APfBnjVhRUbEfXBBu3V2S87hJWyi2Td7ovv6mxrt7yDwKk+Xyz1lsSvr95jv93Q3zox
Ru4VHkn0C4oWVUlF+0R/UfJuuF9Yk9XdYudllX6VIDp53P2HLxRdn7NgyaPnr6HWRgaF1bS0r96y
HSwVEBp1r/jhnXvFknF7N4CubW7uD7/QuLSxYRFxAUERsPShEVHnLl5mZHgEMSIqZqR/YOGCRYqu
yqoel5AWHhl/7sK1JctXcqI0IiB80/adm7ZsQ+9Uj/gQz4tyU0sb+1ACNlAFBwdTj5yeY0QGtVu6
VuPWYyfeEkRdV3loSV4j4dGLZZ0DI2lYDl3EBpPsUMiItNNOxUq/xoR/BuDf0FBLdYxZfHF0/jFO
0WXNBgsGJgU6O+oFLJ8V8EV/69TrwjKIujwqIB4FWKPU8wmLHOYXHBQRW1tbi8zChcvDohMgdlt2
7n347BVKFhUVBUfGRSWkgqk/1DenZo57+rKEfdbeC17Vy6rr2lrbUd4LdghtIs2eUmK7AKSQnQ0T
BGGEf5RbpNOAXfyyWgzIQUlzp7Mpc6cH6HewidRid/2ZvlTxGGdFEdvq+ur19KFjVhG/FLrE0UU6
bDfEiGYY/RopyaXwv6Ve3nLio8dYUd8XMtCUAZbGbzeCe+Lz9qnXUUHoBWioTmDqjT0UM2cEBYwN
DES/jQFginVqu+kzSlA+8LuQMI74wnjOVkfYCEvb/aCXjF4H9rGBvv5s4Og5KOTQllXl+q2KTSea
NVkQ6DPYiqODXHncwZEmerRzJfCLZGT2BvpISOd3VbxHER/8Z9A++El0ENB39PE688UkDCztmwN0
2/XFV/oR3xugmD5c5n0xiIFexwZg1b0vLkD63OXrYGDwx+z5i9mbesAuianjElJHGx/H+9QsmkrL
ymZvRaLsqLgdhETn7HtT1wBHhDKUIlW2c03WT+/FYgmobo+ihYxboyF88IEu0a+FTdt8Lm/FLcnd
9YM5BmhPPYB8tsD9oJfrT72Clemrp24oLS2tqKhAYdj/bo3bbDbkt7e3Q+xev36NdENDQ3FxMSuG
TI/Hg9+JEydC26QkjU5OHI1GWF0GRB4vXrxglCxfvhxH4alPnzOPHW23OTocHsR2S1auRaQVFhYG
ZSURNSQiEdMOX3HihKl+o0Im5k3FIYg4fL/YpDS0uWjRoqyJs/0jc50CDYJlqiC0D61CiwW2rPuM
PyppCx1/DetrvPeNTguARF2bLTHvdO6KRx5dkqksmvkABsjS3pzPwrfkQNaJRqPTpy4CRXmTpsHG
qrCQBqDZ4JshH15ceQ39yiW4mPEyfGX8JqWYL99DFyPDE5DOzc0dPS4X7jsK1NQ35RZMPXT8dFOb
FVEXjh48dgrVL12/BfcaTjb8NBz1JRcaZeX6LUdPnfNI8uvSipjEVI+ofmxsbWjtaGujn72HMCEo
ziuYmTluAvQse7MB5CkgJDYgNBKeZ3AEdVD3HDyKxKt35SP8IwVNPnLu0q4jx0eFZ2/auttwpkGw
8MEqNtpMg0QHjHkmDQ5ZLJhzeuXe5+jX5C2UUFWH5sqfd63cqi1edw0BFKvl++u7Tt5Et6Ms3SsG
ovfoOsXHZFbXNfmHREDZJmSOmTJlCg4IqnT8zIWs7KkuXgmPToECwbzDPx4VFIqSmA7/oBDWN4Rm
VEQiBAUEVdTUIXQNjaLncsJjEjBeBLPgehyNiEWMpY3wD16/ZfvhE2cQJDV32OkLJj4NgD77Pnfh
UnjtEI6ouKSL124eOXm23e6EdOJwVW0DVF96Vt6Slet4VYKscDKCXx3kIR8yQsNko9+yqo9Fd4tD
o6McHu0/ArL8YmfvP3E/LjEdQo2FetngrqHfmaPrRHtXZbdOnj5VX9W6Dj8ogm+hEGpvjEOk1spP
mtzWoJfOX/kQHgdm3TzEKPYB2/Xms0Rf8C3Qn7/XCYxLj4yIR5jyl18Q5jErO+/PoBhIPUwupqxg
7jLw/pSZc+gsRMVBybh4GnVCntIyxqn0TBpVXOGJmWif47iQkBAsCWQFsgibgej1+et3sABuUXjz
5g0WMjGFvl907PjJWE50h5XABoFj1KDuyJEjUQxLu27dOuiu3/zD/zUyFDk4isxhw4adPn0au5GR
kSUlJawWVDF6x9GHDx+yYoJEOtotoDkgd93+ix9qbWKtw0FPcSnS6ybS0umyw5XAQF0eMjJ1ucS5
rj5tk4zPzIEMhLeI5woWXv7QiMCfFKw8AtXE5hC/DLTvrmCZfR31hW+BgcgTXaeM9Gz8gepobLNC
OyWNzQf5iizeuFO8cO12QaXvF3Vw9IWwErRNfNqOvQcRiiYkZkCtsnVKzZmMscGcPHr0CBQ8ePoS
mqe5wwbVFBmXtG7zppLyMkwfSvKKMmvePKxcm811u/gx1qy5w9rt3X2+wGi8A+o2eDoZPXLYAkBl
N9r5A1eqV2+/ZqfygdjHjWW7+bKWF7FI9FOuKOmWtAXrz1ZaELAJLxuJ06PJiPKNdfJwJKfwlCzB
kSWzt15CRIZc2p8Bb3fsl3XaF3DUqNSFVF8MRJ4otm/fjrYuXLiAqQwODmZ6DznQNuPHj0f6ypUr
FouFvUvx6dOn8BRYgW5tUkZWFZH6cp8D6qGUUdAYwqd2JHoqFBacuf590tytawZ6pkixO2Wy/fiH
I0Vvrj2u5ujblByUG6X6Z3V8iyLY0LhGry49qCZjl591I6USi6A0NlsVSSQ8kVTHm5r2Y/df8kQX
BHXGumPoizGZL3ol4OvwaZ0wp4C50wPo0nvUm8Yv2IpW64SXMqTZL0sAVFEYdV+XvSurpsvZExgq
KwaANVEV5aHZkI92cAi7gKgSWDXMCjbB+EyyUf7T2rN+AXPfB7rQwWkkcMxqp06OX37F07MP9PQt
Qp1ym2R1Ky6PEDF6ZebiaxHZG+2CImtuXRbdqvzoKZw4aAa7XRUypx+tFeCBCy6nMGf9cXoCpnMq
zG4M9E5AZ2avR/vCp3XC+M3U94HD4QBl8IC37t5ZWmW++82L/fv3FxQUQCvGxMSMHTsWY563eGFQ
VIRbkbBIOTk5qOvv7z99+nSUSc8quHX3RWVNU2RMxoMn1PCgBUh8a2sraw2F+xpOu0uevO6K7NE3
rn/pVBvoOW+oLc1d61Fqm0VRtkeN3SxyRHe0Ec0FRkBgBrdix2GrQBrARy6VxE8659RcGkdgMBfv
ekp9QePFg+hxIFPvJeyLJnwg9mlwcO7cOXAg4sr6thbB0HvXr5uvh4DETJwy40N985OXJRhx9oRJ
kBL4fmcvXYWXsXHjRv/IJIgOcuB9eET52et3SLsEJTM7b9a8RahY09BidXH1LVTZAr2vk6aVVgsV
5fUOXl9z6IYTLYIMOHTE3dgiVLaRDy5y6U61qJhRLYOg6eu33hF0O0+0O1Vk14n3uuoA9QiYp628
CEFkxcwuvht+3DrBfXDx4tRZhZQBDYUGa+cdIdwKWAn4dWBuuM5IwyWBw418zFpgTCryMbFYmyUr
10TEJCAxY+4C5GBdsVRYtnmLl0+fM5+11hMQyugpJxrg0MnORdvPqRLWSIZmxZ+XNfL/lnw1ZW3Z
3cc1EnEy7WlMPqVt+dbrItwfiVuw4coHFYslu1FH0ics2C54PJL+6YyfUa8LfDO9a2/ufyF+3DrB
4xBVDfE/ZMXBUZcJ8uQ1KjGJKRCLFoujxWK///g5PPLg8OgXJaVrN23DCENDqdudnZ09afqs0soP
C5eu+3NEyJMX7zZt3x1uvDQOrUEoX7z49B4cL1QivKjzTF5+xIP4SSPz1x6CfYIyQxWsU02z5Xa5
+FvaQYvxIVoAHaEWEoouJWUukRUCly8sZSMWEL4FdfR0edqaY/LnvpX8RWCraO50guXgF0e/YJ2+
kSaEMpAnUdMRhCEWRmuIb7w+UnVtw95DxxCK1rd0fGxshvRAUDrsTuQAXg8CahAV4PrL9LJ999de
Gi2ZA2NpVNt7vvbNB7dH4TDjhetPcsalBSwUQqVql4S4x6NrHl2kVx6MFtiUAdtPPMXaQXoyCjY7
ZGhDHpbKQzzL9z4VePPtYijPflmCge1+FgMp6S3z4+QJgBKzuXv/6quXICSwEnQxjFNnXiB8RoiD
o94CtIyxTjxYXqQLjtUVVOLkEeuA9fV20R07bquTvk5eb5S0VxV2GEj0YwiS1N4hNcBx9JlfBhGr
qElX7zU6YbgIWbHrmVEFCw8x9MzceM3aaZO8YBQOEN7yX1Sx93Xqq4mvoAnwcjeEY1SQ+Z7Lbnj5
8iXP8+Xl5WVlZS3WDifvCY+KRMXi4uKoqCjoNLtb+a8/A9ra2iCCpy+ex6oUP32M1rbt3l9aWbNz
70GYq6qPdavWbbI4OayTwy0FRI2DtVdE9X2ttQO+G10AQi/uqaS5XrJBienm2W4QwBKAIoqIahsF
0sqRZVvO0fWBqMEl18jVCpuEFjXze+SsvEH+58HE1NzpCuR7p6ivMp/WifY54F4HAK22uSMyMSM1
cyyWJzwmAcweGZeUlJ6FKJjjOC9l8N+iE1LYR546HK59h491ONzGpXQ9Ljmd3qKkaFeL7pw4e7G2
qRVuSJvNGRSWBCN34dqlUUGhN+4Uw5iNCAhBAiuUOTorMmX8+DkHZi7eNG/uMklzbtj3FkuLoSmY
DVVus9YfPv3Y4qMtvSAC8SjWJkl7WFF//2bN4yoP0ehZZU33oOabco9XS9PCXdEzpxu86/TZkr2i
izx9XRMMPepqVjd39c4jTPeFq0Vw3l69ew//jQ0UHkR9fb2RJC9Kymj0YQCmIyAkAusaEEq/9gDH
D4eWr9nw6Pnryo/1L96Upo/J4WR10tSFHglTKEPpBUfEQKTcgjwyMASs4FBIUEROXsHK4aGhgVG5
SWnr6RviDPKwSKJOthx8CFtEFPMslC/ZEmmxSeRVJbd624VWBcaInVGUoUNflTkhSF58y0R5gUYG
3s5/wzozYIf9fh261YWCASteuHYHln/+4uWYzcbW9rKqj5gzSaEXRphrgJI2h7O2vlGlpp2cuHi5
vK4R0+0fHO4SpPjRWR5Ndwt6XFLmzAXLWx1CSloGqqelj4NwPH3xbOvOg+lZeZW1DVFpoxPSEqpb
5eEJM/Lz5jkVh1NVsqet9eiCTi/5Uc0iadKMlcfgRqgKRy+LGQQbR+j9FKDYpbg37n6ycOOFDgeG
4wFBhj6SG9slKw8vxMtOX7NORoddJtmb0w09M5FDrz8xmHmDBNhau0eYO3/F6HG5HXYX3HG44pCP
2fMXHzx2Km/SNKfTyZxy2J4xY8YkpI4+ee5SQHjUiKDQuOQ0xK3/9ddItyi32pypmdm8rGGSlq3d
BPWIWkxtgma7m17Tp3d5S9yDD+KkFackB5WCN81kz5GnWG9WDGsAScqZvBJehjFWE+jdTNE30/NL
Dr/NmHW0yabQk4a0Q3oUTnnph+6+D/LN1IBhUjKAuj0L0HUyk4MH1g1Garhen3wziBf9Nb6Gwrxt
LzD1iFuh6DSJyEYVQSGSrCNtTDF4GQl6RhuFfYeBfKg1D+H/I+vwB4dKOMVGqv8z+IFH5i1cuVcA
OIU8K7fRLxKKXW66Z03RX0Xbf67UP+m8CDdTssJxYAXAFjVWEGDx7fRvwQ/1ywcdCrHbBDJ65lZZ
olEor5P48Yswx5r3cRdFet8sNmN1jLVhmV7WpjkIpHT51hv7uMIHLMcLu0cvb7azwt7yPdHPoUHE
r7FO3eYCuxBBoKRKzCnczXtUOM4OUUubtpXJELU2RrGPLZxVwr551oPBd510wjWJJL3wDCfbkemF
2+2+9rj0hyzBgPBLypOuSrwqFS65WFpPz9MhR+LI4xq3RMznmbCEHR0dMIEWHOzxFR0Guly6ymkk
Ytx6pNgho4TCyfr+q7VeL9wXvms86Oin5U/3sQxi999vJAxtAknKWmPlIQ0CyzH1Wg/QM619QCbu
RkGZsvgg3BBGMPt1qtqN4gZFMoNZhm4jGtwBojW2BP1gMOVpcKnvHYrSISqnjhWLAvrq/WlA32H3
JMk4J4ED5E0jicwxnxQzi+nEJpN7j1v6iWf/LvwIvec72m8c+b2S1nPn62GedOo8frqn3hfooj/2
pN/vcr5t8fyef8n3vkDU8hD33MIbqs/3c//h6/RdhqeKZx5WsZu/Pos+rAhEh36TpsqlRa007rvW
OgMjRdY0MX/qEaoODZ/jZ1ghXxq6rNNntWT/YNX7mKNvAtpssSsPn7b3Q6H3EBIMbNcHoqpqNsId
vCDU6YqqGN+4YQeINGXhXgRXUH2sYm/VBwefbdlbwLdkl3X6xvll1fH7je30BBqMzNmkK108t27w
dopEH3PhUSSSutJe7hQ0hcbbAv2eO4XuNm6LUdvgdtDvGX9PfHZyei3Q5b7lzzYxEAxKIz3xWTbs
H6CKI60zVr5+XoWAiveeqvhV8AX2qecC+K6xF99pnb4dGqe+rbPRdx+ozOfrBb2O6O+C70z2vk5f
Otc/7dr8hPi6ufou/t4QBh0/6Tp10z9D8vpdzht9V/x7rtkneRri2Z8EvS7EYOq9oZX+fuhznTDp
Pee918whDOFnxg9m2q/RfN+VxO/a+D8MQ3PVDWxCBuVcBWuqZ/qz+EljqCF8I76ICf4d8MMm5N9X
oqDJftgsD+EnAVtxL8zcQcWQjfpxYKv481w/GcL3wJBEDeGLwRswdwYDTNcwmFm/LOg1DjM5hCEM
4ZtBny40k0MYwi8OMDODuf93YMjrG8I/B8xvHJKoIfzbQ1GIaqOfkBGMN9//yn4TlajPSjYOsTLm
/i8ONhYvzNwh/B1QjdcINzrUaRtq/zf/TbkLD7pcLkEwHx39SQD+7/XB4F7xd9ooxtA/iUJixJg7
Q/jeoI86kaI37alTNtXY9eGjt5+51+CCoVJ6l6XBWh3WzpeyHMSe48yX2H+27pDX93mwqTR3hvD1
UIlO6PtBRbJh1/mxc3e/d+rZszZv2HFakHVFxUSjyI+Y5y/1uWCgBi6EfUrUF3X5I+FL2JdOzRAG
Hf0sAXiQbvSbOjJRJYGXK1rUicsuPKiwdOj6loNXt++nL3SWtUFexD5Iwi7ocMA6ipoqacQj6g6Z
VzUnIZ6eDwGzFhhgowCW/qxoDdmofwi8K80UKtbe6/ojgV2WZsAuopeBxwZfCfAtwiSdtHDaoWuP
1u1+4dLdHPGs2HRn1Qb6onvj1QP0bc2gRza++A7GZVUHFQr6EXn6OpLiCi5nwc57lZbXTWTNnroL
D9o4uJmyRIyXmgGfFZjP4teTKHCBdxs0tdYHJPqiBEy2pBifYxE7E5QVfhqAD/AryMrpC1dyC6Zm
jB0fn5KRM2Hmq7eVxizplR/rk9KzJkyenpiWmZA6+r/8I0ZFJoclZR08e521MChgHGmsCtgS8yZ0
kEqnri5a/nz11ntWtcOikMlzjj151cbR79/ZFVgnEV6eWycVvEY2nX6aln/Y+NjqJ9B3fRh/6Idb
FUGHFGI8CLd0eId9yx61RlTHKLpovPnAbZFI7qo7MQWnVZFIsuZQxEUbT5y6YYMAf7sIdcMgS5Q5
rX0DA/COgenIbl9C7weoiJKcrDe324zXmpvvW4G65Y3XORt7lIZez8CiIzM1AKBN/Do58fSFqwuX
rp67YFlWzsT1W3bCSwHRnd/Q+SbQmTIsCVPMSOOXUc7SDILQ+U7IPoAWUCYwODQ2KW35mg1tNldz
h+3UuZuHjtG3KoMDnbwcFB4dGhUnavQV8OhMVOlv3yz5NaCDoQ6SRNsnBIIyY+HZd/U2G7F/sDhG
Zx9zygRrR8cKmjSUcdlV+6X77X4RhxZuu2LVyML1r049qGHtmI2isEY4zcITEc7ZzCXHt+w/K0HR
ISKjX3b+VJKlO6GIsoixo9rJaw9jc7e5FMLTUx4iR9xHzz3esLGEo6LWwuoOLgZZosANPVnZF4qm
xycmp6SPHp01dtz4vLz8SUuXr3xbVm530ddU+9RFgr2oF5NnZoLLOzo6/vQPC4qMq2tqg2MLDdPU
0hYUGp6cltHSbkEVTGebxb501brN23dJKiobCot+SUDfd+hY9oRJb0orBPalX9Zod2Ip/Th07PS5
6MS01Mzs6XMWHD157m35h0nT5wWGxr16Vw2mBNdAaaJ9/AqSgkRbh1XGrvFeOVmTJUV1uqXmVieG
ZnO60KDRHW28tLzyatGdd5Uf6VvRdPqS+63bdyYkZTx68sJoQTt97syEKTMWLl2eMTozJDg4KCRs
ybIVRvv048MGkV3ABDIlNTMgJCYsKqmssp5e1TEkn0mUW9CCwmL9g6Omz1k0bfaC2TPnbFy/kX41
od+V+izoYCgwu+iHfqQBsy9rysdWx6zlOxp5/YNLWrrt6rGLT92yjtBFVOF8OnTNjkSrS5w+78Lc
JZc7RMWqeVRd0iW7DnkjkkdXoVQwX6qsKjLMCH/wZtN/j9j5W/TBsnrOA8tGvyWgY+oZGDF0MJRZ
NBxSFJeg6EvXPZ06/7RLtouyW9Vlu6DkLzlx6MQzF/3UlEErVqsHjGY+wcz9Evxor4+TNf+QiJGB
oeNy8w+fOBMYFgXdGR4dPzY33+l0eifI4eYbWzuqPtaXV9WIned/oIytVqt/dIp/RDwYEcPHrBbd
fXDo+Ombdx9YnBwEqq2tLX1s7l8hsROmzz166tzKdZs6HG44C9eLn4XFp48Kj08fP2nDvl3HL58X
2Dc+uk6aBulQ1Zv3HvpHJiaMmYilRTF4+mAIlBZUOH70Mx9Ig2UjYhMxFmxhUXGjgsI2btsJHczJ
yvY9+yLjEkcFhQeFxxpfSA0Lj0l4+uotmkfjmdl5fwaEr9lxCHEDzKyk6dNmzxvpH7N6/Q5BIZwi
Fi6e91dwzKz5Szy84PG4yj/UYromTZ9979FTcyK6gk0a2OPyjTtTZhSi32GjguDgNXeAi+jbrS12
KTg8cVRg9KzCZfMWrSnIn7Zq5Xr6JaxemxswDH4GJ2MyKAUYTm2zNGPRlg/Ncnk9P3bKjvM361wQ
EarmjA+Uy5Imk3svHLETjt0tee9QNPp2XzC3aMYwZrsoCedaRWlt6cGS/zny1P+Zcfr00zoJc9MZ
IvrCrGVYXCwKJ5MjF0t2nLln0xRBFjQXBFsZM2XJ+euP6GVk42P0QNe6n9CtfTP3S9CLRH11W170
0wLG7BccDimqqKkHP4mq7hEVMNxw/+D2dvqRfovFkpubOzw0Pjg+/feAyGHBMREpY8+cOcNmEzMC
iRoREt3SbsXw3R4uJCIGDOcfEv6ipAxr19rampCWhYpB0Umbd+xJzRxX39KOTtvdQtbE6SNCY8bm
z9hz6ujek0excBRdJYpqXF0fM35iQGTijUdvsKgQJ0nXjp89jY6iE1LiktOhCGDxIFfQAhjI7eLH
kFhD0ujL0a8WPQwOS86fMotXwRcSMt9X12KAEbEJssBjgLv2HRoVFjt1AX01PmiAvrj/+Ll/cPyc
+SswGxDdUxdODw+N27h9j/HZEIWT1fOXr2OMU2bO6cfbVGEbjU3U5Cevn/82bGRAaMT+Iyd4MK5G
/IJgvpKra9uo7ZJh3I2B993aQGAssnGeTtUbWvnsOfuKP5DtZ0omFO5vsMlWvg0qkGgeXXeiT7eb
rNr7KKPwSLtG3C6PLhgRF7wQtNOVYWDlYEU2nHr8/007N2LymwNFDW7FrnHtcP5Ygd6h8oqqHThb
vPPSO4euqIIiWsnyLVfWFb212WWHC8oXSsdccy9Yv77w2iVz3wDLGSB+tI1C/APWHDYqMDFt9IRJ
s9Oz8kYFRg0bGbpkxUZekSxOl19QpH9w3ImzF6/duld0t/jEmQtR8UnBETFvy6vBrx5JjkrLHhYY
UV3bgLWkbhIhBdNm/eUXdOfBY2rMdb2xw+YXmRgal8grMo6yTdCkFevW/T7cf/7idSYpvUFT6TdI
Dxw5ERidHByfwUk01AMXwpLY3RwCFUj+hMnTwKYgJi0rGxJV/qGOSTvTfOcuXobhzZ860yPSjyGA
m+ua20MiY4PDoxvamyCcdx8+He4XHpc4Dv4YJ0sOzv269C20DGwXVIxHUiFg6GXilOlURHUqcmhw
REDIpGmzel1baBn0e+zk6SUr19558KTyY8OBoyeHj4oMjUy4evOOoMq8QuAF/PbXqIDQyJSMMTCq
aB+WM7dgqtnEgGEwGNgOkiApuhv0NNq10YVnL7xT4gt2H7/4ziJonEE2iqnEhYinxUIKZh45ebVa
8NCz18jtyaIQOZ24BWgPmZy9zAWPv+ZfcLzSrvPQMQZYv2ZpE9AKWASExzhK9h9tmL/iukMnVt21
/dSFsNRFFY2KXSD0bEYPoEHWxPdAnxLF+jZ3Bg/gGLDXqKDQWfMWnjh3+vzVSy7BA3WOqQaHnb18
edjI4N+GBWPJwUO/D/eLSUydNW/RqvWbPCIVD/zGZIwPiEy4ebcYS8Z8jvlLVoRGxd57+JQGvISU
1zSMDIv3C492ChwTJ2xY2p379weGRo8dP42Ni52o6DFG6pjBeSuYvWhYSByYb+bcBbVNbe+rPoLR
EYaB+PQx9CPeMEqgLTI28fGLN8ZU0XbwKyrqmo1b4RBGxiWNyZmQM3HSfw0bCQ1S8r7KOGeoQ2zg
fQWHJf3r94CwqNSwyJTA0AQUSErLdPIyBnj6/GVU/8svEJOADeIEhVL85AU6BX95+/ICu7Isv3j1
BmoFncKKzlu8/MDh861WjzF2OkVXb95dvno9fOB7j55he1NWWfWxoaq2kbX2pRwGRrar5F2bnDTj
ad6KstkrznmcaEP10OgHQSUCHWJTyIsKkpa7RSC6S0SIC9eOEU8/j2YkPkEkBCU2na74P0L2jZm9
1iLRz68ZxdiHCM3xsjQDFSP6kUmy99zbySsvdUjk4OmyxNGbyxrtDlXn6XlEFzXFvd2D0U1EfdPe
Qwws0zf9WXy9RH22QK/gFCEkPPmvkdEnzp1lLXiXk7pSihqdkAw+jkoZM6Vwyc7Dp7Mnz544cwH7
YBFKiqKYNW7Sf/7mt23n4fpmh2z4DrBRYPQzF6+yBhFrjQACgn/zC/srODooOpmj7rUGqUB0AWYN
Mnh0+54DgqLBp2K9dwP66ujoWL5xZ3ji6OEhMcGxqcFBEYVzF7U0t0sQJkMHz507Nz8/v7a21qxj
AHQCgiA8ffr0ypUrd+7cQRpAebMEOFKW2flJgJ3cQwG2lqhbVlY2fFTEnAVLbR6PANfROBlIB9Yv
67PqAAqbWV3hLWDufwnAv6rxD21ge1RuDSs4HDLp2usyj2K8MN5olm4y/VYeuXynbNlm+s35zkNm
GS9Ao6bzOvEocDMksvboi3/F7Zm65r6nCz9/Gi/bRSxryJgb+ZwonbzZHDztWf7a6qi0TY8rINSQ
Q5l+/xU84VOLwczqCt9DLN3PDA8Qg+P1DXy1RF3OnTg7MiZr+949ZlYnDIUKJ0dvtTrWr18/YcKE
GTNmFBYWHjlyBC2DsVixVmvH0tUrS6sqsA/vgpM0twB30Q3HjBXwspTL5XI4HKgLrjUb16jbySqi
PMv8LDA6OkB6EgFmjVX6/EiN+ehSjOWgKXO/N4BUSNQI/6jE1Cy3RC9/mQcGD4wMc6dvsGIMusar
soNT9DsvGwOSFt54ZGmDrBM7UUVEetRZMMI4e4e6dMvRxyWtMI4y8Zh1u0oFS2MKsVxuhew48dQv
fsWNZxboDKK5Tc3RiW516Xk8TbbL+v4LZcOTVszZcauOFxXCa7qDYIPAwXqqiq52sWy+YPm9k9QV
/RzqHz86jvrVwSZap+cgoJy9Wy9gJXume4VXzvFLL2MaYIegYnAUh+iJQeP7wth4mVLA7BsrBrvJ
HDzWDqRfpJ8so1tXUk1qGUk2N7dx266eygXNQllRnwnhoShaFW3j+RdRGVufv+yQZbjoHJqnhPJw
G5wegXOrZPziM2ffub3GFvA2xXYhdTrCJQWkcx1i4+0yq1/6mgY7p2itZoGeUGVdhcA4qaeokHcN
fMGK2xF5J15V1dGTOJ8DI2CAYFW88/nVoBI1WG0B3dph7oq5883wGh/WprevbpQj39fzQRoFGCUM
LBMsy/LBl8hEGr/IZOmedZFgaZQxWJnxvdkscliVXmGUpDD3DaAWWnO76aepwPctHdb8KTMQg1kc
1KvxNsjL6qHjp8Oi4+GpXr5xG7kuQZk6ay4NmeLiNm7cyJqFZpYQxOgkKCw+MnZ0Vs6E8Oj4Ves3
w92VZVWSZGxoFuNAy3a7nQ2qpr4pLCquuq7pwdOX7XbaL0QCvujWrVsXL15c3Wj56CHxsw6mTT3/
ukyxqxaFOOn5RkwhLIoh5xZRm75i/5V7L+lnX+lM9AlBVeDntQrk+vMGv4znu8+XShBaxen9opBZ
zgAGBQoJsepaQ7NdiRh/7X8k3U6eU/awFpIFcwQd0CmoBswmDBjD/JTpmx5E9NXsYEkU6pqnQh89
KGEKD9vrd+U8va+Y5tN9Q/t6O4Ll96pVbAjcJSy3cVulN9O7wTufv2RFWlZ2QmrGibMXEf9AZxfd
vgvm+MTNGn3YBp7Z8/e1tx88xaA4jktPTw8JCQkKj/YPDh8VFGb3iFanO2fiZET8aO1K0Z3appao
+GQw6IiAkPj4eDTDuA14+bYMUb6HnfEjZMLk6ROnzEC/2LV7BLS2bNU6xGPFxcXoiJHQA5rLw+dN
njE8JPZq0R3EiqjLSfSDqOu37PAPCX9bXs0rWsroMTfuFCO/tLImMXU0Qjj0zupfun4rJjHVLaqg
PClt3PrN+9KycvYfPYzZg7uLyPB28SO0CY2NyYT8RMen1zY4ahtbCxctS0gd3dBqCQqP+m3YyHWb
t1ucXHOH/dHz1zv2Hhw2MgBS1NIuIqyta25/8upp/vS8I2fOSsT2rvbF1LnzwhIm/TYitmDKbEP8
bDr4GBSBydGPKnfwpNRNEqesa3N3ZV/GBigmE9EFqVOd5ANPOiyqXrj63Oqjb+PzNhU//ODLBgBk
x2QSlQhOYtcq60nNhnP1f8QdPPew6eoD2+S52+2IuHw66kdsej3EwHIGUpeBZX4RqEShFQaW9VVA
XTqVba32uOjRR06edfIiVGloVNyDpy+gOK0uz8lzly5cLWppafESanwuXONkpb7Zfv3mEzAZ5AQc
vH3P/ht37kP/CSq9gAsO2LB154y5C8AHt4sfL121bu2mbVDk12/d27h5K9bG26AiiZWVlX6hUX6R
iRev34akGXqOirGDkyJjEytq6sGXIZGx7yo+SLre2GYNDo9Gp6AQ3bVYHJA9CXJtANVhGT42NIua
DtoghAGhkZOmzz59/jKkCw3GJqUlZ4yJTUpNSEhwOunZLgZGDAOsmoLqml64YuOajVvQEbpD1IeZ
sbmFTdt3Q6QR3SWlZ9KTgYrm4MS45LSbN2/CVrAW7jx4MnpcLrvelT91bnJ6bkxC2qkL53hFAGGp
mWOfvCxBRXaylF5AD47ZtvMYJpC5iy5ewnhfvSuHa9fYZgHNSGOkfsHhsHiwS8ER0VW1DRDI5WvW
z121c9bSfSPCJsWPXrNi06mcgkUr12zGHML1A8fDCMFlhFm59LqlYMlueJX01LVsel/m4GmwA2sp
csTDSS6rR52x4NLe02U1LnnavEvnb1QJuiaq9DzTpyq0kijpCqep7ZywdPvHiKyNZc2STSFHzpet
23kPLqVHFgXV/IyrWacTbJZ8D7E00PMQy/GW8e76HvIFy+wVfRUYrDgKTVOJEjg1JjJ9VFAotGN9
S0dkXGLxi9d2VZuxdNmfIaFB8QlhYWFNTU2sjgQR0DWwS+HCVVu2H25ocYGtM8dOHj1m0q37j8Au
MA6wA2s3bnVwAvip1eqormuEMYHrj12wdebY7EePHnmnhujUj+JVPW70+C279rEBQzCAzOxcyLnh
MkmQkzdlleBRWJuI2ETIGEQXPQ73D4Y+hviBm9HOoUOHFixdiSrgS1CCki9Kyjyicuz0+amzChFE
cBIVRViYjIyM6dOns0XqMcuUOHQ0fmqhf0gEWN8tSuh994EjMEpjc/PH50+xunhwMyQWdm/f4eN+
QWGfRkQJliF+4H7UCo1MrPjQBL0wPMBv1YY1Y3ImRCekgAZsQqeNioxNffikFGRjgPQXs5GcfvTU
Ocwtjk6eMQerg04XLV+N6m7ZMXvBXMzq/YdvU9Inz15zeeOh1/4pk/9KDQ5LKNh/7Hp65niYfQQz
1CIS7mG1Y/LqW/DcqI8n87JGAzY2cAaiejAzHzqU3EUXDpyorvM4m9WPZ5+9n7PK1eyBVyoRGYFT
F7sBKG6yY8/t+HHr31k0SSvlZHLjuV64/3Y7BJgGUHbKX1KXWt3m2cztCt9DLM2Auj0zvxR9tdCn
RPVVoQ/AZtN7SnhOTEmi6vbxizdgoGGjAk+du9ThcKePyQmNisVy/ufIkMevSqnhILosCWBfJycm
ZeUGx49ev2U7zNrmHXvgDgn0k/rwrEQED61WJ4sQXr2rCgxJuP/4OZjMLSrgmISkjFs3i0URZanj
A6rRLqeSyJSx85euQuO0G12fN2/euHHjMBwwBzLHjJ8IVgaR6Ctv0jT0An6FwMCK7j9y6PaD+6j2
8l1JcGS4y+ViVg44ePRMVFzqzMIl/sFRh0+chTFJGT0WDB0ekwDFj9YgNnBHwyKjS0tLWRVfPH36
9OTJk93Wsrq6+tWrV+jCLes1TW2vy6ufva3wwOAYoR0rwxJQQzabea80wG4vRjGMiBXrBtqHUZHR
zwobR+gDf3CuYEgkyd7CkeXbrk9ZfLSkUTlWVH38ermDnm4gHmgVIgr0HibwvyoS/W2tc/LCQ05q
LjyYQ9DAWsN4VE2BzFkEUtWmzFx+8NCVF5XtWATByZF31Z68WZs7PLBCJkmoSEkyTnMjGj1+tT5u
4tG7xR9BHbpEY88bhMW7LrsRBUCnGvMAsL5YuhvYoX7gW4ylvWCZX4e+WhgsG2VKlMfNhwRHPXr2
Cvvw8kcGhl64cgOuBYIWsN3MwoVBcek7DhxlEoVVRZw/e/6iyXMWhSZmZmbnuXi5qd2KgCcpPQuK
E1OK8BrekajL1fWNweHxEdHpkDHIKhQ2NO7U6XOvXrml0GtKnyRK0MicZev2Hj7BVh19bNq0ye12
gw/YLMCq1Da1sgudtJpGEGx8qG9Gpp1zw8HgFIndfIS63jvWRU2BZ+WRBQnMqNN7JmAnoaGh+6Gr
8QuZX7F2w4FDR1Cr17nuFyDVd/sERrO50xXsEIOXxXuiRwEBM1XSJMTm7z5z6227TG6+qjt2q6xV
JW6d3tVgnHEU4StrKg1fq5uE8bM2VbdwGkRN0Snb+zToUrWHFZZxU3feftzMUVOpUwFUZKuoRUzb
cxvNw8aokB2zGiYHcDnIsm1385aerLbyHlkRNNGhS+Wtwpw1xfUulV6op0qFCD4unBdsDF74ZrJ0
P1PxWaAua4TBzP0SDJaN+gREFMxrYidSkQM96vF4oI+R4xvBo31wOdOyKI/CTPnhl+lg5CNtlKXw
6mMvYd7xs91ewWhAKxASdl/SIABNok/vZoBR4oWZ6wO4LdhABmjotn0p+uqiVyCOQ7iKOMsmkDIb
SRh9fsrC882C9lGou1RcfvtZpU1QeXq/lGHENMwYxsfzGnlRbZ276pgTHh/v0AU7fWQdXM4Rq0yu
FDePipk8c+Whs/feuwjx6CIdmUicgs3uJktWXXv1wQE3FBKkaHYYNpVeM6pxEm3hhiehqYdeVMBz
xIQIkD8oqhqrPGPlXiuPRTJvp2DqzyC/dwxk6f8W9CJRPyehXwQmmWwUkMP79++HxUTlTsqntqVf
BQbBxi8MGlWkhpBjt7y8vL6+HpoCTSEH0i5I0rOSVy/flvCqzNNLnLQjdArfjFVByeXLly9YsGDq
1KnLli3jeR5HWy18VFxGYGj8+s17BZkKUnR8hl9QTFBofGxsrFeJAFBAiM3g5nktJMNXsBFGIXCC
S9EnLjkcl7u2vkNxypxHhjm9dP12kwD1BVHDD5w72BFFFYjcgajynTV3zm54AZougesPXW/+I2Hj
yiMVucsvHr7fVmeVHJIKX5veV0wf45A13a2oEjy3hWtvvPlgE9AkaJRBrepUpQ5N23O5NCBuy+My
0aEKvN6GKUNEBft29NK7xevvu0EEz8OUoT1GNhsmm8xvBGvK3DHwFdM4cAyW1/cTATx05cqV4NBw
BEjQuk9eloRExv4VFjQiIiR9Qg516Q1XHvCdUzbLNfXNweHRq9ZtWrJyTXLGGF5RXbz49NXb+pb2
tKzsJSvXwhFtbLOERsVV1TbWNbXB/2xqs+qaglBtRGDY8NC4unY7RAW8OCoo7Prt+1YnB0YVVXq9
NWPs+JXrNjk4AfTAw3Rw0m9/jYqOT84tmFpaWQPnE/EhPNKKmnrEnMP9g+ld8yZ1JgbOCigCcwzH
qbLWFjxh9/ZTz6jnpQjwafOW7nzX5ICPapwddFLLBLEF0ZqkysrrD1LBwiNWSfcoLodMFmy5F5q7
ucZBBZvNWDfAqVN04hTJkYtvNh18gVEIKtQWhAPhravZo4bl7sicd9wCB1LXBHoakNLFyaTCqk5a
c9iu6vRZMx8w+n9d/AMlCqsCiYqKjeMk5erNuwjhmtptWCjv1t7enpaWNnr0aFgAqp47gYrxKRnn
r9wA91ucXEBoJGIt8BtKHDt93i84/HbxY0HRENftPnAkKj75L7+g+4+fw1uCRIHXr9y8FxiTWt3Y
wSRq/ZYdZVUf7z54gmIPnrzwSMqFq0VIQ5wgnHYPB56uaWihz1ypWsrosdv3HHAJUnJGVs7Eye8q
PqD3Ow+eUL7/KrgEsmN/Uc7UzW9qJNnZrulaK0dmrzlSbVXojfOazoSDihNsDFGcqtgqSqlTTqw+
3ZC3/MbtJ67USed2nX5rw4BpIVO0jUomsItR87r8sty5cMmtDpvO6a2SZhOJalX1E3fK/yN5ydaz
7zGZcHN1HU6jCstlFwm8xNwlF0prZXjx9InLzqvkvl2wdD/oR7l4DzGYuT8K/0yJun37tl9AEOzJ
mQtX4pPTIVqwEtSpMcQDM+4VJKRZAkAmeP3U+UsoD3MRm5R66fot8B/0LsTm5dv34HKPpFbW1IVF
xyPTydNTkTMLF6JHtPP+/fvo6GjvBTfkwPcDDbFJaavWby7/8DEsMvVa0ROLQ1m7cW9e/lxIC0+v
U6uCqmdnZ+/duxdGAKEmXD5Em35+fnBWu3l93WAyC4aCUpKoa06X4r5ZZg0Yd2H5sTvYhy8lKEqH
SgrXHLbJmkB0ifA0GoIQ0VssBKvS4FLdLU4SU3D3/4i7HzH/4fqzDVMWH2uhftkneGeJ7RqvCKNT
apfIpMWnmhwIvOxEtsHrq5eVmYufpGae7aA3/ttU3UPvPDJEUiRcs07GLt71uJw+COcL1vjPAIx0
gCT5FmNpNkvfS6J8df8PhizLDx8+HD7Sz3iiVp80fXb+1Blbdu6dt3g5BOZjQwsYd8aMGXl5eWYF
H9g9/P/4Y3hMYiq2iNgEq4svfvICghQekwDbsnjFagiYS1DgEGIXfl32hEkQNgaLxeLv7//y5Ut2
nrqsrCw+Ph7FYGqYJAsaeV36ftnqtW/KKtyizB6wbWi1YGNL0g2sWQaW4+VsExrV7oj6W3j5A08i
8g+Fpu91aoQj7yVegsR2SCR/0U6XRm/0MS8CUJaBqdAFEAwBcJD0MdMdHqVZ1KMLCudvvG6ViQBr
QszLqZ8q+cBDRIdGJkzZ8qFBc7g4RVMgWm8aPNHjlh853eEmvEiaaRwlUr9SoHdByE5CVh58fuXe
R3qStPPKknc4bJelGViOF2buIME7HHP/G9Czne8iUWBZM/V3APMF1NTWQ/3DCMBtgRGwOlz1TS2I
fOnbGjrJA+t3k3xR0TgBviB9ygfzBJcEv+A/RMzUEhhWTkIa9kehjzdSn4UyBgWapX/oQ1qIDejd
2DAGrC7beAVcrsIDwibSUwK0NWoAdfrKCtqaqgsyIhCzFpjS6J3+SmBb6qrR03A0kygyJV+wuLWU
icfGzTnQYROMS07NsDlWgazfdeVhqRWypMmKjACJkkxfzkBPj6ucIGpPqi2phWtcKo21cEhUZJGO
yyWrnIau6HsYKIyR6RgyPSejYTZ1XtAnrbt24NJbB+/iiWDXPLmLjiVM2GGFMyeKku6ml5CoHYQv
yYky36FJS/cXvWxU6H2FmqTBbPqIJ2ufpc01MMDyvTBzvwo9W8Buzx6/Dj0b/wd6fQyYrScv3sSn
pI/LK5izYAk/sC+IsYnuNkcskwmAIVEaJ0LsPvljSHt3Vfhy8HUktaKmFhaP1WVwSwLb0EhTR9va
TVsXLF25Yu3GPQeP8nDJVFJ0pxiuZlxyeuXHBuy2211jxk+E0wgLCasIr5U+Skec0Ae3H5WODM2K
HHfs8K03bs2muCUYHXoBRyPzVp6taCZwuQTjgVl4C5TBASblCnnXpOXP3m51KvS+BwMYr0Fgn9Cp
w+hB6Zcf5Tlbbwuq5JKFRpcSnLVm+d5rbYJkXDiCrOMXfVGxx9ZCyP5rJfdeNEODqZLVbKtvMGIG
HT0b947X3B9UfJlEfREdrHA37hwsINh48ODBlStXrl69+vz5c+RAQdtsts2bN7vd9LVK0PSHjp/O
nzoT7Jg/ZUZ9fb1Rr098/Phx5syZEQaCg4MRAb158yY0NDTcQGxs7NnLFzlFmr1gnl9I0LSZM8LC
wh49eoSKlZWVO3fuPHPmTGRkJL3mJmnLVm4NjUxtaHZkjZsybtw4lAFJubm5QRFh+VOncPSSJ3n4
/GlIZExVbaODE3n6oBwprayBG1nb1PqmrDI5IwvuZUNre2BY1JETZ24XP3YjHqIfF5QrShvDI9NC
49JmLVntIW5qQ2E3CHnb1Lhp79nKBgFOoESs9GkLGinB/IHPIdj0xrz6Gs/G1edsou7SnRyx8TCQ
XWHMhLlwvoDxtAhk3KID7ztEKIvHpZbkSftO32+mr4HinFAzGtQMvV3IjX6ptVXIgzsfDt58ykOk
oWpk3XutttcuWOZg4esEhlXpVZmaOwNDLxLFWmEws34uUKqaOmwRSZnrt++7df/R9j0HJs+YI6qa
zcWBTa/evGtxcms3bUtIzXB4eONJITpTcPAgM6mpqeBv7HrnDpz6vro2Ijbx3kN6uzqAQ8wzFDW9
xeLIzM5buGyVR9JQLCw6HpEVrxCIa2hUHCwJdSld3Kbtu3Gow+Hm4DZJSs6ESX7B4dgaWjvg1CGc
g4VJTB2flJaD7uD+vH5fghgMxC9fswHtdDg8CM8mTpkxPn9KcETMmo1b4Q2id4icrEuvyyr8I7KS
xi4OjJ37R9CE15V1Y3MnL1m2panVzktk7eaiU9ffuDvPmHUDhu7USVWL58K1VyBD7Hwa77OAi6kS
eiM/p5PN+4vO3Sm1Qk7eNCZmLqvlRKtx1yy9vQ+OnPEsJts8HHn2qmXZ+hMdcvertKxZc6dvDLBY
P8Dy+Tbim/4B6NNG/WA6vBhAv/SoW1bTx08+cPI8GBRbYlomXDswd1pW9uETZ0orP0TGJdXUN7Oj
2LzNsps2fLvAUQgJ2BpM3A2tVqd/SMSy1ethMVAMghoSGXvt1j3w+vM3pWB9+HXId4v08ZMrRbfH
5uY7PCIomTJzDnT5y7fvYxJSIIcI5zhZG5szLTI2HaJFL4ASDc6f1cW/Lq2AIar8WL9q/ebM7Fww
PdqMiEnYd/g4OrW5ha2HKzPyto4ISK1v7ICAqPAxibJ+3+kR0VMmzt/eCJVB3Uz0b8IcqYqQRrdr
pGDl3uIrFUQGn8ET5CHevmCFvcrFFyJxWiXyuMyz88yTRpUs3H5ryoKTVMBkNz13Z3SIatQIYoPH
J5PSN5YpS/e2CPSewM5Xwn0C68vc+VvhFTkGM3eQQCXKbLgT7MBPC3qNH56Oos9dsOrS9butFseF
qzfA95AKi8PtHxK+bvN2tyDNWbA4KS2Tnu+yu4qfPId0Qd6On7kA28JiHi8bsTMKBQUFSUlJN+89
fPb6LQKbW/ceOjgpf/bcEWERIfGJI8Mj6MuJFA3GKiYxNXF0Vnhiys1Hj12adujs+ciYtKjY9ODw
xBdvqtyyXl7f/j9GBgfFJPtFxJ88edI7q/PmzVu5ciXrHfp73aY9oZGIj/KrPraJuobt2u2b02ZP
z59aUFLXdvapMzJn654T988WPXr5+o3L47Eo+vU3jc0yyZi46cb9ajtPra6XOehJatqLJMG4qaTV
IY2esrWsUaH31fcGY+jdlx4GjQottS1KvUdbsvlGrZWMnnRw7aarvIsoIsrTJy+61NVFSVMcHJk8
b2czvRmQksQKdAMrbu58T3jnhO36pr8OA2yB9ftrnJnwHRKTKFiJS9fujQqKCgqPnjF3gVuk0Qjc
ji0798IBYyJX29QG87L7wBE4UZA3GIc2m4vKW+dds6xBBnYXYovF3thmbbe7WGyDKEWWdGyKcS8s
moWpQb6s0nwawxhaGl4c4ggcxQbpZNSiNYA1znJY2gtWnt3sB42uENmlic/K7Xkzzo2Zf7Kuhb7v
D8GQLAltnJ676MaqAxVrdj+08ppAzwQap0p8gJ4UeomMbD1w5eKDl276sAUI1YhOnxEGvMQwsEyW
xiFzl/bIWxRt9qqibZfVP5NWnXtY6jL8OSpLXYeAwBWoabbOXXmgzSVhWlRiSq9Z4t8Sv4ZE/TNB
b7agF1wFIrjcwrXHdv/U/cevlzpFlxPSons0xWMV5A0n36dP2vPkRRN9poLeKfeJZemN4vQ8gEcV
SAevrzxwubikCqqCSB5WwLfwJ7HxgUJgZSUIKH2ckL68WDxa4vKffjli/KW7d+9C75jlugItIR5r
84i5c/a9LGtjzfrCLDdgfF2tnxNDEjVoYGzRzRT4olsBcCunO6vbLHOX3U2ecu9Ng4PTLdS14oku
Kg6RbD5+r2D1yUYLB/VvvDILctLF4zKeOyf1HULOgu2VFk6gTz2z473Q0KtE0YiIRkJimyi0iWpw
3vHMGc8/NhFd6f1tKqgBMesQPItXH3/4xA7/FbEZa2kIDEMS9eOAoE4mAie36ESUeDJrbVlQ4uKr
Nx7D7WOiQi8MS8TWSqImrKt21EvE1SlBn8DYmonlkkNFqw5epk/Pq8ZjIn2ACYMX9BIx2oCEoobT
KRGpViG/x20eP72CXXhgYIXNHRR2S5zWYSVk3LwntbW1/SiObuhdkn96fDXNVKIwZgaW5Zv+qfDT
EvZZsHPxisypqvbmvX3CrNNzVj9zqfQFkBrx6PR6kiEnYHXjlgqOyp5K3xNh2pxPwDIzZwxT4dLp
87bw18C0tHIfYPP2CQh3dF6kMZdk19Q1W+/MXXrVJqsWuR5xkfeGEl+WUojiUvR5C6/eftDs0uiT
12iGHeoG1oO5Y4DleGHm/nww6euk0Df9RaASxebOW5/tsvRPhZ+WsM8CZINT5x0tD8rZW92qCbKq
yZX07iZFgCR4ZQHMbrzlBW4f8rEPOet3UVWePjCLerTsgKHAzePfd/DjCq/9nnfhI8eppF5TOfqI
ss8Me9NgjA17nizYXmbVJVmyks6biFixbvi6Qz8DGHlfJ0W++Em9vsEa3t8OWZUam8mseZcWbTzr
tNMTghL9ZBmMzJeFH71OCMs0d/oBilArhiCLU4jY4iJ/Tr72f449cuO56bx5G/FtUCStokaev5bn
LD0K28qOskMMrPA/YI0GF/+QOIqtrrnTFf0c+q6Aa/Ty5cu/ElY/LGvmEeTQx849ms7p9HoOJKqH
P9cv2Ci+kn3pdSTFrfPtKkmbdj589uulO24qqirIfbYG9+9tsydp3FKbm37MCSbWPOCDbyLpn4tf
SaLYEpo7Py9EOHgf60lmwZZLz+iHcAaRZrDvgCdBNuSWborSKqlaaNrBqKm3IxbdQOhG6+N/DzFR
6fuP4EqSzAm7i259ND5XRe9/NSpQDLj3fz76moo+JaqvCj8GrHev/mO7DCznJwQjz63rE+Yf3rH/
tXFS4Su/9MqaMncMsJwvMQj0i4/0fYgySVl8Y+bOkklLz7dy9IlDelaQnpyAzNEzHL5AD80tQl0L
74GTqLpQAtKEH+MV/b8q2NSZO1+FL2qBShSr0G21WKa583fjpyLms3AbrzI3d3442FwZt9W5LAIX
kHctf8GHw1c63PSeQJHehTeE74nv4vWxRTV3+gYrNhC9y0qaO0MYACTSfv2xxT9xz/VXTuM+Kvry
GEyk138bINjMf4lt/HcBmxlzxwdfJlF9tTIo+K6ND+HfFj+Yr36lMxNDGMLPjyGJGsIQBhNDEvVl
YC5Er3FFP4eG8O8DKlHdWIHtsvQQBgVDU/rvg15s1NDyD2EIvvgiiejd64O96tlKz5whDOEnB2Pa
H+mKD0nUEP7JYEz790tUP/iuJPYqyUPoFUNz1RODNSG+7XzpPA9J1K+KobnqicGaEN92vrtEoQMG
c39QYTb9A230rwtzpnqbK/PAv9809jVqlg+Y+5+Db2GWBtjuZ/HFEjWEnx9Y/i9Sq/94/MgJGZKo
fyCGJKobhiTqR+CHTfEQfh6wRWcwswYbQxI1hH8jsEVnMLMGG0Ne34/Dd13IIfwkGJKoH4chifp3
wJBEDeHLIMsyb8Dc/2YwReOFmfvLYkiihvA3w5SkTpi5vyz+2z9gDEMYws+DIYkawhAGE/9t4LdX
DGEIPznAzH87Pw/FUUP458AIxP5mn2tIoobwz8GQRA1hCEQi9PM7RCQyEe3ESnOMj3//ohiSqCH8
zaCfEFddRHOoGrG4zLtazWO/IKhEfdZWfrbArwU2HAYzawh/E+iZBPqdAu3h2/rgmMLgsGUfP378
2daln2869sSQRA3hbwPYVJZlj6IdKbImzXz6r+iVN980gH29Xy79SfDFEvV34efhaczXz0PMPxqY
YTh5IhHdqqLbJfL+Y0f4rPI/0k6Ejd7S1Cr1+p06tjQD5+l+wJoydwYGlGdx3QDrDkkUxZBE/WDo
KrHKZNz8/QeuvFt/sWP03JP1LqLqOue2mCV8wJbm75Io9CuKIs/zA6z7d0oUaB2Uafp2MEoAc98H
5oGfg85fGeBFHf9kkX4Mv8WtzthwceLay8XVnlHJs9pEAntFP/5GPGZxH5gLMBhLYDZkwMz6HOCC
wjVFYoC1/k6J+iXANNPAF2AIfQCOE/16lSqSlno+a8qa1Ycf3Sqzx2bOqWu20w+ZQuI0nRhfkvve
YGtq7nwOKPlFcd2QRH0GbPaHJOqbQb8Hp8mkrKRj4ZwDFl5/WetJz1/Z2OqGB6jwmvnR4B/ydVK2
pubO54CSgyBRYKAv6vWHoRthvukh/C3oZwmYEtI1hagyUT2STpbserL68DO7RmocXHL2/DabAiFi
LQyizmINmjsm0DikAt1xok59OEEhLvpr13UHJJozi5lgLTBAnBjYrlmibwxJ1BC+Cf0uAZUSCJJA
iJ1oa/cVnb/h5DTSZKtOy15S/KzW0Pwia+G7SZTRLH6wafSOjHqRXHxkzZ199c/0VU12ooA+VaIf
7/YBa4HBSxjbZel+MOT1/UPAFp79MoWqUANAgbSXLXAIaQakWeZ3gq5KsqZ7NGJRScbEW2UNooe0
Vre2pWSdbWqXeBk0qcYdE/SCj8Gun2gePFArKENsZSJyMJLXVxy49LJBrHGQwKytDsi6AodT0FSB
EWBW+gYMSdQ/BIwhOI7zeDyiaCp+dgiZgN1AS0vL69eva2tra2pqvgP7doUqSrr+vsk6fsnORptF
VrSSV468ufuKmiQ4XRpMF8RJNsXJ7XZDyL8DSTR+g9fZ3izMn7Xn6r1Gh6ZbiXD/TfXm3UWyiC5F
QRccZKBO3Wfxi0kURowp927fFRJ1uFVRl+B5gwGQRg4SlBl+JoAPwIvNHbYNW3dmZueNyZkQl5Q5
Z/5KWWezpE2cMiMuOb1g2qz4lAy/qJQ/gmKGhcQFxqa7Bm8cjB11lVodGEqNyILmcRDLh2aSM/FS
o8vTKpJX1UJG9no7RwTVJWMaVXomXdNbZYF8cLjTp+9/XupWtHazRQP05CCMKx2GgnXAkmhw0BD3
GJatT6C8hmpYO47w7YIgnXjY5jfu4LlHHs4tSUQpa3SGJq+B9GLezCqDhyGJ6hNYPnSBX5no+BV1
VVCpCv3e/X4FeJ6fM39xQGhkeExCYFjUHyOC/YJiqmqbDGq1CZOnB4ZGjsT/2MTYtLGJmeOjkjMz
86a4pEETKVOiwMdGk+B3TlMbHOKqnUfeNzntCjl25Xn2lHX1LYIM2yRxBmGiR1ax+6rSnTF70/Dk
LZGZ26wa9Us/uaMGw2vEpRKbrHAqBqOafRmH+wAO6kTWEB7pHo7cflyXkLvm9K33vEo8kmB3kSmz
Dxc9rf4FJIoNtX8qcRQ+ibljoNtu/4B/0Gp1ln+o83I2enQ6nVgDb7+yLLMlEQS4ySaQg5LmzgCA
6RY1cvfBs/Wbd06ePnfOgqXj8ia9r6b9uoVBYERjquhceZ9cwC4jG5nesQwEqLV+/fr/GjYyKT2r
qrbByUv1zfb8KYVVH02Jmr9kBcTp/JUbvKJJKniSGhK2DRbYcAwjpWPebCI5c+NJRt56q+ZxE37W
4tPbDpztcMsKJA69KjAy8MYcbqKNmXQyIHVdhU2qtZEZK2410AtTFGa7sGW66JDaPESpbFVSspfV
tLRouqxjVD7FMF1slwEj40TJrRC3ThIK9t14ZoGTyXs4TXG2eKT8KcfaLcQJy/h98KMlCmhtbQUb
ge/BteAGBvNY34B4oPGmlratu/bdvPcQvAKvBkA7t27dQpvefhkZgC8l6AI5aGQgzAqVj2bPXbqV
kDIuMjY9NjErLColK3vyrMLl6PcbNTsIABiFANKYB5bGUa9+AQHsKNL9zw+qFBYWRsQm7tp3iJ5Y
U3VRJbxMPCIYj0rUouWr/YLD7z58iqMQW8aB+DW2wQGjX9MVSVfAqg12ccayvbVWyabqx67eGVOw
ziUi9pdE2UZPrIlEEDWXpC/eeiUwc82tVx28xmF5G9p1G0yLAbNdUE9Ehy7VOUlKwcl/hUx7U1ul
6gKB7+dTzHc+AbiTbkVxa2T51lPrjtxyq/AVJQi7R9IXrj+zec8dCBjME6s76BhMicLAGMz9HoAd
Kbp9Jy4xOX10VmJy6rQZs7JzJ5y9cOljHb3jGAV86hrrjX3v5BrLlp2X7xcWO33OfEw8RETR9CXL
V4ZFRu87cMgorSPnzIUrcxYusztd6A6VJRU6TfeIcvaESQuWrqSiyHSf2WoXsP6cnDA6Oy8sOmHy
9Dmbd+x9+Oz11aL7IREJY3OnQAGjBZTBxgkSGofWt6AzDy9DnUI06GV/XZS15hZ7a7v9bVk5L8G1
wdgJ2kant+89ePm2XIat0LHKepvFFp+UsnDxSg+PxnSPwOVPmzVl5pzZhfPCQkMjI8Jx9P7DR9S2
9DGvkL3i4uJhI0P+HBGyY88RJ6dBoiA8nXGUvmPPoWEjgxNSshYuWzt96vSZM2Y1N7VgqHT7BtDp
BmgnqirDL9agI0VNP3j61oFLxfUy/6DcmT9vV3m9W9RUN5wwTdLkNkW2uyXl/I2WmJQNxS8dVpnj
FF1TrZBETdF54hTgO2IiFPyoRKht5+XoKef/e+iOeeufeKA+6KRBbE0R8lJCdQ/+G3OqEg4hWUz6
3lYXigqS7OEF7nmFPSJvW2WdxKvoBzqq97EbQzJhZn0hfmgchblft3n7cP9g/5AIv6CwUUGhAaER
IwJCZs1bZLFYfDUxVTR0hqgnZGYZEpWYPuavkNjLN+9iwJqqSJo+bfa80Ki4Z6/eYtqQCbZOy86P
SBlr9/DoDgobv05Zr223BUQmpmYXQIM6obIMkegFiJo0cvDk+WEhMSmZGW5RMGIneDJaTUPTs9fv
wKZgVk5W7z16dvLcpWu37iETEr5m49aaBvgUmqRLNrdn9/5jGVmTUjLGjxk/8fSFK6ATFUGxw+WO
iEsOTciwe0SD3UltUyuCn/SsfCvcNV1raGsOjh/9VxANeyBRwYH+OJoxdvzTV29RuFdg3ux2e3hU
6m/DgsOiUrfsONzU7hDAN6buIGcuFPkFRY8MiPptWFBoSFRkRNzbkvJvN1GMp6n7Rjuh5yRkmdx7
9GHN1iutCjn15MmoxG2ljRL1rlQYXKIr9FKqpOvXSxtC0nYfv/IRvh/iGrOdTg6mF1vRnkpERVB0
W+6q+/9zzPX/J36PVTCkpitYFXMomGEslUysblfc6MPZk++BHk2XMBVvah3hY5cV11L77+sR9AQ7
xGBmfSF+qERhonbuO4QAOiw6/viZC+MnTg6JjIU8QMbOnj1rFjLQYXPWNba8e19phbPdCQxy8szC
wOiUM5euY1dXlcqPDeDpjdt2VdTUY54w45eu3vjDP2xkRNKp85dXb9iy7/AxCJVNUMYWzBwVHh8a
lzZpzuKjl84+ffu6jwnTHR4+MWPcf/mFn7543iOJIrqBH6govKxCMATwDfWstMs3bgeFR0MpBIZF
BUfEYHNwIg41d1izcnKDw2PCohICw6JHBUJrROZPnYG6kKja+oawmMTguDQHJ1Hh1FDeBpcsMDSh
pr4NHdW3Nv4WEPm7XyhiRY/bzXNuREGRcUkF02a5qQz2DghVQ2v7gqWrwqMTfx8OIUzcc/AoysO9
wbb/8NlhI8PSsyZMnblwwfyl8woXV1V+7GTDr0cn21GqYBWdAil6ULbryH2HRHYcfrVk0+XSWt4D
awmuhtmWOSKrnEDW7X0ZMeGMUyc8ZJDaSdbMJ4miXAL3kPNUtXUET3n5/4o4Fjv/cpXdw4mWviTK
UI/UH+Fk8rFJGjNrd7XD5YFzqJA2nWw7eXvp+n0NLXaB02DPMVdd63YBO8RgZn0huksUa+vT8L4c
3mGb+z7A3K9avxlStHjFGkTJ2AVLzVmwJDQydubMmcxGOZ3Ow4cPR6dkDQuI8AuPm7d83evXr9k7
gaFd1m3ZMTI8ft3WXdjVNWXG3AXM4kGoJFWVJDEja9ywwIjA2HTwMawfWgbjOmQSEJX0n8ODRobF
DQ+JCU2K3XXoQK8TBrvn5MSQmKTo9Jw2uxXuIhgC7kibzQFSIQagGZRDul6UlEGKQiJjXr0rRxct
Fjs4AduylVvgYq1av4lXZWjMNpsLKiMqLgk2DQSLsjoyOGJUREKLxQEbKqo6J+sIgYLDk95XNUBQ
LS77iPBEDLysCkyPVdBcvAjRha1rareZVPYG0IkeYeUS01P9Q0LR6fY9B0APelm0bD0M1Ikz1yBd
kCJFNrhvkCRKlxEME04ns9ec3Xz8cYtIxk3etX3/LR7urwKZgSbh0BFMlVsmQSmLc+cecSOM0SBQ
BjU9WU6j75motpL/nrzr/057GDfngo36pxDIbrcK+UBXRFFwS6SsiYsat/xDO2YeREkOlYQkzzx3
8x3cCjAlUbu0wPrthp6HvlQWfqhEgfOOnjoHRk/JGIOABD4SDMih46f/GOE/ceJEFIDM7NmzJzw8
fGRE4oiw+H+NCv0jMGrs2LHwCdEsQvCd+4/87h+xdPUGg0Jt/ZYdqIsGaWRlWPMde/b/xzB/v6hk
hOPZEwqWrloHScB6Hz5zZURoLMzUxt2HtxzcW1H/sVeJUhS5pd06IjgyPjPPJfAIAGCjeEWOTkiE
oYBpjUtOhxigTXB8eEw8/DGwDBYMw6EdyWpkzJhRgVFlVdWyLvOq6JHUuQuXRsQm7D9yQhJ5UVai
E9OGh8Y1tFqoRCHuVvWw6LigsMTyaipRoiaPikwKi0+rb+lgEoVM6KDYpLTnb0pNKnuDTBlYg5vD
K9LUWbMghIlpmaAHM3z24q1RgdHrN++VwGUYEqLywZMoDUNSyM6j97edrXjdqiQXbD55vRLelai1
0Tt8dIdO3JJMnr73pEzZ/aSRt0LKFQ+dMnqKhJLAmvGynKgK1e3W2JlH/pest3FTrlgwRWK9zrnp
zet9gUbKaptDTJy48p0dDUoo/LGKi5q8/l1pC7QgnAcMWoF69AHrtxt6HvpSWfjRcRQk6s8R/gmp
oz82tlbWtF+7+WRkQPSfI8KOnzvploTXpRXD/SLCo9Kq6xrBDQ+evgQHwwrBBMHdAsseOXMZ/ljW
+HzaHGIenVTVNqJAzsTJgkiDY0HWQmJT/jUq7NiZU9hHj8amuUUeAUlkbJphS/oEHLOG5raA8Nio
tOz6lnawI0q7Rblw0TLwKKI+UG73CKCktOIDBCx/6kxRMc/L49ftdkPw/vILol4o5SoEYPqKtRsD
wyLnLljikXmIB1yyf/0e8OjFE+yCME4RIuKjUaXkfRWz25FxiTB9kCivWxgcEe0fEl5aWcOI7Aam
wuqa21EdRpSTteLHb/yCYkaPy2t32HhFfPT8NdrHFHU4PHBNYTYbWjuq65oEBDZfAspfJoehIkcU
XfNQ47Nm1921Z+pX7nszd/WJOpuHxqgIg1V6ioHTHG5dPHSxcvOam6WvrCKPQxgTfeHRJ1Dx1nSN
J7KmuMi7Wtt/T37wP0VcXHb4LsdRq+LT7ydA0Khhpjeuax7FfftlxYTpB1s5rYPUVTu1MVN2z1h4
0gYSeoPZxPfBj5aoorsPfh/uh8ghISUjZ8L0sKhkv6CokQGRTZYWl8iv37LdPzg2dfTEew+fIlAB
K4BlEaOPyytgEnXiwnVYrdxJ02lzhkSB4eD1jcvNF6AJoaw0Mjp38m8BUV0lSrV73GDToLA4m7M/
iUILvKT6h8WMCE84cfYiitKTZgTmVLn74Ak6+m3YSOgC2JbyD7VwLCdMno7oiS0SfHS73Q57Ehga
+fTVW9CGruEfrly3CUNYuGwVhAdNRcYl/z4s+MGzJ4KqwPrBVQuLicKE3LhTbJgUAicQAgDlwnov
fvw8KIwWQNDISOwG9A7rnT4mB6Hj69Ly52/eTZg0yz8oJitnIqbULXJPX5XAkv/216jM7DzQPCoo
zA8txie/La82mxgYDG4EKGvrmBJVUDSy7VzVwv1vN5z7MH72/qoml0cjThGRCqRcEVQXhnP6Qk1q
9iaJI5yLrgdkDU2YLRowWoMbAW2geNx6ct6t/yvpwLITFfALjc5MqehhKyC5KowtsjtseuLoHSU1
LosilTS/jRm7Yt/pErdG3J0nIbrBbOD7oHeJ8g7D3B8kgMOu3br358iAf/3+F5hs2Cgs88j4lLRX
78rgtLhFAcsMG/XHX2GQuv/4bdh/0mIhf40KNHwt6iPtP35uRGgcQmy0Rs/16aTV6kRToVGx8D5A
sKJTifp//go+ePyoRxLBtZAreF8wgDAyw/3CKz60MWKAngOEjUJHBTMKfw+IjE/JQAwDngDvILhq
tTogLbCH7yo+wBogLkK/MYmpshnoUkCnzp6/OCwqDjYNZWDiEHplZech59b9R5LhBs1ZsNQvKHb+
4pVtVofdLZ69eH1EQCiaglsIC4ZaoBPzMHXW3PNXbixbvd6wjZHj86e46bl5CpPWTsBVrq+vB22Q
Osg8fGD/kJjo+NH1LRZDm2iPX7wJCo/CoWGjAvGLBrEhCIR267XB/qHRs506jE2LpJ1/WDn1YHP6
4hsbD913iNSrp3YVwqHoHkW3q+KaDQ8WL35YVeui1lqTqTiBIGw+wMLyusepyVUWOSx97X+F33rS
5KFOniyxAqxfL08yUOukE0En9TZt5tIDlW08Rrv9wIu8iU9ff/B4jFOQCj3R2wvMFgx0Y3WWZmA5
XyoLP1SiwJ3nLl+nJ/f8go6cPLtpx9aKj1UI350ColYV2tovOOyP4SGBoYlgC/AuOABGABHR7eLH
xolpcvbq7X+NDAmJiqcUGvcEOTgBSjcpPYueOKLTTFLHTvzXqPAd+/bgqEBPDxBeEzmFxveIcKpr
qUShOkwKo8oXTKLeVdUFxqSiPMzFtZt3rS6+pqFl76Fj4GwQ/+j5K4gKrBA4eGxuvgxFaQBtqqpa
29QGslFyzPiJcLQwCnibo8flwsAyifpQ3zLCP+qP4aHD/SIDQ+IjYtIDQxOgQSA/0PlWF4cuYE5R
EeOC9sHvzMKFcNU4ufdFYZetYcyDw6NBEhKr129/9OydW2AmWnMJ0tZd+xYsWYE5h+V/+fY9PEz4
pTC8vTbYP1BaJLqNkPFL7k5e1+qfvvllGSe46Kl/iV7pUaDUnArpkEnhiocX774TETaJFnRidAXr
JSPwNNImQKRdJe0aCRp3/P+OPHnvXSOEQaWXlrrcD+7lSQZINhbQKpLYvC1vm7Xn7/n07N1b9t+z
E9lDz2O4FMJTM9YbzBYMeJtluyzNwHK6FfgsvlKiUIDB3B8YsLonzlyAFYqOT3Vw0GIqPGds0FxY
D4QcCPSHjQwuXLiyoq610eJqdQhVje3tVrukagheEFwePXXxT7+IsOgUzDdEAjLWbnfDRxqTMwHH
AViM/Kkz/hwVOLVw4bvqugtFd4oeQBolu4eDuQuJjF+9Ycebt6UXLl91I6jpQT7GDU9GVLUlq9aN
Co3xj4iLTx+XNm6Cf0hUVExSQFCEX0BYc6sVTIEWRgUEBgaHUkkywFoQZPXMuQtRMXEj/AKGj/QL
Cg1HmZraephQDNO4Aqnv3nMoMDgyJDRmXE7+mXNX5hQunjZzdvHDxwjJwOMRMSl/+YUsXbV20vTp
E6dOKX70BCNDLaWzJ9aRLyDJGHhLe8fzl6+pzYQLis4M7jcuN9NnzWkLGB5N0DAIw0S6rwa7AS3S
mtQwQCRcPCGzt1dFT3oaknm6ppFToZo0etaDXjvVJEHnLQKZMv9qwbS9kAmFOOEiYuR0o5fljWsQ
9OI8ohwR0+HRLNU2PWHyrd+Sdp14bOU0N0aLwggqGXkMUFnoG4aOziBModYGuc1cdPb8844LD5oz
JxwurZYEVeWVaomeOcLiivSERSfMkRgwsz6HboXZ7mfx9RLVf4FeoRDlwtUbw0ZEjQqMdEku3xYM
bUrOX74O/wfqOWnMhBWbdq3asmds/ozTp0+jO0gL+Ob8xWsj/COTUrOL7jyFMUEVOGOwCTAmTo/5
tppdu3YNGzbsd//w0Li0wOjkU1du8zLElcxZsBhmB+IHFzEyLsnuppeAewVlUFm+fOfxmInTI5My
A6IS6T0H8annz10RRVUS6GWN0tLStLS0NWvW9GrrLBbL06dPjx071tDQgCAH8BZjRGLX7XbzPI+h
IY3RsXwUiEsaFxAS++pdOa9ITsGFQwOZbVaAFTazuoIVMHe+ECqVBBXmR1Ho/TxrD937j7TDaw7X
WO1gBVYEf8Dz1Ou2uPSxk9Y9eMthXOyKCMB698KQKI+qCvDLalxq0LgDIdmnH753cWoX8+VbV4XZ
gprQYYGgfvVGuxY95XLhETVy9J5lm4oc1AOEqpCpt+dTi6HXOel2iO2y9LfgR0sU4u+R/nHwvsSu
Np1F4dBdM+YugOvy+8iAYX7Bo4IjgiLjsrKyUABrg/V8/vJNQEjUX6OC58xfCj0Fs8Yu1yAuZ3EU
ADZdsGDByKBwVI9KSH1dVsVO0/OKeubi1amzChEgwY9iZw76Aa+CRNJidd168LTyQx3HCSAA+RI9
BULpgZw4nU78GsVNYGYYG+EXxEiSxG7Y9V1U5KOWy+UCzwEG1XRpkcbR2IQxf44IufPgCdxgOMM4
xJoCWPVewQp8J4mCH6fpHIJShyAv2XDzj7ite+69oKfhBAyTTjsVKOOKbV2zkpO7t7qJc5NW5LMZ
A4zOu0DVJayLgyMpM17GTd5b0mIF6bCe5mEDXerSm5WwZG5B93R4XLHTnv9n2rWQiTfeVXng1gr0
IrNEryB0enpmLQO9zkm3Q2yXpb8FvUvUl2KA1MAcV31sCA5LDYlI4LpesMPEY4NDgIkpfvx86tSp
BQUFs2fP3rJlS0VFhVmIOjPqxetX9x897OI5+owN1lTR2mwORFNQemYhA5ArjuOwolQUzcbpuQ3I
LdJIsMzPAjMOaNTnoleeOin9DNiEeFeLgWWaO30ABeKTs4eNDL1wtQgRJvxk88AggdHQjbBewUoy
6KpdUeUOj7zxYPG46UfqbISj0+kgMK00KIIWIaKHvCipn7Nin1ul+lElHayub2uf+lWo59nqURNz
N8ZPOOKkmgNOIDRql35ZWZamN6UpMGpqZRufPftgZP72U4+rXPSWV4fx0D29Q4JuapenLbvB9xBL
9zoV7JC584X4oRKlEHqeQKT3CoCzu6h2Nhu8TLkfIbhxigEN0g1gWh+AMgSTYf5FXcKiMMFgv97u
IUUGMfDoOQgdgjXWOGQJGxabnog35ApbvzAIoOeUoHwhTjw2GpZ7HYu+QYnusVos09zpDeyoW9A+
1LXDPMo/jUQRiYdBjUpdEZF5wKG5BKEZAoFIjMoV9TV0kSN3rlfNKDzaImk8sSoInOilJ6OuT2uf
+lWJ3SVPmLd9zuYHbr0F0Sc9j6DSC1WsJAMry9L0xIfkqap3jIhaHzPuAcfRh9mpINEVBesYG7Xx
Nlae1e0G30Ms/ZNK1L8POueayRXbehFMVsy7WmyXpXsFmgAzmL8GvOXZ+WhwrpmgacIrumA8WMUc
RQCyh+qiSq/IIcHJ9GKdoblw0JdaChDGSLp47Wbxkxcoj80LHDUo5wmCew0NyfdbuNgJ27fteWW1
wbfm6Gk0RCwCvYykEzs6eljmnrLpeq2TBoRogTXOWmNpEGecOleJKqm8w6LI4xcd3Xj0KQd6+4au
gAA3xgrpdcva8RvlfyTv3X66xMo7zRJ9g/U+cHxdrZ4YkqgvQ+e8w2Z6t16WgRUz+PLTLkv3CoiF
W1Q9kszTJz+6APJT09B8/sqNt++rOiVKv3zjVk7uhDt37jAOBphEXbh6My1zYnxyVlrmuIfPXiEH
7N6VWgoQxkhKGT02Y+x4GG1sXqBN+MyqKrg10q6ShXvuBCTsP3T5VSvXISgiBAMeA/Uh0LUitav6
4YsPD5954KBPYzBR7DJellZ0mXoLuu6R1Q+Nbf8RvmHz6YdOeP5GGNYXCEevOVhk/eFHz8iMA38k
nrj2rsMtC/Ta8ufAeh84vq5WT1CJGqy2jPmkS+Xd9a73twPxPVpDoM96AZDJfrsBBHjzocK9JY1R
UtrwS4Mrgzbks7gfv96S+AVQlx1CmtY04G0QAZpMbxKlwTfg9Ut7BQqAR1HdW4zR09BgfoqCk9SL
V4uSM8bcffgUh7y9AG5Rjk/JCAyLAusb99fqN+4UQxJi4hLCw8OtViujnF7sItrJc5f9gmJjEjL8
gsMDQiNpyE4pVEUB80bPqaBTm83GLBsqjs3Nx1bT0PL8TSl2GTEvX76cNm3ahcvX2mWy51rlH5kb
t+1saPOgiw56KyLmAxs9D0HF9VG1a+K8jfS0KaigM9Q7JE2EuekQyLtGPnvunoRJd6s6QJKF3sVk
gE2IL0AJIZWKJu88XfVX6qU/0p+sOOZy0GlxomOzkAHvXDGwTN90twLfjn6aHUyJYktr7nQyq7nz
zQBHvn79uqmp6cKFC+z0GpMudtQXYBdwBkujJCpaLJbGxkawL3t+Hvnl5eWVlZU4il0URpsnTpyo
qqryVgQwJ69evXrx4gV6YW2eOXNm69at6BfR3oVz12KikxIT0seMyX7y5Em3M37d8Pz588mTJ4MS
c98g8sOHDxERESAJ6YjYxI3bdsEQhUXH19TU+E7dlJlzzl665pGUHXsPzl24lJeV8Jj4PQePCLKy
evXqVatWsWJMoh6/eJM7cZaTkx2cOHnGHDR48VrR2DF5oHPunEWVFbWxsbGhoaEhISHV1dUY4OIV
a+i1irRMSOz27dtBocvlCggImDRp0ojQ5JFhmVEpMx696RBVB/XXaBQK48QZoSixCeTAmZdnzr91
8pAzjt4QRK8X9Q43TzpcZMm2R/EFlzPmv20TFRlOI722ZJ6bw5DNogYwfKDoRU1c9o7CTZWJ067d
+dhhoywGXaCJna8uYujGBizTN90rn3wL+ml2sCSKVofb0dbicbkpVwqKDia49+gZjiCf3nBKbyKm
bOelwziXRT14bDKNM+nJBvbrzTcOUV0Ipyg0Km5kYGhQeBRHb3emd0vcu3ePNQWoikyFg15NJEs2
7GSDunr1KnjIPyTiL7/AiNiE1Ru2oLUzF6+Cg2MSUzPG5CDwWLpqXVB4NBgrMi6puLiYPX4PoItx
eQXVtY30IWpCKmrqQiJj31V8QAvYXb9lB4zArv2HL16+isIQM5MOHyiyJArC3eJH/hHxCWPyPSI1
DVYXVeh1ze1g4tikVFEjT16WwOZYXfR85ar1m9etW+fbIKj6UN/MwVmqb05IznnxpjIuOc3isnsk
6fT5y3HJ6XALcVQ2rpJfv1U8cXIha98/JPzR89dTZszxCw7Lypnw4OlLdA3635ZXwximj8lxctK8
hRvGjp/SarNcv3stKCLZLlklYtt9dM/sRStGhkDMkl69LhV4l0Za6PqhXUiSKiDZopMZ265tOlFs
TJXJPPSpC/zBHkpCvmXC6+0iqW8WrPeetMTmrN9+qTZvzl7JRfXFJzagV747T0UZF6ZtpKSZuMPG
v5uy6lCbTNJztzx91wr16duXbxpN+e72THvhm8nSvdZlYDlfisGUKEXWL1+8W/OxlYXREKf5S5bT
ywiYX50epeV8OmIShWzj15A7XX/88rlHoneUIlM02kFaUHW7R7x68y6YGJwBWYWAvSgpW7hw4Sc9
AZ2haR12Z1z6uPDkMczaQEKioqLyJk1LzRwHaayua6r82JCUnvn4xes3ZZXRCcm1TW3vq2vh87Ra
7ItXrJ4xY4bREgWELSo+yeL0QDtYndy4vPyjp86hUWyI+9dt2gZeh1wV3brj8XiYH9UDdLl27z+0
esvugJhUJ0dvWjcGS5IzsmbPW5SYNpqTdfD97PmLIVroEYYoLy8PDGc2QAj0SIfdhRlobLMEhyef
OHN13IR8jyzyqnL4+Gl6xyO9BwEqi0rU6QtXw6NSI2JSgiNi0KaLl7bs3GvoIA2T1txhhSxhDuET
5kyczCvqnHmrUkePd0vQYtqwoOgO3mOTuJEhSUGR+aPCsvyCExqb2jVF1OlZCrrIsJ2iLMJRW7H/
+rn75Z7Oy0eMVBQxFgNromI9Bb2O0zxuhazb+XDL7qc3n3NZs/a1eCA/1J32Lpzhcxv/4UOripvo
p69bIkZvPVD0rkkkUxedefHe4dY0XuGMrsy+fNM/nUShUYDtfy1o97KkLVuyccLEGRU19eCPXfsP
jUbUq9C1rPxYj6WFp4Gp9LILkyiHR3r8rKy+pcMtKqgI3Xnr/qN2uxtsB6387NXbk+cuTZs9b8bc
BcfPXNh3+NiajVsPHD0J5oPBgTflpZxe/yN6zoT84UFRcZl5YHHvvCCcOHX+UuGiZdQIrNuE1jwS
PRW2aPlqCAlMh5OXwHzZEyYVFRXBkKIWqj97/a5g2iwmz4YwR8CSwDf72NDMScr+w8chbwmpowOC
Qy9fvgyh6m0asUuVr1NSIVEWhwtkg4Zlq9fvPnDE6vJExSfD3kK286fOdAuIuPXTF67ExMT4Op8w
jLAtkk6a2i1hkanbdh1Oysi0eVwuXrh++/7ocbmohaNMok6cvZSUmnP6/PWPja1UGenkwJET8CQR
fSE9s3DhirUbGlotew8dC42MhSiuXLsRegHDt7mFfwXEXLxXGpEya1T4jDETd12+XRoek1Hf0IJw
mC6VSk8Uwjy1aur0DadvP6+SscaSefGHjZ1eMtJoaU7neOIRNO7WA3v+rDMPyh1PSsRxBfscAnGK
vKTSNyD41OIVBY6BAo/84dv3o+I37zr7yqpoLS511tILNW0KRy/sstP0Pn0ZYFOEhO8hlmaHWBpg
h3wzWZrlA912AZbTK8wSvZWhEjUYoPTBCu3feyoyKnnb7v0wKQePnUxMHe2Q1foOS8HsuYFRsQkJ
CXDDWAUAy+TguHmLVo0eO2n2vOXQ34ePXxjuF56cnnP5xi2PpF4pupM+JhtrD5csNikNa8/usIZw
QvyQXrNmzadRQaRU5cr1m/uOn0vLncpmjQ3b4uTG50+58+AJxAPiNGfBEnbSDMIJt42Zndel7+EH
Njc3s+AKtZav2XC1iL43B2Iwb/HyP0cGoCIUf1J6FrgfDN1qdfCKduV6EWQA4Qfr0STGBGhjEqWN
jEisa24DX9g9PIaAiGjWvEW//TVq4bJVRXeLE9My6X3uig564uLizNoGYF3Lq2tBw8u370cGxFid
Ukhk9OOXz9yiePzM+dnzFxni9EmiUtIhY/QMITZI0cFjpzBR9PF4VQf9W3fuhUO4c+9BjALdbdqx
LTw2DgkU/i00KzpzWUrupv8KzsmavnLN1tP+IYn37j9WFfoZdxWUq4qVkHk7L++79pZeTodZNp4N
80JXFawBBM+i6h84Zf+BhryZh1ClQZOSc3e9KnPRF7vovEhMa8Ogo5JGPAo5fedd+JjCzXsa2/U6
K7EvWFN673krfHiIKD0P0jVIM2fHABbLzPUBO8TSKMB2GVimufNVYC10a5ZhMCVKEtXDB86O8gsP
jYrdtH3Xpes34ZMgGC+tb/CPiQ1KSIyMjBw3bpx3MJCoxrb2qLjUA0fO79p7Eqb9bVl9aGTq9t3H
rU43uGTbrn3xKRmlFR/g44Hhmjts4OxRQWEdDjc0PVy17Oxsr8VjIgWWffymHOyLOAR2huHR81ew
BmgEXAX7MGHydKwsNOyh46fWb94OcUIcAnGCEWOyhLba2tpAfJvVgaOCSh8Wjk5IMd7coME0NbVb
aS1JASPWNTTCsayrq2OzzEjphClRH1vtfwbHXL5xG8YGBufC1SJIzt2HTxDdlX+o7XB4/hoVeP7K
jTabC97anj17aDzYuVp7Dh4dm5uPYpOmz05Oy221eDKzc5MyUrfv2QNKip88Bw0ghklU8ZMXY8dP
g/xAzJjPDImCm4ddlCmrrIEPiaARpilzXC5s47Xb8CFzLXZp2szloRmLb74U48avCxkz5s/k4fkz
1y5esfXQkRM6vaLOq8TZLknzt129XeaikyJLRNHoJVYf6PTt4fS669EbH+ZsffbgdVuzUtdEGqKz
z1Y6VKeoa6KLKCJYwqxgQHWRD5XOnEmbZ6wpquaJk29pspEZa4vvtdmMK7f0RlvEVxiMWcEAmxyG
X0CivCSa+58HFBO9Levo4dO5E6aAZRNSMyZMmhaTmIL1Lqv6CKuSlpUNRo9IygRbG1XQuO5wuQMj
4sISM89euoZlgqBAob59XwVxAsdv2r4bFdlZLBx1clpyRg5MB9xIHIVXNiYzV6EXcNAg4nhUouz7
8FVZaGImOmC+E8aSnJy8e/duo1Ny9uxZ+lATIgNFX7Fmw+3ix/AwHyOCnzkX7h+YkpIhCdv37h4z
ZgxmACYLLZw8fSEoLNEtkDaLEBAS39CCgJlKGsiAQ4igCINyCQqGfOzYsZ4T/eHDh7CwMO/baZjP
Y7PZhg0bZrfb0UVsRs5fITEJWXlBMalutxsFvGcmeJ6HxKIk6MEh5NfW1s6bNw+mbOrUqd36gvoo
LS31Lh9moKGh4datW8hnqqe5XfpYb5cV3emCr6VIgm7hSfyUDQH5W2ocJGfW7ruvG998qCl6dNeu
0pvAqS2iT0R5IAZZ848dvlNqM07Bs+4Anj6JBAfQhfVUBNLqIpHj16VN2WVBhio3uUnOnENPnlkZ
PSAMQL+oRRCoKvAPSXGZFJ2x7cTFGnojJUw4IZnzdl550YzojdViHXkH5Qt2qB+wYqjru8vAcr4O
rAVvs74YTIkCn1y6cGNcTj4v0zcDDxsZAPGAXwTnCqoxf+qMlJzJYQnplM0pqH4DZ527djswJiUh
JQOC5ORluHYNrR1MojZu20lfyWJIFC/r6zftyZ86B6aGPpeu01NkGenZcJW6SdTNhy+C4zMQ2IB+
sNHHjx/9/f1rasxHyuvr6/8Y4V9090HJ+yr0hXgDrSEcmr9kBWKYVRvWQZzQYmxSwoIFC1Adyw/U
NbSkZGRPnbUgISUrNikDIg0fLCI2ASEKXLid+w5xkgqbgLg/NTWVddQNS5YsaWxsxMQyzma0PXjw
ABKCtEtSSipqXr//UF7bzA55VwvCAALgjnqlAoeQBlC310X1XT7WI9KspAa1j3lSdVX2CELHvdfW
7Bk7LxQ3ljaLCzZerHdqdviHiGw0QSCCSG+6gMeluTSyZveN4hILDilQIz6MoamKqiseVbdI5PaL
pilL994va3PSK3VOJ0+WrD9z8U4FJtQg55NE0Qu7ktZiJVOWXlu2+mhLuwIFgkCY590rDhQ9+eCk
5zwM0QVYR6jIdn3BDvUDVgx1fXcZWM7XgbXgbdYXg+X1UYlSFPXWzeKYuBQIRovF/vtwP/gbkKgp
M+eA1Z6+LNlz4lJgdJJLYNoXQ9Jhrq7eeXj2RnFkbOLdh08RLSBegif2vvoj2HrD1p1rN26VsASE
zF+yMiQiad3mXXDAIK7Iefm2LCsz1+0Cj2BgnySqqrF9WEgsVDvGDJ7btGkTXE26kp1LUjBtVlA4
fc4c4uqRtHOXr8OThFMXEhmTmJZS39oMSzV7wbyKigq69gYESXIL4rrNm7fu2tXhcCBQQWcQ6Zv3
Hr56+x4GCkYPG2jbvHkzbA6rNXBgLrxbNzCazR0feIfDYOb2hi4FjHd2SgqxctKZooc58489ruTf
1HCTC3c3uOi7ggQ0S688oZyHviBWI3ZBn7b++NN3bfQF8PRpC/OpGRCA9iAsKFPywZ02af2+y6/h
otHT3AQS4Vm669G1u+UKFUpTNlCFKQWV16rqhJjsTbvOfZQE9jSL7vCQwoUHbpa00pf6Yb9rkOaF
MQwTLIdR4t1l6a8Da4HBzPpCDKZEQRUePngyKiYJ5oWX1c079kCFg2WXrFwLOYGvlTNtXlAMohFw
IwCKdZvDmZGTH5U6DkYM3AnHKbdganhMAmJo8DLiDYQB8OKRLpg6OyAkLiQiISI2EXE8ePpjY0ta
6lhZwgp1kagOjxiZmg1ZAqDI79279/DhQ6b12Uy5BBk2CgJsdXFwSiH/sIoVNJ5x0wfp6QsS4MZ8
imQAuPFuiZc0GCIFEQt6gvzAKKE+EjCYiMScvJQyemxLS4tvxQEDVbxbFzCazR0feIfDYOb2hi4F
dCe8M/D9jnPvIvL2NriUJp5MWb6vliN2VRbo2VdMISaTXkSFMHAqWb/rxp5zxYibkA0eh7rxNojf
RlFfs7do6rxjbU7NpYC7NSJzoqQfKyrLXXUO1aFpWXmAWUusy6Pn1vSpu88/bYAMY0rdGufQ1dV7
is/f59yoIssQJ0+n19cNbBwMLOcXkCjvapn7AwYcm9zcXPAxG+TMmTOhfeB3ZWRkIJCYM2dOWlqa
1frpVTjwzSZMmJCZmQlLgu5QuL29fdGiRQh70AIiDYCVdDgcjx8/hvODwKOjo4NlfpZINAiLAYEE
H3hkmhCp0H0z0IZ36+yfEcNgZnWF4b7C6iFU676ZJQYG1oWXjT4LzKCuuYnqVOiV2pYqu5g+4eqk
hTdqeB5u3oyl29rcnI23m5dx2aB0SBSHYHHF3uJL9x81Ea2VKB6icVA1Ekd4F8fptU6y4mDlHxlb
jzxssFB9pqgKTIsG0bv1vHb3ySarzY3phxTSe8MRVimwTO3tIjdjyfnMBdsrXZyTXi2GdydZRLL6
8J2bzz9y8P0MYOH6HyArZu78TBhMicIs4BcRC7PsUEVGNo0EwNaVlZVs1ws2aziKjvALIBO1vFOJ
hDfd6wWfzxLJjoIsWB4IFXtHJDv0TUAb3q2TIkYMg5nVFeAWdoql5/ZFYF10m4p+QB9CATOrVLTm
LKzOyL1w6PILByENyseZS3dUt7glogv0AwDGg4P0B16fmyckdeLqK/fK6d0jvFXxOIynJ4jFpdXb
ycxlx9ILVk+cv6POpUEW6cUq2G76hK/+9CW3eM2FBo8G3aWobo044BRz5K2LWOudZFj42pxZ910q
cdNH4ah77NLJws0nbr+qN87Id96w8o+RKINpP8HMHRhYeSYYTKiQg2GzXwDSwvK9wCE2d6wAawFl
AJb2Ake9Ce8hJLoVA5DDhBkJtAOvb+P2rTWN9UtWrQDv0nun+4ZBhdm+b8tIAyAVDRoFjCv95o2z
dMiMYIAVYDmsIn5ZJpxhRFyggZ7apu/VJ06OesDs5CcrD7BGLBYLa4dlMhg9fIKZ2zdQBo0Q2YHu
Gx3q1LXnp8y5UNmk1rncLR4yb+VZC68LVICogwXJhgyANrem8ERZuffKo7e1IBJRl1smyzZdzZ53
csvF2lFZO/5KXrN0xw2rSB9zFhU0AAPk0jlNlMRnb1uWrrgNGXVrbnrWXedEYnfqnlbdcbioNC77
0KUn9ibZZTwVz2EOJFGdtfH0m1qrHd4eNeGm3mQzZg6jN7Bi5k7fYMUAc99Az5xBRHeJwjCA79ff
DwDox3qwWQNHYndG4ZyYxPjSygoYqP4lCuUBWFS0wBYVUvThwwd2uQmH0KYgCB8a6p++ftlqs9AT
g53ThXxmSFHl9u3ba9euhdM7ceLE4uJiZELIDx+/iFAwPDq1sdWB6O/ew9d+gdF+QTEj/CLYuzQY
Dai+d+/egoICb44XoAEwdwYARgzUwJ2S1viCTWt333KoFtiaZyWOwoUnbG5qV2T6UCakjl4PhjGB
OWtVyLK9t6/cL+cFBJpSo4uMnnM1f+Xd6RvvLT/86n650y6rHuN98MZztaBIVDQ3FEJplbBk4wX2
/mWqHxRd0uU2UajjSfTo49OX3qyzwVjxEmmEeygQxSaRyfOPPXjXQj80QH0IkGJS/qUj7QusnW4z
yTLNncHGP1OiIAOIu5CAVLhcricvn2fmjLv76IGdg7btb2goPGPGjOTk5Fu3blFeVJScnJyYmJjQ
0NCTJ09CKiBU79+/D4mNCk+ITchIrayvdXjoB2Q5jlu8eDFqoREU27ZtW2BgIJpav359fX09yLh/
/75/cPz9R28XLNmQO3GOW9DuPywJi0q7cetZ1ccWdMReRwEgzgwJCZk2bRoyv5EVmES9q7Wnzdp9
/rmdPimrk/JKafHCuxYXQpw3xmODCpwzejLCuIzrIfKui2+3XWjkQI5qlXRp0trrhYtOud1EQ1lJ
ILKFqB2qiMCMOqyKTIVRIFKLyzVv5Y16BwxWh6DZaeyk0LepVbR60mec2nPifYeoiToCYFV30oN2
uKAbzxy/64LzB2ePSG6Ik9MkfNCYnrXzd0rUPwBg6IiIiCMnz8jG58nmLFgyIiJkWGjAqKiwm08e
OOhdMtTF8uVXJABkzl+yIj45/czFqyGRsYeOn+YV/WVJ2Yf6povXbvqHRDg4AW1Omj4bW5vNOTY3
f+qsQlGS3rx+HRkT7x8Rv3DtNsOLI6ibMXZ8fUsHTBh2obNv3X803D+4zerAoYJps3hZ27nv4IiA
kMzsvDUbt7ILxCiGbda8hdEJKWPGT0Rd7PpigKyAYcFESApxi/rmnWeCcza3O2AvJEHiTz2pnrf1
uJ36quBjjn4fDbIEosHWmsvBkW1Hn19+UO1SdbeiPqvgYyfs2HDxPWaGXY3oBbrOS1qjTc6Zsae6
RRJo9AQZkyWF98ji/muVwzM2FL1p4xHE0peJUQFUiMQRsnz/9dvvm52fTgSaYEP4dfEPlCiYET8/
v3OX6U0Yh4+fDgiNrG6os3icDoGj7E7o15Y2b97MWIRVYQkYqJGBoXcfPnHycs7ESWBrlGdcXvK+
Miw6rr65HenbxY8Dw6JevXsfGZd08twlVdMrKsq37dzzV1Dkqm37mUQdO31+uH8QBCZjTE6HwwM5
dIvy3IVLQ6PiIJnvq2s5WS15XzUqMHTNxi1+weHbdu9HxIKKS1et27X/8OYdu79FouCNwX9y82Tx
+rsFi+54PFAe9HHYe6VtR68WuwnhjTZ0WaQvi1AQxiHEc4oc2bTn9vIdd+2QMoWcuuMaM2X3+1oe
I2JgvXeDW7O7eLJoycNaq4sjqOoRiezS9VZBXnagNDZn75tqqrAMMaeXHGCdIE67LtYfvFRJL0x2
3r3uhdnZL4t/oETB+MBGFd0pdgvyuLyCtZu2YZV8t927d8OLq6ioYCcwALaWNTU1wRExrVYnmHvB
0pVIg8UFVWu1OtLH5KRlZYNnwOJWJ5eQOvrPkQH4BSciTAG7iKoeFJU0d8VGTqanHF6/K79843Zz
h3X+4uVZ2XmwSBaHOzuvIND4Bs+5y9fpLYKq1tJhQ4Ov3pXHJae3WOzFT55DpJ++Klm0fDVMZXOH
zcvNAwSMCX4R1tQ0ijmT1m7cc88KN1fm4exef1a7+8xjjr5ojb6I0xixCoaGY+ihPpyy/1xj/qp7
07c933m65tDJyvjx+yuaRQgj1UIGjCqfwAy7Q5fXbLl57bqVp9fx2uFGonSdR8+asz12xv7KFuO9
OSpPaLSmCgpxKOT8vZJtp945RePpnU4bZUjdp15Yuh/0U4wdYjCzfiD+mTYqPj4enhucLv+QcIvD
RUMn342QAwcOuN1ulDRqUEAO29vb6X3lIn2LwZade42HIDRJJw4Pn5mdC7sEFoeM7Tt8PHvCpEvX
b+VMnDxh8nQPxzOJSs8uWLXtAMqDhdhlX0HRyz/U/THCn5dVGJ9RQWEuQUYaZtN467Im0kebSIvF
gZyKmnrIPwwjxCk4PBqm7MnLEjT1RQBfwvbuOf0sbvL+Rp64JY9O2hWFO3nrxdUXLTAiRJcoIxtv
d4B9oiZC18Dla/e9GpH/+n9Ju5B7qM0v98rEJaUumXh0+viF8douCsajXiAHsd/C9fduP2/mEbHS
K2v0RNCTd20RY7edfsq3EhlhGCItyA6hN7CrsHeni17vOv8co1YEj656PJ3NDknUzwvICWzUjr0H
OxxuMGhzuxVRCuwGpAe/YH1MNHQ5lrDbjLe1tcHywD3ziDLctvCYBJggWlEn8Nzyp8zYf+QEJymI
fK7fvg8jc//xM5R3uMAVVCAzMjIKCwuRYO3jV5SVO/SLHuFOXqR3OYUniwqxucRhIyIaWjvYBwps
buF1aXlsbCx6B+WQB7DpihUrUlJSQCFaY7T1hKkc8J8ODBQ4JdnapuhZC28mFZ57VW8HF2sq71T0
FzWuncdv0LsT4M1p9LkMep4O8aTKc8Qm6OrBSw3jV7X8f5KPHHiqRozfcf7aO7ujF75kaV2WiEav
DTgl/UW5Zc/FD/TUgtKsSppNUYpedSRmXNh/vMSl1ynErmmyoWAg6hZEsFfeNczZchqyyppiYI3/
JBggSaxYr1rgHyhRMD6pqalnL12DqcmfOhPGAQnIBgQD0Qs2DP7ixYvsTLdZxwCcQARLpZU1LkGZ
OmvupOmzIX4fG1vB+u12N3Y3btsF9pg9bxG2FyVlK9dtQiAkyNTRAutPmjRp7NixovFC5gcPHsyb
N+/IsRMhETGTZ8yxe4Sahpa4pMw/R4T4B8eMyZkMY+XkBYgZgjEYw6amJtRicg5cuXJl2rRpSPQj
UZ2gr2qAc9qmkj3ny2Kzt77tcFp1nVPobXFuhaw5fO3S/fcwVfTcW+eX2AHIFTbk79h34vaLhrip
J//3yF2/JW06eafWQ+n4VBJgPZk7OmTT7RH1HfuLtuy556K3kRNR5OChLt9WnJG/06kRh6zCFMKp
NN51jkBNbldJhU3afPyBxxAwsykDrPGfBAMkiRXDAvnusvR3kSi07g1Rfjyg5rOysuhN6xppbLME
hUcvW73+4rWb6WOyCxctg0S9f/8+Li7uxYsX3lnwAkIC0xQVnxwRk4Boh5N1CFJCSkZiWuaooNBW
qx3uSsn7SoRY7NUUcMwEGihQ7N27d/r06ewjYpCrtWvXBgSFwC2EOKEENpcgrVq/6eL1IpubQxY2
hGQNrRZsxop0gXe1GFimuWOA3q1tPATthOQQMnrpmTHzi648cLq0dlW3SqJq8ZAjV18+/2hzwwVF
C6iDJlWIiuoQZLtMT1Vv3X9jy/YjVl699Lz0/zey4NVHAU3J9CyG+fwF64ulGURdtOl8VaO4fusV
ehOF4oIv1yGRrBnbxk4+ZuWJi9CXVaiwS/D46MVi3qaRZy3aom03edBhWFTWlG/jvY6XwcwaPAxW
s6C5Z1ODL1G9dvMjAQLA2QXTZiJQ4RUVcX9MYir7wvSMuQto6CIIMCZFRUXQxWadTiBwOnX+8sp1
G99X09sFYKPAsiXvq56+fAPHj56HMFgCXtzHxha7h0cX3nFCiWDU6B3WhiUUXXfyNFZjEkWvL6sw
OvRN+IiyfDdjwroA1c12DbBMc8cEvXkIdStatAkLLhSuvOrSiUt1GeZFaXerU5bsL6mxwdzwuvGl
DipO2AS4pByCQ0LWHb9161k1zCKOWmTZrYsCXDrYNkpgl3cdszSDW9dKO8TZy87bBcLrolOxN3j0
vIUnHjyvd8GPVSBKcAMk+q4eRFE6IiXxRR2/9cSzdsSzMkI4mcqhAd/Gex0vg5k1eBisZkFzz6YG
X6LQQbfZ+cEAAZAZ+FQwR5AB0bjjpam13eGG6qW3rAGgkE2HUeMTwF4eXpQRLMApMu5vE+HAQK4g
J5Al+vw5fd0B8ulno2CLwDysLQNoAZzLnB36okjUMj6ExDZekSBOnZshnEyiIKj02XCzGKuCHtE+
2xD8wLdEphE50bMJEu0fI5OPXmyKzdl94HIZvDpdFzxqg0Mm7S41b9ZWWAZ6i4cIqcdo0B7aVYjU
oSv/f/b+g7uKZG0XBOe/zOrVvWbNuqv7ztzuNf193z3nVBVOQt57L5xAGAHCCis8CGGFFRJOeCfh
ZbACIZABee+1t7bf6d08kZFSCSFUVFHUoar2QyjJHRkZ8YZ7n3gjIyMFsl3UqRsFz2oxZMMgWJIE
MuumOiTykiG6HBGFtBS9KgGGHCT0B2Ssj1FjM0/1WlyixrtVR+mHkekx2bcr+jHOkySGJ8Yjbtdl
lHmXylZ2O9YevAvdREa1vENRcddHkY+dk7yNgl6iMLx+E4woPo6Epmj8+AoYUX8c+V/QjqJgBIl8
3lNWSx6Q1Q+G7y/h07KmPqSJG1SDHkVW900oxzEgAI/GQ1bEjjVLA0Yv0jmKTHbpeypRTwwv9e46
9jl6FepAf42f9Dd6dOOPsIzCyCKjKT0md/LCfQu33DOTJQvk62NIwiqJt153bMsrYQmFkdeq9e+a
UTnQ0kWZs6Onnjz76M6LpkFDZCOPU4DMvSA+UXW6tdTVhd1IT3ExslBR1+Ofkve0nlCuhk5hBKdy
ShzH1vTwqw8+HBwd5k2Bz5XnV2LSyKmn8eP3xl+2R2F05x8SsWjZCpj+w8M/fyl0anxa1tRnQo8a
/3QYvQvjvTG7EayIztDVP9g9MCSMezUIcAkcHLldEs9dupa1eRvMth1799PnTkMWsggjLCpuy449
aKAuXsJVCA+7LiI28dbdh+hm+psZ3LCNyVi3Nyz1yMLNd/s4hyDa0NrJIglFq222XrrbO+REn9TZ
WAb9EcOJdCpCndqQoO069uDxsyayju/jx3FTQFMdGLE5JW197q3KJjMjcS5JOVz0wm/uweZhk1UQ
OSREPv9gBCdcqmgtbnXt3stWsq0EGQZPjb9jj/q1cvza8L8BY/HjhJbaGB2hefUMmmOT5uQcykNz
xzhw6jpDxzCbzT09PQ0NDTRkb29vbW1tZ2dnVVVVfcMH0A6vyI3treEx0XPnzh1bO5uRkeHj47Nw
4UIkQSb6VO1WSdm0WYHTvYLyjp+mMQN9fX1pSxc7OZb0KFlMmrNgupdvaFRc8ryFdjfr4sVDx07F
JqUeOHpipk/AgGnE5uahEUIjY0ufvhwwkw/ck4yRfq3lHin40Ttm7+mHYCfQmyo5RV7FYK+k9N31
kjq0YAEjPIWM53SqAHkIUAlkM1ZJ25P/5PLjWvLNEkJqH3V4xAUg78bvcUD5YqC4q+DByVtVTkm1
MXJJpXlW1PqadhfpROQWEKCgf2xXRr8VOY13advO3jfDXCNcabyjMVYF9CcF9fkd8dui/fQu6jN1
s/kUf9YeVVNTU1xcfP/+/fLycrooFjm/du0afNB88dNsc4IE0HADQiMvX76MW6YoGnS58+fPJycn
BwQE+Pn5nT171uFwrFy50t/fPyQkJCgoKGVOqs3tfPu+zicoAB1j2bJlERERbjeZZy4sLLx48WJO
Tk56ejqisjkFb9/I3MNn3ta1/TjNu7m5Gek+fvzY19d3doBfv3mYkQROllLmL9qyfbfF6Xaw5Nun
YNQ1G7Zs3r4biiAqPvlN7QcnJy1duQYce/VmcWtXL3oS+irrUI8dOe8bFBMUkzLotvMY7ImawCkj
Ep9z6vyTN10MeeWIFVWHqvBkEStuQ8dBQ8cAVNQuFFS8ru8jL0tp6Hd2lqxW+hm0KCCt8XscnJxW
/Lr79MMGOxmCKntPPlq+50mvTeTZEfKKE5QN6UeMrDKka0ka41CO5ha/H3bx+lMwdG8az1gV0J8U
1Od3xG+L9tO7qM8UzWZSTOxRNJYxGL7fEZA9shZz39HTM4Oioc4DQiN27suFjYFB0b6DRxNS5sF2
QjPNO1XgExg6P30ZhkzZ2dngLrR+uu85PafRYZCGwIfyTs72D96xN5d+yRMdDAE4Se43jfQNW3yD
ws6cv4QGvSf3cHB4tJMTmjt6EPmLqrekoesbLze1d4FSkDQUNcIg5JOXryEe3YkSt4MtfQNj+4fJ
zly8Inn7B88OCMGgbnXWZgfL4a7ahhYkFBYdF6ZvY2Zzs1lbtiPOxMSUwIjEiyXPy+r6Zkfu8w5f
3tDRt2nzzp6uIVWTWNXR0OdavO4eui6dYwTIzIL+P1kIq9tiDk5bnnX6eSP57uAXAl0RXUPUOAsn
3m9wbTx22S6pdrs2Z/HRfReqrRhVonXIjEFNKqto4Cv0KM3BaFm7rjaayOYRE7rQl7ROGtL48Zsw
PgZ6/mt7xdfgz9qjdh864RUad+f+o3uPK8Jj4t+9b+L0vfgCw6JQvY8qnsMgKX/+yk2aJhnvoXZL
S0ujoqLu3r1LYhktYnACKCJxzvyE1PlWF5nTgiftb6y+UPX02YtgObPdzYhKxqp1GKShhyBkZFzS
+cvXKXU8KHs6J20x+jN6FMQovv8YfeOHGV7gHPQNJAHCqayuDQ5L6jeRTTMFVc5cu+HEmXMI6eUX
dOx0oZ0RGts60QPTFmcEhUc1tnVxktLS2c2IkN85d/Hq2WELQxI2/eibebjg7t2yZ+hor980CLJW
12DLzrnRMkQ2JAOMahuF3rXUQbey98iV9h4XxDMufBEkQRPQY5p63Rv3FfW7lU4Lt2Xf5ZsVTTby
EIx0WvJ0i3QndCrioKg4Qcs9fKeqqR/DPU+P+hmQgIpi/P6j8AXpomhIjzp65mJYYhqaL5pJycOy
2MRUdAzwkn9wOBgjPSNzhrcfo38xEQ7RAiMjIxiYLV++fHwSaA5ozeCxc5eukQmrcUBXsThJz9l3
4Ag6hluQ123KxmgNg6ohiyMhdd6GrTtAjGimz15VJ81NW7x8FW5Bous2bd1/+Fj2rn0/zfIBj8EH
craQz6XGdvYO6g85jf1eeP27w8sy1w5b7CERMYUXryDmDVu2I1EXRz73C2fllaQF63/ySj5/6VXi
nFUz/IO9QsKCYtLSVu48ePLuwcO1LvJChvFiEc2avmxPRvwwv9pH3Fm7L/VbyLc9NdlFg42Bhjd+
fAxGNrlk1Spo+08/NTnkHjMXl7btRkXLiMhxml3AgBMgJUY27VGhmDgN3WnVxvwBm8iSXcz0AKOg
CaEWjN//VlBhKH53kf6sPer2o6ezgmOcLJkJeFr55qdZs0cczLXbd+nesWlLlmO8h7ZOexSGQ7iT
Rkv5Z6wcEQY90Dcw9PKNO4iBelKgG2zbnYOBmd1N3mgAt2zfnROfPBcRMoIyb9HSzHUbeYyoyLyc
DDFAj6/e1vUMmn+Y4V3X2OrkxPWbty1cugIBcHt9U1tEzLyqdw26SCojqk5WdHES5JyTlt49MAyS
bO7oxtVd+l7kOLG5+RG7O3b+ce+gZXMWrNEpiLwb2DViDkpaGb9k/9XSWofKSeTlDKPEaOmpoA7Q
haa967PP33rcPiBx5FmcSyQzFR+Bhjd+fAyYaCZOO1hY0enWnrRKPrHbmrvBWiqZ4iBPBkgYvRBR
aBJ+SpyWvftibY8NPQkhCL+PA03o79KjEOkYqK/x4w/Pv5Hq59NVYUzrE17lL9/NCohiRRkNneyL
HxppdXFnzhXN8g20udhN23aGRsUygvEFXk6URFkV9Ue9GJqML0f8hD7fuHnrpq3b7C5GUsgbqTIM
FCh4TgiJTdh16MgIw7tEGSO3c0VXMUhzsHxTd29oXMKth6VuWXUKMisqTlYKi0q6fvthc8/gzIDw
J69r69t6kuctKjx3gSw5UpTX1e8ioxJ6+4f0yJWFS1at27hzw5bdM7yDnrx4A5stM2vzT35BvlEx
oRFeD569MQvKph0FUTGL/8t3YWDAwpp3Laxss0uMRWRr2oZmhqQ9r+lnyYfYMP5yyxAXTZws5eMw
zEN2OUWp7+VXbiuwSRqsG8JaZIHFz6DZH2tVAMk3Chf9C1ERq0jen1f69K29usOVsuzo4+dDLkYj
T4rRVVGm5DUQshW5hvGeJjGCejDv4ePnnQ5RcksseVr3cecdS8j4/S1BMjOaED0H6E8Kw2sUhu9n
YAT6AslpMNKjaG4p6LXvFnp3Ij2q4vnbmISFZrurqb172co1sJpQsbBbfAJD3YLYOzTiFxz++l09
DB6yO1/5M3CIyebcvH1Xv8n4KCCNECfIdV1dXUxMzNq1a8EwxQ9K0SHBXeVV1TMCgnwjorxCQkNj
4m/dfdg3bEG0wREx/lExSYsWW3hx0On2DY2OiJnjHxSTvmydm9NcirY2e+8/vQK8Q2LQzyk9IpXe
3t7o6Gh+9Fty75u64pMWBobGHz1xHrLxitI7PLTvUO78RfPLX78CLWTuL03MOPy0uuN6yWOBZ6Ev
3vazrXbhwt2GNVvPdw9xgm40ITbyH05QKCoyJoKzbG7h/O2qffkvySrW0Znr8RjLvvFbhz4jRwxO
9AZZFi7cq614x5S+YfwTdr1+0ceRr9QgPH0y9/O9nOhAdz5T9Px+ZadTX4VMr04ADTyW7jcFTWv8
+dekS1sIYPz+PGiwP2uPunHncWBIwuHjp7bv2Y+Gi26APgBjxjcozMkJ4BOYIn7BYSvXboChEhWf
bHNz1XUNQeHR5y5dRSsfK2I0IBzhk5WVFRsbG5c81y8oLDwmHkPBnfsPBoRFoS8FhkejI50tuoLR
3ZETp739g8NiE95+aORAaJpWePFqRGzS3IVLhy0OxMXI2qDFfulm8e7cIy9evUH86D+0ViwWCxLi
9C++oZ/3DVsx8OP1qREW4zmVZ11m1mJpcWkZex75puY+bxnuYTmFV12ifVhWD1wfKq92pi7J6zVJ
Iw6Zki2EJ9WGE5zCihNUC6M+fNaYd+aRnQPfIt1hGmA8xrJv/NaBYgW9iZpb0MQB20jmoaeXn1u9
E3ftPfucbI2pPyDWe5TBPvQut2p7UzeyZdsDB8aZCrGdJm2+NPCkl3530LTGn39NurTuAOP350GD
TW5HfVegghrnoz3KZHWHRabODghBH7h+5y584GCrzE1b7OLJYI9X1KJrtzZm71y3KRvDQhAXPMue
VT4sf0ajGg+UGto6mv6A2Qp+w0iSLIZDR1A0lsG4UBP0VUKIAUyIE/IQE/0A1YRzspsF6ZfUUWkR
IQWNn3rScwoaWF+fTTbxwzVB452Kdvxizcx5Bx+/sYDukAI6BWy4vKvvM/e9SM68/PzdoJtMaxM+
IcOzcSCrI1Sttnkk+2BBh9WtfxgXacDQMz6rboTTAcHGexrnAoZ9vFvhn9abNx98tWB75cp9V5rN
Lpu+7heOBh4DIgGX7Th67U5Zg/6JJzJqpDBC/C3xZ+1RaCkmqwAFTxs6dWAMOJygLephSEjqWImc
g6lw6dMqR3eiQ0G3QNgDYUBHaJEy1DWniOQNVT1+kIlIpgJIJ0BKiIYsciVrRFnyxjescfJ6A13T
p7uPm+w40MAiSYHsUqfKillyLT165z/nP9h9+QmHsSGraDyui/VmS3h6Z3Dyq54hBlQhalZZG1LJ
gyYjKgpOkRrabCnp+4YlzU72tWY11g5NALPw09Sn6lGaOm91xbxV5qhFNwYZSOjU1BHyuEmFTB91
KkRSVla283CrSdJ4rYtknWwQpkf1N8afoEf9NYFmR7qMwmsqq7laut0bct8s2nS3tmNkiLyQhz+3
rMpv2/pC007uO/Z4YJCVycdGfm6vZAyGTqm6VUWUee1mdUf2ias2Fn0dyoNYayTM+PDjzsdAVzmo
YDc40dLtVtJPVAWterw2p9lms6HPGOHGAZ1GlAQomSPnH+45+ZQ+EKeRj8EI+mWgt0ya1p8Rnh71
7wExS1RBAGtqWlWdEjLv+LaTN4lxRJYGGfbKk/f9/2dQ2tP6XifZ3h/0bOzvZUSBkSb5aKds4bXT
11+euf3Ern8xRJUlEoOO8eHHn48BfVKWFRGSqNKIpMWufDQz6f7rRsFqI9aXEehjyBrjltlL1xry
i146yAah5DEujXwMRtAvA73F06M8mAjaMowfk2F8AA6jR5W3iLZXDf2RCTdyLzZZyfcZrRh6qSxo
R+scYf3mbD79oN4lksGUSh6kOum9NB4yiy0wLKccOvfg9N23TlEjnzCjIT4enlHQu4wfYyAdm0yH
o2PnP+n3Srl56KxJRj/Tumj4T8HI3NOaD+s3XXPymlud+MjYA0+P+t1AG5zxYzLQADDYALc64lKU
zbsrghNzX7abRtyqxJC3GjlN4QUt79LL/69P+oPKXhhnxF4iHQQ6/CPmwUiL07Rt+y7VtZn7HJxK
nr6SAAg7qbandxk/xkAMTpdVEy9X9P9n5Mk71R1OjANlUXYa4ccDNIIBXtnzniXrL5hFByiSrGf3
4GN4etQfB1CNSPaINMHueNKoecceS5i/ER1FAr+gYaMfSBpj17acqj376OmIaEbLJvutfAw6OuJ5
vnnInbjuQKuZkcnyok/CjYPRISggBe2bZKZFkxibTRPuNdn+K+BET0/P+KGXEZyCw1jUMSwxT5qV
vccmmSydAh/F8yfB18js6VF/HFSFk1TWwXPv27jZccc3HXjkFMlrEAp5809GjxI5ra3JtelIhRUj
PNlB5ieMKQYDIDe61xLqO/9KSXUvYxHpq1Afh/sYtH1QoM+QTkM+AUCeLEm8MMxqP0Rt2HWmcswi
Gn8XPdcUgXELFW9645YcsvAkGI3mS/BRPH8SfI3MpEeNLyB6DtCf3w8Msb4/wb4coChe1M5eqQ2I
O/aueYSsISI7dbkVwjDk6Zasz7rb9X4kaiJ55AQm+Rho9+hXqG9WJjdzuA8YnYqYFHqxGSBcSLjP
wWsuRpPetZvDE/a9beZMgpNMMnyyWpyeMxpT+XZoT85Ti1vjFA4CjAUbDz0FAuO3DsNrFIbv94fx
4o0//7UgPWp82dHz3xzdt8N3K9iXgDZBq6St3HhuxfqLAzaVJ+/hoRu4yaPd0TwZWUSNGh5TgbCM
/iaf8fsLQR4nS6zmdGjapbLmf0avLn3VS2bOyUIomryROP0JQPhHdab0taedrCaSLV4+2iZpPHDv
5y5R/++5+qiExo+vgKdH/RGAQd/Z2RmZdSP7RDWjgIuG9EfRkqqQ7jRKMBiKCYoKk8WtPx0m/Upf
XPQZkOfDnKZPDvyKhsCjJCWHwheVNv0YceHA/Xan5tLUflWaWML0J7pTX19fTEZlk0XiVYvMu8m+
MzposPHw9CjgO7Wjfq/s/ZshYYwlOkX2SnlT4PwzfZ3M6MTdVBMJk+LTApmi+U4A6Zt674STFYtT
kRdsefCPiENXK8jnjGGV0WA0trFGz4uyUxYGXVrKkkt0Cw3qPwYa3vjhwSg8PeobAmaQJGsvXjoj
k/Y2DDGsSyULYwmmmkiYFJ8WyJf3KL1DSQrZHwlGkHKh3Pa/Jl0N31yCXkIufkxKYz8FVbQ5tR37
HlW86xjzHA8a3vjhwSi+0x71q0Cr9ruqdZj4UOoY4G3ce3PJ8ksjZM9jDmMojLj0B0y/+jnO12RE
JbMcLrdms6pK9qH2fyRdSj76rpdcGPuk4M/AMA9HyG8TlUWLrzwuNbN0u75P8DUi/YXh6VHfCizL
7jpya9n2SxYBAgiaICqam3y2HeefTuH9Er4qI+S1ecauiQ9r7D+u6Jg2p6jX5EKUjGG/fQQUI4Bh
3uYDl+88anXBUhvd5WYCvkqkvy7+ND2K1t+k3eb7AnkryTzIWFLXPVy56fov7hP4q0ALwfgxFRAG
nRZdl9E0l+pgbZx8q3bkfy5uSZ178X0jT96F0lwTn2KRl+udqmTlWPnOrdpzl96KZF9LEJxxnab+
J6iCb48pisLTo353kM/f7Mq9n3uuonXAiR5leP8eoIVg/JgKYBX0GcOpGvuyhvkxomDmspvPK3sY
HoM6fRIRF8dBVFj0HlZVyl4Nb9lbTj9kp6kKiUwHTd3To4ApimLyHoWgFMbvPxzjU6fnU+ThOwGV
k9PYm3e616x96JAUTnLSJ1FGiC8DjQcwfuswvHQYXlMBfQXMQt6SlyWt3e5YseX5qh2tF150iBKP
siRzFTh83DfdsoPV1OqWodQVx02iPoGPMAg5Gs5I/juugk9hSPwVMhv3f6Y6jN/j8NkeRVuw8fsP
x/jU6TnFpHn4TkAlvPtuMGbuAYa8lAGD/rcszabxTMgp9TR+/DIw3iNPuiRBa6gbmr3kwr6i1gdv
bE6yKMMNwwh9BH2ObEk7DqLoRgrv6vrsAqw9WUPfEzVZ1a2wPy1ouX1Ns/m1fcHTo343UAmT1t/e
cugG+Sq9Aovqtwz5aDxf3aPIx2/Rb5Yv3rfxsrLsULFZUyTy5i/GfIR8RE1htbGvc+gQZfQkCeaV
ygpqnyY5EIgMGX/907PvB7Tc/uge9ekN//YeNR7fjyRfAoYxNqb9t4CWFVmdRF5it287Xb7gkLBo
/bthnsyY6Bsre/Bt4elRvzP+jd0JoGUlqBa3xjf2afFrKpYerxlmNUYTBc2tvz/lwbfF5KO+r8GX
98bfN5gHYxA1i0PmF6y+vnjL4x6yp5IgqiwxiH59Z/cU/qSgxTKp9vT0qL8iJM3tVM7fesKhxkWV
vBdC7Lrf0KE8hT85aLF8bY+isRg/fm/QyP+9QyYP/nr449uVp0d58FfGH9+ufv9Rnwce/J3h6VEe
ePB7wtOjPPDg94SnR/060HG58eNjTHHJg78PPD3q12GKbjPFJQ/+PiA9anxToOeeObffF7RUjR8e
/KXh6VF/BGipGj88+Etj4qiP1r2nR3ngAQX6Au0Uxu9fgqdHeeDBVPjaHvWd4FflwQMPvh/8uh41
1l8pDN/JYITQYXh54IEHHnjwu8JQsjr+kraFh6I88MADD/6sMJSsDg9F/UYY5afjz1WIX07JHnjw
5fC0Kw9+EUbj0PGdqE1DGh2Gl45v2p49FDUVPKrEg28BT7vy4BdhNA4dHorywAMPvnd4iM2DqfGX
bCEeivLAgz8HPBTlwdTwUJQHfwV4NJ0HHvwdYPTwyWCE+DPAQ1F/O3goygMP/g4wevhkMEL8GeCh
qL8dQFHjYfj+tWDkbRSGrwce/J1gtP7JYIT4M8BDUR78BWGMFXX8uTqkBx54MB4eivLgLwiDnXR4
KMoDD/688FCUBx548G2BUYIxXtBh+H5PMCSbDEYID/5N8FCUBx548HeHQUeTwQjhwb8J/w9PNXjg
gQceePB9wkNRHnjggQcefKfwTPR54IEHHnx3oMYDxd95yY+HojzwwAMPvjsY7KTDQ1EeeOCBB39f
uDVeVDSNI2eqW1NEldNcVq3DrDXQ5YiyDiO0B38gPBTlgQce/N0hwVKRFU0UNYGHU0SRk3mXxjo1
VtQBljKCevDHwqAojBTGg3r+Khh3jsLw9eD7gFErk8EI4YEHfydMaPkqr2kyTCWXKNtE2Wl3snXv
h4uLu48frzt//rzJZEIYD0tNAZQnmZH8BnOSBkXR2Cl+WxpjIlIYvh58HzBqZTIYITzw4O8H2gVk
GeykuTmtz6aWvrMdPP96RuSWmaFbp/muX7C0sLy8nGEYBON58JgHkwP6X9LhoSgPfguMWpkMRggP
PPibASpLFEWO48BAb9tadp16GL/6uc+i5n8mvv0f4SVLDrypswo9kkLVLnqK51nUFEARfVuK+ruB
ameK371M/7ygXXEMhq8HHvxZga4NXqFO0jinxrCSoPGyNuhSntYN7TlRvGbHmV2Hr8Ruee+18O4/
oo/Hrzh/6kZTv00WFfJMikTwZTD6jI7vRKUY0uj4A0RCKqAoYpKOw9en66EoD0X9DA9FefDnx7ju
jNNxThI1QdZGBK20xpyx93rY0kNn7jc027SXLfb52+/OSs4peNDe5dRcmsYqmqyi/fOSYDei+iUY
fUbH342iQEvUGAV4njeS1OGhKA/+/TAaow4P33vw74ak20zEAJJFTdF/qaImcprJLV28VztvQ75X
yvZFe27dfe+6+9a8ctfF4OR12w+faxlw2gXVJaqiSsZqmiqRRRRkHcVfAciR0UV1GL6/ExAhtZ++
Rff3UJQHXwva6Ck8FOXBvxvCGEWp+vQe3HAff3DfjdT0zUV3330Y1ro5rWZI2njoVvTcDccK7g6a
3IKkOV0iMbZgP4Gj6NQgdX8J/F0o6pvm8y+JKUrMU5geePDbegHCwenqUNUUmZhLMq/JnCazmlnT
GE0QFYvC1Nl79hcXx2/MWbz1YVmVOii7RhSlw8JlH7iUumjP3UetdrcGPuLl732Y9WWlpBMsIVVR
52lekvsUdVjVLJrmVBS3JLkkySkrTlV1sqqNV0yy0q+pQ7I0oGgOThPdmmYzopocRvKfAPxE3x6j
MHx1fH1heijq22KKEvMUpgcefH0voDHI5NVb4nitya1KXWY2r7AvY33T5RK2bVizigKjtfU4+OMX
HyUt2rrnyLU+syComqAnqWi/8+OT3x2/rZQcmmJRZasCUtLssmZitR6LXN/uqHjdd/1Vax8jcqrm
5mA7wvHkqMlT59xIfjJAwjEYXjrw07j5t+JXU9R4GL4efB5GSY3C8NVheI3C8PXgb48J7QH9fOxk
6naCq2OBP4UeqxFg6pB/JKhUYzB8fxGKrMLpf4RpVI2FtlW1AQdfWFKfkV2w6+Td6jabReTNLGsX
tKYu+5FTN1MW7j55try50+zkNTevKppKGE3jFNWFpFEgAD35FZL8ISBFMw6GrwHUI5xKjCg4/ZfI
k4dwMI7aHGppQ//R6xVHr1duO16+9ejzbUffJyy++1/RO4/dfG/lNZY8rtNXkkgwQ+EEI41RGIno
MLwmgxFCh+E1CsP3t8LzLMoDD74jGN1aVWVZ5nmeno+pzvEreo0bRgEfGkaSJMNLB3zo3AvOx6LC
OTz1639OkDk9HsUhKKpD0mAlWBXtQU1H4pp9R850NHdrbkl1SQ5WGW4d6im4eT8y/eTCrdXNHS5O
IhNhEjG6BGI3oFhgOUg/jwPGyhnFSAvqu4egz+zpy0MksipEYEnx9HdJ63eXRc7ftzy7oLJt2KQo
ZllttohDgrb98IvglL3Pa0d4kdyiIqcyJyucpHKcSgzLMXwPJeChKA88+O7AMEx/f/+HDx/Kyspe
vHjx+PHj8vLy58+fV1dXd3d3syxrhBuFw+GoqakpKSm5devW2bNnz4xDdnZ2cnJybGzswoULly9f
vmrVqnXr1iHaPzlFETWsyBLG/O3Dtt0nLsav2J7/qK6V01itB1YEx4jvX3FZK0pWZz24+4KvF7Ua
mEs6P/GSS5Sd+n6xMB10Dc8ZBO9yuQYGBlDIb9++tdlslKiMFL9fwBBEHghFcW6tpXH4ZN6dlPiN
a1ccr3wjMrLGKMiqbFdcQ7z5VXNN2qoNiYs2NrY6JFlz2ThJAD/zssow5K0xwUJK4md4KOqvD4zN
9CVFk7iJw+A/FTD8ElQ4iVNEVhZwjtEYjrwCrUHX+pIxKhmijTrShzyYDFQX0GG7k+Gq6xrWb94W
k5gSk5gaGhUXGBYVFB41yzdwlk+Ib2BMaETS5eslJpt7tCGhbJWWzt60JcsRMjgiZuHSFXMXLvEJ
DIXz8g380TfsH16B//QO+tE3FM4nPN47NO70pdsTWe77ANWMFKpu3xBWQVchbQfNiUxIwbk0xiIz
w7xU+mowbfmZfUdffOjmzaJmUxSYU11WZc+xR7ND1hw/88zsUEDFPIwmhRU18uCFFJqqibJGlLOq
wPZQNYYVNAuvPqhpT91Q+H8HbfJPOnWooMrBS6xC5gDHQIWkwA/qKFRIp4DrGE116EaeLrlMVhUq
ZDmGBvtXViTkyrjhV4HqkdGigI2N+EC4hGNFmEAq5K/t4pfvKfZfcDh40fEjlxu77BpZYYf7FI2V
NE7VLpe8i5qzZd7KE/U9IhobXYA3IVPfITwU9W3x16UojEkVm5sz2RxN7Z1v6z88qXz96m3N63e1
D8oq4Lr6B928+JfJ7zcF1DGdwcPgPTYh0TcoLCI2AWSTuiB99/5DRddu3Sx5cKrw/JbtOVGx84PC
Eg4dO9PVbxotWEJRw1bHmqzNEbGJM2b7g6iyd+17+vJ1de372oaWfrPdzvIWFzcw4ugZsnQPDncN
mIbtbu67rA+dmwyo0OtQoPpDFgjLSgqMBZes9licxeVd23Jv5l9/0NAPVcy5NMmuaC9q7Bt3lC9Y
uSP35PWalkEbS5bqsTy4gah1RAgugpKXNIeoWTnN6lL4EbdW+c61J+d95IKTwfN3r8q9evZRZ8kb
6/x1T33i847ffGPSZ0THYEhJgWgRHdHwYAG3oFl5zYJoOc3hEvj2HuZt/UBL5zAPXiJzhzrI4zN6
868EKYefi4JXiN3kkNQ+m/vS/eZthyvW7rr9rG7oQ7fL5IIoKs/zkshw2hCvqd2DzPGT1cuX37l4
pbWpw2XhRUZzeijqd4BRqTp+WznSChirjDEfekJjRrPDVT347wZEiyNNC+M1q4vrG7ZAWbT39HMy
GcTRIy8RrQQBEMztdvf19ZnNZrQtyDNBJOqDwAiJn/SWKaYgcBXhaWAcBQHt+WtBEwVI65ckJyvc
f/x8595Dm7L3rFy7edHS1fHJC2IS5sYlzY+On5M0J31ZZtb5Sze7+0fcvEo0hfgb++YUgDCkCkdh
+H6CCcHGA5lCXnCC4qLlSQPTnzihMQDUH8cxwHP8XQzDfE05cxyH248ePeoXEOTlFwh+Wrtxa8nD
MrPdDdUsqmRAwIhKa6ep9Mmb981dFieHhHVHKMriZDdt2+kXHI57Y5PmPKp47hYUO8NBP6Ld8ILA
odqQWbQiXcdR9x0ChfwzYJorgqhIsIFcguKSNZeiDTNyzslrCQv2n7z4YkTQhiXGqjA2lXvZ0Jm4
cH/SgoLyV+95MJkoo8Wh5FCX+lc2yKjK7YQ9o3EKbxXsNpmt67Wv31fiH3/IJ6YwdfXZig/DA5wy
6FRbe9Utu8uPnq+tHeTNH4tkSEmBX6gAWEgaL2hOTmMdqjQiC6CF8yVNEXP2/dM3PWXpxtb+Pp70
eAH1RJbO/cxXBEZUOtCKDN9PIIgoAxAhLEIVjYxRNbukNfTa1+487puyZ9n2qx/6BBa5hJGGKFVF
YF2yxJlE16BbO5R/Pzoh59yFVoebLI7gFFnQRowk/wz4K1MUgI7pdDoLCgry8/NLS0tfvnz59OnT
W7du4byurq6lpWVoaAj0AAVBE0J4486vADQXPbpcrjNnzy9YnAHdEZc8F2Pbtu5+tGpoHNJlQFSS
ZLfb79+/v3Hjxrlz5y5duvTSpUs9PT1U8Y0B2WdZ1qFjcHAQumxMRdIAFJCfEonNZmtubq6pqXn+
/Dli+10yBUAqoKqqau/evSERKdO9Qv2D4338o/81LWCWT3hweFJQWGJwePJs/6hps0Jmzg4Li0y9
frvUyWisoCvT31spIr8ogTEYvr8EvZ4NIDs4whNFhPFBe3s7ihee8BkfJ4od7QRjCFy1WCxWq7W1
tbW4uDgvLy8nJ+fChQuvXr3CpS+XYQJwIyrOZDJlZ2d7+fjN9AkIj0nYunNPxYvXFicDaTBqhiNl
OIkjFGVz83sPHAkKj57m5RsVn3yy4HzPoAkU5WR5keeIzuJYjnEjx7pmHXPfHWi9UEiqAOUuaopb
Vs0sX9cxeKjw7t6TJW/aHWZJcmiKiVMqqjsXrz08b/n+fSeKa9qtdpmsz2Mlngehq1ZZMWuKk0y7
Qa/jwNsxAuwbEK7faVmUeSl8fl7atkv5ZS3vbKJL0+yCQ5L6UJYaz6C4oe6dCuPS7IY0OgwpKVCW
iBb1orAjrLW+Zyj/1ofFm4r/GZT/r5D8uOWX9557967XYleRPOiVpeFVMOc4GFHpmIKiZJV8yUrQ
ZEZRLZxQXt24Madw57Hb7zqdJkFjNJURWFlk9ZUSoipr/Sbu+sOGhMVnUzNOF96sH+E1VlWh4zhJ
XxsBBfTnwV95og9NAmPJ4/lnvf2DZvsHB4VHefsHB4RGhkbFBYdHY6AKFxGbGBmXlDQ3bfue/WfO
X3r+uhqU9nlGHOvb45y+eNW4PgqMqXEcGBhInLvwXz6h0/0jZodE7zt83O7moI/Q5BQMEDXVyYr1
TW1Llq+aHRDywwxvCHb5xh2ri5HIkx3EqfOQprX3DWzamRuVmh6amJa1fa/Nxbh4WdCnmHUlRQhP
wEBb05yyNmR3r9u2NyAqyT8yYVHmhqr3bSy6nyo4NcmB0RV5aETkNvALbZVkDaUBGXBXTVPHkjWb
pwdETg+M8vLz3ZO7/3XN2+7B/raermHriAA9qComMG7pkw1bd6C0cw7l2VwcJxtCjjl4dvQOwj4Y
sjhwdLAiNGxzR/fjJy8eVTx7U/vBhBwKhChIvjA0JE8NJETeN2x+W9d8/1HlyfzrJ07fOHby4s59
B85fvvahpaN7wASzAmrJzggoHEiu6rYQL0onzpxdlbVl4Yp123KP33z80i2QEhhzUP2DI/Z1m7LR
GAKCE9IWr7lZXOpgBPRi1AH6vdVtP3ziqHdY/IyASO+gqJCoeDCBf2CQv59fgJ9voL8vGpVvUFhI
ZCyEQd4RGxQBJIcB/YUYY0qMJzKWr50+K2TazJBZsyMSkhdHxy3YnXOitcOM3MGc+hxRuTjt4pWS
4LCEWT6hs3zCZvtH+AXFBITEBYbGhYdFBwaERUXGL12ysqW5U0LG8M9oXP8eULVrQBXBv6rxzF9/
VESe3sBgIM+JBFHrH5ZOX6hYveXUvtO321x8u+Du09jCB1p69kv/5CMnr70ZcIAD9C31YB5IAiwM
xIiqJ0+sFAuamyCPuDS+V2BuvdDmrKoMSixYvO52eaXZjusyx7NmVTYZwuiY0P3N6DiaQkZ5Y8UN
6VCESpemvOQk061XI9MXPf9/hT//X/xu/28+Z0OX3npWM4LhKb1d7++fBQ1DgVR/rhb6g3Y8NCbF
DsNpYFjbsrPHJ6TCO7j86OkuN/qG0qkJoGEHMs0oZF7vaZNj1aGH/0w6sPJUHfQPTZ2OXGmKFDSR
X8QUrGmE+Pb4y1OUfPj4aS+/IJ/AUG+/IDDBjzO9oVAwSoVmgQ8cuAEcRh8vnzhzrqOjAxWAukG9
GhGNYqzt4gRqBQE+V1VEO0oSrLTk+enQbjODokNiU16+reOoKYH7ZUmQZCRgdbLnLl2LiksCWR45
cfp9c7uLJ+yFgTVSQEAo6yMnC37wDpwRGBWauGBbzuHeoRGbm7O6WAfLsyJZjqPrU82taC5JeVnb
NG/Z6pmBkT/MDvYNjy+4fLuxa6jHbjbzbocsuGSBI+w4io8640SQdQ+kKypuXuzsHVy+bvOsoKhp
/hHTA6PTli5u7e7k0cVBA4pM+QnaRYBaUTVWlMmTA6ifUfoEa+DniIOBksVQICw6HoN91MLCpSsy
123MyFwbGZs43dsP5iZ4euXarAGzhWoDxI9oGUF48rJy2+6989MzQiMT/YJiA0Li/YOjwRaoXP+Q
iOR5C7fu3Ftd18BJEIn0bwwCUHpWu2PpyjX+oVHT/UKDYlOzdh20OFnIQzu+3kKU5o6e9GUrvfwC
ffxjklKXXLh8x2xn3AJZLeYS2PberhVrM//hHTzNP/zH2UGzA8NWrd+0Y9furPXrN23M2p69JT5l
HuweUDJOkJezRVdNNidk+FWdGKICaDONze2Za7f6BkZ6+4XN8A6eOTsENOMfHJOQsmjXvsPPXr1x
CyJGJxCPmlbUwQcU5eUb/uMM/+lewSAq/2AUUdxPMwP9fIOnT/OOCI9NX5RRW9PwvVEUMq2TE47E
BpXIzBxhKNQ6K2rV7/sWrdh3pqjqZY2rz6b1u7WH1b1Ltx0PTj6euf3Biw9umE1umfCbDGIjtKTf
CYeBDXonIpE1C689qrMlrC9avOX8tfK2qhbzgFNmUWjoOAJPjA+VNaTRgYqgclIgcjI/iJjJSAkD
LlGWBV7hTYJc1uBetv/lfyRd/d/j7vw//U/9I+nohlNP22ysG/1/3Mw8jXZS0CRGgcCjSet1JIGy
QYey5mDVJzVdicv2rNl9ZcP+25XvR3jIgk4lCS4VVpSGY3l1S/qa3UHxy9bvzK/vctkwYmMYOvoB
qDxjMFL5JUB+44ZPYIT49vhlijIk0jGh8v4wTCgpw/eXgFaPEfr+w3mzfANBS9B9UIgtnb2wXexu
1mRz9JusUE8lD8tgSBENFRgCFblp0yaLxQL6Qe0CiAcpYkz04sWLi9dvn7t8Y+3m7Usz161Yu3HZ
qvU5h4/fK33aNWC2Wq16syTtALfQc5Zlt+894BUa5x0a5xUYeeh4vpMhozHSCzWF4XhYeAsWZ6Qt
WY4jVPaKNVmgq+6BYb0TkG7z7Nmz+QsXzw4Kg4YFRQXFzY1OXhCdkALVDLqFaZiQOv/6nXvQ/iDj
Xguz69DJoJiU6f7hP/mGTvML1Y9hMwIifgr0CYqPDoyNPFaYz0INkOLRMXV9gkhFmDOkFHKPHA+M
jPcKJhS1ed/Rzv5eRhIEkBOoAOoejE6UjcoripOTYMfAk3CVpIADoK8NoiJyKm/rG8Oj42f5BOhl
HhoYFrV5++67j8qLH5RiPAHbCyYRFCnlXVZSSx5WhEcnwvxFJSbOmX/05OnSpzC26uGevKw6ciIf
7I5xBmoZUa3btBU0LwkcrChB4IfNI6kL0qfPDpgVGDE9IGLbgRMw3SAMDFAcISqSQCM5f/k6WTI3
O8InIHr9pl0tHf2QFmzHK5KNceblH58WEDkrODogMmHn/sOtnb2SAhVCxhmoR8jZ1t0PIkSNQEiQ
ZeHFKy5OIBr31wNVg0g5GcUo2Nzsq7c1GatAseHeARhgBSOJeYuWopRgUI4NTXC0ucRd+/JgP83w
Cg6NTDp+uqi929zY2jc0wtht7v4+U0d7b3/fMOPmDTVI3b8JpA+PAWWoSLJEzB5kxClodlF7VtO/
Nffa0g1HblY0DLi0tkH59uPBddl3ktIOHTpV3meRYVMSR+wvNC3CS6TkVPKsRuMFlVPQFtu7mL1H
HyYtzQ9IPDhvbUn5e96tMazGCRpsM1ANqUJDIH0xxRgmajkSJ/hJVDkGcjK8XNNlm781/x8pl//f
kS//15B7/3vM5TWF76vNyiAv8ITRYNZ86f7o46E3WgF6gzyXBPVKZPXF41cdKzafikrbe+lR57Cg
ujTOpVpZeZiXXdA0Q/38kauvFm05lboq507Fe4sbXZISKmEwI97JYGT1lzChKAzfT/BNeeGvTFHQ
MoyonD57EYPcaRhhxiRAjzS0dsIugVayMwLUH3kjQFQOHTtFDKwZ3jNm+2dkZAwPDyPFsUR7e3tL
S0u3bdvmF5EAjR9I5tASYU9A9YfEpobGz4XVkp+f39fXN/aUCHeB5PATtAQr6l+zg6f5hhw8dtrq
JBOAoCgo0GGTef3mbEqfSBonYMpdOQeb2rslhQSAhq2oqIhJSPYODMXtXiExM4JivIMjoQfJemK/
QH3wHn2/9Imu1NRBO3/qwnXvkJh/eQf96BMMooItBSF/9AmZHRkyPcjPNzK06NZ1fWQ2iinrU5HB
lWSxyYDJkp6RGRabHBo3JyA69VJJuZODkkcJE+OJk0UYUnBVte9OFBRA3d++9+jClesFFy4VXbt1
6frtd++bqD6FEYmSr29qAytjQIAyx5gAhW93IzpYYKTK3LyEYDiBQxLN7QOJqUuDw5N+mO6TkLrg
wpVr6IasxLMy6xZ5YkzIaunTF6DqiJiEpDkL1m4kFOV22kGuKEBJVqDQfYIj9AKM3Xf8LMYlUDhQ
cIgcJ6wkgzWfv34bEhnrFwR7JW7dxp0fmrthnoInGIm3uOwnCk5OC4giQ4TopAN5p7r7h8dTFDEc
Fe3a7btBYWRp+Jy0xTv25n5oaf/SZvoxJFXhVWPhPhyMyCHryPEzp2KT4mf5+v00C1ZcVGBYJFry
m9oPhEehywTF7pZOFVz2CYgMCI5NTE0/VXCls9eCAsc4m1AA9LFxoqvlMfdvAu1WBtDAOIwEoFtI
dQw5pZNX36SsOrUzv7Kqm+lmtGtlHcs2npu/8sTuQ3dae11gCQEBUUzEPBAVlZEUpyQ7MUBCs0Ye
odyHndrDyv7UVccXbDh7/Oq7181OO6fZHRLaqUxWUejr6kg/1f90R2WhoF14DEgEUjJuu8Xl7nOp
Zx82+y278r+FX/g/Ex/8f8KuB698WFgx0M9JjOKWBJvM2TWWIQzxG0CeKvJkdgZjI1Ubdot5l55G
Ld63I/9JdbvDKWow/4l8xDCShxxi/p3XsZkH5y7dXXDl2ZBDdiGASCY5SRZIv5zqtQISzxdgYlF8
BhOC/b74ZYpC8uNh+P6xMNIeheH7S0AzwSj+wuXr3v7BXvosX3zK3HfvG60uzuLgbW7Jyaojdr6+
sXPJivU/zcSgPnSmT/CWbVtdjFuE/SDLIzbHqfzCiKh4H79gv4CwmQFh67buPnH28qkLV3fm5sXP
Tf/RO/B/Tp8Nf7+AoIwVmZVVb1gBRoqKToC2Bnf28s3A2DkYv8MMSl++uq2rFxqfWFGqgvjbewYq
Xrw+VXgBFh5I1NsvCOZU6dOXPBop0teNdJPVXnSj2Cso8ie/8B98w4KiE27fu88IYlf/YGffgJ2B
sjWG0iNullNli5s5feGyd2D4T96BgREJu2E3WBlGFhhZ5BSJGD1oUkYJ6b1zCpDnOWS6rH/InL5s
pXdQxAz/8Nh5Sx+8qLG63OAkBqamPuRnJMnJcdfvFEcnJIPmf5rlA/qEvobWjkuee+LMOQfDg0RB
J9CntQ0t0KOwCVApKfMXVdW8h70i6u+M8CJOVJwTblRUs8V2/tKNoLCE6V5B3v6hazZsrWtqQVqc
IjACx+mbC3CS2t7dv2XHnrDoeP+QCPA97DAXCyMELCNhUHr1VklIVPzsoMhpPmFpGVltPd3kLtxL
3utSoO3ABJXV1cGREV6+obP9wzZv29PWPajrdzJvCdOw7NkzjAym+4bEJM07e+k62g+qGP1SN5lh
OIIkxH0Hj2LcMGO2Hxobyoqw8rj51Kkx1qpxRIQoTyQKCfUSJhOzTp63ul3X7tyd5RPiFxTp7RcS
mzQ359CRYasNZiuGCIwglz595Rcc5eUbgoLK3pHT3N7HiiqIH+YcJyi8SBaEoWxBUqh+OLK271uC
ZmoMelNDkqSpgipID0HLgumNIgYny1r3gHzySvXc9ReOXGtssmsVjezBopfJK/ftPV3yoYexYuBC
OAyN1y0rIxJL1hqpCiOrVpGs9ra7FcUhqG/q2Q1b7gREbI1OySl+0mviNPgLZF2tS5UGjbldMoWh
izOerwlt6Y6IpZO5oJt1MpR+P6uIDUP8yoNv/zXnzv+Iu/rfoi//t7hL2843fegW7CJZCi+T16fI
wghVEpEdkuBnoJc9LlM50MoFfS6POFl1soqJV4TuIcfFm28WrT55sLC8mxHNmntIcJsZzsmJnd3C
3ZKOTVuvpy49fLmipYUlksP6EskqDGOXQnQcwuAkSx/BkECH4fUFMG7QYXhNBiPEN8Bf/FkUxphX
bhZDV0JpQnXGJ8/NO1WAAe/VGw/PnLuxffeRhJTF3n6R02YF+/hH/TQzKCZ+weuaN1BeTp7F+PrO
/cfh0UkzZ0NzRaXMW/7sVTWMAJPNNWRxDI7YzTZXde2HFWuy/jltFqwB6KY1G7aQ5/Zo1rAwdM11
teRRZGr6NH8o97Blq9aDoiAYWhP+9K5AhOwZNC0iz0KCpnv7LV6+CiN6XhBhwaBFQAfyUMF9QzGp
C71DYv/hHRwYFVd0/SoLG4E8+yG3jzlRQ8+FLnMezDsGeX6c6RMUFo88OpjfNudEZMQBnDpid8K8
m+kf6h+Z4BeZlLJkTdW7euSRdnnoaPRlnEB3v6h6d+b8pWWZa/Qyn/3jzNmQBFbFMOxHkVgbuKW5
o2du2mL68G/56vWtnb1OEK3Ox7St40iUv6a5XK77Dx9HxSdTwzFz3UaYmLB6kRado0OEUOgDZtvB
oydgXqAMUQuoke7BXpfAQPVBMNQaFLpfUPS/pvlHxy2oePmMlQW3iKG7CHJiJQGm0vOqyuDIUBDM
dG/fxRmZL6reQlqYaITlNK2yuhb8hzHEDG+/1VmbYYjTNSCklslWpBrGChhkQEIMgyADLM6mtq4v
pwCacQCF8K7u/dmiq416EihYF49RBZmK7Oofvnz97my/qFk+Yb6BEchR/rkLLoEYIIzIgfghJAYE
aOc/zZwN23r95m3FD0rvPipHdZwsOA9Tcu+Bw9v35GRt2b515174DI1YjeS/DZCpMSBrqFWdGXiy
D7nGq2QRDPonWW3X0cVtzrk9Z9WZvZdan/ZoB651p2VdW7T2wpHzFa3DTtgCDlF/WqVPxxmbmhOK
hf0qghPcitw+bHtQ2Zm+9nzGxpLeNhKaoW9LCeAa3AR2ZlCKhmSTggpIahUJ6jsI6UvVFUbrH7Se
KHrlM//a/4h9/J/zXvlmvtlzva/Rprn0aRIi0seZBfT8Tg6dn2Aj6WtDCEvpgwXEARElrXWYX7fj
XMzcXaeLavtsmk3VhqRhi9bVOmI+U/IuZM72iDl7tx14VNNCVoWgWBi9p3wJphDpO8dfnKKgYkoe
lkHBTffyhbr8YYY3aABc9cOM2T/O8IHDyT9/8vpplq+Xb/DKtRtbOnpYiWdE4jr7+qBrfpju4xtI
tNssHzJCxzg9IXU+XHBETExiKo7QXDRyYjSER9d+aBpPUeeuFftFJf/gE/Lj7KCUtMUTKArB4IYt
ZEUZdCssD6hjMCjH62Mr2mlUrc9km7skc0Zg5A++4QGRMZ+jKIz9ILzV5dyYne0fHIF8zfAOXrkm
u2/QWFz0a0FoUiPLRtCTHpQ+CYyM/4nsUBAOu3DnvgMjDjeZKSUkQSbx9EknjP2Jz/vm9iUrVkNZ
+wSGoHyWrlwDexHaVg+g1TW2QqGDD1AvMKdgRYGdxlMUTsCLOHc6nQ8elYbHkPdYUfjz05fdLHng
YMlyO2htCAdDhxVlcMaq9ZtQNUgrIjax8OIVk30EY2f92Zj65OXrsKiEn2b6//NHxJFU8rDUwfIu
XnSLUO7EDhtxuAovXvYJCkHbgINsl67fZsh6BBXBwIUPy5+Be9BskKNlmWshsMXJOBjBzgg2N4/U
12/KRhtADc7yDQiJiNm+Z//giO3LKQp5R34BhmHyTpyCAMgLYkNUGLssW7lm5doNMEZ/mOE3a3b4
jzMC4pPTThVe7Bs2u0Vet7fIF8whFUj6X9O94JCLmT5EWrRJxIMT/CQNVW/8KE8Ue9mzSiP5bwNd
MY4CmpQoYahk2KwcqzJO2MGa1mtjj95qiN32dM8d09V67cCd/ugVpzN3Xnr4or3XLLLkUZM+CEDT
0ieByUhId4I24pRtrKL0D2kFhR3Ll9+Ljju6ZtOFzmE0C/QNWUEfFGRiIKNXjLopgOgVsrTCJWpu
TmXsouBWtIZO5uaDhojkgZlRr/8z8vyi/Q8vv25tcbAWTsTAgZil6B6jwymaUYop+QCdCaKQUSNu
wxnqDnyF/Fy70z1n0dU9h5809nAmXh2RNTOvPq8d3LDrRmji9oVrTt943N5rVciehBIEVTiF5xWX
keQv4e9CUbQ+xmD4fq+ANoQlhLEk1Aadd0J3RS8NjoiOjIv5yWtmRGz0kpXLz5w/9765ccRh4xWZ
g6okE9oqOv+N4mLSyX2Cf5zu/+OMwH/+SOavfpjh9Y+fZsJRswzMRJWCrgUCNmbvNNugPIl5hJbn
5ITLdx6EJaX9wytodkh0TPLcV2/eQTAye6aQF3dpMOi7DVu3IwYMw6FhS5++FKGzRIF+g4WT1ObO
vvh5i6cHRPzHjIBZgaH558+6BV5fnqAxogBrC9Lipwu2AdmXSLlw5UpkXGJgaPQsn5AF6atbOoao
xqfVh3Mc6U+AltVnQJduy7yMvIhH88//MDvYKyT2R79wEEzS3LTb9x72D49Am8OygQnFktdLyYK6
3kHT2o1b/YLCoNmRqcQ581++eQf9wkkKKuVVdS20MAoQo35EUvHiNScQfhoDSXj0YR4rSJdv3AFz
gwNAVLjxyIn8d++bOnsHuweG6pvaThdeiEueA/3rExACzQ5DYchi5xRBIA8EVAE8x4mlTyvjktJm
zQ4LDkuCORUVN3f+opVrsrZt3ZG7dEUWfgaExJKZXl/yeC8xdX7Jg1IwE8iPcKqk3i99gqSRCyKG
tx+tbtQUeYg4czaaAfIIDkDqsFGK7z928wKqBkMQmh0KWqCTYqwiLBbLwcNHwdsoN/A3WqxfcBjS
oj9jEuYlpiw5cuxs94DFxZHnYLoj01JQ37BNM1atA5NFxSVBzuR5aQuXrsAYC3Y5jPvsXftgB+/c
lwuLdvP23TCqWrv6DMl0/FJL+CqIMiQkAzIoVKuiDQjqi3bnhrzX6dvLNp/rTct9l7D+xprcx3cq
2uwsCeYUyLtQ6CSktRIjSH+QicGQvp25TdSGWK26xb5x99Ulmacrq01DI+ThIrQ+J2NcAfYQyWMq
YkCh/ZLncIb7PATy4pECg8yGIQvGWCZpy4lXP0QemZVQOD3iZlZu47sewa7CvEK7AjO6NYmFiW7c
rMPIqg4UpuH7CXSBYDaR94nR90dYraGPzTnzNCXz+NajFz4M9tpVtd8tP3jZvn77tZCYAweONlfX
aoM2YnvCcmJ1+wttW0YKMqn7L8QEkQxfHV9+aTyMEDqmiOHr8VemKLR1qE4YJbMDgqFEfpo5G10d
A2R9EM30mwdHXDaLC2MxaHljpMWRyR86+pYfVlSERcf9ONPvh+n+P80MmpeWeeZ80fU796CDbhbf
xwk0F4auUA0dvQOdfUNDFpuLE9wCWRGAHoX2B919teRRaGLaP72CZviHzQ4Me1hWgXID94CBaAOD
crG62A1bd0BlQ8dBxUBlQ2Hrs+MEGDN2DZhT01d4hcT8T68g7+CwwyeOjTjtjCS4BM7Jsa3dnQ6W
IY9VVAGmg511H8zLg8r29g+Z7R+RlLrkTU0bz/NQ91Tp01eVaRGBfujJZ/AzRaHfQ0fcf1rlF5n0
X7MCoZfBQFDcCxZnQA/mnyt6+aZm74EjyAhsnej45LDoeJhQs3wCEDJ53kIUFJQHKAqG14vXbwPD
yMQdbocyBSWTBwHjQNOm2ccl1GN17YfEOQtAD95+QTN0WwfR4ugfHA7dTZS4fzBK71ThBYa8iA91
9jNFEckl9UHpi5DwpH/95O/tGznTO2yGdyiOPv7RgSEJfkGxMJGJj2556IZsCbgWJhqxk1zcvccV
iBysjESJcRMZ4+0f9M9ps+BAUYTV5iw4WXC+sbXDwfBIDlQNk3HSTH0OMKToBhO47219A+yweYuW
RsUTsgGpwIR9W9/Y0tE/YmedrEzHQOMpijQkfayD1vim5n3lm5o3te87YLlyosXpRoNHUwTj6kvV
yXQr+gVODMl0oKgNUb4BoJWdIutWyBuyLRZhz9k3vvMuJW9oiVnV6j//3obcirJKk5vVZBbkgtoW
kSM3WSpCPg+hkseUCnoBLCq7qJoF7VFV05a9HVGplw+dqe6ycMOMySYNCqoJprWigmXARmi2cMgW
ehIqBHlF1AzN6aQgcx6y5oZh59Ye1ZqiMs7+z8j8/4y8GZBefbvyqVlknDDJVPQXlTxKxvhS5WV9
HcUYjKzqQGEavp8AF8imgKLK85qd1aqaRpZtLZq34VJRxcAAr9W0cnsOP/cL27c0817B5eYeq+TU
LHat1aWNcGSJHsepdrdskjRGH97qs5JfhgkiGb46vvzSeBghdEwRw9fjj6aob5qZCZA0ycG7r9ws
nk2eM0ElRfoHR9+6e5eRP2qsEMm4QQcaIjowjn1DZpgCs3zJasAfZngvX71+4+5DRwuvPK6sbeg2
9dr4frtQ3dR9+vKdbbnHP3z4ABUDbT4WJ/gAeufSlVshESlePhGz/aOj49NevG6wuyWM+BA/OIoc
VfLq6IrV66HsYN5B47+p/YBhI42HwuFwbN682c/Pz9/ff5ZfcGBs6oFTF3JPXcg7d23e8vVxC5bl
nb2KoaWup4hGvnP/MQwCjMGne/tCgQaGRcUkpKSR1e0ZkXFJB/NOunlConBQW0a2fwm04nB89+7d
0aNHI1IWTfOPmBkcExg7B8d/+YTC/eQf8cMM/1kz/f39wgL8w8PD4ubNTT9XeLmzo1/kNXRxGsOt
W7dCdQQFBSFH169fH6PMT4HwAMuyg4OD586dy8jICAwMjI+PDwsLmzVrVnBw8LJlywoKCh4+fNjf
34/yJ4YnUXM/A7fDByRtt9tfvnx59erVU6dOFRYWFhUVvX//Hp6IHFedTqfVarXZbKBwxIO7UH24
ESfV1dUBIQkzZ4eFRibt2HOgtqEFgQZGTL3D/RanzeVy0WB6Xf0M+BgS/BpMiIfOAf6qqCbEYPh+
eyApOgVBh3vETCBPWcA6PCfA7HGCwPde7Z65qPbHeQ3/EXYtOePcsXOVPf02UDPNn0wsQzoPhhoU
yGNQSZXIhj28U3bbRaXkaceClTcDo4t27Njx5MkTWnF64j/DyPZkkAg7iCAjRXOqqoO8DiWS+TIF
wmnWTpe49VTL7HlV/xnx6D/CLi/eefdlR6/588Q2oVIMXx2oMUlGduCJrswqEuhXXxjPw+C1iuqA
yTlU0e6ek1MZmPWs6C3/rF07cOFtSMqB9I3Fz+ptA27NykMF6Q/eyJvNepGMg5HMJ/jydmLcoGOK
jPyqhveN8BenKFQ0hsBBYYnTZ4XDgQWKrl9jJLeRvI4J1aArenKEM9vd2/fk/NcP08FS0Pj/c4bv
P2f5ewVGBsckB0QmeAdH+YTGeAVFzgwIT09Pb2lpoVFB4dITaL38gvPB4QkzvDH2D542K3DOgox9
B040tPTSJMiIUcPg1715+27YBH7B4bFJcypevBKJGf8z0A+hTFNSUn766SffwNBZgWQJNRL9jx+9
/SPi4coq3zlFGCi68MSAU1s6ewsuXI5OSAa5wl5B5HA4hxFw7HSh1cXBPkBgjPf1TH8RqDA4gdLE
kLPb5Hhd33L9XtmV4kdZ2/eFxKYkzF+StnjFls3b75Y8HOg30bdEyXQEbsIIVCL8gdtRON3d3a9e
vWpsbOzo6EBsU1AUrtITJE1NDfqTAp5gFBgfOMclKt4EiiJCj7Y02vzoOUC1P8Ij5jGf8SnSqFCz
kTHzYWZ5+YZlrt0Eg4Yl2pSYLyKUnl7XOFIBxkD9fy2ohGP4E1EUWeEIAqCOmC+wfFQ368axsdO5
fNvN2Ul7f0g6EJBZlHunp91O3k4irzEZBT8JIDpqgpe0plbmUUXn2s1nc47fbTOxdr0wUDK0WCat
7slBOANCcrCoMDzjBZ4ULSjLqV17Yfade2xm8jGfeSc3HH/1tkeDwYdaJru4fAYTKsXwpUD/ReNA
coQREZNLJZOU8FZdkpp7rids4Q3/Jc9nZ7yftajaK/lOWmZx4aWu9+0KbERYck7Kz6pACB+dyENR
3wlQHEbB6DB8vwKcQmZE3tS8hxEza3ZUUGiKT0DEkZMnnALawGeBxjDmyByRrL6pfb9zX25oVJyP
fxBcQEj4bL9AaH4cA0LCAoLD/AJDsrKyMAAf03RjYHj+7qOyuOTUGbN9fQJD0jNWPqp4amfIBgeI
mSShM8qtuw+WZa5dsSbrYN6J9p7+CZlHyQAYM1oslheVr3bnHNi4dfvm7B2Za9bnHDxc/vSFxYGW
/5HkcGjbLl4etjp7Bs1wbd39iNnicJOtl8cF+22A7hhzrCCPOYE8YvvZQfaP3dfCaBw6UCaG7ycw
QuiYItgUoIqPMuLbt2+DQhNBUT/NDIhNmlv8oJQRRDIbrIqcvjPO9wyjFHT8tqKYACOuScCoqlVR
7eRNM1WBSnbKWqeZPVRYMjtqX8ica+mby6686OiTeKdGHvmT+Tw0fTKNR58bkfaCpilBM+vsBXLq
NNkPnChZs+Hy4WOvG1slVtE4zc5rZP+XMUzIlOGrY+IlHjRKTkAVOGUUbdCpvmuxrt1+/aeYOz7J
5+69GbTIskvhJM2ukgdO5AVvI65PMEW6MmgaXYssQcToieM10SoKJk5609K3aNvNf6Ve/I/4M/8R
f3T5kae33gwOc7AVNTLFqzhEzSJpDhl2nsoTKwwjSN2B/cfDSPKXYITW8eWlNAWMG3QYXt8ef2WK
QmvHUIYV5co3jVeuP752s/zOvfKWrk6BPHH8LMarbzLfrGpuQaaPlwSeczrsbpfTbrNaRswOu41l
3PDkOBbC0wHv2BicQiKvtvBukRNUdFw0ffJRJUbiOUXUX38hqcCiIpHrXIUThrzCMrHRoEBo/Gj/
ZFaccoCM6DFc1YeC+pOtMTcaIXl5E1yIn+Ovjne/FZBwEgebRB+lGo4MB392H5XMbwNtGxRTdC0j
hI4v74HjgRtR2vReWMNlT95u3XFw7YYdJ89caGrvpq8u0UeYNPx3C1oIFL+tKCbAiOsTqIqA3oYi
QSIDI/LRwmeJS055R++NSr9Y1exEU4TJTpQt2jBpJDDiYUcJZE0oHGnPZCWBSB4/aTaTdPVC5aKM
g1n7Ltd222BYWGXerYD5BFQJGpqRpI4JmTJ8dUzML5o7sWxIp3DK6sW7bwOSd/gl56za9/xFQwcY
S+AUsiYBgxN9ywqysdAU+Z0iXVmSWJfEgWlUh6DUdzpWbrs9M/LYPwPzEpdeKqvut0lk4QMjOnnR
oqpunZnRS0WMjPR+qz9ngoMkMgwqBxjfiFqHkeQvwQit41eU0udh3KDD8Pr2+L4oajwM36+A/kac
isGJKOuz4ip5Tomi1V9D+Cz0AD87dCo4eqKhE9FtsyCsThX4TX+iziDzp5VHLH6ys7BEJtVJUDgi
FXVks3NdGjjIRsXTe+FE0Pj1E4OQJjjCVaMywxGjSn8uO95zUvdb8XPSSPtnR958FD/jxjPjBPel
gtASppjQSEij+QyMEL+EsTJBuaEA6ZGsRpNkTpB5kdiIblZffUh0HbKKZkUqg0zM6jeisdF6hA6m
Jz87WsV6MAiEOMamqsaDNDLDkZA0ZjRgCeqbtBbDn8ZDHRrMJ+X5s6MlMClo20OGqIRjEcJNAAJD
YDL+IoMkMApPVufIOGK0RLbCUMlOH9yILA64xZKKzmVrLwZH701ZePxI/usuM+8QrLJmg4GlSAx9
JRZGEnqPCLNbsKuqXX8JW3WymsWtvXpvz9p7e/WuG+du1QxYRZcsOSW3CIJTXeSTFuiPxufyJ8dH
jRGRSqg2jtplimRlZdapaHXdzNajd0Pm7dx48Prjmv4BxsmpDllh9cU5KkkEPVZWMfCc6sUjBCVr
OMA1btANWbQI2QTiOJ4VFIFXxF6bfOJas0/CYa+YnLR1lwputfaPCKh2ASVHVvWS18fJm4+kc4z2
ErLCR3fUE6KQ52EfzTcatfJLMEpkFIavDsNrFIavDsNrFIavDsNLh+H17fEdUZQHfz0YXUqHTlH6
AHUS96UGlhGXjgn9xPDVYXj9SkALUnMTR7LcnGhEQ9Mb8eqYkC4xWBXNyYqwEpATRAJHbWIcxxyH
QYqGozxksfX2D1gsFpaFbY47PgLIia42oOlaHMy7+tb6xp4379pevW1439zW3U82iSemtiEnKb7R
YpzU/QxIbuRBR8+gae7CJf4hEes2bXVywnhpx+MTfaS/e0vmoUh+wRecqnUMsQXF7+dtuTEzfkfk
ov3n7tQ0drtGHCx5Qwn8I4qgc8LoMJRA6yIMFpnYMjzGeQKj8JxGJrkq3vSu3VF0oPBp3YDYyWou
XPz4bbkxUDkoJmRKn14wnD4IwP2Ih3BCy7Cw7cRD/3nbvVM233jZNciiskTG7VRFh3HzrwGpXZQE
hzJGWiKnSRZRHJHlDjtzvaInZdVN74Sj/1dAduqmmw/qXFbwMjLN8/q0x9fCyPm3gZGGjk+q/t8A
D0V58A1htHQdUBbQJ593XwQjLh0T+o/hq8Pw+pWgrAClb3PzbV199U1tvUMjdoa8GDsFiE5XNLPN
sffAkdCoODjfwNAjJ/IdLD+m8eHAAZXVtRuzdwaERkbFxoeHh2dlZb19+xZq1IhIxwSKun77/o8z
/H0Comf5RPgFkZWZEbEJ4TEJoJaG1g6QKGTTWWh8SU5wP2OCNm9q6woMi4pNTEWEdjfP0b1ZdTcB
uJHjOLvdbrVaRd7B84xLUCyiNihpnZy242LVjHm5UUuuz1369PiFrlaTPMCODHPdItkKVSBz3HqW
DAfzk1iU4CpBI9N3kknRrpTWxC/evGrr0drmYViuLjcxHkFPRvKfSG746phwSSLPsMD8ZGch0McI
I7tkzcqrV+89n538+qeYS0t33XrdzbnIozA3mZkkH6X6qPF8IVTYtmgr+nQcamGY0+7VuVfk3p8+
9/A/4q7/K6Z8VurjvJKeAVWza4Igu8kwjNdgmhn3fwWMnH8bGGnomNDF/i0wKMqQSMf3IBYAMQAM
wehIyvDV8Z1IOAUEfT9ZHKnk6N68/jldKjmOekmTAeaksz0TQCOhJ2PnFPR2Gg9+4io9R3I4p0WH
ADQMPOGDExqMComxPG7EcJ5KOxZ+/AAfP2n8Y4APDQl/hKQnOAJjV5Hr4eFhmAtIS7+HrDvHURQk
s8liGrYSZ7KaTbbhITOCQTy6Nu+3AWLQTD1//ryrq4uudKCS0AAU+ImQOLa1te3YsaOuro5mBGAE
8WHZ08x1G0EzYdHxcLMDQhLnLKh48RpXjfuJdv3I5kMxvdJ3SIJbvnr9qvUbV67dEJ2QkrogveRh
mb5bsdLY1rUsc623f1Bc8tzte/afyi9Yu3ZtjI7S0lIIAEBylKEgi+SrR0R/kxdy75c+9fYLDwyJ
37r94I07j67eKgH5gZ+i4pLmpC3u6B1gRJkVJZvVMWK22ayu5qb2O7fvPX5U0dc7JPAyfl69enXf
vn0rVqzYtGnTzZs3R0bIF1dROMiRxclkZK6dTrbkSNi8fTd5oY1QYGLukeP0U0O00l0u1+nTp6Oi
ooKDg4OCgn6c6TvLL3zv0fPFTxu3H7sbtyx3VkJ24rrzl+51tHcITkZjCDWB5kAVgr4KncyxjzEm
KgOtgJE0l6TZebW6ybw55+beYzfr2i1O8mUZfZE5sdA09eNXL74QNHKnoJoYbYDR7lVZVu19FLHk
woz4Y2FLb1+pGOm3ayx5vYnTZIcmucnrRjBwxgElY8T1CUgr0YFzSbFwaptd6G/uM2UfejQ7rjBo
/rOIpfX/iq2My3p3/rnlvUOzkJ0jELlTAx2CmUmWporckOAT0BTHYPjqMLx0TBHDdwJDUB1fKO33
S1HoIZAEAzez2fyp5kLnYRjG4XBQTfRdoa+vb+PGjevWrZs3b97OnTuhjJKTkwsLC+nnW2gYEEN/
f7/JZKI+KHbqPyloveCW2tpaKFbDV0d9ff2RI0fOnTu3f/9+aKLjx48j3bi4uPT0dJxcvnzZ6XQi
CQC66c6dO4sWLcLVyMjIVatWlZWVwZOqdQznt23bFhgY6OXl5e3tDWUUGxsLmVHCSHqCUgbgg3rZ
unXrggULqqqqUAu0jig1ut3uK1euIAZk/OTJk4YMosyx6ojZdfDA8ZDgaG+vQD+f0MCAiJCg6PCw
6JSUlNbWVtoVaRK/Fq9fv0au/f39kei7d+/AQ4BxbRwgZ01NDQonICAgLCwMwtPRQ2dnZ3rGSp/A
UBg6dY2trP7dqfulT8AHcMgO7kUmcJzQQeoaW2ISU8Fqb+sabG7WxYuMqDx5WRURk5CQOr/sWeWH
lg7EABpYnbX53fsmMjEoyWjYqKyIiAgqLQSg0vISyvFniip+UBYZm7p63fa2zhEGhouTdXLStdt3
6V5ct+4+BM1AYF+fEJ/ZwThGRSTOn7fkzOmLI2ZHa3PP8byCvXv35ubmHjhwYNmyZSgc5Hr79u3t
7e3ItZ3ht+3aN9MnIDIu6WH5M8QGct2Vc3DeoqWoDpQM8ouahWznz5/H7WD0Bw8ebNlf8INv3DS/
BK+AlLikzH37L/T2sgw0sDKiak2qNgweIiwj6U/yMFDTv9mkEWdH8aMEXaJWWW/O2Fg0b0X+3Yfd
DgsJzHIqL7G84hJUHkWEYORZ0q+H2ap+aHEfPV2WuvRURnZ57PLH/vPvR69+f7cRyQ+5FRt5PQqx
Q23wGsZrnCY6yKqIn0Eb4aTAJZQGeuLjx4/Tlp/2SyhOXv7mbIm7vF7LOvTBb97+VYdvvhq2DqrD
I1qDTWuVkaaeHFoMQz4ALLJkRf7kmNCuxmOCSIavDsNLxxQxfCcwBNXxhdJ+VxSFdFH6euoYbUMr
Klp1VdPhgwVd3WYXTz5njKZFWpesdvYNnTlfdPLMuYEhMxlw6ctVyd36RIkskOXAyMgY9Ph/xuhc
ijGd8rFTRH2vSuo4VXDybifP8ArZtgBagwaDMNAoY3dBPPoEggr5/FU11JOXXxA014zZ/ouWrYSe
gthuQYb+4iSlf3hkw+at6PMfPnxAozfE+hmkKMAMRLNjtOsWXtU1Hzh1YW7GuvtlTxGeVhM47/bt
2z4+PvQdWAz5Z5E3dgP0k4CQyFj9U1KQTSx/XgnjwDcw1E//RH1UfDLCQDyM6KH1Wjp79x44DK0X
FB4NoyFtyfKFS1cgcHh4+NGjR9EhkSKSo82DAnmB8o1NmrN9d86g2cbrT26Qdxx7B80XrtxAVMnz
FkLxOTmilSAG8o5gVhd3+PhpMr+UlPqm9oOT5YcsNovdAZUNpanX1Rc3P0gkw4YTOrt7t2zfFRJN
3lTzDo2NnZ9x+94jFwcrgohEawpHlLyLk+4+KkuZv+inWT7TZvnAaHj3vpGViNjVdQ34iby/flcP
mWk9Ii/H888iPMYZQ0NDyDgtB0MAHdDsyCxYqmfQTCZ+FBWWTe+gaf6iFUGhienL1r+qbohPmR8e
E1dYdGHEaUMDY2XJwXAXr96EyQJ6uPe4AgLopURWCeoLBdGWFAh87/GTiJjUJcs3PCyrKn7wZN2m
7CUrVqOmUIALFmf0DJhMNlf2rn2oSjQzGGrPX7/tHzaT78HrWUZpo5p27z+0fvO2xctXob6mefkk
zVnw8s07pGi2COs35nj7RabOX4Z4MLhwsE6TfXjPwV0/eAVdul1sFWy85rSJw53mjlWbN0Qnpf3k
He4VvOB/zoj558wIL7/oQ0dPDQ1bYIBLIquoTkUzq0Td67YS8kO6MHqyqMqcAJEExalqXZx28FZN
6qbThy5V1PUzZEpvHIwy1UFXjpDWgD/SIfQIEa2o8RwH9iQbNGoOXrPZ5CETb7HLyr0XLYuWH45I
3b0tr+Jll7r73NvlO65cvt/YN8ChZNGZaEv+tI0Zb9rqjmoePTmyjAQ3uQTRpVlN8kCXy3X9dUN0
5jmvlJsLt5YWlbW22eSKOtfyTdc27Lpxt6K5yyzYIRl5LexnGGnooO1nDIavjt92yfCaDEYIHb9L
uuNhhPj2+H4pCm3FPGzft+dYdOTc3AMnbpU86Edn0JsotOqpwguBoZHQpM8rq3lBRkegHykTRKWz
q7+5pctqc43ZWLSgSbSjGE9R6MnQLC5OsTiEoRHWJRjqiZMVk82ef+7ctj17nr56ZXO7kS7YRe8j
9C5lxOF2skQVood3D5jaewZ6Boehc60utr27Hyr4zPlLr97WDY7Y+4YtXf3D0Ju4vd9kwTj96LET
169ft1qttNgNyQzo3VNTMbTPOXgkJnneTz7BgTGp3mHxl27cwXCbhscJbAIod9gfOLoFCOx8VPEc
DARVW/78lZMTwZ3Q1+ExCRgaV1bXgmXRs0GldKdX8OijimcQCYqVfnsJWpJsrqpqO/cd8PX1xUgf
HZtKOB6DI7blq9cnpMyDueBgRcQJhzKpqnm/Yk0WdGh8yry7D8vqm1qHrU6bi8MlFBpSH3EwOYfy
MHKnmxj9c9osqNeg0PCCggLUF2rqy5ufiiaCOlcx6pZYKBFBtrLSttzjvpFJJwtYJmHaAABifklE
QVTOW10MUoRUyB0cOLK67kPakgwMFx6UPcUQZ8ee/WAI1BGKCA6Fg3Jbu3ELKhGBOX3pBFT8xas3
yKzanDl0SvBTg/7eo3IQBpoiaQycyIoyI5AvbM1ZsIx+IzF75wFQ/+LlK8ueP+XIQEeENe3mxVt3
HySkziO7Mj55gRQhJCuSZWHjKarkYXlgaJxfYExk7PzQyESQaObaDXfuP27r7icaWlJgCZ04cw7G
H2QAy5J61DdHhxVYcOEyRirBETHz05chX7lHjoOlUPKoGtQa7h2xc5u35Xj7h0bHp3YPDKMBOHme
EcSDeUd/8A07dv6aXVKHnFJx+VufsLQfZy8Ijdm6dvPV5IVbN+06E5eaGRiWfDL/gs3ugt6XBLLc
HD2NdmI0XnRGnVBgIHEOWRyWtFftzr0F9xdmHbr6sJpBD0ST4CVUIm1RFEaZUiCqUWWAyPQlFpxE
pvHcotbKap2C5kKld/RrF661L117Y+Oe0uJn1l6T9r5DyzleFbfoyJWHTSZWJS8ZKTy4cwqK0pcp
CnBk6TdZaSjyMjiQkE2rg7lc1pyacdknLD9zU+WZK33NQ4pJFltN3IWS7szsG9sOPmjt5xlVc6Id
km+7MOLHn/E10tCBpA1fHYavjt92yfCaDEYIHb9LuuNhhPj2+K6WS+ite7RVSoIK4+XQgfywkMTk
lEX7Dx97WvkGJOHmyVZj5y9fD8Hwcv6imoZmlryHrdlYrrax5ejpgiWZayLikpatWnvu3Ln79+/T
7xPq8f8MVqJbRIvQC1BJVTWNj8pfHTt5cf/B/KMnC0bsLqrdXlTVrM7aEhQesyxz3cG8Uxuzd9Z8
aCbKS9OgYt7WN4KBjpzIz1i1juzXmZEZEZNAHmCQbdxi56QtXgZJYhP9QyJwhD8slX6TddjqOHz8
FKyZkPDIwsJCiDdZfaNpkEc7DQ0Ny1etjUmaGxqX+pNfWNyC5TdKHiI8yAmdzQg7CuQFKjL38LGg
8KiDeSfBiMiC2e5avzkbnLFp207kFGSMYFC+kGTb7hwM/wvOX3KypBzAH/QSzJ37pU8w4o6Li2tp
aUHHpkyPdCESfiL123cfBkdEHzx6YsThAp/p+8OSTceRLt3/FKqQbrcaEBoBnQh1iRRRfRYHA42c
Mn/hyYJzqFAQw7lL11auWhMQELB06dL6+voJveLTuhsHo8HgD2XBgf8Yfv32/dMCIqHHMWJwctAX
xIhEvjp6+0HJyXPTQOEQAyOG4/mFqJRjpwrAmqyogtFBAAuXEisKVI1boGlsbvbKzWIECwkJefr0
KYT5lKLAyqCB6IRkWJMYqaCEQS0o/GWZG3z8o9es3/Hidf2SFWt8g0JyDh3oNw1BObvIxgYqjCeY
UKBMQoqk2MlzlAlWVNHV27P9I+YuWPH0RZ3FwUNUBINspKb0ykI8Z4uuojlhxFbb0EJX+sG19/TD
bobFfPVWyZDFMWCyQjZY1X7BYUi09OlLFA4nycdO56O1oCpRPk5OhrO5hEN5p/+Pf/qt2Ho0Nm3r
P2YvCE7Y/F+z0v81OyM4bvuKjZcq60yv64eS09bN9IlYt2FbU3MHmgSxOFDMgv6ymEwmG3j9pSKn
ppll+VmbZdWR4vnZBRcf1lpYlbRcSSHvQJGPaXx++waMPPWVeZIqC+SRkcRqolsThhXbgFnrHtCK
7w9mrLmetf3Bo0q2fkDtE5VmZmT3kZqURfkHT1RaXGTNICw4kbyA5RA1Yyw4KWTBrRcbKVuwGZq7
iVOKn1WvyN6XlN6csPjxqdv13RzbK1hsmgRD+MVbfsOOhwdPV76sddoEza0vWiEvN5GxEKO/6TU5
pmjPE1r+FDBu0GF46fjyyA3fPxxG8jqmkHY8vl+KEtEVBe3SxeKoiDle3uRDFRjyg6haOnsw5r1Z
ch+qEGr0dd0Hl6o6FPVNU3Pq0oxZIWH/8vGF++ds39DQ0LCwsNWrVw8ODurx/wyB9CeVkYQBs3n9
5m0RMcnglJj4BUFhSWmL17R2Drt5ze6SThVcnTk7zNs3AkffwJioeDJlj7aI7g2a1L/n6//DDG+o
MAzAV2dtOnD0BPRO9q69KfMWwmrB6BVH9P/p3r4QtfgB+QwEeu+Hlo5dOQdDwyPXr1/f2NiIqtI7
+c9Ac0JRoE2R8SmS4+Xi0udeIbGzQmL3HjwKwkB4Xt+dDAHGADaCuodpAl1stmOkSVQYhvZrN271
DQxdunK1rsLI/qHoRg5WOJF/Njg8+vDx03QzJHAMmAaKDGbQtFk+oVGxFy9epLN8FFROpA4igdEQ
m5gKkkYSRB/pnyuEQt+6c0+A/l3BA0ePD1nsSKi7fxgGGYb5iNZsd5JBhkD2o0Za9EaI1NnTm5aW
hvraunVre3u70YR1TNmOjQaDPyg9RlLNbmH3kdPTA6NQ8jApYCjAIoSEdQ0tS1euAV8GhkX+OJN8
uBb15a1//QSCweK8cOVGfVMbKgs+G7N3gGD0r+6qNe+b1mzYgpa2ceNGi8UCYWhRGOnrMNsdMCgR
D51dJCwlKWgGweEJM2eHX7n+wOaS7twvDYuODYmKyD9faLJbMDx/+aZm49Yd3n5BMG7Igm9i3RId
Cd02nqJulTyCFRUZO+9hWZWbxyCAlDMC62T2M0WFRcehzNHAcAmRoINA+LMXr8BUnebli44zd+GS
xDnzQU7IIELuPXBk2GJnZabw0tmYpISAsND6xlY0eCerWOzcnpzT/+mX5hWdtSjr4vErH/blv5ge
kfGP4Pj/DA76v/xn/df0WK+guUFRad7+0bN9Qy5fuSnwvCRyhJpAPDJZ6MZpvFNVbZr2wSRsz382
f/O5hzW9I4S8VZkFM4FuwDtkbo286fUZEGKSOUIfKBxQuKaNqNrzNvf2M0/nzL+buuDKqUvVFR+a
RjS+QzT1q2rBw/eJqy/lnH3eYSIvUjFO8u4V4TUZo0oZrGPEOxkwyAX7gc1sLu1V7dCq7RcCU3cs
2X6tuHq4oZGzulQnTGpNHVCUyh5TRs6lJXvuv+1mzIziIvtAEMuJbHyBBonmSqYKP58pD0WNYgpp
x+PXUdQ3zifpoaAP/YTI73axZwsuBfqHxyXMWZa5FpoC+hcmC/o/FKuXXyC4AYNEdFQooyGLDfyx
blM2OuT2PfuXrljtHw3LIzwsYW5Ldz+04aheIXqNLIOFalOU/sFhaFu/0OiURcs37z1849Hz3kEz
6f9654fuQEJIF0qWmiBEL+gGB7RA/rkiqGNc6hk0j3/EDcdhQIXx9duWtCWrMMROXZBeVVNPvi2r
TyHiWNvQnJyaNndO+pOKSvIGMHn7Eg4XkX36WjlRvhCXKF9RqaxtCoxJDYhJPXqqECqSWlEAsoAj
gnV3d4PwIiIiVq1a1dvbO1ZTIJUzZ84gC3PS0h+WPUUCnL5lhoMVDx07CWV9/vI1QSEGENRHR+/g
yrUbImITYC82d3TbWTcjCqxErBAyAaXC7pReVlelLJiHhG7dugWTYkwApOhyuU6ePhcakRISkfK4
ohpjWIZXzVax5P5zv0CYlRkdvQP0a0/IHkMey8l0wduTl69hwcCkO3oiv3fIDB/QmMnmhM1aW/9h
ZGQEqdAkcPwcEKapqQmmmL+/f3BwMAxovYRIEaEQICrIlWEYHFF6JpMpNzfX29s7OzvbbDZDcvif
unTnJ/9I77D4kIQFF24/Pn/rYeKilV6hcYmLMt+9e4d4cCNNiKZIgZ/v379PTU2dNWsWWDYlJQXW
J2QA6ZaXlyNdpE5K5uRJhAkMDIRsMBlnzpwJyywvLw+XjIgmw8DAwBYddXV1iAdpjQFXqUlXVla2
YsUKJPfmzRs6cEFIiIrjq1ev9h27sHHXkb1HCste1nX02Ws/dN0uKb95+/HQsF0T7IzN9Kqq+nVt
U4tFuPCy02dl3j8WHfDfcvXo5bYXTfL2vNKF645m7Tt7s6zqUMHZuRmL1+3YfODs5dK3HzpN9p5h
W1V1fVtbNw9CQBPhYbaYNVFiJa2b0869GIpYdzplW0F5l9UCS2h0cSnJ1ThArzuIvaUwpAc5ZNWq
ym6MX/R+ANuGH7ZJ7TatuNq8IrcyaMG5Hcfq6to10dUrCza0RtBel0srejycvuba+qxbH6on7hBN
gWYAAYb0tNBBMTrVZIumWGWyOoNzqnKrW7v9ip2/7qFv9OGkxYUnLla29ludslPQXOBcq0vqZ5Wz
5W1pO4uyzzx+0WJzKOTBoZGMDiM/OpCi4ftLMG74ChgR6UC6hq8Ow3cyGCH+cBjJ65gg7efw/VIU
sgB9cu3K7Yjw+MVLVza1d5c8LFudtRnalj5kxggUY+GbJQ/AJVC4uLpjby50a3h0vI/+AboZQdHk
M7XRyRWv3mHYPp6i9G/akm4jysrVm8WJ8xdP8w3xjUgMTVywfU8OrBy3QLQkfbYPE2TTtl2NbV3j
KQqGFEw60M+Z85dGHMx4ioL0rKjVN3YvW7EpIIR8ruJp5RuMylmJPOOh9FZV8z4haV5qysLy0udk
5puwAATDcRKKYiX1SVVdcNwc/+iUvQfzoJio8qXVAUMHwS5dugSTESoSqhn5wlWqT3H+4sWLUP0r
ebBsYGkhdQj85GVVesZKFCDZFFVEZiWL030w7+Rs/+DsXftQ2kMWh51xm+xWt8ATsVTFLfImq3XJ
yuW+wYEZGRlDQ0OUKSEAlQTnr6pq1qzf7hsYferM5f5hR9+Q7d6jFwsWZfoFxaQtyRw0W8BP/SZL
0bWbjyqeP3v15vGTFxhw7Mk9DGMULAXjGByG5BBm/aZsmCYBwaHLly/v6upCEsgLqcDPAKm73e79
+/dHR0dHRUXdu3cPPvD/tCdAVHDV2bNnQRVZWVlgdPATAg85ufPX76akrwyKSfENj49MTsPJig3b
X9c3IxJklrZ5SgxjwCWgr6+vqKho8eLF4EgwyqNHj4aHh+lVGjlkA3+0tbXBNj1+/DjIqaSkBOxI
hfwckCLuAhAM5+NBfXCEYKgCxI/wtG3QG+GJpFm35rCpjFs3ckgOSE0JPEO26GFkF6+YXNqjyo75
a46ELtyz4WTF9erhVya1tMa66cD9vKLK6jZHt02xEFUt23lYJoxJYKwS7yIfV5N5WeIlcDD5QjRY
gGdYTlD6bFLu+ddpm88XlTe2OyQH6EdnCJL2J3WBsRl0PdmTAbJrnKiJ5IGcrLGc5nBr4Kc75Y2L
N+fHrTi0bMfFcyU1PWYSAHmw2QUro9R1stuO3l+y8ezpoqoRm0aeiI2DXjMEKBMIQLd0ILaSJKoi
Ri0SKKh5QLlS1pmZfS1uweFdBytevWX6TZrdTTYKYQWXizWbWeVDv3vH6ZKNeXfuVvd32CXy0aZP
1uUZ+dGBFA3fX4Jxw1fAiEgH0jV8dRi+k8EI8YfDSF7HBGk/h19NUeNh+P5ugAL6iKI4Vrx1815M
VFJUdBIG9VBeIInb9x5FxibST8mBrq7eKoYFMGC2gb1gE/iHhO/KOQCOaensuXD7UfyCDGiZWw/K
wTfjKUoUOJ2iROhyk83Va7I/eV23/8RZjJe9/YNhVxFSQRsVpUPHTkG5py3JeP2unvgQTxk0Qyf6
oPphsQ1bnfoHighFwUapa2zbsiMnPmlhSHjSuaKbdY2t0PjVdQ3PX1f3m6wgVFAO4g8Nj02In3v/
XhmyyfPINQQjXPApRblF5W1De8riTN/IpO17D1A1RKsZJ6gIaD2MzTFyh+6jVgIu4YhLCAB1fORE
/kyfAL/g8FXrNy1ZsTpz3cbYpFSU3pWbxS5egilT/rwSuQbjghVCImPAFrgaFBE2fbZX9u6dPUMD
sKUwaL1X+sg3KDAsJurgwYN0+D/WDKgk+LM4mbsPy5Lmpnn7k8+0w8Ulzz18/NSIHeRI7F0ny9++
+zAmIQUFC8sJpnBi6vwrN+/QZ0KwrlC2qOu+YcvpsxcSk1OTkpKam5uRBDJF05oUSJ0KY7Va7Xa7
XjykG+BIi4texbke3JAcpYQjVaAYx9AHV5xuxsFBe+InaJUC947dPgbcSxIe5TAEoD8BXEJlwROg
CcETJ/AfC4ZL9GQMNFoKXB07+TTk2FWcj8VJJRyLXyMfr9UbFFkjBE9ZBK2ovNk+fOHxQPyKfN/E
nat23X7ZyJg4rbrJkbXzyvptF28/aQIzjfBkw1e3rPJkXgH6XVTI4yOboDlFMgzjHRxaPkqMFJ1N
Vp+12HecKMncVviiZhijNnKBzFm6MCSAPGNZoDJTKOQbnIIkE8ncEvlc4QCrFT1qXLbtYvzygwcv
Pu10qAOM6kTVyLygOBTFrKpDVlFrGZA27r62bO3R13U9do4soiCG/sdL6WgJ4ISmrqLXIx2RvK3b
b9ZOFL2PXXzOJ/FEZs7b5qY+gUfRkTk7jlXJu2mi5nKK7+s71xwpXrb7Ym2fyyZpTpZEgMLVRLDq
RzRF06IwvEZh+H4BjELRYXiNwvD9BMblURi+OgyvyWCE+CUYoSeDEeJXwrh5FIbvlPiunkV9RFEA
zIuK8pdzUhdGRie+rW+AhsMFJye8qHoXnzLPV18kffHqTWg9qH5QFLTwnLT0LTv2wOjJOZS3JScv
KHbOrKCoE4VFDgbqkQLlgjZrbLXHcHzOwaP7jpy8fq9s95HTC1dtDtG/s4ehPSNISPHd+yb8nB0Q
Aj27fvO2U4UXuvqHIQYjKvqzqID9h/JGHOhc5HOI8G9q71qweHlwePy0mYGz/aL8g6Nh89FZLMhc
39gKXQGHYAvTly9amIEMks/YyZSQEMEkFAVG7DZZk9MzZwbH7D14FONlFA5UErmqM9DevXthP8GA
6OzshD8G3Qhg9A8ddoZ79qp6b+5h5GXeoqUJqfNhbpInSXqScCD13CPHt+3OWbcpOz0jEzk9lHfy
4rUrdx8/fPu+zsmxRJGoYGixpbMDjIV0JxgTFBAasXFk9zFlxGmHEWZ1ORwcAwuMJ9rISA5KH0eY
quAk+vgERzA3vUq+vycp90sr5i5ckr5kWVVVFWJGNmkS3xuMItZheP0SJtTOeOCSEejXwLhZx8QY
FLLaGkYD2rubJ9t7g3juPG2MSd8am3HkwqPOdoc2JGn9rHbqatWW/deu3PvQaZadsN31WTg4NCZ9
tSypJTiyZRxZv0new2ZlzcaosCo+dFi3Hr6UtuVE/q3XIAwJt5G2pYAQFZUhm2CMAxqPIRtpAwLs
M4eodQ6JFVWDyzecSV93svB2zYsmR7NZGSGbfqNtoGDhyDoGSZOGHNYjtzpjV+Rv3H+zf0RlWLK0
EwRKFuNpH9EGEqLAOUmMJ6s5uga0w2cqwubuXZ97//Stpg+DhBQxbhBFXpB5NF0Gw1BFe1Vrzjny
aN2Wq9df9gyDrmQN1/SXnDBsISmSbQC/DHpGJ4cRQseEijN8dfy2VvH1mKKhGiG+Pb5fikK9gKJK
Hz9LSU5LnbvoQ0s7zBcHg0Gp6ubF+qa25avXJ81ZUKx/wxumwLHThSADGAFevoH68/CkgJg50wOj
fMPjD586i9HxeIqSJaRCtDzLCzv25s70CwmOSZ4REOkTkRgaGQt6g9GGhMjTFFEuvHA5ODwaLAWa
gXEAewiqFkNs2FULFmeALyEuJ/P6WizVyfHXbt+LiktNSEmPTVy4aOlqcBtUbfK8hes2be0ZNCMw
dPGIw5VfcPFS0c0Rk40sDJEgHQSjWuATK0qQXZL6prGjrOq9yeZE+0CvowyBNsSOfpEP1AV/Sl3o
cLQlUcC2I58E1vcVQ6YoM+EcTABFA7rFOTzBDdA9GBHrZiLhJDIyJV+dkOkX6NGBYUuBbAAi22hy
Y0C3Fck6YwyeMbImw2j9LTXcJUAZjTEiSVQgdxprBPT10zhBVUIYOJuLLXv6svxZ5cAQebuZ8tO3
ZymU+efcZ2EUsQ7D65cwRc//bcrIuFnHhBgkbQSGEK8JvQ723puexDUF/4rZ45N2cddVi1WQHbI2
zGsVtR2Zu44X3H/TxSkdgtaHhqHJMBg48qYtOg6qizZL3aEW0RQI8Wkjdq1vWMk9UbZyU9H9F92D
brQN8s10GhzMhpEHYbuP80unAXC02WyvW7o35d+KXJYTtjBnx9Fn79thQGsMT177RSBBdEmclewB
ITJolCynFT8fWrbn0cIj96/WmocVzUX260UbIlYPaAPJGWmMghYCugPSunG7de6ygqC5R5btvve0
ixnE7argFG2i2M+R7/wKNpUxq0KHi8kterz2QNmNSqab12w82W+XLNZDeRDjSQZVWdErP1+JE0Bl
mBRGCB0TKs7w1fHbWsXXY4qGaoT49viunkV9BMSPhoV2XF5evnv3broEAP5o3GTEI4pQyu/evWto
aIAKoxrTYrGUlpZmZmauX7/+zZs3CGC325ubm58/fw4lrsdqAJFT9QrgvLu7u6mpqb6+vq+vDzHT
AEhuLGb85DiOPk6AP4BguERBbxkDOIPeDuFxQj3HTsaAAPAcg+H760HvhRhE1ysy2aiScAB0CDo1
ORfGHsP9e4Hag6STuo87IC2QSWGE+CXok66GowvkvsQZN38bGBnQgao3fH8n0A12YJGqiluVXBge
SKJsZcnqgIZBLq+ofs6K8/6xhxdn3b74oLllhBtWxD5lsHmY33eqZM2OE3ef1VsFlfCPIjp5F68w
qmbXl17rtYPmA4fCJ0EYVbKgX5hc8rmS5rSss6t2XnxR38vIMkdepIXjRJXlZTeZGlTJMgkMVHi3
IPGkGtAzGtutBddepqw4EZCaOy1qR0rW5ZwrdW+HuC5BMxE+k3l9/wWkK0sySkomnz3TRlxyWWVn
xoZTe0+Uv++V0JvRL8n7Uqogk+9UcfrutKTiYbSxstUtWljwDctXNQ0dPfd8QebVsKTz8zceu/+u
c0BULKpsV8isn942dJsesslayYumTUeu7z//+FXLiJ3XyOpbpDEOusKY/KHaF+L36vV/H3y/FAWd
O9YO6AlSBBnQdOEDJhgfhoL+xJEaGTT82M8xUE+c4EgjpEwz9hNH+hPAJRqeHvGTBsaR3gj2wsl4
oB3TpGmYSUEjHIPh+ytB8wVuHh4eHrKOcPoTo6aOtrrGD8NWC52aEzC8/B6A8kYuJ3Uf1eFHSmEC
jBC/BPo1A+pQi1BDX+K+KYwM6EDVG76/E1TFRchJZlQZKlvhJNWuaM0WcXvBs+T5pbGpVxauvHnt
UadF0todtnbnyIjCPmmsXrfrxO3ymkGnPMLKrP62ryCLsIL04c3HL+GSaT4Md8ibs1ZZbTUxuQX3
lm7Iu/34fb9F5NEVoONl8AZ5QmMxuTo6LQMmecSl2Titz6K9bmaKHvVvzXuauOxYQOLmuZkHrj1+
96qhp6ZjxCoqDkV2kg9mwFzjdftNJP8ziIxzCHY7K1e9tR87UbVq3fVHz/pHBM0s8UhKX18hCrJT
VK2SZhU1h6BySN6iNZq0QZPkqu5w7TlVFTm/MC790vr9Lys+iGan5JJEp+B2ibiLPGJD/xR4iXEp
db3uA+cebMu7Vva2e5hRGfKVDImsvuDJpMUYPBT1x+OXKcooSx2/e9fy4EtAS54WPs7RScB84+vC
ZrNt3LgxLDpqweL0JSuXxyYnHjqe19bdSY0qEJUR7tcDCSE5eo4Ux5KmIuEIJgZNUiMV52PB6F04
jsmJOwX9qTgcBKLnOBEhof7kmbI+wgNjqVCmx+04Ijb4001DYNECJpOJrhdHgLEbEczBiFYn+6j8
5fpNO7K27Lp8427voLE1CX3zurmjv+jqne17Dq7Jyl62MmvF6o0btuzav38/3W8Q2aGJjoEKA2v7
zJkzR44cQdI0g1OA3jIGw/d3xVh+NfKtI57jBJekVdQO7SsoTdtYELP08Nr9xY09g05FHGLdYAKb
JL2qN50892LfwcqXr3kzw7OEjnSbG8ymvyqrP26RYY7gAiqBx1iQfHSPGGrDovqytT/7xMOlW048
edvKKRrZllhRoMwZXms3a+ceu+Zvvv+P6Nz/5pv9f4Ts+e+RB/7vpBP/I+H49Ll5K/YUX306+K6b
JXzCkRekUNkK2VRVItPJSEF1aKpVUxwyz0pkh3QYMVp1i2l77pU9B+9W1TLDFn1qmuyNIpCZRmRa
kUWV42GbadyIzFo0yaxoZe+F7Lx3YfMLoheeP36lucWsjCiKHaM4rRHDRvKpd30BCdJHv7A6tfuP
G7bturoz/35Nt8MBq4rs66IviABTUjcORl3qQOEbvv9WGNLomEKkP6A1fiN4KOpPAKr6JwB1oesH
Y8bS6XQ+KCtduW5NXEpSYdEFTiY9EEoU7msoCoC1WldXd+PGjatXr75+/RoJwRPpgpmQLsMwZWVl
W7Zs2bBhw86dO7Ozs4uKijo6OhAGElIhcUTgt7U1O3P3LcxYmpaxJH1FRl7+yf6RYWSMVURW+JkS
EBhHxHDs2LEnT54gdeqJGHB+6NAhLy+vWbNmTZ8+3dfXNyQkxMfHZ8mSJRUVFaAuSmz9/f3F958t
WrLWLzAmac7SyJi5P80I9PGPKrpy1+rk6CtrlVUNYZEpPgHR3n6R3r4RweFJS1dsPHnyZH19vS7F
JGhsbExJSfHz81u6dCndsgTJGdcmAwKQPjMKw/d3BZWBNAM0Ek1r6GW3nbgXvDBn+d7bBcWNAzaN
4TSHMmLDEEIFK/M7dpXs3FNcfL/N5BBZ8vQR5Eaesch0AQCiIUv29CdJOB99CdetSQ5N63Rqm4+X
hSw6cvzCyyE7eVtDEu0argo8LLZ7byxxy89Oi8xJzrxcWssMClqLWfvQp5l4jXzNkBfcPHmXAHKS
5oD/UHK6I0yjcyEoElaaILsJOaha94At/2L1gpXXS572OxTVjvonT1GJVUeySubmUKgYfsmMRhjU
JGu3X5tCFub5x+Sv3VFW9oZxkEdNEq9bfrwCGRncLugfBnZryoiovWgwz8k8uWb3/cuP3A59byUi
Hww0kVhyKFuk89Hjge9SGRrS6JhCpD+gNX4jfFfLJTyYHFQNvXnzZv369Vu27aiua4ApQBfC0XfC
yp5V5hzKW7l1Q0pGum90mH9sREL6/JXZG5/Xv3VpspNMdBGgmQJEo4220U/JjzZles5KZCekOWnp
gaGR/5ruhePsgJAFi5cdyjvZPWBC0m5BKX/+KnnewpDI2MTU+YuWrYxNmuPtF4STF1XkXTSyCkP/
VFLB+UsBoRGhUbFrNmzZsmPP1p17A8OiwmMStu3OGXEwHFmKQd7jATMVXb0Rn7rAKygyMCb19OVi
C0NERAY4fWOhgguXA0IiIMOzV9UDZpuTFR2sQPZzgPYhS7/Ii8m37z0KiYzx8gu8dP2Wi5dQPg/K
niakzo9OSHn1to4lezVp5y5d8wkMnekTcKrwQmffUGtXH4R0chIEJkN1kSzfYMlHy0m6iOTxkxdJ
c9Polk7z05cNmi1kXQnZGuOPUwpgDShpUu/QzyrohxdESRBFq9V6toZP3VMRufTI+WsVbruLKFkF
+tkuio4uUSttHsk6emXL0QtN/SP64xUoNpHspqDBauGIxocmJkv3dAd+go9ikQQeSt0sah02ddfp
J8u3n773qtZMloe3SKoJTDLMaZ2MduGJJXrDzelLTh+68rSpqQkjCYM1vxCSKnGqQDYBkd0K51S1
Xqdy4lLz0qyyo2cquweckAVlDHYBYZIhCCSH1SW0qpoZ4Xs47fxLc1pO2U+phyIzT16saKcznm5F
YhSBR8RkxQeKjVNUVpFkRlItmlYz6M48XLT+xKWnLV0mRXUpxFL/HGjhe/Dvgoei/gQAkaDnFxcX
w2KY6TX74tUbZF8fUIiovq1r2LpjD/Smf0jEwszlGVlrsnZmb9q9Y+Ou7Xlnz3zoaqNqh/Y0t9vd
2dlpMpmgPfETnvRkDGM+UDEgjLuPyqDr45Lnnr98HXxgc/N0r1XfoLBFy1Z09Q9D+4OHdP5QQFcg
ztqGlsXLV/kEhizLXEs23dCXkrvJzqQnZ/kELFy64t37JrPd1dDSAUUfEBq5a98Bs50sRJQl8emT
J0lJSb6BIYHhMdP9QqcHRGzae7TPQpTUGEVBEr/gcLDggsUZq9Zt3L5n/9VbJX3DIwjAimRtCDij
pbN33abssOj40Ki4XTkHL9+4A8oEIeWdKrA43YgEKd4seUAWf/oFevsHQwzECfYFfeJeZBP5QqIQ
3s1Lze3dm7fvSl2Qfv3OvS07dk+b5YNzskOSvgQR7nP43SlK38+fGDwwKFhOs7m18srhzM1X5i4/
s37vpeuPGnpMArERZY3lFbeo2kXl8dvm9YcunLz1pK4PxEU2qoOZpNe9pNsKPHnAhDzQIiaTXCAo
hnw1QybZ6+hjDxW8WLQ2/86TbgtPvvDE6AsEh1it7O3I2l23l226sPlA8fWKlh5WcZC5QpJNHOn0
7JeA7LqlcoTsYW85tDv3e9NXnbxQ/Lpp2GERBE7hYerxqsipZNMkVhFcIuvgWbuqNQyIBy68CZhz
KDTt+O4z1W873HaygSv6BXmSRh15lZ44nBA3ImtNZvHo1Xer9914+G6wB6YZ5EVxeCjqO4aHov4E
QD/hef7WrVsBAQHBYeF3H5Vzun4cGrHuyT08yzcQLPK2voG+XQuHE0YSBBXmAjlHSKfTWV5evmHD
hri4uN27d4+MjECB0tGukYYOOvillwYHB5dlrvELDss7dQY2k25bKH3DlmUr10zz8gUjPq18gx6u
G0kSfenV5uYuXb8dnzIPen/ztl3DVjsvK7qil2GmZK7bCNqAybVq/UbQGHhu+56cpvZuxCAiXZm8
5drX1wfT6MK1WzMDwmcFR2ftOtg5ROwVSImRsFuQkXckDUZJmb9oxZqsyLik2f7By1evv3qreMRB
NsfT3+8mJLTv4BFwj5dv4H/+axqsH7IVHisgBlx1cmLvkHn77hwYc6/e1g5ZHDUfmjdv3w2mT8/I
hEnq4gWrk4HxdPdhWWRcIlgZRhsjyPTFhrQly2FFkRX8Oj1/DijG8TB8fyVwI2qE1gtYQyVLrFW7
U6l5P7jvyIPouYfW76koq+FFQVZEQYJdy8tWjnwVo3mYO3ix9MjV5+3DTptI3sB1kgV/MMV0aQjV
Qfnr5pisgPn0hzScoDKMbGcUu9miXCt5P3flqexjz2+9tLVZtX6HZme0gSGtuIpftOlRYNKBA/lP
W/sFOw+LU5b1iKjAAE4g8OdA0h89gb3qUJwuUa35wO7PrVm37tGzV2abKLo0k6jZeMXmlm2cyrlk
3iErDlUdYKXnjd1rjlX5L8qfFrM/50xt57DiFDSO198Cli1kKpCIoCCnMIJ5VbMLqhVOVC8/H1mx
58He/Bf13QJuIdO0kJrMbRqvV4+BRDAZ9Dr5KhgRjcLw/SUYoSeDEeKvCw9F/QmAPoNhaVlZWXh4
+Cxvn6KrN50sDy1ccOFyQso8GDpPXlZ9fsKJADqutbU1Nzc3LCxs7ty5jY2NiHPCQvwxQF3hand3
NyyV6ISUJy9fw7CAEYOhrouXQYpgF2j/h+XPyHwjWetFXi6GPRebNAeXQJkr12a19wxAxYMPcJVs
FTFkXr1+04zZ/mAXYgb5ByMYNH59U5uTEwTyYikCki6HVF5W10UlL/CPSt5x6FSPyUEpChlEWm5B
GnG4yWQd3VtdUqpq3oNCQiJjj5zIx0+QJUw9ENh0L9/VWZvvlz55WP4UBQWrDp73HpeDUGEz0He/
QJ+IE4YX4re62A1bd8yc7b8xe2dn31BzRw9uJznVGZHs+548F2JTgxUlc+feI0QCm8wotW8GWh2A
XZTvVVk3H7qXsGTvhr1Fl0te945wjCywMIZUtyIzDoYZdImHLjzKOnD51LXKAYdoZ8k7gCAgEBxx
o1HpP8jDKEIrZP0KjCnFqShOTet1avm36n0XPvBd+ihs04t/Lrr632NP/mfiab8FReELLgUnFc7P
LLxyv6XHLtskwa2wLMqALNhGQX5UFDShz4GGQbN0S1J1m2vvsftb9l5/9mbY6tJ5U1HJfmGKC5zr
4t0WVnTIWrtFPnGzPirjdNyqop3Xym7X9QzBbJJVsmm8zrUy2RSC09+RIm9zyWA/BFC1YUa7/axx
WfaJI0WvWgZ4HnYjGB9NTiLLc0RF5T5eWT5B7xu+OgyvrwAiN+LSYfj+EibcNR5GiL8uPBT1JwAa
IiiqqqoqJibGLyDoyo07Dp2ijp48A7NkyYrVnX2DULJkyuIzDk0c6gBHWCr0C1UITn30FAzQngB/
nDscjnkLl4D/YEBYnCzICVYULhzPLwwMiwoKj75Z8gA+RD+Q6TUZpgmkuveoHKYJCAy21NOXVXSz
qCGLbemK1UFhUReuXEdgUNew1Xnn/mPw33/9a1r+uSI32SEAV5A+oaj6ls7khRneobF7jxX2WzD6
J/oPl+FocoiT/gTHNLR2gqdBeDCDIENtQwsx4/yCpnv73b73CDeabC6QUPGD0h9nzob1BkMQt4M1
cXTz5DOVNheH3IGWFi1bCarbmL2jsa0LOXr85MW123fPXbr2oPQJrtY1toKZZvoE/DRrNtkZUn+7
GTJ8U6A6oEYbGhpg+/rGb/Bbem3zmbpuTuvhNLsCfnFr2qCmtGgax4nMk+raTYfOP64f6nRrThSa
oKksChUEpk90ESND1OkJf+TJ1hhFwToDyQwxWl7Rh7C5p33jz/4vIQ/+1+iHAbu7s59pWQ/s/7+F
JT4ZTw7cYN8OkteAMTjgVMbK9wlkdZ7NqbrcZMbwIyCVz4E2MIBl2Xtlncu3PHz42jICO488CAOz
kCkCidUkXhbI0Egre9myYuOxgMSd4YsKzzxydIhar+YY0RiGbA3mIAsgyIbo+I+8B6Z/tUOnKIHs
RltcXrt5/7kNB67UDbrNDOEyTWLIh3ZlmwpzWhWRqPljaSd0CsNXh+H1FaBdbAyG7y9hwl3jYYT4
68JDUX8CUC4pKSnx8fGZ7RdwKO8kjAmbiy2+/xgGRGRcEiwJWBVQ39DmVHEzAvkOBRQ0tAk8oeYQ
CRo04hlTEMCE3jgG+NtsNpgdoVFxUP2Iys2T2Ty7mz18/HRQeFREbCLdZQNxQYvQZxkQAIq7q394
+579YKmde3PtjAAL7Pnrt7BCICpOEIY87GGEsmeVMEd8AkNhXbk5Y0UfFa+9vR2mnp+f3+LFi2Hw
gZ7hTwNYLJaWlhaGYeDJizLSgjywcmISU88WXQFlmu2ubbtz/IJif5jmn73z0L2HL4qu3jtwpCB5
7rKg0ITdOcdBtxC13zRyMO/kifyzhRevwMY6VXg+Y9W6qKionJyc5uZmFBeglwQBTR3HwsLCiIiI
uLi4Z8+eQc7PmaG/CH3YQBwFebUaDmKR3ThkhueIBSGprUOOnMIHkUtz03feKHo59N6m9gjkI0yS
BnlgG/GcCDtG63VJ18uad+bdKrz5AlaUE7WPuiCkJIrkKaGmLyankMnnkwwnuDTGrjih4PvM9sIr
T5KXnzly0566tfa/h9/473EXV+T3rTzWMCvlUNyK0+Xvhi0M2eSV4T7LPSgfIz86DF8d5FMYCtiU
U2HokLak9Y9wl+/UrNlScP72+26bykiKw8Wqkk1TbarM252yg9e6bPyVipbohQcXrLx08XZv6QuT
jdfsosiQRYJo17xMniHBNIQRiALUZywlxS2yKKIet1DZaUnfeebgpScfhtwuiXwzypBmMhhCe/Ax
aMsfg+H7FTAi0jFVgxl3yUNRfwKgwqANX758CR3qHxRy6fptt/7JJQz/b997CO0fHBGD8X5zR8/g
iN3OcM0d3S/f1Fy/fbfixWsSEnpJls1mc15eXmRk5Jo1a3p6ehAtLKoJrQRASOqJY82HJkQOcwTG
B4wJuJMF5+niAhADWBBmBFgn9/Cx3fsPXb1VUvr0Ze6R4/MWLZkdEBKTkAIrBKoWBGln+DPnL8HQ
gXHj5RcEy4zOnoGiEDOoDmSDdkkTherv6+vLyMjw9fVNTEwsLy+n684hLcdxbW1tq1evDgkJAYH5
BQYHhkb6BoWtWLMe+WUEEfYZ9DxsqZaO7vOXb8xduNQnMMw/GCbd/KQ5CyEPWTyoEM5mBBmltGXH
HnAb8hifPHdP7iGkiyRAfigEyDNWFAD1ATWaTKahoSEEgw/8fyNLIdbRgld0LuE1gdN4TuOGVOl+
myNl080fog+HLbi2dPODp+8HhiWbWW7mFBt5hCSTl3pYUbNxauuwe8Ohy9vP3K/vsgwxskshz130
N4c0iegWMmE29llbEBUyzuuOA11oZAOJ5l7X0ZNXVq7Z8ajiTfeI0OGUdxSW/ZiS/d+8dvwr6uC6
AxXvutlBTnOrqp2DOevgRBON7VNMaEuGrw6JTAY6QHAwlu2sVnS9clHGgau3PzR18CxPtjmSJTKE
gmEHLhlktJJXvUu3FQXEH01Ku3G7rN8iyBaZdSoWTiXfGkUwFBlZCCGBldDARAH1QAoQTKi2c9rr
Xu3glbf7Cp409wkspzlt5LUxxW2Uw6QwhPbgY6BOjQLSYfh+BYyIdEzRYMZf+tNQFC0sNGNoEMPr
bwNkHAqxtLQ0LS3NPzB43aZsq5OlC9icnHDr7sPEOQvACiAq2Dfz05dB50bEJnj7w7hIeFD6hFIU
dOuOHTsCAwOh94uLi936txumLkywYM37xkXLViLyn2b56EvggpauXAOu6h4YpibUsNWRd6pg7sIl
MJJwFYSBk30Hj9Y1tjIi2WkUwWA2jdhdYNasLdshG4TEMXvn3srqWvL9WUVj+Y/EQH67u7sfPHhQ
X1/vdDpBTkR96cBVnIMkYF1Vvn7zpvZD75DJwfB0i+Exx0kyK8uIfMThNNnsDpYjQ27yEUsJNt+Y
A4OiGMccTeIXMaFrjYcRQscUwcispkocxwswB8jWroJmU7WGQWbD0Tvxa85FLDm9cPOtsneOASfZ
ztUuDrvUDp63qBKZqrLDCrHLJS+aV247+biqHWFciuJWFSholCPKiCSMP5yRt1D1+T39oQsjay5V
M/OaTdH6eeVGeVvmlhMHjl6ob+wRYMcI5P2nbrdwr67lennbuw5mmNWsguaEgU7mzlhFc4AEjezp
oNmhoD10UjCq4NAkp6Ygm/mXXq3bcqHiZa/ZqT9JknlYjIwkoBDMAkqA332ydM7Kg1sPXr/xsKtr
SIOZ6FQwSIFZZQLPYWCAiqRGItl6XRV4jXOoLpPIQ/j3A9ye8zVb8x5fL+vsHyGjCELL5M1k8paT
IY2OCbVj+Or4wvqdACPE94oJtWP4/lvxhSL9OSgKbY0qqSlaz18YyDWyTybu791buWpN/rki+p1c
8uKOPo8Hy+BF1dtdOQeXrVwzb+GSyLikBYszYNBU132gRgNuh6JHDE+fPr158yb9gOykVtR4kAlD
8jBJRXJOlocJQlbo6ZQDLUHJAJHDUKOpsKJCJvFYETcSnhideERg/R0j4nA74SSR7JUNI4xoG0Xj
xvb4HQVttRT4CTnRBmgzwBGepEz0lysRP+Jk9Odk4xx+kjdoOEXkMbAh51DVZDfbj4N95Ghyv4gp
Cs0IoWPKsoUN40SpgEHMDuVx5eCSDffmrC5Ly6o8dqZjsFdlWcnFsYzYJShdiupUYDm5NLtdMznl
vAtPlm07X3i/udUiQW2TEhQkVmX0LfL0gteXtBF+ormSWWSdLJhUyUq/YUlrGGFzr1Skbjl+v7rb
rmhuUWZAE4oEEofNBclciFKzsOoIp1h4xSkqDGwVkBx5aKVbZ2MwcqMD+TV8P4FLVR83mLecuJe2
vqjk6YhNIB+p1VfhWRzKoEVhn7T0bTl9J2RJXkha3pV7rcQ0xQBC5OiOsmg2aDCkFo2qIptf4Aju
A7M7VBQMXztg33L82d7zHU29CAubG5yMW52q4gQ1uzXFOmXtGL46vrB+J8AI8b1iQu0Yvv9WfKFI
nom+PxkwiEYPtLk5aCNwA87hXBxZaU2rGRr8F7nnC6G3HANf2G9lMAdVIz870BKoguxEjUgmjYf6
j8Hw1QF1S8lGIO/HSIzEcQoML7LjtDQ6hfUpRl+IGXWqQvaCUsFSOCGUSZxu4REjRqSOzFviSN/Y
BTdDeFK8hKrJCzSwuujDNtwFf1hdemDCETSnJKQeIVm+DXqWIbvOFuRhkwrheUIXol1lXIJgtirl
L3pXbX8QlnZ5Ufbjmy+6W4YG7JJVEJ3ERCDjMbIzgl3hTIoyqGgve5ktR24eOHm/rnXEJWpwkAQk
TT4Tr7KoeVSRnnWUCW6GyQR/6HZOk22ywKhkTb9W12zZevjyqj2nb1S/bx/3SBJAseu3/3awqmoH
6xLzDUXOwz7C/7Bxhqzq3sKaJXsfFD7u6rKRTWZl1eoSbQxMcFYpvNsRvvBSQFJexpbiO2XdI05i
r+n8igITx2Vq7HmdXiH8iCQyqKNhUbv6cnjV4bK1R+5VNA6NwMb6akxogd85DKF1TCH5hPo1fP8M
8FDUnwy8JHOScu7S1W27952/fL2zb/DV27r56cvOXbrmcrnAT0a43wNGc9YxRes3QuiYgqI48v6x
MWVn3DkKRD4ehq8OMlWjfw2ElcnLXuR9TPK+l4ifgkyimhSfyGA43At2AbWD4+1uHjYfzmGRWl0c
tcMIJxE7T4VnR0//o4rnN0sePHlZ1TNogqd+iSx8x0lX/1DNh+by568eP3nxsPwpHE4eVzyn7xEj
GLETiEECVQxtTVbDm93c0+oPpy8X7z32KHHJ2dnRu1JWH79X09ntFtywlZxuCTYeyotsCESeuHCq
Zpe01y3WjQfv5RS+e1bnsHFkuo0R9ezotKTT90dfYxoFVBJxsFcQT+8wd+ZCxaqNxypetSESGEew
bIwi1oFiN+77rdBtWvqQCxxL6sAhaLVt7pU7b+YUVjQPumCxmdxujDBsPG/mpTuv+udvLJidtDn3
fGnTMGdHZkXCP6R1yMjXRIEMQXWQt4tVrccpn7pZk7X/9p0XAyZGcaGVTF4Uvw4TWuB3DkNoHVNI
PqF+Dd8/A74VRRklocPw+v1gxKvjz9We6LMfqGkcITyOtOngSKewxnxwQs+p/9hVTiQ3O1n+8vXb
iXMWBIRGRsUlwUGTWq1WhEcSY7f/WiAJ3AvQFBmGfCwVJ25oFn0dAat/Ax7nOAK4hMA8z+MSgBOW
5ykhgUtYSXSLvIN1dw30lTy8f7rgzPPnz4eHh6mRR9NCoiaT6dGjRydPnjx//vzly5dfvnyJMDS/
CCDqq8zh2roGzl+6sThjdRz5KGPm/sPH2zq6EQbpIhIqGE6oGCN228VrV+48uMeRCS7FRXZdgGUj
Vb59E58y1zcozC84PCg82ssvCCf4mZ6R2djWCSOJFRWYpDXvG5euXPPTLJ+ImISQiJjgiJjw6PjT
Zy/q39Miqz+6+gfpa1IzfQICw6JQEYuWrdywdcfJgvMtnT10MlOEJaHaZAW6VIBRU1n9YU76htnB
82b4z4uYn7f3fEdlp9btllyqVdEGyYYOYDSWLLiDBebkFTujVr7tyc27d+Nee9cwUd9umHEkWkJg
xKhA4cHhRFbBnsRygrWhL9kjs5zkURfZPuJJE7vh4L2VO4pKq3ps5EMX+r1k8RtpJ58DaQ2jQMEa
vlODd6msRcVAQlKQUFXbyLaT95fuvHjj9ZCZZ2DquTmnW1aHGPX0zXfBc3f7p+w5cqW+n9W/wqTJ
xEpWBInYj6Te9TWIujmoZwoyI0ciRxgZ1mW7rN2q619/6NqNxw1uGIqoXbQCya1JFl0UA7RJjMHw
nQxGiO8VE6rA8P02MNLQMaEAx8MIoWOKYF8PD0X9oYDAsHWcTifUKAgAihUqlapjmhGq9HECPW6x
WBwOB3zG8ogTQSJPg6wuFqP456+r123KjohNPHTsVHtPP2UORIWQ9PhrgduRHBJtbm5+8ODBnj17
NmzYsHz58lWrVh0/fry9vR2yQfjGxsa8vDxcgj+wbt26tWvXZmVl7d69e8++fQ/LS61uJ9nnQhLf
1tflnToxd1Gab3Cgj79fcHDw/PnzQUXgPKSFvI+MjJw5c4Z+1T4gICAoKMjX13fevHmVlZWUogSJ
bHtQ9uTNkoz1/5rmFxaZvCB9Ver8jFk+YbHxKTdv3kRB0RKD5EBNTc2BAwcyVmd6B/itylpnZ9xk
yZj+FIOVhCeVL+jSkvjkublHjsP0vH3v4et39YMjNkF/k5eVyHeWt+zY4xMYsmPP/gGzra27H9Zq
eExCfMq8Dy3tiMfBik3tXcER5CuXi5evQgAXL9sZgRFlVn+Xmb5AxrCCxAsoU9Ow7ULRrbiU9Gk+
kd7BiQuWb3lS1zmAW1TyYoACvSuxIsdI+qcDGVkZdnGnLpas3JB7ovBRSzfHI0JYQhq4Fk2BaHDU
MXkegxqmqhmcpHJkWm9Umwv65wjtI1LR2SfrdhedK65sGnK7NBkcwmmMADOMPMQkxfs50PZAgZoy
fKcG2jBsQUYy89rtV10p649uO3H7eZPFoWpDDh6DHZeklVYPJ60845+8d/OBW28aBuycwgmyggau
kBeIZfJ4SVZUQSZPKokbnymy6pGBxajVVA1tLSw+dK3ibbuV2Kegdh6FAFZDaX5kRUFyIw86DN/J
YIT4XjGhCgzfbwMjDR0TCnA8jBA6pgj29fhlikLyn4MRYjIYIXQYXr8fjHhHYfh+p4B41JEhr83p
ull8f1fOwaR5C+enL523YCHcqjXr9u4/8La2jnx3R9VfOpQUWEvHTp6KTUhatiLzfUMTL0q4xEuK
g+Gv3iqJTkihm7piII+jD1xgaHR0NMwRqtkBkra+Wp3+hPomMnzSvmGmwIvhyTYJ7xubV6/LCouM
DgwJ9fbxS0hIAOvk5ORA6RcWFr5//x6RIx6bzXb95u3N2dvWZm3MyT1YdOVa7sHDQSFhs33912Rt
ePf+AyfBCGDzz533DQqe5ee/OmtDY3uHi+Orqt9lrlnn5eO3NGNFZ0+fpD/UR9ZghQmS5HSzToZ9
W1ufnDo3ICjkVnEJJ0igDZPVvXPvkZCIhISUheXP3jgYubq2efO2faERsVExcfUNTZyIEpP6B4cQ
rX9gMO6d7uXv7R+SNCets28QNOBG/Ar4QHj8tDxW3wIDhtGctHTYPY+fvEAYTlbIfoPkUZNqcbJH
TuTDPPUPiUBNvah6dyL/LAhp2+4ck80J7c5KyojduXr9JphZ/sHhs30DZszy8fINmr94xYt3jQM2
Vp9G0zoGleP5L+ek7QmOWBIcOW/R8nXLVmXFJs9bsWbDoHmIFa2caCEbKAhk4tDq1nod6uO3lq25
5/Lyz/UMWUGWkIcT9VUh5ANGNkkBfxFrCbqYbAdBiAqWBZyoqi5BdELZ86LqcGvVtSN7DtzYkXu1
tZ/lJZWsb5TQDhCJS1DdgsrAmoKJRxvGpKA9i8LwGgVIGAIRosR4iQjIwvqBqWqT1E6XduNlT8Ka
3ANXyttsipM8vdMYh/imzjpv3bmfIjYt236n0URea2JlcBKrKQ5Vcqtogmj4GJyR8Rl5tCaT77A7
FNUlk36AJoeLmovRXrzqX5V1quRxM9kIQyIP3PQPZ+hUDcrWHxwaUuqA8EYr10GzMymMEJPBCKHD
8PrDgaSNLOkwfHVQwcZg+H4FjIhGYfh+AuPyKAzfb4BfpiijVCaDEcKDqYDKQ0GRKkRXMtmdOXn5
sXMXTw+IDIlNAbvM8g2E+9d0r5DImOIHpfTFUnRsu5slr6AGh0XGJZU/f0Ue10tqR+/g/dInyzLX
zvINSFuyfO+BIzUfml69rbv3uKKxtXNwcJDOeqHF4LyiogKmz61bt2AV6ZIQfNqYMOpHipys9Q2P
LFq2EuZCYFgUzIimtg6YZSSA/gCJBgb0BqnqD29kSGVzczA71m7cCrKEnVHzvolGOGx15hzKCwqP
Tp6X9rD8GU0CxkreqYLQqDj4331Ujtv1zOrrEnU9A1vkRvH92MTUOWmL65va4MPoH62vbWhZsSYL
BQXxVq7NArtDSH1Do4ohi4NEokcFqUw219miKyERKbCUElLS+4cdSAWiclDfinTr3h2kPkufnQNR
gddBRTN9AtZkbX726o2TE1xwvIzwhRev4BIyBR6CtAsWZ0AeGElklw0JcnIwwrJ37cNwoaqq6uGj
0ozMddNmh/7TO3L5pgMX71VvOnjZPz47OPHQ7IjNiek7K+u7m7sGTxVcWLJs9Ynj53hW4XjB7nTY
GQenKv0O7tjFipRlp7bmPvvQxqDAUc4YDYyNNij0N3B/tn3IyvXRl3BZlXMJnI0RapvMmVnHc/JK
XtabydOdKfvvbwOZkITdRh42oSRguXMOSXSr2ocBflv+k9W5Vx/V9ZoElWxvoWkd3e61q0+EJWaf
vtfYxWs2jexhwelv2RLzR0buYSrp61GIwzkcgxpTNEQpuFyCm1FYXhsY1o4VPN+Sc+19v9NEqJkA
MnzannUZDXx69TfAiEuH4fWHAxkxJNBh+OowvHT8Lvn93uChqG8NNJqfKcrq5s5fL4mfvzRu/rK3
De02N8a2ZMGY2e46cPREQEjE7v2H3je3wwqxuVkowbCoOGjJw8dPQ/kOmG0F5y9BV0Kx4pLFyWD4
DfVN9Lu+OA2dljZlENWDBw/mzZsXFBQUHBy8ePHi1tZWIoreghGMyDUKqtyhfGs+NEfGJUIdgwyQ
HHxwdazRI9qxc5xAQjIrJcqgz4xV62BMzE9f1tTWyekfB2FFpaOnf4m+Vyyy8KDsqb4wgWTqwpUb
EbEJSOXI8dN0hQISQlRDI7atO/fA3y84PCQy9va9h06WJysVJTKuRmav3CxenbU5LDoeZhCo3csv
COR9+cYdFy+C/BAPHCJEdsAlqfOWh0amRsXN6x+243b6Si8sCThkDbGh0GjpPXnxesHiZYgwa8v2
zr4h+Hxo6Vi/eds0L99d+w7UNbY2d/Scu3QNgsFn+5793QMmiARDCsyHbDo5iZElK8vZOHnngXzf
0AX/NS0uJGZd/Nzd8xYf8A1bPs031ScseVZQxMzAkB+9g2b6R3sHJMcmL7l043FHL198tydzVfGu
Pc8bOxiXpjnBXCp5KZiSE53DHAd9HkznKlQOqfrRl3CHRO3J++G1O/NPnr/vYMlKCrQ4VSJ7OtA7
f0fImk1fCQHiUMjruKpW1+7MPVm27+idF9VdJodsE7ROs7gr77p31Irk5blFD5vNCm/T3E6Nc2u8
W+EECIjmRCmX2EL6hB4oijx8IlfIkyeZrEnnJe3J8+4d+4pyjlyraR50KqpDEV0YrekgQT9RynoM
Bj69+htgxKXD8PrDgYwYEugwfHUYXjp+l/x+b/h1z6KmKKm/JH6P/KLR6ApDpyg7J1y4cTc6dZFv
ZNKl2w/cPHmFiCxQVrXSpy9CI2OhB/fkHoIt5WTFs0VXoc0xlseIHkoWxgpsC6jv6Pjkp5VvGEF2
Cx+9rwq9RtsoTqDdYDyVlZUdPXoU/ETtoUlzAW2Le6Fqq97VI3Lo/ZKHZfCBzjBCTAbyoEBSYPYl
pMyDRZI0N626rmHsjSjoblhRG7buAJEkpM5/8rJKD09MkLf1jWBZUBeugl2o5LhKrCV9Hd3rd/Xp
GZkgqsx1GxtaO5Fxs8155nwRSAIGGUxGhASjlzwsBSmCWvbsPwTm0xMlVh3KpLGta9GS9aGRKcHh
iTXvW8lKPD0VDP0xKDdextLlR3JN7d1ICJFDTgwOwKwbs3fCVouKT3pT+4Eu63cw/OnCC15+gZBq
38Gj+AluJtHqS8x77NK5m9XxC7K9gtJ8guaHRS159aZt2MS/b+h+V9f8sPzJteLbtx8/KLh+fe6K
jbNC5yam7zp8tnTFlmNpq3JPXnjeYxJdAiEb/bNGbvLUaRxolVGQhRDkDSfyEi7COxXNLGmV7aY1
+wvXri+4XfzB7lREGWqeKH1NZejmdUZEk8GIV8eXaze3OiBoZPGoRdAcqvbyg2Xf6dLil/2DvDYi
as0D7LYDd+ZlHN5/4mHbkNsuk2k9nnzsidGn94jNpPeIMaA2WNjqehVpAo/skWURcPWN1jXbik5f
ftLvEBGCJc+hRFGTRDIz/VkY+dHx/2/vTP+qONY8/t/M25kX85kX88n93CVzE3PViILigoq4gnsU
NVyNohFFcVfEDdyJuKIIguKuKIhbEAXDouwHDmfpfa15nqoCCeIRFRS1vqkcD3W6q6uqq59fP9XV
VV+kyX4/upmyrgzwWhISFYq+KC+cftgRGwFIlGK5F67dmTp30ffhUYdPnAtq+CYpXKNgnfMvXx8X
PQWMIJhIuNMHG5p1+tz4mGkgACfP5oJNBGM6a95C8LTAUte3eDvte2fQdVynGw9Jsw1CBe4Ua3/w
2RXMVwdMVEBdwEBPjZsDEnXs1Fk04iElCnYByZw1Px6MO+hTfuE1sOZwXw8uEfwE3/2yvm3nHtDX
iDHjj2SdhNRAElp90oVLV+EQEJm0fmNARdcKDD1zSrAIDorlxas3wfeKnfMTE6Sql43gPw0LjwQX
s6U9ADUDEn69qHjuwsXgVP20+N819U0gikxrmTs4dUZ85LhpE2Jm/f60mrmJugNy7rZLPvCKwE9q
9HihSqEa9x44AoI0OCx82cqk2oYWj0/KyS+EEoGIbt+1F1QKpBcqZNS4id8NHgb+IsgtlK7VL8Nm
a3A5jw0jY/aFRW37x9CEfw6d88/BE7fvyvS0+TVdsSzVNBXQQd2R/FbgeXPzhn1ZYdHx30bEL9uU
fbG0vk52211dJeB1gNaAhwDnDM4cr2EGa3gM1zYILvSPL+GC8vz+0vPrrqyZq3fnPKqTG4njI/js
xoSTgBMymNTLUUOdw/e05ioJSq4hueRZg7Z+14WUvQVFf/g9Lilp1JIz746cmxq9cP9vuVUSW0nQ
MEw7gP4caBDtIcT2QQ8FH/StMR0uCzg/2KZcHNZi6eTp7569O8+tWHMo/16t3yU+25Jx/Idm0sFA
eIvxZnh5KL0v1BcPVAWvlNcY4LXEJYpntifYBoL3g715SgcAo6GEm+6bdx4uXLIyLGJibsF19KJc
nPm7uq4paf0mMPdDR4zauTcD7Ltf0VP3ZMCd+6ChYeAoBDWzLaAcP5MTNQmfYKUfylRNS7P4kySw
8tjt0zE+G4wyxEia0ez1ywZshq2wE56zDkDV4LoHVFVNT0+PjIyMjY09c+ZMa2srpKmY6CuAbLxo
9EAmFTrkHWSgIRA8ei4nbPyE8OhJu4/+dqbwyon8gvPXb1y8cZutrwGbgcMEmvf9kLB58UuK7j14
UlV78PSZ2EWLh0WNj09c1RiUgo5T3dpWUHRn14HMwqv37j96nn/pzsEj2bGzl3z7ffiyxJSyp3Ve
2fGozpHsgojo2H+NnDAxbsHU+QnDxk4eEhn9Y/jo+Pj48vJytFvUdwSggPfv34+Li5s0adKCBQte
vnzJns+xllxfX5+0bkfE6ClDwqJA638cPn7wj2Njps0/duKCXzI1G9wj3adqJ3PyImOmfhsW/t3I
kX8fPuyHcZEjIr7ZuT+tTjbqLFJUra9MvR0Vu23MhBWDBs/4buKE70ePGx8ddzj9eN0fTbpqaa4V
IPr9ppqLT8oetfjBvThxsWLmorTJs5MT1x24ePOxhK8OvOqnotcZB7vwIL9QIvQ2cOpVyLdO528F
NzRo2TU+e39O+fgF+5ZuLHhSrYJBx0lYe0e3s89jewLuNGiXYpe2CwGqGVSHuA3t6u6DV1P3lRQ/
cpt85Mpdf0LS2bjZ2/bsyKso84GYsDGUcEDsuwOXD/3YV7BDMFwHbqQMA1c1c1TH9ihW6uGCxC3n
rj7UGrwmeP9QUa832hDwdCnvtONABgrCi0ThsRQeRflU5Q2Rvfeja4JCovqXbhIFd+6F1+6uTNo8
IWb2sZPnH5Y9u3Lzztncgs07dsG9PGjP/EUJZc/+gPv06rrGGbPm/e2fP8DN+4atqZJuy4YJgrH/
yLExE2LA2Vq3aRuIwalzedm5BXCzD9ts3bnnTukjUBRZx84ucAtgs+mz5t2591CW5a4OFssbA2IY
8B1MfEZGRlhY2NChQydMmAD7ZhzOhHSmzZw7aVospLY74xB4MK1+ae/R3waNiPghfORfvvth2Jhx
QyPHjImZ+uPoseHjxkNB2NJNimEVXLkxc95CUCmc4i8iEjaIjpu153DmixaPAqaL3jxfLb4HjlhY
RNToqKkQhoSNGTkmJutUnmpS6aXdQD7dPpV3ecHSVZHR0wdHjIMwYlzM4qXLQZ8g26yhAqwUkiQV
FxdDQW7fvg1/gnPJCggabBiGN2CUPHiWcej4lh3pcAqKSsrqmnxBFc4Rv9GXDE0xVXC9Wtsaa2oq
yx6XPn9W5jFJo0FuVCrxmwpHzNr99zErE3ecq5dxbr02n+v1QTkcX6BZttSgY9YEydGrtXkP2mCX
kj+CUxekzfv3/os365t9xK/hxBCqiQMjOzPMMs9wbXrTAS4s/oSznsONCE7CqhHZJOlH86bOW795
39UH1fhECHILmMobp3btBjtcJzy2R0Cc8NVg2Me0sFMTbldM3ZIl1Xe7yjcr8cDuU0+hmBdLydKN
V/4enjhlUcbl202+VnzvWAeniL59BTtDQrgvNv9XYLId4HK7bsByHUkjd0vbJ03fuC/zFnpOcOLo
WQP4br2Dp0t5130HLFAQXiQKj6XwKMqnKm+I7L0fXRN8t44+wZvg1UnhUZRuEtXqD57JKfz38mS4
eR8WPm5E5LiwkWMGh0X8818/jhk/KSe/kPVKgd/TFpDWb94+aEjYkOERCctXBVQd7AT8JBvOzbul
oGSDhg7/dtCQIWERoFgjInHC7+Gjxk6Jnf2kshq2bG7zH/rtBLhc3w3+MXbOT2y4BFMpNmCsRyDz
4HP4fL6ioqLk5OSYaXHRU2MnTJ4+PmbarPnxWafPtbQHwC3zyXpVbcOxk9nJKZs3bN6xJ/3Qqexc
EMJbd0qv3br7qLyisRU0hb0hZLe0B0FBwe1bm7LlZPb56heNumXj+6f8phwDeIRQM7UNTdX1jS+b
WlhdsQAq1TWoht0ZcIahLrDKZ/ConuiWOGSyM6jcK7VM9FgUyzYUyTA10tqgp56vill2csSMtPik
7Lyi1spmx+eQAHG8xLQlnGFJtWzwMR/UtWZerUzJLD9y1b9y+73FK0+npF6+98Tn09C1NeiLtzgr
REhLAsYdu7NoeWWLtErW1ZLy5B2HEpJSL92uBKEy8IEMbVXg7eC6US282JQQdiqEKen+k4mzNoG6
GERWXVlxzXbLuv+8acWmA7M3lqZfDqblvhw5Py1i9vpZqzJy7tY06DiCAj0n2IuGLkB+/pQlOBY0
M9Ya/WrwRbvnaPb1nxJ2bthS+LTSDurgqDk6SDBO7PQKtq/ga0NIVN/ALyMKj6K83tEHn81t8sEj
2ekHjx8/kwOyVPq4vF1SwJrjAxucQQC3YUazPSg/elLxsqkVrTN9zNNpVdkXkC7wVwKKJuvYn2U4
bpAu045OjOlIdJEn2IyZrW556xHYsnNjPJBDRxbQF4ZYEfDQuB3OP4fGFPIBm0PCEMC+YxHw6FAQ
2AwzjLnCzBsmWEFqrF4LYFZpFfUQ8HVQ+uomC9337AKtew6P6omuiYOdp/Ma8IBFgNwSI0i0Fse8
8KRp9obLP8RmDYq7PH7ZvitP2oMGroeHD4JkKCd7IdiSnaZm3TpX1B69qGTcvNLoBUVjZhzMu17T
6DcDFpHQWTR012+Blriqi7MjUFl+A1CpumNDpUk6eVDevizp0Pq00xUtQfAqWm074EIjsVx8i1XH
BShMyIMJmsqLTWGnr0fgJ74RhcdSuv8EtzE2VLeuEdNv22X1+twVuRvTW1Mz7aiFt/46Km3xhgtl
jThbftAxFDMIQm3Z4BhDAzTpVBdQyFDgIQhpaWlZs+H49AVHbz5ukwhpt8GVCqi2jD/ii1NvzK3g
60FIVN+Adr0DHkWhlpwFNID4+q1J3zG0Cc4TYYHdRXuFC/vABUm36QzwU7c/4QYcP2n400/0k04t
i2/Zw6dh4zXNAuwCubLpA5vQgCGgD3QQ/LNLYJnpzBIXCDCokBX2nQYb7dOrzboGrJg3BMheRy3h
vXvndwhdfsHQVa6o4r0Cq74DHtUTdKQz2D78x3JBhm3FNiTHj8vYGoamO82SUeHRt534fcS8fYPj
dsUm5+3Jr/vda3px5iFwgnCmHmJqNg4+A0/ArDdaj1x6OjLuYPiUM+ETjyWuu1v70pUldBN01zJc
HRwyGycZl1y4W0BVp9IOVYfuFOSV9i/iVHt4ImFTj+1UtMj7jl9elpx+Kq+kJaCDNxG0LdWFoIP6
U23FZZJwugm46cFU3ggvNoVHdQB3DnAHArcHOBstVCf82xGI4rM0WXIsD3Hvt9mx6/LnppTNTKz6
IerapLjtGceKK15KkFVaDsNxZOIqLn3m1JEEtJ9Xh8ZWDk0CH0xZUPqgroN3eKO4evWGw0uWH7hd
0qaBv4ij/QKW64M7HDjPkBIuEdwFnlb/wGukAx4rGAAIiRJ8ZVA9BxsKt/wm0VWQKELAvnpkM6fw
xfSFuyOmbxocvXb9/lv3a3QFrDi4VyaKDNxDoJCgzJi6rkm2+1vBnciZS7+P+nXJ+jMlz6UgLkCF
XXk4RADEg77D1Ak/OgM0gPX6QdKOBrqH/aIO+aNW2nE4f+n2zMLHNTV+vc1w8ZkYqhm4fAbKdxd4
uhQeRQELy2MpPLYnbFyAA//rCOwfuG+yDFP3Ku62rPLwRXnDFub/bUrm5GU52dc8bTo/rqqqrKeu
N9DSwg2UDi53k9dK2X5mbkLq0fP3atstleorJMjGo7LEe4Sn1T/wY1CERA0ohEQJvi7QTUOd0kzS
phKP31H8Nnlc6SSueTgqLm/wpLS4xLSzdx4HiaFY4EXgihJ2K+zAnrTgIyLQoWaVpBy88LeoxUPi
1mScuuI1IC0rYKDN1izVMAMEfSaN2zwKPzwDXDFdxinnLHBBiGKRJtk5faNy6ZastXtPv/SrMiEt
Cq5AiH2qCIiqjX5kF3i6FB5F6b1E4aqA4I+hK4ROmU679djatZVBsjnr+bDY/CFTbv7f2PNZ+Xpb
0NYNP+QLJYUacTZUsjeAc6g4Aa8aqKxVF/187JfEcy9b8FUnv6vquFo8PppiafJM9wRLqp/gx6Cw
nAgGCEKiBAMObioo72cv+M4dQCKdaLatExIwbb9FSp61rNh4PCpu49Co9dPijxc8kHFhV4NIcLvv
qMSR6Wx4zIdxVdlplazcotoFaw5+O2bxxoNXmgzSqNLx4XBEF4cO4qMulBX4Ahb4TxacZ4XiupYD
4oSzz5GqF/Kq9Ye27M+t8Egeh/hM8GCw6w4ShC35zu8ClJEdhcFjewQyiJkHkcK1a32u6SOkXiMH
zpd9H3f4HzG75627XHAvCHqs4kRLFj6Ic4I83bdh0wky4BPkxxMwD5y+NWX+2iW/7i6v8YFriHVk
O7ptgfbyzAgEPSEkSjDgYDaOAQaXx74LfGcKPi/qQsB9Uef3nikoXb76yqjxxybPyd564OnNyvoX
phzE+YxMl85k4ErEBbPtOEF0oqTWoHP0/MOImRsj5qf9tDHncplXonYdX/ihkoT9cCBPkFl8tIaO
Wrd889xQZMdt0Kzs6w9+XpOx+9CNR8/a/KrjVw3NNhXVT90YSI4OqqPJwP+vJ/gm3kGisOPQoItS
+VQiN5nWoYuVw2N3Doo+ODf54bXnEKMrpCZIyi1SZ5qKDdUCwt07QJxYToqLi5etPLkt48W9CqPV
tIOkUSM1quUBr83R6DzlAsGbERIl+ALBh0GubbmqhfY3aBEJtEcz3eLiulkr7g2LOTh4/PZN6def
1HhVA59KmYqBz4LYHAcAaIxFlACKRUsDOX7kzpRlJ1MO33weUCTsoZMMEsRhd2Be3zzBELPUzEx3
zjcPfwYCgW0HTk5bmbb2YN6NiiY/nfII8kAHFbKV3t8ZlnjP4IMvOCwd1QKaxAKUEoKuElNTHMtP
nN+9xriEXYMmpU5LuLQ3S2pra6NOIYL7doEnS+FRFKxAhY7kwSFBWqtunr9bPz/p1PTll0vKFbg5
gG3Y6jNs+76C1XAnPFbQD/AqpvCo/kdIlOBLxJaJrbi2bjk4/N2vkvvPtFWbbo+acih2yYFdmbcf
VMmS6yq2oRgBWfVaJjhNYDrBbKNnAdYcHRgbx+61t5qNL6XqdrfVJK2mG7AV1VXB1QL3DPd4sxOA
skAHUmqa5vf7WceXQZdzbA4odZKNnYouUelssPAb7VQ0sNft3WFWo0e4/wVHxQGhUBkKBItoOD8R
rqZBPBI5XVA9elrqjIRDaSeu1WukyZRgR8gwOKDMGeKHobBkGTyKgoNV3XbTtkGgnlYayZsuxS7c
mXbkWpUP/C8sO25DX6Bm2/cVkCbLDIPHCvoBXsUUHtX/cInih6WEaJHdfhL0CK8siqixj0bXqlYU
uJ8nQZkU329MXHtiUuy2mYuO7cqs/aONBHR8H1bDOXlMm8gurt0uU6kBbXiVggu2nY4Ox0DncYBA
RQwibPqmAPVL3utSBSUEzwkCiJNBewU7gBRf5aFvoD2QDi66JBs4u0VQJYpM3BbLzL3/JH7NvvCJ
a5ckniqrdLwKAa9KJWqQvODNlxLCILAY2ACUTCduox4selL7a8qZlasLyp7gkXSHSFbAdBW+A4Xt
9VYgWb4Dhce+Db41RVx97wGvO8pAqEAhUX0PryyKqLGPBlQ1GwYN5rLOJZcr1DmrT4+avj15R2Hh
7WZvEAeNoyiA1OCStSa+gotv4aLXAmfpz8PlIAbOHn0LFQNTKIu+mwW/0Q06wvuAMoeD2emgc8wz
8EEJhoB2XoLPZ4BwE91P3AaDnC/xjp7z23+N3TRy8d6c4vIGRdOdoO348d1bKCodqdFJaIPA/EKo
8/sVwUUbi0fO3HPs0rM2w5FMfIHXpe9N4bu8XeAJvQ1Ilu9A4bFvg29N6ZZzQW/gdUcZCBUoOvre
AX7eKKL1f3pMl5hgBH2G06CR+oAbrJGtgt/V2UkPYqes3LcrN/dssbdVtcD3wXMFSqAS9Jn6Ed44
KCFayPsZ3xDQyZUsPCIkBpoLZYUABQdXkTTqjs8ielAhJQ99i1cWDIna9dfItBHzsu/cucO63cyO
Md+d8JxRuv0Eeo1L/hJZIS0S0YLEKq9z1mytWrE6+9SpU42NjSBXoR848XQpIWpJIACERL0D/Kqi
iEvr0wO36KZhm4ZmGj5Zu1/hj199ZNiUdat23WhpNAyFyAH6LhPvwAObDT6TzHbtJ3jjoIRoIfAT
34jCYz8ASBCh36irZ4Njw4JlBIOmUS8767Mqv4k58N+jU78Zn7pkQ15Vi8km2AVFeT0PLGMMlnAn
jqsbNug+TiYhO+RaSdPchIzVWy61+PmzK03DV5341j3B06WE3lIgEBL1DsDl1BUe20fwRDvgsW+D
b03hUV86YNfgE8qrmGbQcp5VS0nJl6Mn7R8/Yd/u9LvP64MqoTPzwJ0+mGi0gVAzEECiDNrt1Y8w
s8sIcUYwT13gsR8Cm3PQgRKrmuPTSLtOJIXoAceoe2FtOlA5+ueC/5xx7j9mZMfsvn/kob8hoDhm
K2QS3B0QFZ5IF3jOKKw4LBK+BN2XsmO2BUnO+eaEhILlibm37jXIcKeAmoWCB58skTfBEmS8dWPB
V46QqIECv2Qpvb9u+Q4UHvVFw+wp3P77/f77FU1zEg+ET16TvP1SwTWP1we1AT6Ta6g+Op8pDzgw
gvd8sc6vfoSfCcpHNb50LIfjgHMjaThMRGp3nQbN3X36bvic64NnFvxlUtaYlZcyHwRfWKQd3EkQ
Equ3fZ5QEFYWqHxczcTVb5ZVzVm8Z/mq/Av5bS0eHCmomEGn1/LPK4jyUWtJ8BkiJGqgwC9ZSu+v
W74DhUd90ViUR48epaSkTJy1efqKwitP5TbHkHEgdTMhLcRWieY6JEAD2E3Zxf49asLxKU0PHkMf
ws8E5aMaX1AHAyXKIKpEDB8hV8paY5acjV1+739/8f9vXOGMtTdr6/2O1ExMD7H9jksCfM/eAsUB
fVIUJTXzwdi4lPQTt156cVkX3cbRJ1CvHcM+3g6vIIqQKEFohET1Pfzio4gr8EMB84e9WJbpWH5N
uV1WGRO/Zfj0tLlJt3Ku1mga+AP4KAV0i28/YIBTz9oAg8d+EJBIpy8I6gCBjV1XCWi0bWiy0eC1
tx97PHzW/m8mHx+6tPx/4kp/XXPhjxrDFyQGznUE/xt0wAMIC0+0ZwxwymBLxXYl29UkzQiqzsEj
RYt+3p+4+nRFlQLOmgHeEz03xKYzGv1Z/XmxKeIqEDB4g6D0slUIiep7+BmgiIvzA3FwMlVFM802
LzlztmbyjF3rdhSUVgee+ySpY4g5VDLrABxQ9INEQRmZOwgFh++QpkMHRziK06K6cn2bvfPA08jp
2RPmXxm7+FLY4t+2Xqn1S66kWAbVEVBzthMT/hDoLi6dqOMr0I7fcu898/6y/nTyjlvXShW/n5gW
BgMHqLtQTpygkH7tCi82RVwFAgZvEJR+kah+uOo+J3ixKd3ql8e+hrg43wNedxTFNVss69y1qtkL
MuOX5F69qgSCxHZ1i7zAZW8/Fjw3lBDnlG9B6YdTD+LEOi3x0rMtF+dg0nAKjMtV3qTDl8Yu2H/4
grwo6e7CX2/lF7d5bNLk+AhRqOdkMzlDNQFnCicqCpU9kyiKpUume6u0elnSnl+SDxeWNHkM4kVt
wzUU8XVg8KBYavgV1zLjOws+Iry1UT6VtYHj8hxQeGwfISTqHeDFpnRrDTz2NT5Vo/ms4XVHaZTc
n7fm/RiTHLt4b/bFas3Ed17x5p60ofH9WPDcUEKcU74FpR9OPZQcigx6g28QsymaJJ+bvuv80Pgr
U1MepxYEJieeOH69qlbGqZVM4mimRBwFu/XQh8L9UJwwCU0jfp5qj5jgQRFdootzmATuCEAeVdfw
6l5it+OU5/A3HICqHfyr800EHxve2iifytrAcXkOKDy2j+ASxdOmhChnv2bls4bXCIVHCfoCVVWz
srLS09Obm5sNfDz/dcGbFMNwXR2iZOLWuG6Z5tZVeOXoxNLwhLZNGSR+dUtswvW8Ig9dud4BRxMd
m6+uwgRfGkKi+gZeIxQeJegLWH1CwwvRLL9gWItimK6uExW8G8ly/QY5dbk2Zum5qMSiH+Jvrtx6
58w1T5MGzpMTtGEblUoU9bsEgs8ZLlHMBHTCIl+H/9wBj+1/+PE64LEfAE+oAx77AXArQuFRgj6C
nyR6e8Sjvhp4ySm661FJk99RqtvIiQLP2Nk5EbOyo1fk5lWoQceViSO5puKqJo6/V1zXdKG2Qo6J
+HB4zjrgsQLBG+ANpQMeG5J3exb1qYDCMOvP4LEfAE+I0suaCg1Pi8KjBIK+xTUMNRhQrDVbT4VN
WPnLxty7T4OyQzR8YdnESW8dy7Et16EPnWBzvHDYnv0Fb/GUPrmOBF82vK1QhESFgidEERIl+Dyg
ywGbCi76BF9l5iOZLmqUQfv0WIBYaIN8qZD+daN4i6cIiRK8Fd5WKJ9YonguKDzqM4FnmiKuOoFA
MJDhporyRdorIVHd4ZmmCIkSCAQDGW6qKEKiBAKBQCD4eAiJEggEAsEARUiUoB/hHRCU3vdC8B0o
oq9VIPiaERIl6Ee4zlCERAkEgneFSxS3B5RuRoHHUoS9EAxYeBul8CiBQPCZIyRK8IXA2yiFRwkE
gs8cLlGgPV1hkQx+0VO6/SQQDBxY02XwKIFA0P/wq64DHttHvP1ZFFcnSp8fXiAQCASfNaALXCEo
PLaPEBIlEAgEgvenHyWKkP8HvczPg1P0/08AAAAASUVORK5CYIJQSwECLQAUAAYACAAAACEAyRkA
9gsBAAAYAgAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAI
AAAAIQALqu451AAAAJMBAAALAAAAAAAAAAAAAAAAADwBAABfcmVscy8ucmVsc1BLAQItABQABgAI
AAAAIQBdpucfFwIAAPMEAAASAAAAAAAAAAAAAAAAADkCAABkcnMvcGljdHVyZXhtbC54bWxQSwEC
LQAUAAYACAAAACEAqiYOvrwAAAAhAQAAHQAAAAAAAAAAAAAAAACABAAAZHJzL19yZWxzL3BpY3R1
cmV4bWwueG1sLnJlbHNQSwECLQAUAAYACAAAACEA9fJ0yRUBAACLAQAADwAAAAAAAAAAAAAAAAB3
BQAAZHJzL2Rvd25yZXYueG1sUEsBAi0ACgAAAAAAAAAhAGzrAIqaYgEAmmIBABQAAAAAAAAAAAAA
AAAAuQYAAGRycy9tZWRpYS9pbWFnZTEucG5nUEsFBgAAAAAGAAYAhAEAAIVpAQAAAA==
">
   <v:imagedata src="/upload/pay/image003.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:20px;margin-top:5px;width:218px;
  height:88px'><img width=218 height=88
  src="/upload/pay/image004.png"
  v:shapes="_x0420__x0438__x0441__x0443__x043d__x043e__x043a__x0020_1"></span><![endif]></td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl107 dir=LTR style='border-top:none'>&nbsp;</td>
  <td class=xl112 dir=LTR style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 style='height:15.0pt'></td>
  <td class=xl70 dir=LTR>&nbsp;</td>
  <td class=xl99 colspan=3 dir=LTR style='mso-ignore:colspan'>Customer:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl88 colspan=3 style='mso-ignore:colspan'>Supplier:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 style='height:15.0pt'></td>
  <td class=xl70 dir=LTR>&nbsp;</td>
  <td class=xl71 colspan=4 dir=LTR style='mso-ignore:colspan'>Official stamp:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl99 colspan=5 dir=LTR style='mso-ignore:colspan'>Official stamp:</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 style='height:15.0pt'></td>
  <td class=xl70 dir=LTR>&nbsp;</td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl71 dir=LTR></td>
  <td class=xl102 dir=LTR>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 style='height:15.0pt'></td>
  <td class=xl85 dir=LTR>&nbsp;</td>
  <td class=xl87 colspan=12 dir=LTR style='mso-ignore:colspan'>Economic and
  information system POHODA<span style='mso-spacerun:yes'>&nbsp;</span></td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl68 dir=LTR>&nbsp;</td>
  <td class=xl86 dir=LTR>&nbsp;</td>
 </tr>
</table>

</body>

</html>

<?

if ($_REQUEST['BLANK'] == 'Y')
	$blank = true;

$pageWidth  = 595.28;
$pageHeight = 841.89;

$background = '#ffffff';
if ($params['BILLEN_BACKGROUND'])
{
	$path = $params['BILLEN_BACKGROUND'];
	if (intval($path) > 0)
	{
		if ($arFile = CFile::GetFileArray($path))
			$path = $arFile['SRC'];
	}

	$backgroundStyle = $params['BILLEN_BACKGROUND_STYLE'];
	if (!in_array($backgroundStyle, array('none', 'tile', 'stretch')))
		$backgroundStyle = 'none';

	if ($path)
	{
		switch ($backgroundStyle)
		{
			case 'none':
				$background = "url('" . $path . "') 0 0 no-repeat";
				break;
			case 'tile':
				$background = "url('" . $path . "') 0 0 repeat";
				break;
			case 'stretch':
				$background = sprintf(
					"url('%s') 0 0 repeat-y; background-size: %.02fpt %.02fpt",
					$path, $pageWidth, $pageHeight
				);
				break;
		}
	}
}

$margin = array(
	'top' => intval($params['BILLEN_MARGIN_TOP'] ?: 15) * 72/25.4,
	'right' => intval($params['BILLEN_MARGIN_RIGHT'] ?: 15) * 72/25.4,
	'bottom' => intval($params['BILLEN_MARGIN_BOTTOM'] ?: 15) * 72/25.4,
	'left' => intval($params['BILLEN_MARGIN_LEFT'] ?: 20) * 72/25.4
);

$width = $pageWidth - $margin['left'] - $margin['right'];

?>

<body style="margin: 0pt; padding: 0pt;"<? if ($_REQUEST['PRINT'] == 'Y') { ?> onload="setTimeout(window.print, 0);"<? } ?>>

<div style="margin: 0pt; padding: <?=join('pt ', $margin); ?>pt; width: <?=$width; ?>pt; background: <?=$background; ?>">

<table class="header">
	<tr>
		<? if ($params["BILLEN_PATH_TO_LOGO"]) { ?>
		<td style="padding-right: 5pt; ">
			<? $imgParams = CFile::_GetImgParams($params['BILLEN_PATH_TO_LOGO']); ?>
			<? $imgWidth = $imgParams['WIDTH'] * 96 / (intval($params['BILLEN_LOGO_DPI']) ?: 96); ?>
			<img src="<?=$imgParams['SRC']; ?>" width="<?=$imgWidth; ?>" />
		</td>
		<? } ?>
		<td>
			<b><?=$params["SELLER_COMPANY_NAME"]; ?></b><br><?
			if ($params["SELLER_COMPANY_ADDRESS"]) {
				$sellerAddress = $params["SELLER_COMPANY_ADDRESS"];
				if (is_array($sellerAddress))
				{
					if (!empty($sellerAddress))
					{
						$addrValue = implode('<br>', $sellerAddress)
						?><div style="display: inline-block; vertical-align: top;"><b><?= $addrValue ?></b></div><?
						unset($addrValue);
					}
				}
				else
				{
					?><b><?= nl2br($sellerAddress) ?></b><?
				}
				unset($sellerAddress);
				?><br><?
			} ?>
			<? if ($params["SELLER_COMPANY_PHONE"]) { ?>
			<b><?=sprintf("Tel.: %s", $params["SELLER_COMPANY_PHONE"]); ?></b><br>
			<? } ?>
		</td>
	</tr>
</table>
<br>
