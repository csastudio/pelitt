<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$context = \Bitrix\Main\Application::getInstance()->getContext();
$documentRoot = $context->getServer()->getDocumentRoot();
$pathModules = $documentRoot . '/local/libs/Modules/';
$pathInclude = $documentRoot . '/local/php_interface/include/';

if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php')) {
    $loader = include $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';
}

CModule::AddAutoloadClasses('', array(
    # Base
    'Lexand\IBHelper' => '/local/libs/ib.helper.php',

    # Handlers
    'ApiHandler\ApiHandlerProcess' => '/local/libs/Modules/ApiHandler/ApiHandlerProcess.php',
    'JsonHandler\JsonHandlerProcess' => '/local/libs/Modules/JsonHandler/JsonHandlerProcess.php',
    'HiloadHandler\HiloadHandlerProcess' => '/local/libs/Modules/HiloadHandler/HiloadHandlerProcess.php',
    'ErrorHandler\ErrorHandlerProcess' => '/local/libs/Modules/ErrorHandler/ErrorHandlerProcess.php',
    'SuccessHandler\SuccessHandlerProcess' => '/local/libs/Modules/SuccessHandler/SuccessHandlerProcess.php',

    # Modules

    # SPSR
    'SPSR\SPSRMethod' => '/local/libs/Modules/SPSR/SPSRMethod.php',
    'SPSR\PutItems' => '/local/libs/Modules/SPSR/PutItems.php',
    'SPSR\SalesOrder' => '/local/libs/Modules/SPSR/SalesOrder.php',
    'SPSR\PutContractors' => '/local/libs/Modules/SPSR/PutContractors.php',
    'SPSR\PurchaseOrder' => '/local/libs/Modules/SPSR/PurchaseOrder.php',
    'SPSR\StockReport' => '/local/libs/Modules/SPSR/StockReport.php',
    'SPSR\SPSR_Exception' => '/local/libs/Modules/SPSR/SPSR_Exception.php',
    'SPSR\SPSR_Client' => '/local/libs/Modules/SPSR/SPSR_Client.php',

    # TransactPro
    'TransactPro\Client' => '/local/libs/Modules/TransactPro/Client.php',

    # SalesOrder
    'SalesOrder\SalesOrderProcess' => '/local/libs/Modules/SalesOrder/SalesOrderProcess.php',
    'SalesOrder\SalesOrderListProcess' => '/local/libs/Modules/SalesOrder/SalesOrderListProcess.php',
    'SalesOrder\SalesOrderHash' => '/local/libs/Modules/SalesOrder/SalesOrderHash.php',
    'SalesOrder\SalesOrderList' => '/local/libs/Modules/SalesOrder/SalesOrderList.php',
    'SalesOrder\SalesOrderData' => '/local/libs/Modules/SalesOrder/SalesOrderData.php',
    'SalesOrder\SalesOrderPostProcess' => '/local/libs/Modules/SalesOrder/SalesOrderPostProcess.php',
    'SalesOrder\SalesOrderPostTracking' => '/local/libs/Modules/SalesOrder/SalesOrderPostTracking.php',
    'SalesOrder\SalesOrderPostAbstract' => '/local/libs/Modules/SalesOrder/SalesOrderPostAbstract.php',

    # StoreNCash
    'StoreNCash\StoreNCashAddAbstract' => '/local/libs/Modules/StoreNCash/StoreNCashAddAbstract.php',
    'StoreNCash\StoreNCashUser' => '/local/libs/Modules/StoreNCash/StoreNCashUser.php',
    'StoreNCash\StoreNCashProcess' => '/local/libs/Modules/StoreNCash/StoreNCashProcess.php',
    'StoreNCash\StoreNCashProducts' => '/local/libs/Modules/StoreNCash/StoreNCashProducts.php',
    'StoreNCash\StoreNCashCurrency' => '/local/libs/Modules/StoreNCash/StoreNCashCurrency.php',
    'StoreNCash\StoreNCashOrderAdd' => '/local/libs/Modules/StoreNCash/StoreNCashOrderAdd.php',
    'StoreNCash\StoreNCashOrganizationAdd' => '/local/libs/Modules/StoreNCash/StoreNCashOrganizationAdd.php',
));
require ($pathInclude . 'OrderHandler.php');


/**
 * Отладочная функция для вывода массивов
 * 0 - печать на экран
 * 2 - печать в файл
 * @param $var
 * @param int $mode
 * @param string $str
 * @param int $die
 */
function gG($var, $mode = 0, $str = 'Var', $die = 0)
{
    switch ($mode) {

        case 0:  # печать на экран
            echo "<pre>";
            echo "######### {$str} ##########<br/>";
            print_r($var);
            echo "</pre>";
            if ($die) die();
            break;

        case 2:  # печать в файл
            $handle = fopen("/upload/debug.txt", "a+");
            fwrite($handle, "######### {$str} ##########\n");
            fwrite($handle, (string)$var);
            fwrite($handle, "\n\n\n");

            fclose($handle);
            break;
    }

}